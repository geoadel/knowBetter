﻿knowBetter_languages = []; 
knowBetter_words = [];
knowBetter_users = [];
knowBetter_tags = [];
knowBetter_comments = []; 
knowBetter_categories = []; 
this.knowBetter_words = [{
	 	name : "en skog",
	 	id : "8",
	 	language_ID : "1",
	 	related_IDs : ["10", "13", "2409", "9", "lbotmEgCymz8"],
	 	categories_IDs : ["2756"]
 	 }, { 
	 	name : "der Wald",
	 	id : "9",
	 	language_ID : "2",
	 	related_IDs : ["10", "13", "2409", "8", "lbotmEgCymz8"],
	 	categories_IDs : ["2756"],
	 	hasImage : true
 	 }, { 
	 	name : "the forrest",
	 	id : "10",
	 	language_ID : "3",
	 	related_IDs : ["13", "2409", "8", "9", "pvm6orYu9k11", "lbotmEgCymz8"],
	 	categories_IDs : ["2756"],
	 	tags : []
 	 }, { 
	 	name : "the woods",
	 	id : "13",
	 	language_ID : "3",
	 	related_IDs : ["10", "2409", "8", "9", "lbotmEgCymz8"],
	 	categories_IDs : ["2756"]
 	 }, { 
	 	name : "ett vatten",
	 	id : "25",
	 	language_ID : "1",
	 	related_IDs : ["130", "154", "1691"],
	 	categories_IDs : ["40", "59"],
	 	tags : []
 	 }, { 
	 	name : "der",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	related_IDs : ["101", "1703", "3177", "3180", "83"],
	 	id : "62",
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3178", "3179"]
 	 }, { 
	 	name : "wer",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	related_IDs : ["103", "1704", "84", "FwcLhf2Dcmw8"],
	 	id : "64",
	 	tags : []
 	 }, { 
	 	name : "was",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	related_IDs : ["104", "1133", "1705", "86"],
	 	id : "66"
 	 }, { 
	 	name : "warum",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	related_IDs : ["106", "1706", "2996", "87"],
	 	id : "69"
 	 }, { 
	 	name : "wo",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1707", "88"],
	 	id : "70"
 	 }, { 
	 	name : "wann",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	related_IDs : ["105", "1708", "89"],
	 	id : "71"
 	 }, { 
	 	name : "wen",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1709", "84", "90"],
	 	id : "72"
 	 }, { 
	 	name : "ja",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1710", "91"],
	 	id : "73"
 	 }, { 
	 	name : "nein",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1711", "2157", "92"],
	 	id : "74"
 	 }, { 
	 	name : "aber",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	related_IDs : ["109", "1712", "3181", "93"],
	 	id : "75"
 	 }, { 
	 	name : "vielleicht",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1713", "94"],
	 	id : "76"
 	 }, { 
	 	name : "und",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1247", "1714", "95"],
	 	id : "77"
 	 }, { 
	 	name : "oder",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1248", "1715", "96"],
	 	id : "78"
 	 }, { 
	 	name : "wenn",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	related_IDs : ["105", "1727", "89", "97"],
	 	id : "79"
 	 }, { 
	 	name : "dann",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1249", "1718", "98"],
	 	id : "80"
 	 }, { 
	 	name : "als ob",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1716", "99"],
	 	id : "81"
 	 }, { 
	 	name : "wie",
	 	language_ID : "2",
	 	categories_IDs : ["33", "34"],
	 	related_IDs : ["100", "107", "108", "1122", "1123", "1728", "2873", "85"],
	 	id : "82"
 	 }, { 
	 	name : "the",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["101", "1703", "3177", "3178", "3179", "3180", "62"],
	 	id : "83"
 	 }, { 
	 	name : "who",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1704", "1709", "64", "72", "90", "rxYDYG1liI8e"],
	 	id : "84",
	 	tags : []
 	 }, { 
	 	name : "how",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1728", "82", "jABkXYb127Ht", "107"],
	 	id : "85",
	 	tags : []
 	 }, { 
	 	name : "what",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1136", "1705", "66"],
	 	id : "86"
 	 }, { 
	 	name : "why",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1706", "2996", "69", "JoG040wLz4sX"],
	 	id : "87",
	 	tags : []
 	 }, { 
	 	name : "where",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1707", "70"],
	 	id : "88"
 	 }, { 
	 	name : "when",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1708", "1727", "71", "79", "bbE7NynxaUEi"],
	 	id : "89",
	 	tags : []
 	 }, { 
	 	name : "whom",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1709", "72"],
	 	id : "90"
 	 }, { 
	 	name : "yes",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1710", "73", "nxFluqsNeOuP"],
	 	id : "91",
	 	tags : []
 	 }, { 
	 	name : "no",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1711", "2157", "74", "cM4heQAHVUQT"],
	 	id : "92",
	 	tags : []
 	 }, { 
	 	name : "but",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1712", "3181", "75", "182"],
	 	id : "93",
	 	tags : []
 	 }, { 
	 	name : "probably",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1713", "76"],
	 	id : "94"
 	 }, { 
	 	name : "and",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1247", "1714", "77", "D86f3GZ0BG3R", "r28Z3QvEj2Lv"],
	 	id : "95",
	 	tags : []
 	 }, { 
	 	name : "or",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1248", "1715", "78", "p72T1dwKbnkM", "AqfGdFVxoVeC"],
	 	id : "96",
	 	tags : []
 	 }, { 
	 	name : "if",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1727", "79"],
	 	id : "97"
 	 }, { 
	 	name : "then",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1249", "1718", "80"],
	 	id : "98"
 	 }, { 
	 	name : "as if",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["81"],
	 	id : "99",
	 	sentence : true
 	 }, { 
	 	name : "like",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1728", "82"],
	 	id : "100"
 	 }, { 
	 	name : "den",
	 	language_ID : "1",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1703", "3177", "3178", "3179", "62", "83"],
	 	id : "101",
	 	tags : [],
	 	formPrimaryName : "utrum",
	 	formsPrimary_IDs : ["3180"]
 	 }, { 
	 	name : "vem",
	 	language_ID : "1",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1704", "64", "84"],
	 	id : "103"
 	 }, { 
	 	name : "vad",
	 	language_ID : "1",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1134", "1705", "66", "86"],
	 	id : "104"
 	 }, { 
	 	name : "när",
	 	language_ID : "1",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1708", "1727", "71", "79", "89", "97"],
	 	id : "105"
 	 }, { 
	 	name : "varför",
	 	language_ID : "1",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1706", "2996", "69", "87"],
	 	id : "106"
 	 }, { 
	 	name : "hur",
	 	language_ID : "1",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1728", "82", "85"],
	 	id : "107",
	 	tags : []
 	 }, { 
	 	name : "som",
	 	language_ID : "1",
	 	categories_IDs : ["33"],
	 	related_IDs : ["100", "1728", "82"],
	 	id : "108"
 	 }, { 
	 	categories_IDs : ["33"],
	 	related_IDs : ["1712", "2842", "3181", "75", "93"],
	 	language_ID : "1",
	 	name : "men",
	 	id : "109"
 	 }, { 
	 	name : "el plátano",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["132", "157", "360"],
	 	id : "111"
 	 }, { 
	 	name : "la uva",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["133", "361"],
	 	id : "112"
 	 }, { 
	 	name : "el mélon",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["137", "362"],
	 	id : "113"
 	 }, { 
	 	name : "la cebolla",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["138", "363"],
	 	id : "114"
 	 }, { 
	 	name : "la pera",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["139", "364"],
	 	id : "115"
 	 }, { 
	 	name : "la naranja",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["156", "379"],
	 	id : "116"
 	 }, { 
	 	name : "el tomate",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["140", "365"],
	 	id : "117"
 	 }, { 
	 	name : "el coliflor",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["141", "366"],
	 	id : "118"
 	 }, { 
	 	name : "la lechuga",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["142", "367"],
	 	id : "119"
 	 }, { 
	 	name : "la patata",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["143", "368"],
	 	id : "120"
 	 }, { 
	 	name : "la fresa",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["144", "369"],
	 	id : "121"
 	 }, { 
	 	name : "la fruta",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["145", "146", "370", "371"],
	 	id : "122"
 	 }, { 
	 	name : "la verdura",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["147", "2817", "372", "373"],
	 	id : "123"
 	 }, { 
	 	name : "el carne",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["148", "1690"],
	 	id : "124"
 	 }, { 
	 	name : "el vaso",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["149", "374"],
	 	id : "125"
 	 }, { 
	 	name : "el vegetariano, la vegetariana",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["150", "375"],
	 	id : "126"
 	 }, { 
	 	name : "el vegano, la vegana",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["152", "172", "376"],
	 	id : "128",
	 	tags : []
 	 }, { 
	 	name : "el agua",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["154", "1691", "25"],
	 	id : "130"
 	 }, { 
	 	name : "la pasta",
	 	tag : "(sólo sg.)",
	 	language_ID : "5",
	 	categories_IDs : ["40"],
	 	related_IDs : ["155", "378"],
	 	id : "131"
 	 }, { 
	 	name : "die Banane",
	 	categories_IDs : ["40"],
	 	related_IDs : ["111", "157", "360"],
	 	language_ID : "2",
	 	id : "132",
	 	hasImage : true
 	 }, { 
	 	name : "die Traube",
	 	categories_IDs : ["40"],
	 	related_IDs : ["112", "158", "361"],
	 	language_ID : "2",
	 	id : "133",
	 	hasImage : true
 	 }, { 
	 	name : "the apple",
	 	categories_IDs : ["40"],
	 	related_IDs : ["135", "159", "136", "6kRynX9bmtma"],
	 	language_ID : "3",
	 	id : "134",
	 	tags : []
 	 }, { 
	 	name : "der Apfel",
	 	categories_IDs : ["40"],
	 	related_IDs : ["134", "136", "159"],
	 	language_ID : "2",
	 	id : "135",
	 	hasImage : false,
	 	tags : []
 	 }, { 
	 	name : "la manzana",
	 	categories_IDs : ["40"],
	 	related_IDs : ["134", "135"],
	 	language_ID : "5",
	 	id : "136"
 	 }, { 
	 	name : "die Melone",
	 	categories_IDs : ["40"],
	 	related_IDs : ["113", "160", "362"],
	 	language_ID : "2",
	 	id : "137"
 	 }, { 
	 	name : "die Zwiebel",
	 	categories_IDs : ["40"],
	 	related_IDs : ["114", "161", "363"],
	 	language_ID : "2",
	 	id : "138"
 	 }, { 
	 	name : "die Birne",
	 	categories_IDs : ["40"],
	 	related_IDs : ["115", "162", "364"],
	 	language_ID : "2",
	 	id : "139"
 	 }, { 
	 	name : "die Tomate",
	 	categories_IDs : ["40"],
	 	related_IDs : ["117", "163", "2881", "365"],
	 	language_ID : "2",
	 	id : "140"
 	 }, { 
	 	name : "der Blumenkohl",
	 	categories_IDs : ["40"],
	 	related_IDs : ["118", "164", "366"],
	 	language_ID : "2",
	 	id : "141"
 	 }, { 
	 	name : "der Salat",
	 	categories_IDs : ["40"],
	 	related_IDs : ["119", "165", "367"],
	 	language_ID : "2",
	 	id : "142"
 	 }, { 
	 	name : "die Kartoffel",
	 	categories_IDs : ["40"],
	 	related_IDs : ["120", "166", "2884", "368"],
	 	language_ID : "2",
	 	id : "143"
 	 }, { 
	 	name : "die Erdbeere",
	 	categories_IDs : ["40"],
	 	related_IDs : ["121", "167", "369"],
	 	language_ID : "2",
	 	id : "144"
 	 }, { 
	 	name : "das Obst",
	 	categories_IDs : ["40"],
	 	related_IDs : ["122", "146", "168", "370", "371"],
	 	language_ID : "2",
	 	id : "145"
 	 }, { 
	 	name : "die Frucht",
	 	categories_IDs : ["40"],
	 	related_IDs : ["122", "145", "168", "370", "371"],
	 	language_ID : "2",
	 	id : "146"
 	 }, { 
	 	name : "das Gemüse",
	 	categories_IDs : ["40"],
	 	related_IDs : ["123", "169", "372", "373"],
	 	language_ID : "2",
	 	id : "147"
 	 }, { 
	 	name : "das Fleisch",
	 	categories_IDs : ["40"],
	 	related_IDs : ["124", "1690", "170"],
	 	language_ID : "2",
	 	id : "148"
 	 }, { 
	 	name : "das Glas",
	 	categories_IDs : ["40"],
	 	related_IDs : ["125", "171", "374"],
	 	language_ID : "2",
	 	id : "149"
 	 }, { 
	 	name : "der Vegetarier, die Vegetarierin",
	 	categories_IDs : ["40"],
	 	related_IDs : ["126", "173", "375"],
	 	language_ID : "2",
	 	id : "150"
 	 }, { 
	 	name : "der Veganer, die Veganerin",
	 	categories_IDs : ["40"],
	 	related_IDs : ["128", "172", "376"],
	 	language_ID : "2",
	 	id : "152"
 	 }, { 
	 	name : "das Wasser",
	 	categories_IDs : ["40", "59"],
	 	related_IDs : ["130", "1691", "25"],
	 	language_ID : "2",
	 	id : "154"
 	 }, { 
	 	name : "die Nudel",
	 	categories_IDs : ["40"],
	 	related_IDs : ["131", "174", "378"],
	 	language_ID : "2",
	 	id : "155"
 	 }, { 
	 	name : "die Orange",
	 	categories_IDs : ["40"],
	 	related_IDs : ["116", "175", "379"],
	 	language_ID : "2",
	 	id : "156"
 	 }, { 
	 	name : "en banan",
	 	categories_IDs : ["40"],
	 	related_IDs : ["111", "132", "360"],
	 	language_ID : "1",
	 	id : "157",
	 	hasImage : true
 	 }, { 
	 	name : "en druva",
	 	categories_IDs : ["40"],
	 	related_IDs : ["112", "133", "361"],
	 	language_ID : "1",
	 	id : "158"
 	 }, { 
	 	name : "ett äpple",
	 	categories_IDs : ["40"],
	 	related_IDs : ["134", "135", "136"],
	 	language_ID : "1",
	 	id : "159"
 	 }, { 
	 	name : "en melon",
	 	categories_IDs : ["40"],
	 	related_IDs : ["113", "137", "362"],
	 	language_ID : "1",
	 	id : "160"
 	 }, { 
	 	name : "en lök",
	 	categories_IDs : ["40"],
	 	related_IDs : ["114", "138", "363"],
	 	language_ID : "1",
	 	id : "161"
 	 }, { 
	 	name : "ett pärron",
	 	categories_IDs : ["40"],
	 	related_IDs : ["115", "139", "364"],
	 	language_ID : "1",
	 	id : "162"
 	 }, { 
	 	name : "en tomat",
	 	categories_IDs : ["40"],
	 	related_IDs : ["117", "140", "365"],
	 	language_ID : "1",
	 	id : "163"
 	 }, { 
	 	name : "en blomkål",
	 	categories_IDs : ["40"],
	 	related_IDs : ["118", "141", "366"],
	 	language_ID : "1",
	 	id : "164"
 	 }, { 
	 	name : "en sallad",
	 	categories_IDs : ["40"],
	 	related_IDs : ["119", "142", "367"],
	 	language_ID : "1",
	 	id : "165"
 	 }, { 
	 	name : "en potatis",
	 	categories_IDs : ["40"],
	 	related_IDs : ["120", "143", "368"],
	 	language_ID : "1",
	 	id : "166"
 	 }, { 
	 	name : "en jordgubbe",
	 	categories_IDs : ["40"],
	 	related_IDs : ["121", "144", "369"],
	 	language_ID : "1",
	 	id : "167"
 	 }, { 
	 	name : "en frukt",
	 	categories_IDs : ["40"],
	 	related_IDs : ["122", "145", "146", "370", "371"],
	 	language_ID : "1",
	 	id : "168"
 	 }, { 
	 	name : "en grönsaker",
	 	tag : "(bara pl.)",
	 	categories_IDs : ["40"],
	 	related_IDs : ["123", "147", "2818", "372", "373"],
	 	language_ID : "1",
	 	id : "169"
 	 }, { 
	 	name : "ett kött",
	 	categories_IDs : ["40"],
	 	related_IDs : ["124", "148", "1690"],
	 	language_ID : "1",
	 	id : "170"
 	 }, { 
	 	name : "ett glas",
	 	categories_IDs : ["40"],
	 	related_IDs : ["125", "149", "374"],
	 	language_ID : "1",
	 	id : "171"
 	 }, { 
	 	name : "en vegan",
	 	categories_IDs : ["40"],
	 	related_IDs : ["128", "152", "376"],
	 	language_ID : "1",
	 	id : "172"
 	 }, { 
	 	name : "en vegetarian",
	 	categories_IDs : ["40"],
	 	related_IDs : ["150", "375"],
	 	language_ID : "1",
	 	id : "173"
 	 }, { 
	 	name : "en nudel",
	 	categories_IDs : ["40"],
	 	related_IDs : ["131", "155", "378"],
	 	language_ID : "1",
	 	id : "174"
 	 }, { 
	 	name : "en apelsin",
	 	categories_IDs : ["40"],
	 	related_IDs : ["116", "156", "379"],
	 	language_ID : "1",
	 	id : "175"
 	 }, { 
	 	name : "das Roß",
	 	categories_IDs : ["57"],
	 	related_IDs : ["177", "178", "179", "2330", "2331", "2347"],
	 	language_ID : "2",
	 	id : "176",
	 	tags : []
 	 }, { 
	 	name : "das Pferd",
	 	categories_IDs : ["57"],
	 	related_IDs : ["176", "178", "179", "2331", "2347"],
	 	language_ID : "2",
	 	id : "177"
 	 }, { 
	 	name : "der Gaul",
	 	categories_IDs : ["57"],
	 	related_IDs : ["176", "177", "179", "2330", "2331", "2347"],
	 	language_ID : "2",
	 	id : "178"
 	 }, { 
	 	name : "the horse",
	 	categories_IDs : ["57"],
	 	related_IDs : ["176", "177", "178", "2330", "2331", "2347", "UEbo6iw4rkOn"],
	 	language_ID : "3",
	 	id : "179",
	 	tags : []
 	 }, { 
	 	name : "ju",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1124", "1125", "1128", "1719", "1720", "182"],
	 	language_ID : "1",
	 	id : "181"
 	 }, { 
	 	name : "doch",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1126", "1719", "181", "93"],
	 	language_ID : "2",
	 	id : "182"
 	 }, { 
	 	name : "väl",
	 	categories_IDs : ["33"],
	 	related_IDs : ["184", "2877"],
	 	language_ID : "1",
	 	id : "183"
 	 }, { 
	 	name : "vermutlich",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1713", "183", "2877"],
	 	language_ID : "2",
	 	id : "184"
 	 }, { 
	 	name : "en",
	 	categories_IDs : ["37"],
	 	related_IDs : ["186", "248", "279"],
	 	language_ID : "1",
	 	id : "185"
 	 }, { 
	 	name : "die Eins",
	 	categories_IDs : ["37"],
	 	related_IDs : ["185", "248", "279"],
	 	language_ID : "2",
	 	id : "186"
 	 }, { 
	 	name : "die Zwei",
	 	categories_IDs : ["37"],
	 	related_IDs : ["188", "249", "280"],
	 	language_ID : "2",
	 	id : "187"
 	 }, { 
	 	name : "två",
	 	categories_IDs : ["37"],
	 	related_IDs : ["187", "249", "280"],
	 	language_ID : "1",
	 	id : "188"
 	 }, { 
	 	name : "tre",
	 	categories_IDs : ["37"],
	 	related_IDs : ["190", "250", "281"],
	 	language_ID : "1",
	 	id : "189"
 	 }, { 
	 	name : "die Drei",
	 	categories_IDs : ["37"],
	 	related_IDs : ["189", "250", "281"],
	 	language_ID : "2",
	 	id : "190"
 	 }, { 
	 	name : "die Vier",
	 	categories_IDs : ["37"],
	 	related_IDs : ["192", "251", "282"],
	 	language_ID : "2",
	 	id : "191"
 	 }, { 
	 	name : "fyra",
	 	categories_IDs : ["37"],
	 	related_IDs : ["191", "251", "282"],
	 	language_ID : "1",
	 	id : "192"
 	 }, { 
	 	name : "fem",
	 	categories_IDs : ["37"],
	 	related_IDs : ["194", "252", "283"],
	 	language_ID : "1",
	 	id : "193"
 	 }, { 
	 	name : "die Fünf",
	 	categories_IDs : ["37"],
	 	related_IDs : ["193", "252", "283"],
	 	language_ID : "2",
	 	id : "194"
 	 }, { 
	 	name : "sex",
	 	categories_IDs : ["37"],
	 	related_IDs : ["196", "253", "284"],
	 	language_ID : "1",
	 	id : "195"
 	 }, { 
	 	name : "die Sechs",
	 	categories_IDs : ["37"],
	 	related_IDs : ["195", "253", "284"],
	 	language_ID : "2",
	 	id : "196"
 	 }, { 
	 	name : "sju",
	 	categories_IDs : ["37"],
	 	related_IDs : ["198", "254", "285"],
	 	language_ID : "1",
	 	id : "197"
 	 }, { 
	 	name : "die Sieben",
	 	categories_IDs : ["37"],
	 	related_IDs : ["197", "254", "285"],
	 	language_ID : "2",
	 	id : "198"
 	 }, { 
	 	name : "åtta",
	 	categories_IDs : ["37"],
	 	related_IDs : ["200", "256", "286"],
	 	language_ID : "1",
	 	id : "199"
 	 }, { 
	 	name : "die Acht",
	 	categories_IDs : ["37"],
	 	related_IDs : ["199", "256", "286"],
	 	language_ID : "2",
	 	id : "200"
 	 }, { 
	 	name : "noll",
	 	categories_IDs : ["37"],
	 	related_IDs : ["202", "255", "289"],
	 	language_ID : "1",
	 	id : "201"
 	 }, { 
	 	name : "die Null",
	 	categories_IDs : ["37"],
	 	related_IDs : ["201", "255", "289"],
	 	language_ID : "2",
	 	id : "202"
 	 }, { 
	 	name : "nio",
	 	categories_IDs : ["37"],
	 	related_IDs : ["205", "257", "287"],
	 	language_ID : "1",
	 	id : "203"
 	 }, { 
	 	name : "die Neun",
	 	categories_IDs : ["37"],
	 	related_IDs : ["203", "257", "287"],
	 	language_ID : "2",
	 	id : "205"
 	 }, { 
	 	name : "tio",
	 	categories_IDs : ["37"],
	 	related_IDs : ["207", "258", "288"],
	 	language_ID : "1",
	 	id : "206"
 	 }, { 
	 	name : "die Zehn",
	 	categories_IDs : ["37"],
	 	related_IDs : ["206", "258", "288", "2939"],
	 	language_ID : "2",
	 	id : "207"
 	 }, { 
	 	name : "elva",
	 	categories_IDs : ["37"],
	 	related_IDs : ["209", "259", "290"],
	 	language_ID : "1",
	 	id : "208"
 	 }, { 
	 	name : "die Elf",
	 	categories_IDs : ["37"],
	 	related_IDs : ["208", "259", "290"],
	 	language_ID : "2",
	 	id : "209"
 	 }, { 
	 	name : "tolv",
	 	categories_IDs : ["37"],
	 	related_IDs : ["211", "260", "291"],
	 	language_ID : "1",
	 	id : "210"
 	 }, { 
	 	name : "die Zwölf",
	 	categories_IDs : ["37"],
	 	related_IDs : ["210", "260", "291"],
	 	language_ID : "2",
	 	id : "211"
 	 }, { 
	 	name : "die Dreizehn",
	 	categories_IDs : ["37"],
	 	related_IDs : ["261", "292"],
	 	language_ID : "2",
	 	id : "213"
 	 }, { 
	 	name : "fjorton",
	 	categories_IDs : ["37"],
	 	related_IDs : ["216", "262", "2834", "293"],
	 	language_ID : "1",
	 	id : "214"
 	 }, { 
	 	name : "die Vierzehn",
	 	categories_IDs : ["37"],
	 	related_IDs : ["214", "262", "293", "790"],
	 	language_ID : "2",
	 	id : "216"
 	 }, { 
	 	name : "femton",
	 	categories_IDs : ["37"],
	 	related_IDs : ["219", "263", "294"],
	 	language_ID : "1",
	 	id : "217"
 	 }, { 
	 	name : "die Fünfzehn",
	 	categories_IDs : ["37"],
	 	related_IDs : ["217", "263", "294"],
	 	language_ID : "2",
	 	id : "219"
 	 }, { 
	 	name : "sexton",
	 	categories_IDs : ["37"],
	 	related_IDs : ["221", "264", "295"],
	 	language_ID : "1",
	 	id : "220"
 	 }, { 
	 	name : "die Sechzehn",
	 	categories_IDs : ["37"],
	 	related_IDs : ["220", "264", "295"],
	 	language_ID : "2",
	 	id : "221"
 	 }, { 
	 	name : "sjutton",
	 	categories_IDs : ["37"],
	 	related_IDs : ["223", "265", "296"],
	 	language_ID : "1",
	 	id : "222"
 	 }, { 
	 	name : "die Siebzehn",
	 	categories_IDs : ["37"],
	 	related_IDs : ["222", "265", "296"],
	 	language_ID : "2",
	 	id : "223"
 	 }, { 
	 	name : "arton",
	 	categories_IDs : ["37"],
	 	related_IDs : ["225", "266", "297"],
	 	language_ID : "1",
	 	id : "224"
 	 }, { 
	 	name : "die Achtzehn",
	 	categories_IDs : ["37"],
	 	related_IDs : ["224", "266", "297"],
	 	language_ID : "2",
	 	id : "225"
 	 }, { 
	 	name : "nitton",
	 	categories_IDs : ["37"],
	 	related_IDs : ["228", "267", "298"],
	 	language_ID : "1",
	 	id : "226"
 	 }, { 
	 	name : "die Neunzehn",
	 	categories_IDs : ["37"],
	 	related_IDs : ["226", "267", "298"],
	 	language_ID : "2",
	 	id : "228"
 	 }, { 
	 	name : "tjugo",
	 	categories_IDs : ["37"],
	 	related_IDs : ["230", "268", "299"],
	 	language_ID : "1",
	 	id : "229"
 	 }, { 
	 	name : "die Zwanzig",
	 	categories_IDs : ["37"],
	 	related_IDs : ["229", "268", "299"],
	 	language_ID : "2",
	 	id : "230"
 	 }, { 
	 	name : "trettio",
	 	categories_IDs : ["37"],
	 	related_IDs : ["232", "269", "300"],
	 	language_ID : "1",
	 	id : "231",
	 	tags : []
 	 }, { 
	 	name : "die Dreißig",
	 	categories_IDs : ["37"],
	 	related_IDs : ["231", "269", "300"],
	 	language_ID : "2",
	 	id : "232"
 	 }, { 
	 	name : "fyrtio",
	 	categories_IDs : ["37"],
	 	related_IDs : ["234", "270", "301"],
	 	language_ID : "1",
	 	id : "233"
 	 }, { 
	 	name : "die Vierzig",
	 	categories_IDs : ["37"],
	 	related_IDs : ["233", "270", "301"],
	 	language_ID : "2",
	 	id : "234"
 	 }, { 
	 	name : "die Fünfzig",
	 	categories_IDs : ["37"],
	 	related_IDs : ["236", "271", "302"],
	 	language_ID : "2",
	 	id : "235"
 	 }, { 
	 	name : "femtio",
	 	categories_IDs : ["37"],
	 	related_IDs : ["235", "271", "302"],
	 	language_ID : "1",
	 	id : "236"
 	 }, { 
	 	name : "sextio",
	 	categories_IDs : ["37"],
	 	related_IDs : ["238", "272", "303"],
	 	language_ID : "1",
	 	id : "237"
 	 }, { 
	 	name : "die Sechzig",
	 	categories_IDs : ["37"],
	 	related_IDs : ["237", "272", "303"],
	 	language_ID : "2",
	 	id : "238"
 	 }, { 
	 	name : "sjuttio",
	 	categories_IDs : ["37"],
	 	related_IDs : ["240", "273", "304"],
	 	language_ID : "1",
	 	id : "239"
 	 }, { 
	 	name : "die Siebzig",
	 	categories_IDs : ["37"],
	 	related_IDs : ["239", "273", "304"],
	 	language_ID : "2",
	 	id : "240"
 	 }, { 
	 	name : "åttio",
	 	categories_IDs : ["37"],
	 	related_IDs : ["242", "274", "305"],
	 	language_ID : "1",
	 	id : "241"
 	 }, { 
	 	name : "die Achzig",
	 	categories_IDs : ["37"],
	 	related_IDs : ["241", "274", "305"],
	 	language_ID : "2",
	 	id : "242"
 	 }, { 
	 	name : "nittio",
	 	categories_IDs : ["37"],
	 	related_IDs : ["244", "275", "306"],
	 	language_ID : "1",
	 	id : "243"
 	 }, { 
	 	name : "die Neunzig",
	 	categories_IDs : ["37"],
	 	related_IDs : ["243", "275", "306"],
	 	language_ID : "2",
	 	id : "244"
 	 }, { 
	 	name : "ett hundra",
	 	categories_IDs : ["37"],
	 	related_IDs : ["247", "276", "307"],
	 	language_ID : "1",
	 	id : "245"
 	 }, { 
	 	name : "die Hundert",
	 	categories_IDs : ["37"],
	 	related_IDs : ["245", "276", "307"],
	 	language_ID : "2",
	 	id : "247"
 	 }, { 
	 	name : "one",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["185", "186", "279"],
	 	id : "248"
 	 }, { 
	 	name : "two",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["187", "188", "280", "2828", "OU1XNBg6Cz2O"],
	 	id : "249"
 	 }, { 
	 	name : "three",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["189", "190", "281", "OU1XNBg6Cz2O"],
	 	id : "250"
 	 }, { 
	 	name : "four",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["191", "192", "282"],
	 	id : "251"
 	 }, { 
	 	name : "five",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["193", "194", "283"],
	 	id : "252"
 	 }, { 
	 	name : "six",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["195", "196", "284", "OU1XNBg6Cz2O"],
	 	id : "253"
 	 }, { 
	 	name : "seven",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["197", "198", "285"],
	 	id : "254"
 	 }, { 
	 	name : "zero",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["201", "202", "289"],
	 	id : "255"
 	 }, { 
	 	name : "eight",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["199", "200", "286", "AwyU31OjUJ3k"],
	 	id : "256"
 	 }, { 
	 	name : "nine",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["203", "205", "287", "lzxJwHxP7qNQ"],
	 	id : "257"
 	 }, { 
	 	name : "ten",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["206", "207", "288"],
	 	id : "258"
 	 }, { 
	 	name : "eleven",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["208", "209", "290"],
	 	id : "259"
 	 }, { 
	 	name : "twelf",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["210", "211", "291"],
	 	id : "260"
 	 }, { 
	 	name : "thirteen",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["213", "292"],
	 	id : "261"
 	 }, { 
	 	name : "fourteen",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["214", "216", "293"],
	 	id : "262"
 	 }, { 
	 	name : "fifteen",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["217", "219", "294"],
	 	id : "263"
 	 }, { 
	 	name : "sixteen",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["220", "221", "295", "lzxJwHxP7qNQ"],
	 	id : "264"
 	 }, { 
	 	name : "seventeen",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["222", "223", "296"],
	 	id : "265"
 	 }, { 
	 	name : "eighteen",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["224", "225", "297"],
	 	id : "266"
 	 }, { 
	 	name : "nineteen",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["226", "228", "298"],
	 	id : "267"
 	 }, { 
	 	name : "twenty",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["229", "230", "299", "AwyU31OjUJ3k"],
	 	id : "268"
 	 }, { 
	 	name : "thirty",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["231", "232", "300", "AwyU31OjUJ3k"],
	 	id : "269"
 	 }, { 
	 	name : "fourty",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["233", "234", "301"],
	 	id : "270"
 	 }, { 
	 	name : "fifty",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["235", "236", "302"],
	 	id : "271"
 	 }, { 
	 	name : "sixty",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["237", "238", "303"],
	 	id : "272"
 	 }, { 
	 	name : "seventy",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["239", "240", "304"],
	 	id : "273"
 	 }, { 
	 	name : "eighty",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["241", "242", "305"],
	 	id : "274"
 	 }, { 
	 	name : "ninety",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["243", "244", "306"],
	 	id : "275"
 	 }, { 
	 	name : "one hundred",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["245", "247", "307"],
	 	id : "276"
 	 }, { 
	 	name : "a thousand",
	 	language_ID : "3",
	 	categories_IDs : ["37"],
	 	related_IDs : ["278", "308", "309"],
	 	id : "277"
 	 }, { 
	 	name : "die Tausend",
	 	categories_IDs : ["37"],
	 	related_IDs : ["277", "308", "309"],
	 	language_ID : "2",
	 	id : "278"
 	 }, { 
	 	name : "uno",
	 	categories_IDs : ["37"],
	 	related_IDs : ["185", "186", "248"],
	 	language_ID : "5",
	 	id : "279"
 	 }, { 
	 	name : "dos",
	 	categories_IDs : ["37"],
	 	related_IDs : ["187", "188", "249", "2833"],
	 	language_ID : "5",
	 	id : "280"
 	 }, { 
	 	name : "tres",
	 	categories_IDs : ["37"],
	 	related_IDs : ["189", "190", "250"],
	 	language_ID : "5",
	 	id : "281"
 	 }, { 
	 	name : "cuatro",
	 	categories_IDs : ["37"],
	 	related_IDs : ["191", "192", "251"],
	 	language_ID : "5",
	 	id : "282"
 	 }, { 
	 	name : "cinco",
	 	categories_IDs : ["37"],
	 	related_IDs : ["193", "194", "252"],
	 	language_ID : "5",
	 	id : "283"
 	 }, { 
	 	name : "seis",
	 	categories_IDs : ["37"],
	 	related_IDs : ["195", "196", "253"],
	 	language_ID : "5",
	 	id : "284"
 	 }, { 
	 	name : "siete",
	 	categories_IDs : ["37"],
	 	related_IDs : ["197", "198", "254"],
	 	language_ID : "5",
	 	id : "285"
 	 }, { 
	 	name : "ocho",
	 	categories_IDs : ["37"],
	 	related_IDs : ["199", "200", "256"],
	 	language_ID : "5",
	 	id : "286"
 	 }, { 
	 	name : "nueve",
	 	categories_IDs : ["37"],
	 	related_IDs : ["203", "205", "257"],
	 	language_ID : "5",
	 	id : "287"
 	 }, { 
	 	name : "diez",
	 	categories_IDs : ["37"],
	 	related_IDs : ["206", "207", "258"],
	 	language_ID : "5",
	 	id : "288"
 	 }, { 
	 	name : "cero",
	 	categories_IDs : ["37"],
	 	related_IDs : ["201", "202", "255"],
	 	language_ID : "5",
	 	id : "289"
 	 }, { 
	 	name : "once",
	 	categories_IDs : ["37"],
	 	related_IDs : ["208", "209", "259"],
	 	language_ID : "5",
	 	id : "290"
 	 }, { 
	 	name : "doce",
	 	categories_IDs : ["37"],
	 	related_IDs : ["210", "211", "260"],
	 	language_ID : "5",
	 	id : "291"
 	 }, { 
	 	name : "trece",
	 	categories_IDs : ["37"],
	 	related_IDs : ["213", "261"],
	 	language_ID : "5",
	 	id : "292"
 	 }, { 
	 	name : "catorce",
	 	categories_IDs : ["37"],
	 	related_IDs : ["214", "216", "262"],
	 	language_ID : "5",
	 	id : "293"
 	 }, { 
	 	name : "quince",
	 	categories_IDs : ["37"],
	 	related_IDs : ["217", "219", "263"],
	 	language_ID : "5",
	 	id : "294"
 	 }, { 
	 	name : "dieciséis",
	 	categories_IDs : ["37"],
	 	related_IDs : ["220", "221", "264"],
	 	language_ID : "5",
	 	id : "295"
 	 }, { 
	 	name : "diecisiete",
	 	categories_IDs : ["37"],
	 	related_IDs : ["222", "223", "265"],
	 	language_ID : "5",
	 	id : "296"
 	 }, { 
	 	name : "dieciocho",
	 	categories_IDs : ["37"],
	 	related_IDs : ["224", "225", "266"],
	 	language_ID : "5",
	 	id : "297"
 	 }, { 
	 	name : "diecinueve",
	 	categories_IDs : ["37"],
	 	related_IDs : ["226", "228", "267"],
	 	language_ID : "5",
	 	id : "298"
 	 }, { 
	 	name : "veinte",
	 	categories_IDs : ["37"],
	 	related_IDs : ["229", "230", "268"],
	 	language_ID : "5",
	 	id : "299"
 	 }, { 
	 	name : "treinta",
	 	categories_IDs : ["37"],
	 	related_IDs : ["231", "232", "269"],
	 	language_ID : "5",
	 	id : "300"
 	 }, { 
	 	name : "cuarenta",
	 	categories_IDs : ["37"],
	 	related_IDs : ["233", "234", "270"],
	 	language_ID : "5",
	 	id : "301"
 	 }, { 
	 	name : "cincuenta",
	 	categories_IDs : ["37"],
	 	related_IDs : ["235", "236", "271"],
	 	language_ID : "5",
	 	id : "302"
 	 }, { 
	 	name : "sesenta",
	 	categories_IDs : ["37"],
	 	related_IDs : ["237", "238", "272"],
	 	language_ID : "5",
	 	id : "303"
 	 }, { 
	 	name : "setenta",
	 	categories_IDs : ["37"],
	 	related_IDs : ["239", "240", "273"],
	 	language_ID : "5",
	 	id : "304"
 	 }, { 
	 	name : "ochenta",
	 	categories_IDs : ["37"],
	 	related_IDs : ["241", "242", "274"],
	 	language_ID : "5",
	 	id : "305"
 	 }, { 
	 	name : "noventa",
	 	categories_IDs : ["37"],
	 	related_IDs : ["243", "244", "275"],
	 	language_ID : "5",
	 	id : "306"
 	 }, { 
	 	name : "ciento",
	 	categories_IDs : ["37"],
	 	related_IDs : ["245", "247", "276"],
	 	language_ID : "5",
	 	id : "307"
 	 }, { 
	 	name : "mil",
	 	categories_IDs : ["37"],
	 	related_IDs : ["277", "278", "309"],
	 	language_ID : "5",
	 	id : "308"
 	 }, { 
	 	name : "tusen",
	 	categories_IDs : ["37"],
	 	related_IDs : ["277", "278", "308"],
	 	language_ID : "1",
	 	id : "309"
 	 }, { 
	 	name : "stor",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1800", "311", "341", "343"],
	 	language_ID : "1",
	 	id : "310"
 	 }, { 
	 	name : "groß",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1800", "310", "341", "343"],
	 	language_ID : "2",
	 	id : "311"
 	 }, { 
	 	name : "kort",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1804", "313", "3192", "342"],
	 	language_ID : "1",
	 	id : "312"
 	 }, { 
	 	name : "kurz",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1804", "312", "3192", "342"],
	 	language_ID : "2",
	 	id : "313"
 	 }, { 
	 	name : "liten",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1803", "316", "3191", "344"],
	 	language_ID : "1",
	 	id : "314"
 	 }, { 
	 	name : "klein",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1803", "314", "3191", "344"],
	 	language_ID : "2",
	 	id : "316"
 	 }, { 
	 	name : "kulört",
	 	categories_IDs : ["60"],
	 	related_IDs : ["1799", "3087", "318", "345"],
	 	language_ID : "1",
	 	id : "317",
	 	tags : []
 	 }, { 
	 	name : "bunt",
	 	categories_IDs : ["60"],
	 	related_IDs : ["1799", "3087", "317", "345"],
	 	language_ID : "2",
	 	id : "318"
 	 }, { 
	 	name : "gammal",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1796", "3190", "320", "346"],
	 	language_ID : "1",
	 	id : "319"
 	 }, { 
	 	name : "alt",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1796", "319", "3190", "346"],
	 	language_ID : "2",
	 	id : "320"
 	 }, { 
	 	name : "ung",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1802", "322", "347"],
	 	language_ID : "1",
	 	id : "321"
 	 }, { 
	 	name : "jung",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1802", "321", "347"],
	 	language_ID : "2",
	 	id : "322"
 	 }, { 
	 	name : "ny",
	 	categories_IDs : ["60"],
	 	related_IDs : ["1806", "3194", "324", "348"],
	 	language_ID : "1",
	 	id : "323"
 	 }, { 
	 	name : "neu",
	 	categories_IDs : ["60"],
	 	related_IDs : ["1806", "3194", "323", "348"],
	 	language_ID : "2",
	 	id : "324"
 	 }, { 
	 	name : "grön",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1801", "326", "349"],
	 	language_ID : "1",
	 	id : "325"
 	 }, { 
	 	name : "grün",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1801", "325", "349"],
	 	language_ID : "2",
	 	id : "326"
 	 }, { 
	 	name : "röd",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1807", "3195", "328", "350"],
	 	language_ID : "1",
	 	id : "327"
 	 }, { 
	 	name : "rot",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1807", "3195", "327", "350"],
	 	language_ID : "2",
	 	id : "328"
 	 }, { 
	 	name : "blå",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1797", "330", "351"],
	 	language_ID : "1",
	 	id : "329"
 	 }, { 
	 	name : "blau",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1797", "329", "351"],
	 	language_ID : "2",
	 	id : "330"
 	 }, { 
	 	name : "vit",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1810", "3198", "332", "352"],
	 	language_ID : "1",
	 	id : "331"
 	 }, { 
	 	name : "weiß",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1810", "3198", "331", "352"],
	 	language_ID : "2",
	 	id : "332"
 	 }, { 
	 	name : "svart",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1808", "3196", "334", "353"],
	 	language_ID : "1",
	 	id : "333"
 	 }, { 
	 	name : "schwarz",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1808", "3196", "333", "353"],
	 	language_ID : "2",
	 	id : "334"
 	 }, { 
	 	name : "brun",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1798", "336", "354"],
	 	language_ID : "1",
	 	id : "335"
 	 }, { 
	 	name : "braun",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1798", "335", "354"],
	 	language_ID : "2",
	 	id : "336"
 	 }, { 
	 	name : "vacker",
	 	categories_IDs : ["60"],
	 	related_IDs : ["1809", "3085", "3086", "3197", "338", "355"],
	 	language_ID : "1",
	 	id : "337",
	 	tags : []
 	 }, { 
	 	name : "schön",
	 	categories_IDs : ["60"],
	 	related_IDs : ["1809", "3085", "3086", "3197", "337", "355"],
	 	language_ID : "2",
	 	id : "338"
 	 }, { 
	 	name : "lång",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1805", "3193", "340", "356", "358"],
	 	language_ID : "1",
	 	id : "339"
 	 }, { 
	 	name : "lang",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1805", "3193", "339", "356", "358"],
	 	language_ID : "2",
	 	id : "340"
 	 }, { 
	 	name : "big",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1800", "310", "311", "9BXJuuT2mYR4"],
	 	language_ID : "3",
	 	id : "341",
	 	tags : []
 	 }, { 
	 	name : "short",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1804", "312", "313", "3192", "uiYPoNsP9Orw"],
	 	language_ID : "3",
	 	id : "342",
	 	tags : []
 	 }, { 
	 	name : "large",
	 	categories_IDs : ["60"],
	 	related_IDs : ["1800", "310", "311"],
	 	language_ID : "3",
	 	id : "343"
 	 }, { 
	 	name : "small",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1803", "314", "316", "3191"],
	 	language_ID : "3",
	 	id : "344"
 	 }, { 
	 	name : "coloured",
	 	categories_IDs : ["60"],
	 	related_IDs : ["1799", "3087", "317", "318"],
	 	language_ID : "3",
	 	id : "345"
 	 }, { 
	 	name : "old",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1796", "319", "3190", "320", "Q0JOzDvAItFx"],
	 	language_ID : "3",
	 	id : "346",
	 	tags : [],
	 	formsSecondary : ["older [than]", "oldest"],
	 	formsSecondaryNames : ["comparative", "superlative"]
 	 }, { 
	 	name : "young",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1802", "321", "322"],
	 	language_ID : "3",
	 	id : "347"
 	 }, { 
	 	name : "new",
	 	categories_IDs : ["60"],
	 	related_IDs : ["1806", "3194", "323", "324"],
	 	language_ID : "3",
	 	id : "348"
 	 }, { 
	 	name : "green",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1801", "325", "326", "T0EkiFW0NdZn"],
	 	language_ID : "3",
	 	id : "349"
 	 }, { 
	 	name : "red",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1807", "3195", "327", "328"],
	 	language_ID : "3",
	 	id : "350"
 	 }, { 
	 	name : "blue",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1797", "329", "330", "nvoHYN3Whtfl", "ijEgwjqlizr5"],
	 	language_ID : "3",
	 	id : "351",
	 	tags : []
 	 }, { 
	 	name : "white",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1810", "3198", "331", "332", "k1V3SDu9bmft", "n21VuZLdDydW"],
	 	language_ID : "3",
	 	id : "352",
	 	tags : []
 	 }, { 
	 	name : "black",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1808", "3196", "333", "334"],
	 	language_ID : "3",
	 	id : "353"
 	 }, { 
	 	name : "brown",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1798", "335", "336"],
	 	language_ID : "3",
	 	id : "354"
 	 }, { 
	 	name : "beautiful",
	 	categories_IDs : ["60"],
	 	related_IDs : ["1809", "3085", "3086", "3197", "337", "338"],
	 	language_ID : "3",
	 	id : "355"
 	 }, { 
	 	name : "long",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1805", "3193", "339", "340", "358", "x76cEC3LdZt2"],
	 	language_ID : "3",
	 	id : "356",
	 	tags : []
 	 }, { 
	 	name : "en tall",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2391", "2413", "359"],
	 	language_ID : "1",
	 	id : "357"
 	 }, { 
	 	name : "tall",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["1805", "3193", "339", "340", "356"],
	 	language_ID : "3",
	 	id : "358"
 	 }, { 
	 	categories_IDs : ["56"],
	 	related_IDs : ["2391", "2413", "357"],
	 	language_ID : "2",
	 	name : "die Kiefer",
	 	id : "359"
 	 }, { 
	 	name : "the banana",
	 	categories_IDs : ["40", "45", "60"],
	 	related_IDs : ["111", "132", "157", "Q0JOzDvAItFx"],
	 	language_ID : "3",
	 	id : "360",
	 	tags : []
 	 }, { 
	 	name : "the grape",
	 	categories_IDs : ["40"],
	 	related_IDs : ["112", "133", "158"],
	 	language_ID : "3",
	 	id : "361"
 	 }, { 
	 	name : "the melon",
	 	categories_IDs : ["40"],
	 	related_IDs : ["113", "137", "160"],
	 	language_ID : "3",
	 	id : "362"
 	 }, { 
	 	name : "the onion",
	 	categories_IDs : ["40"],
	 	related_IDs : ["114", "138", "161", "zZXRu1xNRIbf"],
	 	language_ID : "3",
	 	id : "363",
	 	tags : []
 	 }, { 
	 	name : "the pear",
	 	categories_IDs : ["40"],
	 	related_IDs : ["115", "139", "162"],
	 	language_ID : "3",
	 	id : "364"
 	 }, { 
	 	name : "the tomato",
	 	categories_IDs : ["40"],
	 	related_IDs : ["117", "140", "163"],
	 	language_ID : "3",
	 	id : "365"
 	 }, { 
	 	name : "the cauliflower",
	 	categories_IDs : ["40"],
	 	related_IDs : ["118", "141", "164"],
	 	language_ID : "3",
	 	id : "366"
 	 }, { 
	 	name : "the salad",
	 	categories_IDs : ["40"],
	 	related_IDs : ["119", "142", "165", "hOKBaLEHhhPv"],
	 	language_ID : "3",
	 	id : "367"
 	 }, { 
	 	name : "the potato",
	 	categories_IDs : ["40"],
	 	related_IDs : ["120", "143", "166"],
	 	language_ID : "3",
	 	id : "368"
 	 }, { 
	 	name : "the strawberry",
	 	categories_IDs : ["40"],
	 	related_IDs : ["121", "144", "167"],
	 	language_ID : "3",
	 	id : "369"
 	 }, { 
	 	name : "the fruits",
	 	categories_IDs : ["40"],
	 	related_IDs : ["122", "145", "146", "168", "371"],
	 	language_ID : "3",
	 	id : "370"
 	 }, { 
	 	name : "the fruit",
	 	categories_IDs : ["40"],
	 	related_IDs : ["122", "145", "146", "168", "370"],
	 	language_ID : "3",
	 	id : "371"
 	 }, { 
	 	name : "the vegetables",
	 	categories_IDs : ["40"],
	 	related_IDs : ["123", "147", "169", "2816", "373"],
	 	language_ID : "3",
	 	id : "372"
 	 }, { 
	 	name : "the groceries",
	 	categories_IDs : ["40"],
	 	related_IDs : ["123", "147", "169", "372"],
	 	language_ID : "3",
	 	id : "373"
 	 }, { 
	 	name : "the glas",
	 	categories_IDs : ["40"],
	 	related_IDs : ["125", "149", "171"],
	 	language_ID : "3",
	 	id : "374"
 	 }, { 
	 	name : "the vegetarian",
	 	categories_IDs : ["40"],
	 	related_IDs : ["126", "150", "173"],
	 	language_ID : "3",
	 	id : "375"
 	 }, { 
	 	name : "the vegan",
	 	categories_IDs : ["40"],
	 	related_IDs : ["128", "152", "172"],
	 	language_ID : "3",
	 	id : "376"
 	 }, { 
	 	name : "the noodle",
	 	categories_IDs : ["40"],
	 	related_IDs : ["131", "155", "174"],
	 	language_ID : "3",
	 	id : "378"
 	 }, { 
	 	name : "the orange",
	 	categories_IDs : ["40"],
	 	related_IDs : ["116", "156", "175"],
	 	language_ID : "3",
	 	id : "379"
 	 }, { 
	 	name : "vara",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["2842", "415", "452", "1731"],
	 	id : "380"
 	 }, { 
	 	name : "göra",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1751", "416", "453"],
	 	id : "381",
	 	tags : [],
	 	formsSecondary : ["gjorde", "gjort"],
	 	formsSecondaryNames : ["Preteritum", "Supinum"]
 	 }, { 
	 	name : "behöva",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1737", "417", "454"],
	 	id : "382"
 	 }, { 
	 	name : "blir",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1763", "3184", "418", "455"],
	 	id : "383"
 	 }, { 
	 	name : "få",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["3092", "3094", "3096", "3098", "3101", "419", "484"],
	 	id : "384",
	 	tags : []
 	 }, { 
	 	name : "förstå",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1762", "3183", "420", "456"],
	 	id : "385"
 	 }, { 
	 	name : "gilla",
	 	language_ID : "1",
	 	categories_IDs : ["35", "63"],
	 	related_IDs : ["421", "458", "1730"],
	 	id : "386"
 	 }, { 
	 	name : "gå",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1741", "422", "457"],
	 	id : "387",
	 	tags : [],
	 	formsSecondary : ["gick", "gått"],
	 	formsSecondaryNames : ["Preteritum", "Supinum"]
 	 }, { 
	 	name : "ha",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1742", "423", "459"],
	 	id : "388",
	 	tags : [],
	 	formsSecondary : ["hade", "haft"],
	 	formsSecondaryNames : ["Preteritum", "Supinum"]
 	 }, { 
	 	name : "sälja",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1761", "424", "460"],
	 	id : "389"
 	 }, { 
	 	name : "stämma",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1759", "2868", "425"],
	 	id : "390"
 	 }, { 
	 	name : "heta",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1743", "426", "483"],
	 	id : "391",
	 	tags : [],
	 	formsSecondary : ["hette", "hetat"],
	 	formsSecondaryNames : ["Preteritum", "Supinum"]
 	 }, { 
	 	name : "hjälpa",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1733", "427", "461"],
	 	id : "392"
 	 }, { 
	 	name : "ingå",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1735", "428", "462"],
	 	id : "393"
 	 }, { 
	 	name : "känna",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1745", "1746", "1747", "2538", "2867", "429"],
	 	id : "394",
	 	tags : []
 	 }, { 
	 	name : "kunna",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1744", "430", "463"],
	 	id : "395",
	 	tags : [],
	 	formsSecondary : ["kunde", "kunnat"],
	 	formsSecondaryNames : ["Preteritum", "Supinum"]
 	 }, { 
	 	name : "ligga",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1750", "3182", "431", "464"],
	 	id : "396"
 	 }, { 
	 	name : "lära",
	 	language_ID : "1",
	 	categories_IDs : ["35", "55"],
	 	related_IDs : ["1748", "1749", "2627", "433"],
	 	id : "397"
 	 }, { 
	 	name : "läsa",
	 	language_ID : "1",
	 	categories_IDs : ["35", "46", "55"],
	 	related_IDs : ["1567", "1606", "2627", "433", "485"],
	 	id : "398"
 	 }, { 
	 	name : "måste",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1753", "434", "466"],
	 	id : "399"
 	 }, { 
	 	name : "prata",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["435", "467", "2677"],
	 	id : "400"
 	 }, { 
	 	name : "se",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1732", "436", "470"],
	 	id : "401"
 	 }, { 
	 	name : "äta",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1740", "437", "469"],
	 	id : "402"
 	 }, { 
	 	name : "skola",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1756", "438", "471"],
	 	id : "403"
 	 }, { 
	 	name : "skriva",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1755", "439", "472"],
	 	id : "404"
 	 }, { 
	 	name : "stava",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1738", "441", "486"],
	 	id : "406"
 	 }, { 
	 	name : "stå",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["442", "474", "1731"],
	 	id : "407"
 	 }, { 
	 	name : "köpa",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1734", "2818", "443", "475"],
	 	id : "408",
	 	tags : []
 	 }, { 
	 	name : "ta",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1754", "444", "476"],
	 	id : "409",
	 	tags : [],
	 	formsSecondary : ["tog", "tagit"],
	 	formsSecondaryNames : ["Preteritum", "Supinum"]
 	 }, { 
	 	name : "söka",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1760", "445", "477"],
	 	id : "410"
 	 }, { 
	 	name : "veta",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["2867", "446", "ugTZyT2iO2vq"],
	 	id : "411"
 	 }, { 
	 	name : "villja",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["447", "480", "1730"],
	 	id : "412"
 	 }, { 
	 	name : "åka",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1741", "448", "479"],
	 	id : "413"
 	 }, { 
	 	name : "tänka",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1739", "449", "481"],
	 	id : "414"
 	 }, { 
	 	name : "sein",
	 	categories_IDs : ["35"],
	 	related_IDs : ["380", "452", "1731"],
	 	language_ID : "2",
	 	id : "415"
 	 }, { 
	 	name : "machen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1751", "381", "453"],
	 	language_ID : "2",
	 	id : "416"
 	 }, { 
	 	name : "brauchen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1737", "382", "454"],
	 	language_ID : "2",
	 	id : "417"
 	 }, { 
	 	name : "werden",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1763", "3184", "383", "455"],
	 	language_ID : "2",
	 	id : "418"
 	 }, { 
	 	name : "dürfen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["3099", "3100", "3101", "384", "484"],
	 	language_ID : "2",
	 	id : "419",
	 	tags : []
 	 }, { 
	 	name : "verstehen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1762", "3183", "385", "456"],
	 	language_ID : "2",
	 	id : "420"
 	 }, { 
	 	name : "mögen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["386", "458", "1730"],
	 	language_ID : "2",
	 	id : "421"
 	 }, { 
	 	name : "gehen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1741", "387", "457"],
	 	language_ID : "2",
	 	id : "422"
 	 }, { 
	 	name : "haben",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1742", "2839", "388", "459"],
	 	language_ID : "2",
	 	id : "423"
 	 }, { 
	 	name : "verkaufen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1761", "389", "460"],
	 	language_ID : "2",
	 	id : "424"
 	 }, { 
	 	name : "stimmen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1759", "2868", "390"],
	 	language_ID : "2",
	 	id : "425"
 	 }, { 
	 	name : "heißen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1743", "391", "483"],
	 	language_ID : "2",
	 	id : "426"
 	 }, { 
	 	name : "helfen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1733", "392", "461"],
	 	language_ID : "2",
	 	id : "427"
 	 }, { 
	 	name : "beinhalten",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1735", "393", "462"],
	 	language_ID : "2",
	 	id : "428"
 	 }, { 
	 	name : "kennen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1747", "2867", "394"],
	 	language_ID : "2",
	 	id : "429"
 	 }, { 
	 	name : "können",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1744", "2835", "2855", "395", "463"],
	 	language_ID : "2",
	 	id : "430"
 	 }, { 
	 	name : "liegen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1750", "3182", "396", "464", "PJ3JUUslu99U"],
	 	language_ID : "2",
	 	id : "431"
 	 }, { 
	 	name : "lernen",
	 	categories_IDs : ["35", "46", "55"],
	 	related_IDs : ["1748", "1749", "2627", "397", "398"],
	 	language_ID : "2",
	 	id : "433"
 	 }, { 
	 	name : "müssen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1753", "399", "466"],
	 	language_ID : "2",
	 	id : "434"
 	 }, { 
	 	name : "sehen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1732", "401", "470"],
	 	language_ID : "2",
	 	id : "436"
 	 }, { 
	 	name : "essen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1740", "402", "469"],
	 	language_ID : "2",
	 	id : "437"
 	 }, { 
	 	name : "sollen",
	 	categories_IDs : ["35", "63"],
	 	related_IDs : ["1756", "403", "471", "JNLqTZiSXA0q"],
	 	language_ID : "2",
	 	id : "438"
 	 }, { 
	 	name : "schreiben",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1755", "2932", "404", "472"],
	 	language_ID : "2",
	 	id : "439",
	 	tags : []
 	 }, { 
	 	name : "bleiben",
	 	categories_IDs : ["35"],
	 	related_IDs : ["3045", "473", "JNLqTZiSXA0q"],
	 	language_ID : "2",
	 	id : "440",
	 	tags : []
 	 }, { 
	 	name : "buchstabieren",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1738", "406", "486"],
	 	language_ID : "2",
	 	id : "441"
 	 }, { 
	 	name : "stehen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["407", "474", "1731"],
	 	language_ID : "2",
	 	id : "442"
 	 }, { 
	 	name : "kaufen",
	 	categories_IDs : ["34", "35", "51"],
	 	related_IDs : ["1734", "408", "475"],
	 	language_ID : "2",
	 	id : "443",
	 	tags : [],
	 	formsSecondary : ["", "Ich kaufe", "Er / Sie / Es kauft", "", "Wir haben gekauft", "Ihr habt gekauft", "", "Ihr werdet kaufen", "Du wirst kaufen", "Sie werden kaufen"],
	 	formsSecondaryNames : ["Präsens", "1. Ps. Sg.", "3. Ps. Sg.", "Perfekt", "1. Ps. Pl.", "2. Ps. Pl.", "Futur I", "2. Ps. Pl.", "2. Ps. Sg.", "3. Ps. Pl."]
 	 }, { 
	 	name : "nehmen",
	 	categories_IDs : ["33", "35"],
	 	related_IDs : ["1754", "409", "476"],
	 	language_ID : "2",
	 	id : "444"
 	 }, { 
	 	name : "suchen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1760", "410", "477"],
	 	language_ID : "2",
	 	id : "445"
 	 }, { 
	 	name : "wissen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["2867", "411", "ugTZyT2iO2vq"],
	 	language_ID : "2",
	 	id : "446"
 	 }, { 
	 	name : "wollen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["2936", "412", "480", "1730"],
	 	language_ID : "2",
	 	id : "447"
 	 }, { 
	 	name : "fahren",
	 	categories_IDs : ["35", "54"],
	 	related_IDs : ["1741", "413", "479"],
	 	language_ID : "2",
	 	id : "448"
 	 }, { 
	 	name : "denken",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1739", "414", "481"],
	 	language_ID : "2",
	 	id : "449"
 	 }, { 
	 	name : "be",
	 	categories_IDs : ["35"],
	 	related_IDs : ["2840", "380", "415", "1731"],
	 	language_ID : "3",
	 	id : "452"
 	 }, { 
	 	name : "make",
	 	categories_IDs : ["35", "40"],
	 	related_IDs : ["1751", "381", "416", "zZXRu1xNRIbf", "COlaiIjNXzag"],
	 	language_ID : "3",
	 	id : "453",
	 	tags : []
 	 }, { 
	 	name : "need",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1737", "382", "417", "8NZmApTLoDwr", "m2SPLXssEXqh"],
	 	language_ID : "3",
	 	id : "454",
	 	tags : []
 	 }, { 
	 	name : "become",
	 	categories_IDs : ["35", "45", "60"],
	 	related_IDs : ["1763", "3184", "383", "418", "Q0JOzDvAItFx"],
	 	language_ID : "3",
	 	id : "455",
	 	tags : []
 	 }, { 
	 	name : "understand",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1762", "3183", "385", "420"],
	 	language_ID : "3",
	 	id : "456"
 	 }, { 
	 	name : "go",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1741", "387", "422"],
	 	language_ID : "3",
	 	id : "457"
 	 }, { 
	 	name : "like",
	 	categories_IDs : ["35"],
	 	related_IDs : ["386", "421", "1730"],
	 	language_ID : "3",
	 	id : "458"
 	 }, { 
	 	name : "have",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1742", "388", "423"],
	 	language_ID : "3",
	 	id : "459"
 	 }, { 
	 	name : "sell",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1761", "389", "424"],
	 	language_ID : "3",
	 	id : "460"
 	 }, { 
	 	name : "help",
	 	categories_IDs : ["35", "63"],
	 	related_IDs : ["1733", "392", "427", "1n6nLUTDt5my"],
	 	language_ID : "3",
	 	id : "461",
	 	tags : []
 	 }, { 
	 	name : "contain",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1735", "393", "428"],
	 	language_ID : "3",
	 	id : "462"
 	 }, { 
	 	name : "can",
	 	categories_IDs : ["33", "35", "40"],
	 	related_IDs : ["1744", "2836", "2856", "395", "430"],
	 	language_ID : "3",
	 	id : "463",
	 	tags : []
 	 }, { 
	 	name : "lie",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1750", "3182", "396", "431", "itaj5phGE7DZ"],
	 	language_ID : "3",
	 	id : "464",
	 	tags : [],
	 	formsSecondary : ["lay", "lain"],
	 	formsSecondaryNames : ["Past Tense", "Past Participle"]
 	 }, { 
	 	name : "have to",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1753", "399", "434"],
	 	language_ID : "3",
	 	id : "466"
 	 }, { 
	 	name : "eat",
	 	categories_IDs : ["35", "40", "44"],
	 	related_IDs : ["1740", "402", "437"],
	 	language_ID : "3",
	 	id : "469",
	 	tags : []
 	 }, { 
	 	name : "see",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1732", "401", "436", "icy4IVQ8XUPX"],
	 	language_ID : "3",
	 	id : "470",
	 	tags : []
 	 }, { 
	 	name : "ought to",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1756", "403", "438"],
	 	language_ID : "3",
	 	id : "471"
 	 }, { 
	 	name : "write",
	 	categories_IDs : ["35", "52", "61"],
	 	related_IDs : ["1755", "404", "439", "2964"],
	 	language_ID : "3",
	 	id : "472",
	 	tags : []
 	 }, { 
	 	name : "stand",
	 	categories_IDs : ["35"],
	 	related_IDs : ["407", "442", "1731"],
	 	language_ID : "3",
	 	id : "474"
 	 }, { 
	 	name : "buy",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1734", "2816", "408", "443"],
	 	language_ID : "3",
	 	id : "475",
	 	tags : []
 	 }, { 
	 	name : "take",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1754", "409", "444", "uD4zfeN3DfEA"],
	 	language_ID : "3",
	 	id : "476"
 	 }, { 
	 	name : "search",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1760", "410", "445"],
	 	language_ID : "3",
	 	id : "477"
 	 }, { 
	 	name : "drive",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1741", "413", "448", "2958"],
	 	language_ID : "3",
	 	id : "479",
	 	tags : []
 	 }, { 
	 	name : "want",
	 	categories_IDs : ["35"],
	 	related_IDs : ["412", "447", "1730"],
	 	language_ID : "3",
	 	id : "480"
 	 }, { 
	 	name : "think",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1739", "414", "449", "QfiYJZGxINJX"],
	 	language_ID : "3",
	 	id : "481"
 	 }, { 
	 	name : "be called",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1743", "391", "426"],
	 	language_ID : "3",
	 	id : "483"
 	 }, { 
	 	name : "be allowed",
	 	categories_IDs : ["35"],
	 	related_IDs : ["3101", "384", "419"],
	 	language_ID : "3",
	 	id : "484"
 	 }, { 
	 	name : "read",
	 	categories_IDs : ["35", "46", "55"],
	 	related_IDs : ["1567", "1606", "398"],
	 	language_ID : "3",
	 	id : "485"
 	 }, { 
	 	name : "spell",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1738", "406", "441"],
	 	language_ID : "3",
	 	id : "486"
 	 }, { 
	 	name : "geradeaus",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1770", "3012", "507", "529"],
	 	id : "487"
 	 }, { 
	 	name : "rechts",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1779", "508", "530"],
	 	id : "488"
 	 }, { 
	 	name : "links",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1774", "509", "531"],
	 	id : "489"
 	 }, { 
	 	name : "oben",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1777", "510", "532"],
	 	id : "490"
 	 }, { 
	 	name : "der Norden",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1776", "512", "534"],
	 	id : "492"
 	 }, { 
	 	name : "der Osten",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1778", "513", "535"],
	 	id : "493"
 	 }, { 
	 	name : "der Süden",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1780", "514", "536"],
	 	id : "494"
 	 }, { 
	 	name : "der Westen",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1790", "515", "537"],
	 	id : "495"
 	 }, { 
	 	name : "hinten",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1772", "538", "516"],
	 	id : "496"
 	 }, { 
	 	name : "vorne",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1788", "517", "539"],
	 	id : "497"
 	 }, { 
	 	name : "hinter",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1773", "518", "540"],
	 	id : "498"
 	 }, { 
	 	name : "bei",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["3091", "520", "542", "3187"],
	 	id : "500"
 	 }, { 
	 	name : "in der Nähe",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1775", "3187", "521", "848"],
	 	id : "501"
 	 }, { 
	 	name : "fern",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1769", "3185", "3186", "522", "849"],
	 	id : "502"
 	 }, { 
	 	name : "fast",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1768", "523", "850", "851"],
	 	id : "503"
 	 }, { 
	 	name : "hier",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1771", "524", "852"],
	 	id : "504"
 	 }, { 
	 	name : "dort",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1767", "525", "853"],
	 	id : "505"
 	 }, { 
	 	name : "weiter",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1789", "3189", "526", "854"],
	 	id : "506"
 	 }, { 
	 	name : "straight",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1770", "3012", "487", "529", "vSdF9cgCICK0"],
	 	language_ID : "3",
	 	id : "507",
	 	tags : []
 	 }, { 
	 	name : "right",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1779", "488", "530"],
	 	language_ID : "3",
	 	id : "508"
 	 }, { 
	 	name : "left",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1774", "489", "531"],
	 	language_ID : "3",
	 	id : "509"
 	 }, { 
	 	name : "up",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1777", "490", "532"],
	 	language_ID : "3",
	 	id : "510"
 	 }, { 
	 	name : "the north",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1776", "492", "534"],
	 	language_ID : "3",
	 	id : "512"
 	 }, { 
	 	name : "the east",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1778", "493", "535"],
	 	language_ID : "3",
	 	id : "513"
 	 }, { 
	 	name : "the south",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1780", "494", "536"],
	 	language_ID : "3",
	 	id : "514"
 	 }, { 
	 	name : "the west",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1790", "495", "537"],
	 	language_ID : "3",
	 	id : "515"
 	 }, { 
	 	name : "front",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1788", "497", "539"],
	 	language_ID : "3",
	 	id : "517"
 	 }, { 
	 	name : "behind",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1773", "498", "540", "S4zXcilJKrkP", "1WlJ9m1EaBRn"],
	 	language_ID : "3",
	 	id : "518",
	 	tags : []
 	 }, { 
	 	name : "at",
	 	categories_IDs : ["38"],
	 	related_IDs : ["3091", "500", "542", "3187"],
	 	language_ID : "3",
	 	id : "520"
 	 }, { 
	 	name : "close",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1775", "3187", "501", "848"],
	 	language_ID : "3",
	 	id : "521"
 	 }, { 
	 	name : "far",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1769", "3185", "3186", "502", "849"],
	 	language_ID : "3",
	 	id : "522"
 	 }, { 
	 	name : "nearly",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1768", "503", "850", "851"],
	 	language_ID : "3",
	 	id : "523"
 	 }, { 
	 	name : "here",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1771", "504", "852"],
	 	language_ID : "3",
	 	id : "524"
 	 }, { 
	 	name : "there",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1767", "505", "853"],
	 	language_ID : "3",
	 	id : "525"
 	 }, { 
	 	name : "next",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1789", "3189", "506", "854"],
	 	language_ID : "3",
	 	id : "526"
 	 }, { 
	 	name : "rakt fram",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1770", "3012", "487", "507"],
	 	language_ID : "1",
	 	id : "529"
 	 }, { 
	 	name : "höger",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1779", "488", "508"],
	 	language_ID : "1",
	 	id : "530"
 	 }, { 
	 	name : "vänster",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1774", "489", "509"],
	 	language_ID : "1",
	 	id : "531"
 	 }, { 
	 	name : "uppe",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1777", "490", "510"],
	 	language_ID : "1",
	 	id : "532"
 	 }, { 
	 	name : "en norr",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1776", "492", "512"],
	 	language_ID : "1",
	 	id : "534"
 	 }, { 
	 	name : "en öster",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1778", "493", "513"],
	 	language_ID : "1",
	 	id : "535",
	 	tags : []
 	 }, { 
	 	name : "en söder",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1780", "494", "514"],
	 	language_ID : "1",
	 	id : "536"
 	 }, { 
	 	name : "väster",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1790", "495", "515"],
	 	language_ID : "1",
	 	id : "537"
 	 }, { 
	 	name : "bak",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1772", "496", "516"],
	 	language_ID : "1",
	 	id : "538"
 	 }, { 
	 	name : "framm",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1788", "497", "517"],
	 	language_ID : "1",
	 	id : "539"
 	 }, { 
	 	name : "bakom",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1773", "498", "518"],
	 	language_ID : "1",
	 	id : "540"
 	 }, { 
	 	name : "hos",
	 	categories_IDs : ["38"],
	 	related_IDs : ["3091", "500", "520", "3187"],
	 	language_ID : "1",
	 	id : "542",
	 	tags : []
 	 }, { 
	 	name : "en basis",
	 	categories_IDs : ["36"],
	 	related_IDs : ["575", "611", "643"],
	 	language_ID : "1",
	 	id : "543"
 	 }, { 
	 	name : "en människa",
	 	categories_IDs : ["36"],
	 	related_IDs : ["577", "613", "645", "646"],
	 	language_ID : "1",
	 	id : "545"
 	 }, { 
	 	name : "en struktur",
	 	categories_IDs : ["36"],
	 	related_IDs : ["578", "614", "647"],
	 	language_ID : "1",
	 	id : "546"
 	 }, { 
	 	name : "en fras",
	 	categories_IDs : ["36"],
	 	related_IDs : ["579", "615", "648", "649"],
	 	language_ID : "1",
	 	id : "547"
 	 }, { 
	 	name : "ett verb",
	 	categories_IDs : ["36"],
	 	related_IDs : ["580", "616", "650"],
	 	language_ID : "1",
	 	id : "548"
 	 }, { 
	 	name : "ett substantiv",
	 	categories_IDs : ["36"],
	 	related_IDs : ["581", "582", "617", "651"],
	 	language_ID : "1",
	 	id : "549"
 	 }, { 
	 	name : "ett tal",
	 	categories_IDs : ["36"],
	 	related_IDs : ["583", "618", "652"],
	 	language_ID : "1",
	 	id : "550"
 	 }, { 
	 	name : "en skala",
	 	categories_IDs : ["36"],
	 	related_IDs : ["584", "619", "653"],
	 	language_ID : "1",
	 	id : "551"
 	 }, { 
	 	name : "en tid",
	 	categories_IDs : ["36", "39"],
	 	related_IDs : ["585", "620", "654", "681", "693", "857"],
	 	language_ID : "1",
	 	id : "552"
 	 }, { 
	 	name : "en mat",
	 	categories_IDs : ["36"],
	 	related_IDs : ["586", "621", "655"],
	 	language_ID : "1",
	 	id : "553"
 	 }, { 
	 	name : "en resa",
	 	categories_IDs : ["36"],
	 	related_IDs : ["587", "622", "656"],
	 	language_ID : "1",
	 	id : "554"
 	 }, { 
	 	name : "en sport",
	 	categories_IDs : ["36", "42"],
	 	related_IDs : ["566", "588", "623", "657"],
	 	language_ID : "1",
	 	id : "555"
 	 }, { 
	 	name : "en emotion",
	 	categories_IDs : ["36", "43"],
	 	related_IDs : ["589", "624", "658"],
	 	language_ID : "1",
	 	id : "556"
 	 }, { 
	 	name : "en kropp",
	 	categories_IDs : ["36", "44"],
	 	related_IDs : ["590", "625", "659"],
	 	language_ID : "1",
	 	id : "557"
 	 }, { 
	 	name : "ett utseende",
	 	categories_IDs : ["36", "45"],
	 	related_IDs : ["591", "626", "660"],
	 	language_ID : "1",
	 	id : "558"
 	 }, { 
	 	name : "ett sämhalle",
	 	categories_IDs : ["36"],
	 	related_IDs : ["592", "627", "661"],
	 	language_ID : "1",
	 	id : "559"
 	 }, { 
	 	name : "en fritid",
	 	categories_IDs : ["36", "46"],
	 	related_IDs : ["593", "628", "662"],
	 	language_ID : "1",
	 	id : "560"
 	 }, { 
	 	name : "en politik",
	 	categories_IDs : ["36", "48"],
	 	related_IDs : ["594", "629", "1857", "663"],
	 	language_ID : "1",
	 	id : "561",
	 	hasAudio : true
 	 }, { 
	 	name : "en religion",
	 	categories_IDs : ["36", "49"],
	 	related_IDs : ["595", "630", "664"],
	 	language_ID : "1",
	 	id : "562"
 	 }, { 
	 	name : "en filosofi",
	 	categories_IDs : ["36", "49"],
	 	related_IDs : ["596", "631", "665"],
	 	language_ID : "1",
	 	id : "563"
 	 }, { 
	 	name : "en miljö",
	 	categories_IDs : ["36"],
	 	related_IDs : ["598", "633", "666"],
	 	language_ID : "1",
	 	id : "565"
 	 }, { 
	 	name : "en idrott",
	 	categories_IDs : ["36", "42"],
	 	related_IDs : ["555", "588", "623", "657"],
	 	language_ID : "1",
	 	id : "566"
 	 }, { 
	 	name : "en ekonomi",
	 	categories_IDs : ["36", "51"],
	 	related_IDs : ["568", "599", "634", "667"],
	 	language_ID : "1",
	 	id : "567"
 	 }, { 
	 	name : "en hushållning",
	 	categories_IDs : ["36", "51"],
	 	related_IDs : ["567", "599", "634", "667"],
	 	language_ID : "1",
	 	id : "568"
 	 }, { 
	 	name : "en kommunikation",
	 	categories_IDs : ["36", "52"],
	 	related_IDs : ["600", "635", "668"],
	 	language_ID : "1",
	 	id : "569"
 	 }, { 
	 	name : "en konst",
	 	categories_IDs : ["36", "53"],
	 	related_IDs : ["601", "636", "669"],
	 	language_ID : "1",
	 	id : "570"
 	 }, { 
	 	name : "en trafik",
	 	categories_IDs : ["36", "54"],
	 	related_IDs : ["602", "637", "670"],
	 	language_ID : "1",
	 	id : "571"
 	 }, { 
	 	name : "en planta",
	 	categories_IDs : ["36", "56"],
	 	related_IDs : ["603", "638", "671"],
	 	language_ID : "1",
	 	id : "572"
 	 }, { 
	 	name : "ett djur",
	 	categories_IDs : ["36", "57"],
	 	related_IDs : ["604", "639", "672"],
	 	language_ID : "1",
	 	id : "573"
 	 }, { 
	 	name : "folktom",
	 	categories_IDs : ["36", "59"],
	 	related_IDs : ["605", "640", "673"],
	 	language_ID : "1",
	 	id : "574"
 	 }, { 
	 	name : "die Basis",
	 	categories_IDs : ["36"],
	 	related_IDs : ["543", "611", "643"],
	 	language_ID : "2",
	 	id : "575"
 	 }, { 
	 	name : "der Mensch",
	 	categories_IDs : ["36"],
	 	related_IDs : ["2930", "545", "613", "645", "646"],
	 	language_ID : "2",
	 	id : "577"
 	 }, { 
	 	name : "die Struktur",
	 	categories_IDs : ["36"],
	 	related_IDs : ["546", "614", "647"],
	 	language_ID : "2",
	 	id : "578"
 	 }, { 
	 	name : "die Phrase",
	 	categories_IDs : ["36"],
	 	related_IDs : ["547", "615", "648", "649"],
	 	language_ID : "2",
	 	id : "579"
 	 }, { 
	 	name : "das Verb",
	 	categories_IDs : ["36"],
	 	related_IDs : ["548", "616", "650"],
	 	language_ID : "2",
	 	id : "580"
 	 }, { 
	 	name : "das Nomen",
	 	categories_IDs : ["36"],
	 	related_IDs : ["549", "582", "617", "651"],
	 	language_ID : "2",
	 	id : "581"
 	 }, { 
	 	name : "das Substantiv",
	 	categories_IDs : ["36"],
	 	related_IDs : ["549", "581", "617", "651"],
	 	language_ID : "2",
	 	id : "582"
 	 }, { 
	 	name : "die Zahl",
	 	categories_IDs : ["36"],
	 	related_IDs : ["550", "618", "652"],
	 	language_ID : "2",
	 	id : "583"
 	 }, { 
	 	name : "die Skala",
	 	categories_IDs : ["36"],
	 	related_IDs : ["551", "619", "653"],
	 	language_ID : "2",
	 	id : "584"
 	 }, { 
	 	name : "die Zeit",
	 	categories_IDs : ["36", "39"],
	 	related_IDs : ["552", "620", "654"],
	 	language_ID : "2",
	 	id : "585"
 	 }, { 
	 	name : "das Essen",
	 	categories_IDs : ["36"],
	 	related_IDs : ["2913", "553", "621", "655"],
	 	language_ID : "2",
	 	id : "586"
 	 }, { 
	 	name : "die Reise",
	 	categories_IDs : ["36", "41"],
	 	related_IDs : ["554", "622", "656"],
	 	language_ID : "2",
	 	id : "587"
 	 }, { 
	 	name : "der Sport",
	 	categories_IDs : ["36", "42"],
	 	related_IDs : ["555", "566", "623", "657"],
	 	language_ID : "2",
	 	id : "588"
 	 }, { 
	 	name : "die Emotion",
	 	categories_IDs : ["36", "43"],
	 	related_IDs : ["556", "624", "658"],
	 	language_ID : "2",
	 	id : "589"
 	 }, { 
	 	name : "der Körper",
	 	categories_IDs : ["36", "44"],
	 	related_IDs : ["557", "625", "659"],
	 	language_ID : "2",
	 	id : "590"
 	 }, { 
	 	name : "das Aussehen",
	 	categories_IDs : ["36", "45"],
	 	related_IDs : ["558", "626", "660"],
	 	language_ID : "2",
	 	id : "591"
 	 }, { 
	 	name : "die Gesellschaft",
	 	categories_IDs : ["36"],
	 	related_IDs : ["559", "627", "661"],
	 	language_ID : "2",
	 	id : "592"
 	 }, { 
	 	name : "die Freizeit",
	 	categories_IDs : ["36", "46"],
	 	related_IDs : ["560", "628", "662"],
	 	language_ID : "2",
	 	id : "593"
 	 }, { 
	 	name : "die Politik",
	 	categories_IDs : ["36", "48"],
	 	related_IDs : ["561", "629", "1857", "663"],
	 	language_ID : "2",
	 	id : "594",
	 	hasAudio : true
 	 }, { 
	 	name : "die Religion",
	 	categories_IDs : ["36", "49"],
	 	related_IDs : ["2930", "562", "630", "664"],
	 	language_ID : "2",
	 	id : "595"
 	 }, { 
	 	name : "die Philosophie",
	 	categories_IDs : ["36", "49"],
	 	related_IDs : ["563", "631", "665"],
	 	language_ID : "2",
	 	id : "596"
 	 }, { 
	 	name : "die Umwelt",
	 	categories_IDs : ["36"],
	 	related_IDs : ["565", "633", "666"],
	 	language_ID : "2",
	 	id : "598"
 	 }, { 
	 	name : "die Wirtschaft",
	 	categories_IDs : ["36", "51"],
	 	related_IDs : ["567", "568", "634", "667"],
	 	language_ID : "2",
	 	id : "599"
 	 }, { 
	 	name : "die Kommunikation",
	 	categories_IDs : ["36", "52"],
	 	related_IDs : ["569", "635", "668"],
	 	language_ID : "2",
	 	id : "600"
 	 }, { 
	 	name : "die Kunst",
	 	categories_IDs : ["36", "53"],
	 	related_IDs : ["570", "636", "669"],
	 	language_ID : "2",
	 	id : "601"
 	 }, { 
	 	name : "der Verkehr",
	 	categories_IDs : ["36", "54"],
	 	related_IDs : ["571", "637", "670"],
	 	language_ID : "2",
	 	id : "602"
 	 }, { 
	 	name : "die Pflanze",
	 	categories_IDs : ["36", "56"],
	 	related_IDs : ["572", "638", "671"],
	 	language_ID : "2",
	 	id : "603"
 	 }, { 
	 	name : "das Tier",
	 	categories_IDs : ["36", "57"],
	 	related_IDs : ["573", "639", "672"],
	 	language_ID : "2",
	 	id : "604"
 	 }, { 
	 	name : "unbelebt",
	 	categories_IDs : ["36", "59"],
	 	related_IDs : ["574", "640", "673"],
	 	language_ID : "2",
	 	id : "605"
 	 }, { 
	 	name : "en beskriving",
	 	categories_IDs : ["36", "60"],
	 	related_IDs : ["607", "641", "674"],
	 	language_ID : "1",
	 	id : "606"
 	 }, { 
	 	name : "die Beschreibung",
	 	categories_IDs : ["36", "60"],
	 	related_IDs : ["606", "641", "674"],
	 	language_ID : "2",
	 	id : "607"
 	 }, { 
	 	name : "ett arbete",
	 	categories_IDs : ["36"],
	 	related_IDs : ["609", "610", "642", "675"],
	 	language_ID : "1",
	 	id : "608"
 	 }, { 
	 	name : "ett jobb",
	 	categories_IDs : ["36"],
	 	related_IDs : ["608", "610", "642", "675"],
	 	language_ID : "1",
	 	id : "609"
 	 }, { 
	 	name : "die Arbeit",
	 	categories_IDs : ["36"],
	 	related_IDs : ["2934", "608", "609", "642", "675"],
	 	language_ID : "2",
	 	id : "610"
 	 }, { 
	 	name : "the base",
	 	categories_IDs : ["36"],
	 	related_IDs : ["543", "575", "643"],
	 	language_ID : "3",
	 	id : "611"
 	 }, { 
	 	name : "the human",
	 	categories_IDs : ["36"],
	 	related_IDs : ["545", "577", "645", "646"],
	 	language_ID : "3",
	 	id : "613"
 	 }, { 
	 	name : "the structure",
	 	categories_IDs : ["36"],
	 	related_IDs : ["546", "578", "647"],
	 	language_ID : "3",
	 	id : "614"
 	 }, { 
	 	name : "the phrase",
	 	categories_IDs : ["36"],
	 	related_IDs : ["547", "579", "648", "649"],
	 	language_ID : "3",
	 	id : "615"
 	 }, { 
	 	name : "the verb",
	 	categories_IDs : ["36"],
	 	related_IDs : ["548", "580", "650"],
	 	language_ID : "3",
	 	id : "616"
 	 }, { 
	 	name : "the noun",
	 	categories_IDs : ["36"],
	 	related_IDs : ["549", "581", "582", "651"],
	 	language_ID : "3",
	 	id : "617"
 	 }, { 
	 	name : "the number",
	 	categories_IDs : ["36"],
	 	related_IDs : ["550", "583", "652"],
	 	language_ID : "3",
	 	id : "618"
 	 }, { 
	 	name : "the scale",
	 	categories_IDs : ["36"],
	 	related_IDs : ["551", "584", "653"],
	 	language_ID : "3",
	 	id : "619"
 	 }, { 
	 	name : "the time",
	 	categories_IDs : ["36", "39", "45", "60"],
	 	related_IDs : ["552", "585", "654", "681", "857", "nvoHYN3Whtfl"],
	 	language_ID : "3",
	 	id : "620",
	 	tags : []
 	 }, { 
	 	name : "the food",
	 	categories_IDs : ["36"],
	 	related_IDs : ["553", "586", "655", "pGhKZWyE0sDp"],
	 	language_ID : "3",
	 	id : "621",
	 	tags : []
 	 }, { 
	 	name : "the journey",
	 	categories_IDs : ["36", "41"],
	 	related_IDs : ["554", "587", "656", "W1IBRDuzyX5N"],
	 	language_ID : "3",
	 	id : "622",
	 	tags : []
 	 }, { 
	 	name : "the sport",
	 	categories_IDs : ["36", "42"],
	 	related_IDs : ["555", "566", "588", "657"],
	 	language_ID : "3",
	 	id : "623"
 	 }, { 
	 	name : "the emotion",
	 	categories_IDs : ["36", "43"],
	 	related_IDs : ["556", "589", "658"],
	 	language_ID : "3",
	 	id : "624"
 	 }, { 
	 	name : "the body",
	 	categories_IDs : ["36", "44"],
	 	related_IDs : ["557", "590", "659"],
	 	language_ID : "3",
	 	id : "625"
 	 }, { 
	 	name : "the appearance",
	 	categories_IDs : ["36", "45"],
	 	related_IDs : ["558", "591", "660"],
	 	language_ID : "3",
	 	id : "626"
 	 }, { 
	 	name : "the society",
	 	categories_IDs : ["36"],
	 	related_IDs : ["559", "592", "661"],
	 	language_ID : "3",
	 	id : "627"
 	 }, { 
	 	name : "the leisure time",
	 	categories_IDs : ["36", "46"],
	 	related_IDs : ["560", "593", "662"],
	 	language_ID : "3",
	 	id : "628"
 	 }, { 
	 	name : "the politics",
	 	categories_IDs : ["36", "48"],
	 	related_IDs : ["561", "594", "1857", "663"],
	 	language_ID : "3",
	 	id : "629"
 	 }, { 
	 	name : "the religion",
	 	categories_IDs : ["36", "49"],
	 	related_IDs : ["562", "595", "664"],
	 	language_ID : "3",
	 	id : "630"
 	 }, { 
	 	name : "the philosophy",
	 	categories_IDs : ["36", "49"],
	 	related_IDs : ["563", "596", "665"],
	 	language_ID : "3",
	 	id : "631"
 	 }, { 
	 	name : "the environment",
	 	categories_IDs : ["36"],
	 	related_IDs : ["565", "598", "666"],
	 	language_ID : "3",
	 	id : "633"
 	 }, { 
	 	name : "the economy",
	 	categories_IDs : ["36", "51"],
	 	related_IDs : ["567", "568", "599", "667"],
	 	language_ID : "3",
	 	id : "634"
 	 }, { 
	 	name : "the communication",
	 	categories_IDs : ["36", "52"],
	 	related_IDs : ["569", "600", "668"],
	 	language_ID : "3",
	 	id : "635"
 	 }, { 
	 	name : "the art",
	 	categories_IDs : ["36", "53"],
	 	related_IDs : ["570", "601", "669"],
	 	language_ID : "3",
	 	id : "636"
 	 }, { 
	 	name : "the traffic",
	 	categories_IDs : ["36", "54"],
	 	related_IDs : ["571", "602", "670"],
	 	language_ID : "3",
	 	id : "637"
 	 }, { 
	 	name : "the plant",
	 	categories_IDs : ["36", "56"],
	 	related_IDs : ["572", "603", "671"],
	 	language_ID : "3",
	 	id : "638"
 	 }, { 
	 	name : "the animal",
	 	categories_IDs : ["36", "57"],
	 	related_IDs : ["573", "604", "672"],
	 	language_ID : "3",
	 	id : "639"
 	 }, { 
	 	name : "inanimate",
	 	categories_IDs : ["36", "49", "59"],
	 	related_IDs : ["574", "605", "673"],
	 	language_ID : "3",
	 	id : "640"
 	 }, { 
	 	name : "the description",
	 	categories_IDs : ["36", "60"],
	 	related_IDs : ["606", "607", "674"],
	 	language_ID : "3",
	 	id : "641"
 	 }, { 
	 	name : "the work",
	 	categories_IDs : ["36"],
	 	related_IDs : ["608", "609", "610", "675"],
	 	language_ID : "3",
	 	id : "642"
 	 }, { 
	 	name : "la base",
	 	categories_IDs : ["36"],
	 	related_IDs : ["543", "575", "611"],
	 	language_ID : "5",
	 	id : "643"
 	 }, { 
	 	name : "el ser humano",
	 	categories_IDs : ["36"],
	 	related_IDs : ["545", "577", "613", "646"],
	 	language_ID : "5",
	 	id : "645"
 	 }, { 
	 	name : "el hombre",
	 	categories_IDs : ["36"],
	 	related_IDs : ["545", "577", "613", "645"],
	 	language_ID : "5",
	 	id : "646"
 	 }, { 
	 	name : "la estructura",
	 	categories_IDs : ["36"],
	 	related_IDs : ["546", "578", "614"],
	 	language_ID : "5",
	 	id : "647"
 	 }, { 
	 	name : "el cliché",
	 	categories_IDs : ["36"],
	 	related_IDs : ["547", "579", "615", "649"],
	 	language_ID : "5",
	 	id : "648"
 	 }, { 
	 	name : "el tópico",
	 	categories_IDs : ["36"],
	 	related_IDs : ["547", "579", "615", "648"],
	 	language_ID : "5",
	 	id : "649"
 	 }, { 
	 	name : "el verbo",
	 	categories_IDs : ["36"],
	 	related_IDs : ["548", "580", "616"],
	 	language_ID : "5",
	 	id : "650"
 	 }, { 
	 	name : "el sustantivo",
	 	categories_IDs : ["36"],
	 	related_IDs : ["549", "581", "582", "617"],
	 	language_ID : "5",
	 	id : "651"
 	 }, { 
	 	name : "el número",
	 	categories_IDs : ["36"],
	 	related_IDs : ["550", "583", "618"],
	 	language_ID : "5",
	 	id : "652"
 	 }, { 
	 	name : "la escala",
	 	categories_IDs : ["36"],
	 	related_IDs : ["551", "584", "619"],
	 	language_ID : "5",
	 	id : "653"
 	 }, { 
	 	name : "el tiempo",
	 	categories_IDs : ["36", "39"],
	 	related_IDs : ["552", "585", "620", "681", "857"],
	 	language_ID : "5",
	 	id : "654"
 	 }, { 
	 	name : "la comida",
	 	categories_IDs : ["36"],
	 	related_IDs : ["553", "586", "621"],
	 	language_ID : "5",
	 	id : "655"
 	 }, { 
	 	name : "el viaje",
	 	categories_IDs : ["36"],
	 	related_IDs : ["554", "587", "622"],
	 	language_ID : "5",
	 	id : "656"
 	 }, { 
	 	name : "el deporte",
	 	categories_IDs : ["36", "42"],
	 	related_IDs : ["555", "566", "588", "623"],
	 	language_ID : "5",
	 	id : "657"
 	 }, { 
	 	name : "la emoción",
	 	categories_IDs : ["36", "43"],
	 	related_IDs : ["556", "589", "624"],
	 	language_ID : "5",
	 	id : "658"
 	 }, { 
	 	name : "el cuerpo",
	 	categories_IDs : ["36", "44"],
	 	related_IDs : ["557", "590", "625"],
	 	language_ID : "5",
	 	id : "659"
 	 }, { 
	 	name : "el aspecto",
	 	categories_IDs : ["36", "45"],
	 	related_IDs : ["558", "591", "626"],
	 	language_ID : "5",
	 	id : "660"
 	 }, { 
	 	name : "la sociedad",
	 	categories_IDs : ["36"],
	 	related_IDs : ["559", "592", "627"],
	 	language_ID : "5",
	 	id : "661"
 	 }, { 
	 	name : "el tiempo libre",
	 	categories_IDs : ["36", "46"],
	 	related_IDs : ["560", "593", "628"],
	 	language_ID : "5",
	 	id : "662"
 	 }, { 
	 	name : "la religión",
	 	categories_IDs : ["36", "49"],
	 	related_IDs : ["562", "595", "630"],
	 	language_ID : "5",
	 	id : "664"
 	 }, { 
	 	name : "la filosofía",
	 	categories_IDs : ["36", "49"],
	 	related_IDs : ["563", "596", "631"],
	 	language_ID : "5",
	 	id : "665"
 	 }, { 
	 	name : "el medio ambiente",
	 	categories_IDs : ["36"],
	 	related_IDs : ["565", "598", "633"],
	 	language_ID : "5",
	 	id : "666"
 	 }, { 
	 	name : "la economía",
	 	categories_IDs : ["36", "51"],
	 	related_IDs : ["567", "568", "599", "634"],
	 	language_ID : "5",
	 	id : "667"
 	 }, { 
	 	name : "la comunicación",
	 	categories_IDs : ["36", "52"],
	 	related_IDs : ["569", "600", "635"],
	 	language_ID : "5",
	 	id : "668"
 	 }, { 
	 	name : "el arte",
	 	categories_IDs : ["36", "53"],
	 	related_IDs : ["570", "601", "636"],
	 	language_ID : "5",
	 	id : "669"
 	 }, { 
	 	name : "el tráfico",
	 	categories_IDs : ["36", "54"],
	 	related_IDs : ["571", "602", "637"],
	 	language_ID : "5",
	 	id : "670"
 	 }, { 
	 	name : "la planta",
	 	categories_IDs : ["36", "56"],
	 	related_IDs : ["572", "603", "638"],
	 	language_ID : "5",
	 	id : "671"
 	 }, { 
	 	name : "el animal",
	 	categories_IDs : ["36", "57"],
	 	related_IDs : ["573", "604", "639"],
	 	language_ID : "5",
	 	id : "672"
 	 }, { 
	 	name : "inanimado, inanimada",
	 	categories_IDs : ["36", "49", "59"],
	 	related_IDs : ["574", "605", "640"],
	 	language_ID : "5",
	 	id : "673"
 	 }, { 
	 	name : "la descripción",
	 	categories_IDs : ["36", "60"],
	 	related_IDs : ["606", "607", "641"],
	 	language_ID : "5",
	 	id : "674"
 	 }, { 
	 	name : "el trabajo",
	 	categories_IDs : ["36"],
	 	related_IDs : ["608", "609", "610", "642"],
	 	language_ID : "5",
	 	id : "675"
 	 }, { 
	 	name : "la orientación",
	 	categories_IDs : ["36", "38"],
	 	related_IDs : ["2860", "677"],
	 	language_ID : "5",
	 	id : "676"
 	 }, { 
	 	name : "die Orientierung",
	 	categories_IDs : ["36", "38"],
	 	related_IDs : ["2860", "676"],
	 	language_ID : "2",
	 	id : "677"
 	 }, { 
	 	name : "das Lernen",
	 	categories_IDs : ["36", "55"],
	 	related_IDs : ["2620"],
	 	language_ID : "2",
	 	id : "678"
 	 }, { 
	 	name : "die Uhr",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["734", "791", "856"],
	 	id : "680"
 	 }, { 
	 	name : "die Uhrzeit",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["552", "620", "654", "857"],
	 	id : "681"
 	 }, { 
	 	name : "wie lange",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["736", "792", "858"],
	 	id : "682"
 	 }, { 
	 	name : "bis wann",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["737", "793", "860"],
	 	id : "683"
 	 }, { 
	 	name : "bis",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["739", "795", "859"],
	 	id : "685"
 	 }, { 
	 	name : "heute",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["740", "796", "862"],
	 	id : "686"
 	 }, { 
	 	name : "gestern",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["741", "797", "863"],
	 	id : "687"
 	 }, { 
	 	name : "vorgestern",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["742", "798", "864"],
	 	id : "688"
 	 }, { 
	 	name : "morgen",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["2913", "743", "799", "865"],
	 	id : "689"
 	 }, { 
	 	name : "übermorgen",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["744", "800", "866"],
	 	id : "690"
 	 }, { 
	 	name : "die Woche",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["2941", "2950", "745", "801", "867", "61PXgQhVZZop"],
	 	id : "691"
 	 }, { 
	 	name : "der Tag",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["746", "802", "868"],
	 	id : "692"
 	 }, { 
	 	name : "die Stunde",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["552", "747", "803", "857"],
	 	id : "693"
 	 }, { 
	 	name : "die Minute",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["2939", "748", "804", "869"],
	 	id : "694"
 	 }, { 
	 	name : "die Sekunde",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["749", "805", "870"],
	 	id : "695"
 	 }, { 
	 	name : "das Jahr",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["750", "806", "871"],
	 	id : "696"
 	 }, { 
	 	name : "der Monat",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["751", "807", "872"],
	 	id : "697"
 	 }, { 
	 	name : "das Jahrzehnt",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["752", "808", "873"],
	 	id : "698"
 	 }, { 
	 	name : "das Jahrhundert",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["753", "809", "874"],
	 	id : "699"
 	 }, { 
	 	name : "das Jahrtausend",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["754", "810", "875"],
	 	id : "700"
 	 }, { 
	 	name : "die Nacht",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["755", "811", "876"],
	 	id : "701"
 	 }, { 
	 	name : "der Morgen",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["706", "756", "789", "812", "817", "877"],
	 	id : "702"
 	 }, { 
	 	name : "der Mittag",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["2881", "757", "813", "814", "878"],
	 	id : "703"
 	 }, { 
	 	name : "der Abend",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["705", "758", "815", "879"],
	 	id : "704"
 	 }, { 
	 	name : "der Nachmittag",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["2899", "704", "758", "759", "815", "816", "879"],
	 	id : "705"
 	 }, { 
	 	name : "der Vormittag",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["702", "756", "789", "817", "877"],
	 	id : "706"
 	 }, { 
	 	name : "der 14. Mai",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["761", "818", "885"],
	 	id : "707"
 	 }, { 
	 	name : "der Montag",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["762", "819", "886"],
	 	id : "708"
 	 }, { 
	 	name : "der Dienstag",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["2851", "2936", "763", "820", "887"],
	 	id : "709"
 	 }, { 
	 	name : "der Mittwoch",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["764", "821", "888"],
	 	id : "710"
 	 }, { 
	 	name : "der Donnerstag",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["765", "822", "889"],
	 	id : "711"
 	 }, { 
	 	name : "der Freitag",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["766", "823", "890"],
	 	id : "712"
 	 }, { 
	 	name : "der Samstag",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["767", "824", "891"],
	 	id : "713"
 	 }, { 
	 	name : "der Sonntag",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["768", "825", "892"],
	 	id : "714"
 	 }, { 
	 	name : "der Januar",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["769", "826", "893"],
	 	id : "715"
 	 }, { 
	 	name : "der Februar",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["770", "827", "894"],
	 	id : "716"
 	 }, { 
	 	name : "der März",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["771", "828", "895"],
	 	id : "717"
 	 }, { 
	 	name : "der April",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["772", "829", "896"],
	 	id : "718"
 	 }, { 
	 	name : "der Mai",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["773", "830", "897"],
	 	id : "719"
 	 }, { 
	 	name : "der Juni",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["774", "831", "898"],
	 	id : "720"
 	 }, { 
	 	name : "der Juli",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["775", "832", "899"],
	 	id : "721"
 	 }, { 
	 	name : "der August",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["776", "833", "900"],
	 	id : "722"
 	 }, { 
	 	name : "der September",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["777", "834", "901"],
	 	id : "723"
 	 }, { 
	 	name : "der Oktober",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["778", "835", "902"],
	 	id : "724"
 	 }, { 
	 	name : "der Dezember",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["779", "838", "904"],
	 	id : "725"
 	 }, { 
	 	name : "das Silvester",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["780", "839", "905"],
	 	id : "726"
 	 }, { 
	 	name : "das Ostern",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["781", "840", "906"],
	 	id : "727"
 	 }, { 
	 	name : "das Weihnachten",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["782", "841", "907"],
	 	id : "728"
 	 }, { 
	 	name : "das Neujahr",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["783", "842", "908"],
	 	id : "729"
 	 }, { 
	 	name : "der Frühling",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["784", "843", "909"],
	 	id : "730"
 	 }, { 
	 	name : "der Sommer",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["785", "844", "910"],
	 	id : "731"
 	 }, { 
	 	name : "der Herbst",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["786", "845", "911"],
	 	id : "732"
 	 }, { 
	 	name : "der Winter",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	related_IDs : ["787", "846", "912"],
	 	id : "733"
 	 }, { 
	 	name : "the clock",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["680", "791", "856"],
	 	id : "734"
 	 }, { 
	 	name : "how long",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["682", "792", "858"],
	 	id : "736"
 	 }, { 
	 	name : "till when",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["683", "793", "860"],
	 	id : "737"
 	 }, { 
	 	name : "until",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["685", "795", "859"],
	 	id : "739"
 	 }, { 
	 	name : "today",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["686", "796", "862"],
	 	id : "740"
 	 }, { 
	 	name : "yesterday",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["2816", "687", "797", "863"],
	 	id : "741"
 	 }, { 
	 	name : "the day before yesterday",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["688", "798", "864"],
	 	id : "742"
 	 }, { 
	 	name : "the tomorrow",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["689", "799", "865"],
	 	id : "743"
 	 }, { 
	 	name : "the day after tomorrow",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["690", "800", "866"],
	 	id : "744"
 	 }, { 
	 	name : "the week",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["691", "801", "867"],
	 	id : "745"
 	 }, { 
	 	name : "the day",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["692", "802", "868"],
	 	id : "746"
 	 }, { 
	 	name : "the hour",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["693", "803", "857"],
	 	id : "747"
 	 }, { 
	 	name : "the minute",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["694", "804", "869"],
	 	id : "748"
 	 }, { 
	 	name : "the second",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["695", "805", "870"],
	 	id : "749"
 	 }, { 
	 	name : "the year",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["696", "806", "871"],
	 	id : "750"
 	 }, { 
	 	name : "the month",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["697", "807", "872"],
	 	id : "751"
 	 }, { 
	 	name : "the decade",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["698", "808", "873"],
	 	id : "752"
 	 }, { 
	 	name : "the century",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["699", "809", "874"],
	 	id : "753"
 	 }, { 
	 	name : "the millenium",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["700", "810", "875"],
	 	id : "754"
 	 }, { 
	 	name : "the night",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["701", "811", "876"],
	 	id : "755"
 	 }, { 
	 	name : "the morning",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["2824", "702", "706", "789", "812", "817", "877"],
	 	id : "756"
 	 }, { 
	 	name : "the noon",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["703", "813", "814", "878"],
	 	id : "757"
 	 }, { 
	 	name : "the evening",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["704", "705", "815", "879"],
	 	id : "758"
 	 }, { 
	 	name : "the afternoon",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["705", "816", "879"],
	 	id : "759"
 	 }, { 
	 	name : "the 14th of May",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["707", "818", "885"],
	 	id : "761"
 	 }, { 
	 	name : "the Monday",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["708", "819", "886"],
	 	id : "762"
 	 }, { 
	 	name : "the Tuesday",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["2852", "709", "820", "887"],
	 	id : "763"
 	 }, { 
	 	name : "the Wednesday",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["710", "821", "888"],
	 	id : "764"
 	 }, { 
	 	name : "the Thursday",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["711", "822", "889"],
	 	id : "765"
 	 }, { 
	 	name : "the Friday",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["712", "823", "890"],
	 	id : "766"
 	 }, { 
	 	name : "the Saturday",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["713", "824", "891"],
	 	id : "767"
 	 }, { 
	 	name : "the Sunday",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["714", "825", "892"],
	 	id : "768"
 	 }, { 
	 	name : "the January",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["715", "826", "893"],
	 	id : "769"
 	 }, { 
	 	name : "the February",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["716", "827", "894"],
	 	id : "770"
 	 }, { 
	 	name : "the March",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["717", "828", "895"],
	 	id : "771"
 	 }, { 
	 	name : "the April",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["718", "829", "896"],
	 	id : "772"
 	 }, { 
	 	name : "the May",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["719", "830", "897"],
	 	id : "773"
 	 }, { 
	 	name : "the June",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["720", "831", "898"],
	 	id : "774"
 	 }, { 
	 	name : "the July",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["721", "832", "899"],
	 	id : "775"
 	 }, { 
	 	name : "the August",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["722", "833", "900"],
	 	id : "776"
 	 }, { 
	 	name : "the September",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["723", "834", "901"],
	 	id : "777"
 	 }, { 
	 	name : "the October",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["724", "835", "902"],
	 	id : "778"
 	 }, { 
	 	name : "the December",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["725", "838", "904"],
	 	id : "779"
 	 }, { 
	 	name : "the New Year's Eve",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["726", "839", "905"],
	 	id : "780"
 	 }, { 
	 	name : "the Easter",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["727", "840", "906"],
	 	id : "781"
 	 }, { 
	 	name : "the Christmas",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["728", "841", "907"],
	 	id : "782"
 	 }, { 
	 	name : "the New Year's Day",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["729", "842", "908"],
	 	id : "783"
 	 }, { 
	 	name : "the spring",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["730", "843", "909"],
	 	id : "784"
 	 }, { 
	 	name : "the summer",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["731", "844", "910", "lYM6rEKlMF00"],
	 	id : "785",
	 	tags : []
 	 }, { 
	 	name : "the autumn",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["732", "845", "911"],
	 	id : "786"
 	 }, { 
	 	name : "the winter",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["733", "846", "912"],
	 	id : "787"
 	 }, { 
	 	name : "the fortnight",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["790"],
	 	id : "788"
 	 }, { 
	 	name : "the forenoon",
	 	language_ID : "3",
	 	categories_IDs : ["39"],
	 	related_IDs : ["702", "706", "756", "817", "877"],
	 	id : "789"
 	 }, { 
	 	categories_IDs : ["39"],
	 	related_IDs : ["216", "788"],
	 	language_ID : "2",
	 	name : "die Vierzehn Tage",
	 	id : "790"
 	 }, { 
	 	name : "en klocka",
	 	categories_IDs : ["39"],
	 	related_IDs : ["680", "734", "856"],
	 	language_ID : "1",
	 	id : "791"
 	 }, { 
	 	name : "hur lång",
	 	categories_IDs : ["39"],
	 	related_IDs : ["682", "736", "858"],
	 	language_ID : "1",
	 	id : "792"
 	 }, { 
	 	name : "hur länge",
	 	categories_IDs : ["39"],
	 	related_IDs : ["683", "737", "860"],
	 	language_ID : "1",
	 	id : "793"
 	 }, { 
	 	name : "före",
	 	categories_IDs : ["39"],
	 	related_IDs : ["861", "499", "519", "738"],
	 	language_ID : "1",
	 	id : "794"
 	 }, { 
	 	name : "till",
	 	categories_IDs : ["39"],
	 	related_IDs : ["685", "739", "859"],
	 	language_ID : "1",
	 	id : "795"
 	 }, { 
	 	name : "i dag",
	 	categories_IDs : ["39"],
	 	related_IDs : ["686", "740", "862"],
	 	language_ID : "1",
	 	id : "796"
 	 }, { 
	 	name : "i går",
	 	categories_IDs : ["39"],
	 	related_IDs : ["687", "741", "863"],
	 	language_ID : "1",
	 	id : "797"
 	 }, { 
	 	name : "i förrgår",
	 	categories_IDs : ["39"],
	 	related_IDs : ["688", "742", "864"],
	 	language_ID : "1",
	 	id : "798"
 	 }, { 
	 	name : "i morgon",
	 	categories_IDs : ["39"],
	 	related_IDs : ["689", "743", "865"],
	 	language_ID : "1",
	 	id : "799"
 	 }, { 
	 	name : "i övermorgon",
	 	categories_IDs : ["39"],
	 	related_IDs : ["690", "744", "866"],
	 	language_ID : "1",
	 	id : "800"
 	 }, { 
	 	name : "en vecka",
	 	categories_IDs : ["39"],
	 	related_IDs : ["691", "745", "867"],
	 	language_ID : "1",
	 	id : "801"
 	 }, { 
	 	name : "en dag",
	 	categories_IDs : ["39"],
	 	related_IDs : ["692", "746", "868"],
	 	language_ID : "1",
	 	id : "802"
 	 }, { 
	 	name : "en timme",
	 	categories_IDs : ["39"],
	 	related_IDs : ["693", "747", "857"],
	 	language_ID : "1",
	 	id : "803"
 	 }, { 
	 	name : "en minut",
	 	categories_IDs : ["39"],
	 	related_IDs : ["694", "748", "869"],
	 	language_ID : "1",
	 	id : "804"
 	 }, { 
	 	name : "en sekund",
	 	categories_IDs : ["39"],
	 	related_IDs : ["695", "749", "870"],
	 	language_ID : "1",
	 	id : "805"
 	 }, { 
	 	name : "ett år",
	 	categories_IDs : ["39"],
	 	related_IDs : ["696", "750", "871"],
	 	language_ID : "1",
	 	id : "806"
 	 }, { 
	 	name : "en månad",
	 	categories_IDs : ["39"],
	 	related_IDs : ["697", "751", "872"],
	 	language_ID : "1",
	 	id : "807"
 	 }, { 
	 	name : "ett årtionde",
	 	categories_IDs : ["39"],
	 	related_IDs : ["698", "752", "873"],
	 	language_ID : "1",
	 	id : "808"
 	 }, { 
	 	name : "ett århundrade",
	 	categories_IDs : ["39"],
	 	related_IDs : ["699", "753", "874"],
	 	language_ID : "1",
	 	id : "809"
 	 }, { 
	 	name : "ett årtusende",
	 	categories_IDs : ["39"],
	 	related_IDs : ["700", "754", "875"],
	 	language_ID : "1",
	 	id : "810"
 	 }, { 
	 	name : "en natt",
	 	categories_IDs : ["39"],
	 	related_IDs : ["701", "755", "876"],
	 	language_ID : "1",
	 	id : "811"
 	 }, { 
	 	name : "en morgon",
	 	categories_IDs : ["39"],
	 	related_IDs : ["2826", "702", "756", "877"],
	 	language_ID : "1",
	 	id : "812"
 	 }, { 
	 	name : "en lunchtid",
	 	categories_IDs : ["39"],
	 	related_IDs : ["703", "757", "814", "878"],
	 	language_ID : "1",
	 	id : "813"
 	 }, { 
	 	name : "en middagstid",
	 	categories_IDs : ["39"],
	 	related_IDs : ["703", "757", "813", "878"],
	 	language_ID : "1",
	 	id : "814"
 	 }, { 
	 	name : "en kväll",
	 	categories_IDs : ["39"],
	 	related_IDs : ["704", "705", "758", "879"],
	 	language_ID : "1",
	 	id : "815"
 	 }, { 
	 	name : "en eftermiddag",
	 	categories_IDs : ["39"],
	 	related_IDs : ["705", "759", "879"],
	 	language_ID : "1",
	 	id : "816"
 	 }, { 
	 	name : "en förmiddag",
	 	categories_IDs : ["39"],
	 	related_IDs : ["702", "706", "756", "789", "877"],
	 	language_ID : "1",
	 	id : "817"
 	 }, { 
	 	name : "fjortonde maj",
	 	categories_IDs : ["39"],
	 	related_IDs : ["707", "761", "885"],
	 	language_ID : "1",
	 	id : "818"
 	 }, { 
	 	name : "en måndag",
	 	categories_IDs : ["39"],
	 	related_IDs : ["708", "762", "886"],
	 	language_ID : "1",
	 	id : "819"
 	 }, { 
	 	name : "en tisdag",
	 	categories_IDs : ["39"],
	 	related_IDs : ["2854", "709", "763", "887"],
	 	language_ID : "1",
	 	id : "820"
 	 }, { 
	 	name : "en onsdag",
	 	categories_IDs : ["39"],
	 	related_IDs : ["710", "764", "888"],
	 	language_ID : "1",
	 	id : "821"
 	 }, { 
	 	name : "en torsdag",
	 	categories_IDs : ["39"],
	 	related_IDs : ["711", "765", "889"],
	 	language_ID : "1",
	 	id : "822"
 	 }, { 
	 	name : "en fredag",
	 	categories_IDs : ["39"],
	 	related_IDs : ["712", "766", "890"],
	 	language_ID : "1",
	 	id : "823"
 	 }, { 
	 	name : "en lördag",
	 	categories_IDs : ["39"],
	 	related_IDs : ["713", "767", "891"],
	 	language_ID : "1",
	 	id : "824"
 	 }, { 
	 	name : "en söndag",
	 	categories_IDs : ["39"],
	 	related_IDs : ["714", "768", "892"],
	 	language_ID : "1",
	 	id : "825"
 	 }, { 
	 	name : "en januari",
	 	categories_IDs : ["39"],
	 	related_IDs : ["715", "769", "893"],
	 	language_ID : "1",
	 	id : "826"
 	 }, { 
	 	name : "en februari",
	 	categories_IDs : ["39"],
	 	related_IDs : ["716", "770", "894"],
	 	language_ID : "1",
	 	id : "827"
 	 }, { 
	 	name : "en mars",
	 	categories_IDs : ["39"],
	 	related_IDs : ["717", "771", "895"],
	 	language_ID : "1",
	 	id : "828"
 	 }, { 
	 	name : "en april",
	 	categories_IDs : ["39"],
	 	related_IDs : ["718", "772", "896"],
	 	language_ID : "1",
	 	id : "829"
 	 }, { 
	 	name : "en maj",
	 	categories_IDs : ["39"],
	 	related_IDs : ["719", "773", "897"],
	 	language_ID : "1",
	 	id : "830"
 	 }, { 
	 	name : "en juni",
	 	categories_IDs : ["39"],
	 	related_IDs : ["720", "774", "898"],
	 	language_ID : "1",
	 	id : "831"
 	 }, { 
	 	name : "en juli",
	 	categories_IDs : ["39"],
	 	related_IDs : ["721", "775", "899"],
	 	language_ID : "1",
	 	id : "832"
 	 }, { 
	 	name : "en augusti",
	 	categories_IDs : ["39"],
	 	related_IDs : ["722", "776", "900"],
	 	language_ID : "1",
	 	id : "833"
 	 }, { 
	 	name : "en september",
	 	categories_IDs : ["39"],
	 	related_IDs : ["723", "777", "901"],
	 	language_ID : "1",
	 	id : "834"
 	 }, { 
	 	name : "en oktober",
	 	categories_IDs : ["39"],
	 	related_IDs : ["724", "778", "902"],
	 	language_ID : "1",
	 	id : "835"
 	 }, { 
	 	name : "en november",
	 	categories_IDs : ["39"],
	 	related_IDs : ["837", "847", "903"],
	 	language_ID : "1",
	 	id : "836"
 	 }, { 
	 	name : "der November",
	 	categories_IDs : ["39"],
	 	related_IDs : ["836", "847", "903"],
	 	language_ID : "2",
	 	id : "837"
 	 }, { 
	 	name : "en december",
	 	categories_IDs : ["39"],
	 	related_IDs : ["725", "779", "904"],
	 	language_ID : "1",
	 	id : "838"
 	 }, { 
	 	name : "en nyårsafton",
	 	categories_IDs : ["39"],
	 	related_IDs : ["726", "780", "905"],
	 	language_ID : "1",
	 	id : "839"
 	 }, { 
	 	name : "en påsk",
	 	categories_IDs : ["39"],
	 	related_IDs : ["727", "781", "906"],
	 	language_ID : "1",
	 	id : "840"
 	 }, { 
	 	name : "en jul",
	 	categories_IDs : ["39"],
	 	related_IDs : ["728", "782", "907"],
	 	language_ID : "1",
	 	id : "841"
 	 }, { 
	 	name : "en Nyårsdag",
	 	categories_IDs : ["39"],
	 	related_IDs : ["729", "783", "908"],
	 	language_ID : "1",
	 	id : "842"
 	 }, { 
	 	name : "en vår",
	 	categories_IDs : ["39"],
	 	related_IDs : ["730", "784", "909"],
	 	language_ID : "1",
	 	id : "843"
 	 }, { 
	 	name : "en sommar",
	 	categories_IDs : ["39"],
	 	related_IDs : ["731", "785", "910"],
	 	language_ID : "1",
	 	id : "844"
 	 }, { 
	 	name : "en höst",
	 	categories_IDs : ["39"],
	 	related_IDs : ["732", "786", "911"],
	 	language_ID : "1",
	 	id : "845"
 	 }, { 
	 	name : "en vinter",
	 	categories_IDs : ["39"],
	 	related_IDs : ["733", "787", "912"],
	 	language_ID : "1",
	 	id : "846"
 	 }, { 
	 	categories_IDs : ["39"],
	 	related_IDs : ["836", "837", "903"],
	 	language_ID : "3",
	 	name : "the November",
	 	id : "847"
 	 }, { 
	 	name : "i närheten",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1775", "3187", "501", "521"],
	 	language_ID : "1",
	 	id : "848"
 	 }, { 
	 	name : "avlägsen",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1769", "3185", "3186", "502", "522"],
	 	language_ID : "1",
	 	id : "849"
 	 }, { 
	 	name : "narapå",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1768", "503", "523", "851"],
	 	language_ID : "1",
	 	id : "850"
 	 }, { 
	 	name : "nästan",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1768", "503", "523", "850"],
	 	language_ID : "1",
	 	id : "851"
 	 }, { 
	 	name : "här",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1771", "504", "524"],
	 	language_ID : "1",
	 	id : "852"
 	 }, { 
	 	name : "där",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1767", "505", "525"],
	 	language_ID : "1",
	 	id : "853"
 	 }, { 
	 	name : "vidare",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1789", "3189", "506", "526"],
	 	language_ID : "1",
	 	id : "854"
 	 }, { 
	 	name : "el reloj",
	 	categories_IDs : ["39"],
	 	related_IDs : ["680", "734", "791"],
	 	language_ID : "5",
	 	id : "856"
 	 }, { 
	 	name : "la hora",
	 	categories_IDs : ["39"],
	 	related_IDs : ["552", "620", "654", "681", "693", "747", "803"],
	 	language_ID : "5",
	 	id : "857"
 	 }, { 
	 	name : "cuánto tiempo",
	 	categories_IDs : ["39"],
	 	related_IDs : ["682", "736", "792"],
	 	language_ID : "5",
	 	id : "858"
 	 }, { 
	 	name : "hasta",
	 	categories_IDs : ["39"],
	 	related_IDs : ["685", "739", "795"],
	 	language_ID : "5",
	 	id : "859"
 	 }, { 
	 	name : "hasta cuándo",
	 	categories_IDs : ["39"],
	 	related_IDs : ["683", "737", "793"],
	 	language_ID : "5",
	 	id : "860"
 	 }, { 
	 	name : "menos",
	 	categories_IDs : ["39"],
	 	related_IDs : ["794", "499", "519", "738"],
	 	language_ID : "5",
	 	id : "861"
 	 }, { 
	 	name : "hoy",
	 	categories_IDs : ["39"],
	 	related_IDs : ["686", "740", "796"],
	 	language_ID : "5",
	 	id : "862"
 	 }, { 
	 	name : "ayer",
	 	categories_IDs : ["39"],
	 	related_IDs : ["2817", "687", "741", "797"],
	 	language_ID : "5",
	 	id : "863"
 	 }, { 
	 	name : "anteayer",
	 	categories_IDs : ["39"],
	 	related_IDs : ["688", "742", "798"],
	 	language_ID : "5",
	 	id : "864"
 	 }, { 
	 	name : "mañana",
	 	categories_IDs : ["39"],
	 	related_IDs : ["689", "743", "799"],
	 	language_ID : "5",
	 	id : "865"
 	 }, { 
	 	name : "pasado mañana",
	 	categories_IDs : ["39"],
	 	related_IDs : ["690", "744", "800"],
	 	language_ID : "5",
	 	id : "866"
 	 }, { 
	 	name : "la semana",
	 	categories_IDs : ["39"],
	 	related_IDs : ["691", "745", "801"],
	 	language_ID : "5",
	 	id : "867"
 	 }, { 
	 	name : "el día",
	 	categories_IDs : ["39"],
	 	related_IDs : ["692", "746", "802"],
	 	language_ID : "5",
	 	id : "868"
 	 }, { 
	 	name : "el minuto",
	 	categories_IDs : ["39"],
	 	related_IDs : ["694", "748", "804"],
	 	language_ID : "5",
	 	id : "869"
 	 }, { 
	 	name : "el segundo",
	 	categories_IDs : ["39"],
	 	related_IDs : ["695", "749", "805"],
	 	language_ID : "5",
	 	id : "870"
 	 }, { 
	 	name : "el año",
	 	categories_IDs : ["39"],
	 	related_IDs : ["696", "750", "806"],
	 	language_ID : "5",
	 	id : "871"
 	 }, { 
	 	name : "el mes",
	 	categories_IDs : ["39"],
	 	related_IDs : ["697", "751", "807"],
	 	language_ID : "5",
	 	id : "872"
 	 }, { 
	 	name : "la década",
	 	categories_IDs : ["39"],
	 	related_IDs : ["698", "752", "808"],
	 	language_ID : "5",
	 	id : "873"
 	 }, { 
	 	name : "el siglo",
	 	categories_IDs : ["39"],
	 	related_IDs : ["699", "753", "809"],
	 	language_ID : "5",
	 	id : "874"
 	 }, { 
	 	name : "el milenio",
	 	categories_IDs : ["39"],
	 	related_IDs : ["700", "754", "810"],
	 	language_ID : "5",
	 	id : "875"
 	 }, { 
	 	name : "la noche",
	 	categories_IDs : ["39"],
	 	related_IDs : ["701", "755", "811"],
	 	language_ID : "5",
	 	id : "876"
 	 }, { 
	 	name : "la mañana",
	 	categories_IDs : ["39"],
	 	related_IDs : ["2825", "702", "706", "756", "789", "812", "817"],
	 	language_ID : "5",
	 	id : "877"
 	 }, { 
	 	name : "el mediodía",
	 	categories_IDs : ["39"],
	 	related_IDs : ["703", "757", "813", "814"],
	 	language_ID : "5",
	 	id : "878"
 	 }, { 
	 	name : "la tarde",
	 	categories_IDs : ["39"],
	 	related_IDs : ["704", "705", "758", "759", "815", "816"],
	 	language_ID : "5",
	 	id : "879"
 	 }, { 
	 	name : "anoche",
	 	categories_IDs : ["39"],
	 	related_IDs : ["1285", "881", "913"],
	 	language_ID : "5",
	 	id : "880"
 	 }, { 
	 	name : "gestern abend",
	 	categories_IDs : ["39"],
	 	related_IDs : ["1285", "880", "913"],
	 	language_ID : "2",
	 	id : "881"
 	 }, { 
	 	name : "nachher",
	 	categories_IDs : ["39"],
	 	related_IDs : ["2855", "2896", "883", "884", "914", "915"],
	 	language_ID : "2",
	 	id : "882"
 	 }, { 
	 	name : "despues",
	 	categories_IDs : ["39"],
	 	related_IDs : ["2857", "882", "884", "914", "915"],
	 	language_ID : "5",
	 	id : "883"
 	 }, { 
	 	name : "afterwards",
	 	categories_IDs : ["39"],
	 	related_IDs : ["882", "883", "914", "915"],
	 	language_ID : "3",
	 	id : "884"
 	 }, { 
	 	name : "el catorce de mayo",
	 	categories_IDs : ["39"],
	 	related_IDs : ["707", "761", "818"],
	 	language_ID : "5",
	 	id : "885"
 	 }, { 
	 	name : "el lunes",
	 	categories_IDs : ["39"],
	 	related_IDs : ["708", "762", "819"],
	 	language_ID : "5",
	 	id : "886"
 	 }, { 
	 	name : "el martes",
	 	categories_IDs : ["39"],
	 	related_IDs : ["2853", "709", "763", "820"],
	 	language_ID : "5",
	 	id : "887"
 	 }, { 
	 	name : "el miercoles",
	 	categories_IDs : ["39"],
	 	related_IDs : ["710", "764", "821"],
	 	language_ID : "5",
	 	id : "888"
 	 }, { 
	 	name : "el jueves",
	 	categories_IDs : ["39"],
	 	related_IDs : ["711", "765", "822"],
	 	language_ID : "5",
	 	id : "889"
 	 }, { 
	 	name : "el viernes",
	 	categories_IDs : ["39"],
	 	related_IDs : ["712", "766", "823"],
	 	language_ID : "5",
	 	id : "890"
 	 }, { 
	 	name : "el sabado",
	 	categories_IDs : ["39"],
	 	related_IDs : ["713", "767", "824"],
	 	language_ID : "5",
	 	id : "891"
 	 }, { 
	 	name : "el domingo",
	 	categories_IDs : ["39"],
	 	related_IDs : ["714", "768", "825"],
	 	language_ID : "5",
	 	id : "892"
 	 }, { 
	 	name : "el enero",
	 	categories_IDs : ["39"],
	 	related_IDs : ["715", "769", "826"],
	 	language_ID : "5",
	 	id : "893"
 	 }, { 
	 	name : "el febrero",
	 	categories_IDs : ["39"],
	 	related_IDs : ["716", "770", "827"],
	 	language_ID : "5",
	 	id : "894"
 	 }, { 
	 	name : "el marzo",
	 	categories_IDs : ["39"],
	 	related_IDs : ["717", "771", "828"],
	 	language_ID : "5",
	 	id : "895"
 	 }, { 
	 	name : "el abril",
	 	categories_IDs : ["39"],
	 	related_IDs : ["718", "772", "829"],
	 	language_ID : "5",
	 	id : "896"
 	 }, { 
	 	name : "el mayo",
	 	categories_IDs : ["39"],
	 	related_IDs : ["719", "773", "830"],
	 	language_ID : "5",
	 	id : "897"
 	 }, { 
	 	name : "el junio",
	 	categories_IDs : ["39"],
	 	related_IDs : ["720", "774", "831"],
	 	language_ID : "5",
	 	id : "898"
 	 }, { 
	 	name : "el julio",
	 	categories_IDs : ["39"],
	 	related_IDs : ["721", "775", "832"],
	 	language_ID : "5",
	 	id : "899"
 	 }, { 
	 	name : "el agosto",
	 	categories_IDs : ["39"],
	 	related_IDs : ["722", "776", "833"],
	 	language_ID : "5",
	 	id : "900"
 	 }, { 
	 	name : "el septiembre",
	 	categories_IDs : ["39"],
	 	related_IDs : ["723", "777", "834"],
	 	language_ID : "5",
	 	id : "901"
 	 }, { 
	 	name : "el octubre",
	 	categories_IDs : ["39"],
	 	related_IDs : ["724", "778", "835"],
	 	language_ID : "5",
	 	id : "902"
 	 }, { 
	 	name : "el noviembre",
	 	categories_IDs : ["39"],
	 	related_IDs : ["836", "837", "847"],
	 	language_ID : "5",
	 	id : "903"
 	 }, { 
	 	name : "el diciembre",
	 	categories_IDs : ["39"],
	 	related_IDs : ["725", "779", "838"],
	 	language_ID : "5",
	 	id : "904"
 	 }, { 
	 	name : "la Nochevieja",
	 	categories_IDs : ["39"],
	 	related_IDs : ["726", "780", "839"],
	 	language_ID : "5",
	 	id : "905"
 	 }, { 
	 	name : "la pascua",
	 	categories_IDs : ["39"],
	 	related_IDs : ["727", "781", "840"],
	 	language_ID : "5",
	 	id : "906"
 	 }, { 
	 	name : "la navidad",
	 	categories_IDs : ["39"],
	 	related_IDs : ["728", "782", "841"],
	 	language_ID : "5",
	 	id : "907"
 	 }, { 
	 	name : "el año nuevo",
	 	categories_IDs : ["39"],
	 	related_IDs : ["729", "783", "842"],
	 	language_ID : "5",
	 	id : "908"
 	 }, { 
	 	name : "la primavera",
	 	categories_IDs : ["39"],
	 	related_IDs : ["730", "784", "843"],
	 	language_ID : "5",
	 	id : "909"
 	 }, { 
	 	name : "el verano",
	 	categories_IDs : ["39"],
	 	related_IDs : ["731", "785", "844"],
	 	language_ID : "5",
	 	id : "910"
 	 }, { 
	 	name : "el otoño",
	 	categories_IDs : ["39"],
	 	related_IDs : ["732", "786", "845"],
	 	language_ID : "5",
	 	id : "911"
 	 }, { 
	 	name : "el invierno",
	 	categories_IDs : ["39"],
	 	related_IDs : ["733", "787", "846"],
	 	language_ID : "5",
	 	id : "912"
 	 }, { 
	 	name : "i går kväll",
	 	categories_IDs : ["39"],
	 	related_IDs : ["1285", "880", "881"],
	 	language_ID : "1",
	 	id : "913"
 	 }, { 
	 	name : "efteråt",
	 	categories_IDs : ["39"],
	 	related_IDs : ["2858", "882", "883", "884", "915"],
	 	language_ID : "1",
	 	id : "914"
 	 }, { 
	 	name : "sedan",
	 	categories_IDs : ["39"],
	 	related_IDs : ["882", "883", "884", "914"],
	 	language_ID : "1",
	 	id : "915"
 	 }, { 
	 	name : "später",
	 	categories_IDs : ["39"],
	 	related_IDs : ["1795", "917", "920"],
	 	language_ID : "2",
	 	id : "916"
 	 }, { 
	 	name : "senare",
	 	categories_IDs : ["39"],
	 	related_IDs : ["1795", "916", "920"],
	 	language_ID : "1",
	 	id : "917"
 	 }, { 
	 	name : "früher",
	 	categories_IDs : ["39"],
	 	related_IDs : ["1793", "919", "921"],
	 	language_ID : "2",
	 	id : "918"
 	 }, { 
	 	name : "tidigare",
	 	categories_IDs : ["39"],
	 	related_IDs : ["1793", "918", "921"],
	 	language_ID : "1",
	 	id : "919"
 	 }, { 
	 	name : "later",
	 	categories_IDs : ["39"],
	 	related_IDs : ["1795", "2856", "916", "917"],
	 	language_ID : "3",
	 	id : "920",
	 	tags : []
 	 }, { 
	 	name : "earlier",
	 	categories_IDs : ["39"],
	 	related_IDs : ["1793", "918", "919", "L3N18n8pPvQy"],
	 	language_ID : "3",
	 	id : "921",
	 	tags : []
 	 }, { 
	 	name : "bevor",
	 	categories_IDs : ["39"],
	 	related_IDs : ["3003", "924", "925", "738", "519"],
	 	language_ID : "2",
	 	id : "922"
 	 }, { 
	 	name : "förran",
	 	categories_IDs : ["39"],
	 	related_IDs : ["3003", "922", "925", "738", "519"],
	 	language_ID : "1",
	 	id : "924"
 	 }, { 
	 	name : "antes de que",
	 	categories_IDs : ["39"],
	 	related_IDs : ["3003", "922", "924", "738", "519"],
	 	language_ID : "5",
	 	id : "925"
 	 }, { 
	 	name : "Wie geht's dir?",
	 	categories_IDs : ["34"],
	 	related_IDs : ["2870", "2963", "927", "928", "930"],
	 	language_ID : "2",
	 	sentence : true,
	 	id : "926"
 	 }, { 
	 	name : "Wie geht es Ihnen?",
	 	categories_IDs : ["34"],
	 	related_IDs : ["2963", "926", "928", "929"],
	 	language_ID : "2",
	 	sentence : true,
	 	id : "927"
 	 }, { 
	 	name : "Hur är det?",
	 	categories_IDs : ["34"],
	 	related_IDs : ["2963", "926", "927", "929", "930"],
	 	language_ID : "1",
	 	sentence : true,
	 	id : "928",
	 	responses : [2879]
 	 }, { 
	 	name : "Cómo está usted?",
	 	categories_IDs : ["34"],
	 	related_IDs : ["2963", "927", "928"],
	 	language_ID : "5",
	 	sentence : true,
	 	id : "929"
 	 }, { 
	 	name : "Cómo estás?",
	 	categories_IDs : ["34"],
	 	related_IDs : ["926", "928"],
	 	language_ID : "5",
	 	sentence : true,
	 	id : "930"
 	 }, { 
	 	name : "Hi! Hallo!",
	 	categories_IDs : ["34"],
	 	related_IDs : ["2863", "3073", "932", "933"],
	 	language_ID : "2",
	 	sentence : true,
	 	id : "931"
 	 }, { 
	 	name : "Tjena!",
	 	categories_IDs : ["34"],
	 	related_IDs : ["3073", "931", "933"],
	 	language_ID : "1",
	 	sentence : true,
	 	id : "932"
 	 }, { 
	 	name : "Hej!",
	 	categories_IDs : ["34"],
	 	related_IDs : ["3073", "443", "931", "932"],
	 	language_ID : "1",
	 	sentence : true,
	 	id : "933"
 	 }, { 
	 	name : "Tack ska du ha.",
	 	categories_IDs : ["34"],
	 	related_IDs : ["2875", "935", "936"],
	 	language_ID : "1",
	 	sentence : true,
	 	id : "934"
 	 }, { 
	 	name : "Vielen Dank.",
	 	categories_IDs : ["34"],
	 	related_IDs : ["1729", "2875", "934", "936"],
	 	language_ID : "2",
	 	sentence : true,
	 	id : "935"
 	 }, { 
	 	name : "Tack så mycket.",
	 	categories_IDs : ["34"],
	 	related_IDs : ["2875", "934", "935"],
	 	language_ID : "1",
	 	sentence : true,
	 	id : "936"
 	 }, { 
	 	name : "Hej då.",
	 	categories_IDs : ["34"],
	 	related_IDs : ["3074", "938"],
	 	language_ID : "1",
	 	sentence : true,
	 	id : "937"
 	 }, { 
	 	name : "Tschüss.",
	 	categories_IDs : ["34"],
	 	related_IDs : ["2862", "3074", "937"],
	 	sentence : true,
	 	language_ID : "2",
	 	id : "938"
 	 }, { 
	 	name : "Gibt es hier eine Apotheke?",
	 	categories_IDs : ["34"],
	 	related_IDs : ["3075", "940"],
	 	language_ID : "2",
	 	sentence : true,
	 	id : "939"
 	 }, { 
	 	name : "Finns det ett apotek här?",
	 	categories_IDs : ["34"],
	 	related_IDs : ["3075", "939"],
	 	language_ID : "1",
	 	sentence : true,
	 	id : "940"
 	 }, { 
	 	name : "Wo ist der Bahnhof?",
	 	categories_IDs : ["34"],
	 	related_IDs : ["2874", "942", "4BN0TINFgaDM"],
	 	language_ID : "2",
	 	sentence : true,
	 	id : "941"
 	 }, { 
	 	name : "Var är tågstationen, tack?",
	 	categories_IDs : ["34"],
	 	related_IDs : ["2874", "941", "4BN0TINFgaDM"],
	 	language_ID : "1",
	 	sentence : true,
	 	id : "942"
 	 }, { 
	 	name : "Wann ist die Post geöffnet?",
	 	categories_IDs : ["34"],
	 	related_IDs : ["2830", "2961", "944"],
	 	sentence : true,
	 	language_ID : "2",
	 	id : "943"
 	 }, { 
	 	name : "När öppna posten?",
	 	categories_IDs : ["34"],
	 	related_IDs : ["2961", "943"],
	 	language_ID : "1",
	 	sentence : true,
	 	id : "944"
 	 }, { 
	 	name : "Ursäkta, hur heter den här gatan?",
	 	categories_IDs : ["34"],
	 	related_IDs : ["3076", "946"],
	 	language_ID : "1",
	 	sentence : true,
	 	id : "945"
 	 }, { 
	 	name : "Entschuldigung, wie heißt diese Straße?",
	 	categories_IDs : ["34"],
	 	related_IDs : ["2861", "3076", "945"],
	 	language_ID : "2",
	 	sentence : true,
	 	id : "946"
 	 }, { 
	 	name : "das Fahrrad",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1035", "1079", "1080", "2939", "989"],
	 	language_ID : "2",
	 	id : "947"
 	 }, { 
	 	name : "das Land",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1036", "1081", "990"],
	 	language_ID : "2",
	 	id : "948"
 	 }, { 
	 	name : "der Ort",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1037", "1082", "1083", "991"],
	 	language_ID : "2",
	 	id : "949"
 	 }, { 
	 	name : "die Stadt",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1038", "1084", "992"],
	 	language_ID : "2",
	 	id : "950"
 	 }, { 
	 	name : "der Bahnhof",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1039", "1085", "2855", "2939", "993"],
	 	language_ID : "2",
	 	id : "951"
 	 }, { 
	 	name : "der Flughafen",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1034", "1040", "1086"],
	 	language_ID : "2",
	 	id : "952"
 	 }, { 
	 	name : "die Bushaltestelle",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1041", "1087", "994", "995"],
	 	language_ID : "2",
	 	id : "953"
 	 }, { 
	 	name : "das Hotel",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1042", "1088", "996"],
	 	language_ID : "2",
	 	id : "954"
 	 }, { 
	 	name : "die Herberge",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1043", "1089", "997"],
	 	language_ID : "2",
	 	id : "955"
 	 }, { 
	 	name : "der Urlaub",
	 	categories_IDs : ["41", "46", "61"],
	 	related_IDs : ["1000", "1045", "1090", "999"],
	 	language_ID : "2",
	 	id : "957"
 	 }, { 
	 	name : "das Museum",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1001", "1046", "1091"],
	 	language_ID : "2",
	 	id : "958"
 	 }, { 
	 	name : "die Postkarte",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1002", "1047", "1092"],
	 	language_ID : "2",
	 	id : "959"
 	 }, { 
	 	name : "der Brief",
	 	categories_IDs : ["41", "52"],
	 	related_IDs : ["1003", "1048", "1093"],
	 	language_ID : "2",
	 	id : "960"
 	 }, { 
	 	name : "übernachten",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1005", "1049", "1050", "1094"],
	 	language_ID : "2",
	 	id : "961"
 	 }, { 
	 	name : "das Abenteuer",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1004", "1051", "1095"],
	 	language_ID : "2",
	 	id : "962"
 	 }, { 
	 	name : "der Strand",
	 	categories_IDs : ["2756", "41"],
	 	related_IDs : ["1006", "1052", "1096"],
	 	language_ID : "2",
	 	id : "963"
 	 }, { 
	 	name : "die Wüste",
	 	categories_IDs : ["2756", "41"],
	 	related_IDs : ["1008", "1054", "1098"],
	 	language_ID : "2",
	 	id : "965"
 	 }, { 
	 	name : "der Berg",
	 	categories_IDs : ["2756", "41"],
	 	related_IDs : ["1009", "1055", "1099"],
	 	language_ID : "2",
	 	id : "966"
 	 }, { 
	 	name : "der See",
	 	categories_IDs : ["2756", "41"],
	 	related_IDs : ["1010", "1056", "1100"],
	 	language_ID : "2",
	 	id : "967"
 	 }, { 
	 	name : "das Tal",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1011", "1101", "2541"],
	 	language_ID : "2",
	 	id : "968"
 	 }, { 
	 	name : "der Kontinent",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1012", "1058", "1102"],
	 	language_ID : "2",
	 	id : "969"
 	 }, { 
	 	name : "Europa",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1013", "1059", "1103", "2922"],
	 	language_ID : "2",
	 	id : "970"
 	 }, { 
	 	name : "Asien",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1014", "1060", "1104"],
	 	language_ID : "2",
	 	id : "971"
 	 }, { 
	 	name : "Südamerika",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1015", "1062", "1105"],
	 	language_ID : "2",
	 	id : "972"
 	 }, { 
	 	name : "Nordamerika",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1016", "1063", "1107"],
	 	language_ID : "2",
	 	id : "973"
 	 }, { 
	 	name : "Afrika",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1017", "1061", "1108"],
	 	language_ID : "2",
	 	id : "974"
 	 }, { 
	 	name : "Australien",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1018", "1109", "2542"],
	 	language_ID : "2",
	 	id : "975"
 	 }, { 
	 	name : "Deutschland",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1019", "1065", "1110", "2930"],
	 	language_ID : "2",
	 	id : "976"
 	 }, { 
	 	name : "Spanien",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1020", "1066", "1111"],
	 	language_ID : "2",
	 	id : "977"
 	 }, { 
	 	name : "Schweden",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1021", "1067", "1112"],
	 	language_ID : "2",
	 	id : "978"
 	 }, { 
	 	name : "Großbritannien",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1022", "1068", "1113"],
	 	language_ID : "2",
	 	id : "979"
 	 }, { 
	 	name : "Vereinigte Staaten von Amerika, USA",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1023", "1069", "1114"],
	 	language_ID : "2",
	 	id : "980"
 	 }, { 
	 	name : "China",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1024", "1070", "1116"],
	 	language_ID : "2",
	 	id : "981"
 	 }, { 
	 	name : "Indien",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1025", "1071", "1811"],
	 	language_ID : "2",
	 	id : "982"
 	 }, { 
	 	name : "Südafrika",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1026", "1072", "1106"],
	 	language_ID : "2",
	 	id : "983"
 	 }, { 
	 	name : "Chile",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1027", "1073", "1115"],
	 	language_ID : "2",
	 	id : "984"
 	 }, { 
	 	name : "Kanada",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1028", "1074", "1117"],
	 	language_ID : "2",
	 	id : "985"
 	 }, { 
	 	name : "Frankreich",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1029", "1075", "1118", "2922"],
	 	language_ID : "2",
	 	id : "986"
 	 }, { 
	 	name : "Russland",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1030", "1077", "1119"],
	 	language_ID : "2",
	 	id : "987"
 	 }, { 
	 	name : "Kenia",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1031", "1076", "1120"],
	 	language_ID : "2",
	 	id : "988"
 	 }, { 
	 	name : "the bicycle",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1035", "1079", "1080", "947"],
	 	language_ID : "3",
	 	id : "989"
 	 }, { 
	 	name : "the country",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1036", "1081", "3290", "948"],
	 	language_ID : "3",
	 	id : "990",
	 	tags : []
 	 }, { 
	 	name : "the place",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1037", "1082", "1083", "949"],
	 	language_ID : "3",
	 	id : "991"
 	 }, { 
	 	name : "the city",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1038", "1084", "950", "T0EkiFW0NdZn"],
	 	language_ID : "3",
	 	id : "992"
 	 }, { 
	 	name : "the train station",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1039", "1085", "2856", "951"],
	 	language_ID : "3",
	 	id : "993"
 	 }, { 
	 	name : "the bus terminal",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1041", "1087", "953", "995"],
	 	language_ID : "3",
	 	id : "994"
 	 }, { 
	 	name : "the coach station",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1041", "1087", "953", "994"],
	 	language_ID : "3",
	 	id : "995"
 	 }, { 
	 	name : "the hotel",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1042", "1088", "954"],
	 	language_ID : "3",
	 	id : "996"
 	 }, { 
	 	name : "the hostel",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1043", "1089", "955"],
	 	language_ID : "3",
	 	id : "997"
 	 }, { 
	 	name : "the holiday",
	 	categories_IDs : ["41", "46", "61"],
	 	related_IDs : ["1000", "1045", "1090", "957"],
	 	language_ID : "3",
	 	id : "999",
	 	tags : []
 	 }, { 
	 	name : "the vacation",
	 	categories_IDs : ["41", "46", "61"],
	 	related_IDs : ["1045", "1090", "957", "999"],
	 	language_ID : "3",
	 	id : "1000"
 	 }, { 
	 	name : "the museum",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1046", "1091", "958", "lzxJwHxP7qNQ"],
	 	language_ID : "3",
	 	id : "1001"
 	 }, { 
	 	name : "the postcard",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1047", "1092", "959"],
	 	language_ID : "3",
	 	id : "1002"
 	 }, { 
	 	name : "the letter",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1048", "1093", "960"],
	 	language_ID : "3",
	 	id : "1003"
 	 }, { 
	 	name : "the adventure",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1051", "1095", "962"],
	 	language_ID : "3",
	 	id : "1004"
 	 }, { 
	 	name : "stay overnight",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1049", "1050", "1094", "961"],
	 	language_ID : "3",
	 	id : "1005"
 	 }, { 
	 	name : "the beach",
	 	categories_IDs : ["2756", "41"],
	 	related_IDs : ["1052", "1096", "963"],
	 	language_ID : "3",
	 	id : "1006"
 	 }, { 
	 	name : "the desert",
	 	categories_IDs : ["2756", "41", "59"],
	 	related_IDs : ["1054", "1098", "965"],
	 	language_ID : "3",
	 	id : "1008"
 	 }, { 
	 	name : "the mountain",
	 	categories_IDs : ["2756", "41"],
	 	related_IDs : ["1055", "1099", "966"],
	 	language_ID : "3",
	 	id : "1009"
 	 }, { 
	 	name : "the lake",
	 	categories_IDs : ["2756", "41"],
	 	related_IDs : ["1056", "1100", "967"],
	 	language_ID : "3",
	 	id : "1010"
 	 }, { 
	 	name : "the valley",
	 	categories_IDs : ["2756", "41"],
	 	related_IDs : ["1101", "2541", "968"],
	 	language_ID : "3",
	 	id : "1011"
 	 }, { 
	 	name : "the continent",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1058", "1102", "969"],
	 	language_ID : "3",
	 	id : "1012"
 	 }, { 
	 	name : "Europe",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1059", "1103", "970"],
	 	language_ID : "3",
	 	id : "1013"
 	 }, { 
	 	name : "Asia",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1060", "1104", "971"],
	 	language_ID : "3",
	 	id : "1014"
 	 }, { 
	 	name : "South America",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1062", "1105", "972"],
	 	language_ID : "3",
	 	id : "1015"
 	 }, { 
	 	name : "North America",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1063", "1107", "973"],
	 	language_ID : "3",
	 	id : "1016"
 	 }, { 
	 	name : "Africa",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1061", "1108", "974"],
	 	language_ID : "3",
	 	id : "1017"
 	 }, { 
	 	name : "Australia",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1109", "2542", "975"],
	 	language_ID : "3",
	 	id : "1018",
	 	tags : []
 	 }, { 
	 	name : "Germany",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1065", "1110", "976"],
	 	language_ID : "3",
	 	id : "1019"
 	 }, { 
	 	name : "Spain",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1066", "1111", "977"],
	 	language_ID : "3",
	 	id : "1020"
 	 }, { 
	 	name : "Sweden",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1067", "1112", "978"],
	 	language_ID : "3",
	 	id : "1021"
 	 }, { 
	 	name : "Great Britain",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1068", "1113", "979"],
	 	language_ID : "3",
	 	id : "1022"
 	 }, { 
	 	name : "United States of America, USA",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1069", "1114", "980"],
	 	language_ID : "3",
	 	id : "1023"
 	 }, { 
	 	name : "China",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1070", "1116", "981"],
	 	language_ID : "3",
	 	id : "1024"
 	 }, { 
	 	name : "India",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1071", "1811", "982"],
	 	language_ID : "3",
	 	id : "1025"
 	 }, { 
	 	name : "South Africa",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1072", "1106", "983"],
	 	language_ID : "3",
	 	id : "1026"
 	 }, { 
	 	name : "Chile",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1073", "1115", "984"],
	 	language_ID : "3",
	 	id : "1027"
 	 }, { 
	 	name : "Canada",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1074", "1117", "985"],
	 	language_ID : "3",
	 	id : "1028"
 	 }, { 
	 	name : "France",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1075", "1118", "986"],
	 	language_ID : "3",
	 	id : "1029"
 	 }, { 
	 	name : "Russia",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1077", "1119", "987"],
	 	language_ID : "3",
	 	id : "1030"
 	 }, { 
	 	name : "Kenya",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1076", "1120", "988"],
	 	language_ID : "3",
	 	id : "1031"
 	 }, { 
	 	name : "die Insel",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1033", "1078", "1121"],
	 	language_ID : "2",
	 	id : "1032"
 	 }, { 
	 	name : "the island",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1032", "1078", "1121"],
	 	language_ID : "3",
	 	id : "1033"
 	 }, { 
	 	name : "the airport",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1040", "1086", "952"],
	 	language_ID : "3",
	 	id : "1034"
 	 }, { 
	 	name : "en cykel",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1079", "1080", "947", "989"],
	 	language_ID : "1",
	 	id : "1035"
 	 }, { 
	 	name : "ett land",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1081", "948", "990"],
	 	language_ID : "1",
	 	id : "1036"
 	 }, { 
	 	name : "en ort",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1082", "1083", "949", "991"],
	 	language_ID : "1",
	 	id : "1037"
 	 }, { 
	 	name : "en stad",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1084", "950", "992"],
	 	language_ID : "1",
	 	id : "1038"
 	 }, { 
	 	name : "en järnvägsstation",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1085", "951", "993"],
	 	language_ID : "1",
	 	id : "1039"
 	 }, { 
	 	name : "en flyplats",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1034", "1086", "952"],
	 	language_ID : "1",
	 	id : "1040"
 	 }, { 
	 	name : "en busshållplats",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1087", "953", "994", "995"],
	 	language_ID : "1",
	 	id : "1041"
 	 }, { 
	 	name : "ett hotell",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1088", "954", "996"],
	 	language_ID : "1",
	 	id : "1042"
 	 }, { 
	 	name : "ett härbärge",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1089", "955", "997"],
	 	language_ID : "1",
	 	id : "1043"
 	 }, { 
	 	name : "en semester",
	 	categories_IDs : ["41", "46", "61"],
	 	related_IDs : ["1000", "1090", "957", "999"],
	 	language_ID : "1",
	 	id : "1045"
 	 }, { 
	 	name : "ett museum",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1001", "1091", "958"],
	 	language_ID : "1",
	 	id : "1046"
 	 }, { 
	 	name : "ett vykort",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1002", "1092", "959"],
	 	language_ID : "1",
	 	id : "1047"
 	 }, { 
	 	name : "ett brev",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1003", "1093", "960"],
	 	language_ID : "1",
	 	id : "1048"
 	 }, { 
	 	name : "övernatta",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1005", "1050", "1094", "961"],
	 	language_ID : "1",
	 	id : "1049"
 	 }, { 
	 	name : "stanna över natten",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1005", "1049", "1094", "961"],
	 	language_ID : "1",
	 	id : "1050"
 	 }, { 
	 	name : "ett äventyr",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1004", "1095", "962"],
	 	language_ID : "1",
	 	id : "1051"
 	 }, { 
	 	name : "en strand",
	 	categories_IDs : ["2756", "41"],
	 	related_IDs : ["1006", "1096", "963"],
	 	language_ID : "1",
	 	id : "1052"
 	 }, { 
	 	name : "en öken",
	 	categories_IDs : ["2756", "41", "59"],
	 	related_IDs : ["1008", "1098", "965"],
	 	language_ID : "1",
	 	id : "1054"
 	 }, { 
	 	name : "ett berg",
	 	categories_IDs : ["2756", "41"],
	 	related_IDs : ["1009", "1099", "966"],
	 	language_ID : "1",
	 	id : "1055"
 	 }, { 
	 	name : "en sjö",
	 	categories_IDs : ["2756", "41"],
	 	related_IDs : ["1010", "1100", "967"],
	 	language_ID : "1",
	 	id : "1056"
 	 }, { 
	 	name : "die See",
	 	categories_IDs : ["2756", "41"],
	 	related_IDs : ["2293", "964", "1007", "1097"],
	 	language_ID : "2",
	 	id : "1057"
 	 }, { 
	 	name : "en kontinent",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1012", "1102", "969"],
	 	language_ID : "1",
	 	id : "1058"
 	 }, { 
	 	name : "Europa",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1013", "1103", "970"],
	 	language_ID : "1",
	 	id : "1059"
 	 }, { 
	 	name : "Asien",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1014", "1104", "971"],
	 	language_ID : "1",
	 	id : "1060"
 	 }, { 
	 	name : "Afrika",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1017", "1108", "974"],
	 	language_ID : "1",
	 	id : "1061"
 	 }, { 
	 	name : "Sysamerika",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1015", "1105", "972"],
	 	language_ID : "1",
	 	id : "1062"
 	 }, { 
	 	name : "Nordamerika",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1016", "1107", "973"],
	 	language_ID : "1",
	 	id : "1063"
 	 }, { 
	 	name : "Tyskland",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1019", "1110", "976"],
	 	language_ID : "1",
	 	id : "1065"
 	 }, { 
	 	name : "Spanien",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1020", "1111", "977"],
	 	language_ID : "1",
	 	id : "1066"
 	 }, { 
	 	name : "Sverige",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1021", "1112", "978"],
	 	language_ID : "1",
	 	id : "1067"
 	 }, { 
	 	name : "Storbritannien",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1022", "1113", "979"],
	 	language_ID : "1",
	 	id : "1068"
 	 }, { 
	 	name : "Amerikas förenta stater (USA)",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1023", "1114", "980"],
	 	language_ID : "1",
	 	id : "1069"
 	 }, { 
	 	name : "Kina",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1024", "1116", "981"],
	 	language_ID : "1",
	 	id : "1070"
 	 }, { 
	 	name : "Indien",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1025", "1811", "982"],
	 	language_ID : "1",
	 	id : "1071"
 	 }, { 
	 	name : "Sydafrika",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1026", "1106", "983"],
	 	language_ID : "1",
	 	id : "1072"
 	 }, { 
	 	name : "Chile",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1027", "1115", "984"],
	 	language_ID : "1",
	 	id : "1073"
 	 }, { 
	 	name : "Kanada",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1028", "1117", "985"],
	 	language_ID : "1",
	 	id : "1074"
 	 }, { 
	 	name : "Frankrike",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1029", "1118", "986"],
	 	language_ID : "1",
	 	id : "1075"
 	 }, { 
	 	name : "Kenya",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1031", "1120", "988"],
	 	language_ID : "1",
	 	id : "1076"
 	 }, { 
	 	name : "Ryssland",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1030", "1119", "987"],
	 	language_ID : "1",
	 	id : "1077"
 	 }, { 
	 	name : "en ö",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1032", "1033", "1121"],
	 	language_ID : "1",
	 	id : "1078"
 	 }, { 
	 	name : "la bicicleta",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1035", "1080", "947", "989"],
	 	language_ID : "5",
	 	id : "1079"
 	 }, { 
	 	name : "la bici",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1035", "1079", "947", "989"],
	 	language_ID : "5",
	 	id : "1080"
 	 }, { 
	 	name : "el país",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1036", "948", "990"],
	 	language_ID : "5",
	 	id : "1081"
 	 }, { 
	 	name : "el lugar",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1037", "1083", "949", "991"],
	 	language_ID : "5",
	 	id : "1082"
 	 }, { 
	 	name : "el sitio",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1037", "1082", "949", "991"],
	 	language_ID : "5",
	 	id : "1083"
 	 }, { 
	 	name : "la ciudad",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1038", "950", "992"],
	 	language_ID : "5",
	 	id : "1084"
 	 }, { 
	 	name : "la estación",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1039", "2857", "951", "993"],
	 	language_ID : "5",
	 	id : "1085"
 	 }, { 
	 	name : "el aeropuerto",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1034", "1040", "952"],
	 	language_ID : "5",
	 	id : "1086"
 	 }, { 
	 	name : "la estación de autobuses",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1041", "953", "994", "995"],
	 	language_ID : "5",
	 	id : "1087"
 	 }, { 
	 	name : "el hotel",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1042", "954", "996"],
	 	language_ID : "5",
	 	id : "1088"
 	 }, { 
	 	name : "el albergue",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1043", "955", "997"],
	 	language_ID : "5",
	 	id : "1089"
 	 }, { 
	 	name : "las vacaciones",
	 	categories_IDs : ["41", "46", "61"],
	 	related_IDs : ["1000", "1045", "957", "999"],
	 	language_ID : "5",
	 	id : "1090"
 	 }, { 
	 	name : "el museo",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1001", "1046", "958"],
	 	language_ID : "5",
	 	id : "1091"
 	 }, { 
	 	name : "la tarjeta postal",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1002", "1047", "959"],
	 	language_ID : "5",
	 	id : "1092"
 	 }, { 
	 	name : "la carta",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1003", "1048", "960"],
	 	language_ID : "5",
	 	id : "1093"
 	 }, { 
	 	name : "pasar la noche",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1005", "1049", "1050", "961"],
	 	language_ID : "5",
	 	id : "1094"
 	 }, { 
	 	name : "la aventura",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1004", "1051", "962"],
	 	language_ID : "5",
	 	id : "1095"
 	 }, { 
	 	name : "la playa",
	 	categories_IDs : ["2756", "41"],
	 	related_IDs : ["1006", "1052", "963"],
	 	language_ID : "5",
	 	id : "1096"
 	 }, { 
	 	name : "el desierto",
	 	categories_IDs : ["2756", "41", "59"],
	 	related_IDs : ["1008", "1054", "965"],
	 	language_ID : "5",
	 	id : "1098"
 	 }, { 
	 	name : "la montaña",
	 	categories_IDs : ["2756", "41"],
	 	related_IDs : ["1009", "1055", "966"],
	 	language_ID : "5",
	 	id : "1099"
 	 }, { 
	 	name : "el lago",
	 	categories_IDs : ["2756", "41"],
	 	related_IDs : ["1010", "1056", "967"],
	 	language_ID : "5",
	 	id : "1100"
 	 }, { 
	 	name : "el valle",
	 	categories_IDs : ["2756", "41"],
	 	related_IDs : ["1011", "2541", "968"],
	 	language_ID : "5",
	 	id : "1101"
 	 }, { 
	 	name : "el continente",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1012", "1058", "969"],
	 	language_ID : "5",
	 	id : "1102"
 	 }, { 
	 	name : "Europa",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1013", "1059", "970"],
	 	language_ID : "5",
	 	id : "1103"
 	 }, { 
	 	name : "Asia",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1014", "1060", "971"],
	 	language_ID : "5",
	 	id : "1104"
 	 }, { 
	 	name : "América del Sur",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1015", "1062", "972"],
	 	language_ID : "5",
	 	id : "1105"
 	 }, { 
	 	name : "el África del Sur",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1026", "1072", "983"],
	 	language_ID : "5",
	 	id : "1106"
 	 }, { 
	 	name : "la América del Norte",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1016", "1063", "973"],
	 	language_ID : "5",
	 	id : "1107"
 	 }, { 
	 	name : "la África",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1017", "1061", "974"],
	 	language_ID : "5",
	 	id : "1108"
 	 }, { 
	 	name : "la Australia",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1018", "2542", "975"],
	 	language_ID : "5",
	 	id : "1109"
 	 }, { 
	 	name : "la Alemania",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1019", "1065", "976"],
	 	language_ID : "5",
	 	id : "1110"
 	 }, { 
	 	name : "la España",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1020", "1066", "977"],
	 	language_ID : "5",
	 	id : "1111"
 	 }, { 
	 	name : "la Suecia",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1021", "1067", "978"],
	 	language_ID : "5",
	 	id : "1112"
 	 }, { 
	 	name : "la Gran Bretaña",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1022", "1068", "979"],
	 	language_ID : "5",
	 	id : "1113"
 	 }, { 
	 	name : "(los) Estados Unidos",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1023", "1069", "980"],
	 	language_ID : "5",
	 	id : "1114"
 	 }, { 
	 	name : "el Chile",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1027", "1073", "984"],
	 	language_ID : "5",
	 	id : "1115"
 	 }, { 
	 	name : "la China",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1024", "1070", "981"],
	 	language_ID : "5",
	 	id : "1116"
 	 }, { 
	 	name : "el Canadá",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1028", "1074", "985"],
	 	language_ID : "5",
	 	id : "1117"
 	 }, { 
	 	name : "la Francia",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1029", "1075", "986"],
	 	language_ID : "5",
	 	id : "1118"
 	 }, { 
	 	name : "la Rusia",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1030", "1077", "987"],
	 	language_ID : "5",
	 	id : "1119"
 	 }, { 
	 	name : "el Kenia",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1031", "1076", "988"],
	 	language_ID : "5",
	 	id : "1120"
 	 }, { 
	 	name : "la isla",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1032", "1033", "1078"],
	 	language_ID : "5",
	 	id : "1121"
 	 }, { 
	 	name : "Das ist wie Fahrradfahren - das verlernt man nicht.",
	 	related_IDs : ["82"],
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["34"],
	 	id : "1122"
 	 }, { 
	 	name : "Zeig mal, wie du das machst.",
	 	related_IDs : ["2873", "82"],
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : [],
	 	id : "1123"
 	 }, { 
	 	name : "Ni kan koka ju! Det är läcker.",
	 	related_IDs : ["1126", "181"],
	 	language_ID : "1",
	 	sentence : true,
	 	categories_IDs : ["34"],
	 	id : "1124"
 	 }, { 
	 	name : "Ju fler desto bättre.",
	 	related_IDs : ["1127", "181", "3078"],
	 	language_ID : "1",
	 	sentence : true,
	 	categories_IDs : ["34"],
	 	id : "1125"
 	 }, { 
	 	name : "Ihr könnt ja doch kochen! Das schmeckt gut.",
	 	categories_IDs : ["34"],
	 	related_IDs : ["1124", "182"],
	 	language_ID : "2",
	 	sentence : true,
	 	id : "1126"
 	 }, { 
	 	name : "Je mehr, desto besser!",
	 	categories_IDs : ["34"],
	 	related_IDs : ["1125", "1128", "1721", "2864", "3078"],
	 	language_ID : "2",
	 	id : "1127",
	 	sentence : true
 	 }, { 
	 	categories_IDs : ["33"],
	 	related_IDs : ["1127", "1130", "1720", "181", "2864"],
	 	language_ID : "2",
	 	name : "je, desto",
	 	id : "1128"
 	 }, { 
	 	name : "Je öfter du übst, desto besser kannst du Klavier spielen.",
	 	related_IDs : ["1128"],
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : [],
	 	id : "1130"
 	 }, { 
	 	categories_IDs : ["34"],
	 	related_IDs : ["1132", "1136"],
	 	name : "Vad heter du?",
	 	sentence : true,
	 	language_ID : "1",
	 	id : "1131",
	 	tags : [],
	 	responses_IDs : ["3070"]
 	 }, { 
	 	categories_IDs : ["34"],
	 	related_IDs : ["1131", "1135", "1136"],
	 	language_ID : "2",
	 	name : "Wie heißt du?",
	 	id : "1132",
	 	sentence : true
 	 }, { 
	 	categories_IDs : ["34"],
	 	related_IDs : ["1134", "66", "vO6qE1RQZaeY"],
	 	name : "Was meinst du?",
	 	sentence : true,
	 	language_ID : "2",
	 	id : "1133"
 	 }, { 
	 	categories_IDs : ["34"],
	 	related_IDs : ["104", "1133", "vO6qE1RQZaeY"],
	 	name : "Vad menar du?",
	 	language_ID : "1",
	 	id : "1134",
	 	sentence : true
 	 }, { 
	 	categories_IDs : ["34"],
	 	related_IDs : ["1132", "1136"],
	 	name : "Cómo se llamas?",
	 	sentence : true,
	 	language_ID : "5",
	 	id : "1135"
 	 }, { 
	 	categories_IDs : [],
	 	related_IDs : ["1131", "1132", "1135", "86"],
	 	name : "What is your name?",
	 	sentence : true,
	 	language_ID : "3",
	 	id : "1136"
 	 }, { 
	 	name : "das Büro",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1171", "1209", "1265"],
	 	id : "1137"
 	 }, { 
	 	name : "der Stift",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1172", "1210", "1266"],
	 	id : "1138"
 	 }, { 
	 	name : "der Kugelschreiber",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1173", "1211", "1267"],
	 	id : "1139"
 	 }, { 
	 	name : "der Computer",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1174", "1212", "1268"],
	 	id : "1140"
 	 }, { 
	 	name : "die Tastatur",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1175", "1213", "1269"],
	 	id : "1141"
 	 }, { 
	 	name : "die Werkstatt",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1176", "1214", "1270"],
	 	id : "1142"
 	 }, { 
	 	name : "die Pause",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1177", "1215", "1271", "1272"],
	 	id : "1143"
 	 }, { 
	 	name : "der Schreibtisch",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1178", "1216", "1273"],
	 	id : "1144"
 	 }, { 
	 	name : "das Werkzeug",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1179", "1217", "1274"],
	 	id : "1145"
 	 }, { 
	 	name : "der Kunde",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1180", "1218"],
	 	id : "1146",
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3102"]
 	 }, { 
	 	name : "der Klient",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1181", "1219", "1852", "3130"],
	 	id : "1147",
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3103"]
 	 }, { 
	 	name : "der Chef",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1182", "1220", "1221", "1276", "3131"],
	 	id : "1148",
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3104"]
 	 }, { 
	 	name : "der Ingenieur",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1183", "1222", "1277", "3105"],
	 	id : "1149",
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3106"]
 	 }, { 
	 	name : "der Psychologe",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1184", "1223", "1278", "3108"],
	 	id : "1150",
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3107"]
 	 }, { 
	 	name : "die LKW-Fahrerin",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1185", "1224", "1279", "3132"],
	 	id : "1151",
	 	tags : [],
	 	formPrimaryName : "weiblich",
	 	formsPrimary_IDs : ["3109"]
 	 }, { 
	 	name : "der Arzt",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1186", "1187", "1225", "1280", "3133"],
	 	id : "1152",
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3110"]
 	 }, { 
	 	name : "der Arbeiter",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1188", "1226", "1281", "3134"],
	 	id : "1153",
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3111"]
 	 }, { 
	 	name : "das Reinigungspersonal",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1189", "1282", "3135"],
	 	id : "1154",
	 	tags : []
 	 }, { 
	 	name : "der Feuerwehrmann",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1190", "1227", "1283", "3124", "3136"],
	 	id : "1155",
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3112"]
 	 }, { 
	 	name : "die Polizistin",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1191", "1228", "1284", "3125", "3126", "3137"],
	 	id : "1156",
	 	tags : [],
	 	formPrimaryName : "weiblich",
	 	formsPrimary_IDs : ["3113"]
 	 }, { 
	 	name : "die Mechanikerin",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1192", "1229", "1854", "3215"],
	 	id : "1157",
	 	tags : [],
	 	formPrimaryName : "weiblich",
	 	formsPrimary_IDs : ["3114"]
 	 }, { 
	 	name : "der Altenpfleger",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1193", "1847", "3209"],
	 	id : "1158",
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3115"]
 	 }, { 
	 	name : "die Anwältin",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1194", "1230", "1848", "3210"],
	 	id : "1159",
	 	tags : [],
	 	formPrimaryName : "weiblich",
	 	formsPrimary_IDs : ["3116"]
 	 }, { 
	 	name : "die Priesterin",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1195", "1231", "1856", "3216"],
	 	id : "1160",
	 	tags : [],
	 	formPrimaryName : "weiblich",
	 	formsPrimary_IDs : ["3117"]
 	 }, { 
	 	name : "die Journalistin",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1196", "1232", "1851", "3213"],
	 	id : "1161",
	 	tags : [],
	 	formPrimaryName : "weiblich",
	 	formsPrimary_IDs : ["3118"]
 	 }, { 
	 	name : "der Taxifahrer",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1197", "1233", "1858", "3218"],
	 	id : "1162",
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3119"]
 	 }, { 
	 	name : "die Politikerin",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1198", "1234", "1857", "3217"],
	 	id : "1163",
	 	tags : [],
	 	formPrimaryName : "weiblich",
	 	formsPrimary_IDs : ["3120"]
 	 }, { 
	 	name : "der Stuhl",
	 	language_ID : "2",
	 	categories_IDs : ["19", "61"],
	 	related_IDs : ["1199", "1235", "1855"],
	 	id : "1164"
 	 }, { 
	 	name : "der Besucher",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1200", "1236", "1850", "3212"],
	 	id : "1165",
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3121"]
 	 }, { 
	 	name : "der Krankenpfleger",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1201", "1237", "1853", "3127", "3214"],
	 	id : "1166",
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3122"]
 	 }, { 
	 	name : "das Gehalt",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1202", "1203", "1239", "1846"],
	 	id : "1167"
 	 }, { 
	 	name : "die Rente",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1204", "1240", "1859"],
	 	id : "1168"
 	 }, { 
	 	name : "die Beamte",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1205", "1241", "1849", "3211"],
	 	id : "1170",
	 	tags : [],
	 	formPrimaryName : "weiblich",
	 	formsPrimary_IDs : ["3123"]
 	 }, { 
	 	name : "the office",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1137", "1209", "1265"],
	 	language_ID : "3",
	 	id : "1171"
 	 }, { 
	 	name : "the pen",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1138", "1210", "1266"],
	 	language_ID : "3",
	 	id : "1172"
 	 }, { 
	 	name : "the ball pen",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1139", "1211", "1267"],
	 	language_ID : "3",
	 	id : "1173"
 	 }, { 
	 	name : "the computer",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1140", "1212", "1268"],
	 	language_ID : "3",
	 	id : "1174"
 	 }, { 
	 	name : "the keyboard",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1141", "1213", "1269", "Csn80o3nIXbR"],
	 	language_ID : "3",
	 	id : "1175",
	 	tags : []
 	 }, { 
	 	name : "the workshop",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1142", "1214", "1270"],
	 	language_ID : "3",
	 	id : "1176"
 	 }, { 
	 	name : "the break",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1143", "1215", "1271", "1272", "BIu1cZPihXQy"],
	 	language_ID : "3",
	 	id : "1177",
	 	tags : []
 	 }, { 
	 	name : "the desk",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1144", "1216", "1273"],
	 	language_ID : "3",
	 	id : "1178"
 	 }, { 
	 	name : "the tool",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1145", "1217", "1274"],
	 	language_ID : "3",
	 	id : "1179"
 	 }, { 
	 	name : "the customer",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1146", "1218"],
	 	language_ID : "3",
	 	id : "1180"
 	 }, { 
	 	name : "the client",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1147", "1219", "1852", "3130", "fOXJ9imgXcTk", "3102", "3103"],
	 	language_ID : "3",
	 	id : "1181",
	 	tags : []
 	 }, { 
	 	name : "the superior",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1148", "1220", "1221", "1276", "3131"],
	 	language_ID : "3",
	 	id : "1182"
 	 }, { 
	 	name : "the engineer",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1149", "1222", "1277", "3105", "3106"],
	 	language_ID : "3",
	 	id : "1183"
 	 }, { 
	 	name : "the psychologist",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1150", "1223", "1278", "3107", "3108"],
	 	language_ID : "3",
	 	id : "1184"
 	 }, { 
	 	name : "the truck driver",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1151", "1224", "1279", "3109", "3132"],
	 	language_ID : "3",
	 	id : "1185"
 	 }, { 
	 	name : "the doctor",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1152", "1187", "1225", "1280", "3110", "3133", "I7u5rYQCUvvh"],
	 	language_ID : "3",
	 	id : "1186",
	 	tags : []
 	 }, { 
	 	name : "the physician",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1152", "1186", "1225", "1280", "3110", "3133"],
	 	language_ID : "3",
	 	id : "1187"
 	 }, { 
	 	name : "the worker",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1153", "1226", "1281", "3111", "3134"],
	 	language_ID : "3",
	 	id : "1188"
 	 }, { 
	 	name : "the cleaning staff",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1154", "1282", "3135"],
	 	language_ID : "3",
	 	id : "1189",
	 	tags : []
 	 }, { 
	 	name : "the firefighter",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1155", "1227", "1283", "3057", "3112", "3124", "3136"],
	 	language_ID : "3",
	 	id : "1190"
 	 }, { 
	 	name : "the police officer",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1156", "1228", "1284", "3113", "3125", "3126", "3137"],
	 	language_ID : "3",
	 	id : "1191"
 	 }, { 
	 	name : "the mechanic",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1157", "1229", "1854", "3114", "3215"],
	 	language_ID : "3",
	 	id : "1192"
 	 }, { 
	 	name : "the geriatric nurse",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1158", "1847", "3115", "3209"],
	 	language_ID : "3",
	 	id : "1193"
 	 }, { 
	 	name : "the lawyer",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1159", "1230", "1848", "3116", "3210", "6jRmQcUJR1dU"],
	 	language_ID : "3",
	 	id : "1194",
	 	tags : []
 	 }, { 
	 	name : "the priest",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1160", "1231", "1856", "3117", "3216"],
	 	language_ID : "3",
	 	id : "1195"
 	 }, { 
	 	name : "the journalist",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1161", "1232", "1851", "3118", "3213"],
	 	language_ID : "3",
	 	id : "1196"
 	 }, { 
	 	name : "the taxi driver",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1162", "1233", "1858", "3119", "3218"],
	 	language_ID : "3",
	 	id : "1197"
 	 }, { 
	 	name : "the politician",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1163", "1234", "1857", "3120", "3217"],
	 	language_ID : "3",
	 	id : "1198"
 	 }, { 
	 	name : "the chair",
	 	categories_IDs : ["19", "61"],
	 	related_IDs : ["1164", "1235", "1855"],
	 	language_ID : "3",
	 	id : "1199"
 	 }, { 
	 	name : "the visitor",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1165", "1236", "1850", "3121", "3212"],
	 	language_ID : "3",
	 	id : "1200"
 	 }, { 
	 	name : "the nurse",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1166", "1237", "1853", "3122", "3127", "3214", "o2MS1I28761L"],
	 	language_ID : "3",
	 	id : "1201",
	 	tags : []
 	 }, { 
	 	name : "the salary",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1167", "1203", "1239", "1846"],
	 	language_ID : "3",
	 	id : "1202"
 	 }, { 
	 	name : "the wage",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1167", "1202", "1239", "1846"],
	 	language_ID : "3",
	 	id : "1203"
 	 }, { 
	 	name : "the pension",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1168", "1240", "1859"],
	 	language_ID : "3",
	 	id : "1204"
 	 }, { 
	 	name : "the civil servant",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1170", "1241", "1849", "3123", "3211"],
	 	language_ID : "3",
	 	id : "1205"
 	 }, { 
	 	name : "ett kontor",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1137", "1171", "1265"],
	 	language_ID : "1",
	 	id : "1209"
 	 }, { 
	 	name : "ett stift",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1138", "1172", "1266"],
	 	language_ID : "1",
	 	id : "1210"
 	 }, { 
	 	name : "en kulspetspenna",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1139", "1173", "1267"],
	 	language_ID : "1",
	 	id : "1211"
 	 }, { 
	 	name : "en dator",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1140", "1174", "1268"],
	 	language_ID : "1",
	 	id : "1212"
 	 }, { 
	 	name : "ett tangentbord",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1141", "1175", "1269"],
	 	language_ID : "1",
	 	id : "1213"
 	 }, { 
	 	name : "en verkstad",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1142", "1176", "1270"],
	 	language_ID : "1",
	 	id : "1214"
 	 }, { 
	 	name : "en paus",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1143", "1177", "1271", "1272"],
	 	language_ID : "1",
	 	id : "1215"
 	 }, { 
	 	name : "ett skrivbord",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1144", "1178", "1273"],
	 	language_ID : "1",
	 	id : "1216"
 	 }, { 
	 	name : "ett verktyg",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1145", "1179", "1274"],
	 	language_ID : "1",
	 	id : "1217"
 	 }, { 
	 	name : "en kund",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1146", "1180"],
	 	language_ID : "1",
	 	id : "1218"
 	 }, { 
	 	name : "en klient",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1147", "1181", "1852", "3130"],
	 	language_ID : "1",
	 	id : "1219"
 	 }, { 
	 	name : "en chef",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1148", "1182", "1221", "1276", "3131"],
	 	language_ID : "1",
	 	id : "1220"
 	 }, { 
	 	name : "en ledare",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1148", "1182", "1220", "1276", "3131"],
	 	language_ID : "1",
	 	id : "1221"
 	 }, { 
	 	name : "en ingenjör",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1149", "1183", "1277", "3105", "3106"],
	 	language_ID : "1",
	 	id : "1222"
 	 }, { 
	 	name : "en psykolog",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1150", "1184", "1278", "3107", "3108"],
	 	language_ID : "1",
	 	id : "1223"
 	 }, { 
	 	name : "en lastbilschaufför",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1151", "1185", "1279", "3109", "3132"],
	 	language_ID : "1",
	 	id : "1224"
 	 }, { 
	 	name : "en läkare",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1152", "1186", "1187", "1280", "3110", "3133"],
	 	language_ID : "1",
	 	id : "1225"
 	 }, { 
	 	name : "en arbetare",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1153", "1188", "1281", "3111", "3134"],
	 	language_ID : "1",
	 	id : "1226"
 	 }, { 
	 	name : "en brandman",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1155", "1190", "1283", "3112", "3136"],
	 	language_ID : "1",
	 	id : "1227",
	 	tags : [],
	 	formPrimaryName : "manlig",
	 	formsPrimary_IDs : ["3124"]
 	 }, { 
	 	name : "en polisman",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1156", "1191", "1284", "3113", "3137"],
	 	language_ID : "1",
	 	id : "1228",
	 	tags : [],
	 	formPrimaryName : "manlig",
	 	formsPrimary_IDs : ["3125", "3126"]
 	 }, { 
	 	name : "en mekaniker",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1157", "1192", "1854", "3114", "3215"],
	 	language_ID : "1",
	 	id : "1229"
 	 }, { 
	 	name : "en advokat",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1159", "1194", "1848", "3116", "3210"],
	 	language_ID : "1",
	 	id : "1230",
	 	tags : []
 	 }, { 
	 	name : "en präst",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1160", "1195", "1856", "3117", "3216"],
	 	language_ID : "1",
	 	id : "1231"
 	 }, { 
	 	name : "en journalist",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1161", "1196", "1851", "3118", "3213"],
	 	language_ID : "1",
	 	id : "1232"
 	 }, { 
	 	name : "en taxichaufför",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1162", "1197", "1858", "3119", "3218"],
	 	language_ID : "1",
	 	id : "1233"
 	 }, { 
	 	name : "en politiker",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1163", "1198", "1857", "3120", "3217"],
	 	language_ID : "1",
	 	id : "1234"
 	 }, { 
	 	name : "en stol",
	 	categories_IDs : ["19", "61"],
	 	related_IDs : ["1164", "1199", "1855"],
	 	language_ID : "1",
	 	id : "1235"
 	 }, { 
	 	name : "en besökare",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1165", "1200", "1850", "3121", "3212"],
	 	language_ID : "1",
	 	id : "1236"
 	 }, { 
	 	name : "en sjuksköterska",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1201", "1853", "3122", "3214"],
	 	language_ID : "1",
	 	id : "1237",
	 	tags : [],
	 	formPrimaryName : "kvinnlig",
	 	formsPrimary_IDs : ["3127"]
 	 }, { 
	 	name : "en lön",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1167", "1202", "1203", "1846"],
	 	language_ID : "1",
	 	id : "1239"
 	 }, { 
	 	name : "en pension",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1168", "1204", "1859"],
	 	language_ID : "1",
	 	id : "1240"
 	 }, { 
	 	name : "en ämbetsman",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1170", "1205", "1849", "3123", "3211"],
	 	language_ID : "1",
	 	id : "1241"
 	 }, { 
	 	name : "plötzlich",
	 	categories_IDs : ["39"],
	 	related_IDs : ["1244", "1286", "1794"],
	 	language_ID : "2",
	 	id : "1243"
 	 }, { 
	 	name : "plötsligt",
	 	categories_IDs : ["39"],
	 	related_IDs : ["1243", "1286", "1794"],
	 	language_ID : "1",
	 	id : "1244"
 	 }, { 
	 	name : "weil",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1246", "1726", "QUBj22S8UA2i"],
	 	language_ID : "2",
	 	id : "1245"
 	 }, { 
	 	name : "eftersom",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1245", "1726", "QUBj22S8UA2i"],
	 	language_ID : "1",
	 	id : "1246"
 	 }, { 
	 	name : "och",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1714", "77", "95"],
	 	language_ID : "1",
	 	id : "1247"
 	 }, { 
	 	name : "eller",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1715", "78", "96"],
	 	language_ID : "1",
	 	id : "1248"
 	 }, { 
	 	name : "då",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1718", "80", "98"],
	 	language_ID : "1",
	 	id : "1249"
 	 }, { 
	 	name : "wegen",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1251", "1725", "QUBj22S8UA2i"],
	 	language_ID : "2",
	 	id : "1250",
	 	tags : []
 	 }, { 
	 	name : "därför",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1250", "1725"],
	 	language_ID : "1",
	 	id : "1251"
 	 }, { 
	 	name : "från",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1253", "1254", "1724", "2878", "3128"],
	 	language_ID : "1",
	 	id : "1252"
 	 }, { 
	 	name : "von",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1252", "1254", "1724", "2878", "3128"],
	 	language_ID : "2",
	 	id : "1253",
	 	tags : []
 	 }, { 
	 	name : "för att",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1252", "1253", "1724", "2878", "3128"],
	 	language_ID : "1",
	 	id : "1254"
 	 }, { 
	 	name : "mit",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1256", "1722", "2865", "2936"],
	 	language_ID : "2",
	 	id : "1255"
 	 }, { 
	 	name : "med",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1255", "1722", "2865"],
	 	language_ID : "1",
	 	id : "1256"
 	 }, { 
	 	name : "auch",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1258", "1717", "2859"],
	 	language_ID : "2",
	 	id : "1257"
 	 }, { 
	 	name : "också",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1257", "1717", "2859"],
	 	language_ID : "1",
	 	id : "1258"
 	 }, { 
	 	name : "um",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1260", "3129"],
	 	language_ID : "2",
	 	id : "1259",
	 	tags : []
 	 }, { 
	 	name : "om",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1259", "3129"],
	 	language_ID : "1",
	 	id : "1260"
 	 }, { 
	 	name : "på",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1262"],
	 	language_ID : "1",
	 	id : "1261"
 	 }, { 
	 	name : "auf",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1261"],
	 	language_ID : "2",
	 	id : "1262",
	 	tags : []
 	 }, { 
	 	name : "ohne",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1264", "1723", "2866"],
	 	language_ID : "2",
	 	id : "1263"
 	 }, { 
	 	name : "utan",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1263", "1723", "2866"],
	 	language_ID : "1",
	 	id : "1264"
 	 }, { 
	 	name : "la oficina",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1137", "1171", "1209"],
	 	language_ID : "5",
	 	id : "1265"
 	 }, { 
	 	name : "el lápiz",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1138", "1172", "1210"],
	 	language_ID : "5",
	 	id : "1266"
 	 }, { 
	 	name : "el bolígrafo",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1139", "1173", "1211"],
	 	language_ID : "5",
	 	id : "1267"
 	 }, { 
	 	name : "el ordenador",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1140", "1174", "1212"],
	 	language_ID : "5",
	 	id : "1268"
 	 }, { 
	 	name : "el teclado",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1141", "1175", "1213"],
	 	language_ID : "5",
	 	id : "1269"
 	 }, { 
	 	name : "el taller",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1142", "1176", "1214"],
	 	language_ID : "5",
	 	id : "1270"
 	 }, { 
	 	name : "la pausa",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1143", "1177", "1215", "1272"],
	 	language_ID : "5",
	 	id : "1271"
 	 }, { 
	 	name : "el descanso",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1143", "1177", "1215", "1271"],
	 	language_ID : "5",
	 	id : "1272"
 	 }, { 
	 	name : "el escritorio",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1144", "1178", "1216"],
	 	language_ID : "5",
	 	id : "1273"
 	 }, { 
	 	name : "la herramienta",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1145", "1179", "1217"],
	 	language_ID : "5",
	 	id : "1274"
 	 }, { 
	 	name : "la jefa",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1182", "1220", "1221", "3104"],
	 	language_ID : "5",
	 	id : "1276",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3131"]
 	 }, { 
	 	name : "el ingeniero",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1149", "1183", "1222", "3105", "3106"],
	 	language_ID : "5",
	 	id : "1277",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3105"]
 	 }, { 
	 	name : "el psicólogo",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1150", "1184", "1223"],
	 	language_ID : "5",
	 	id : "1278",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3108"]
 	 }, { 
	 	name : "la camionera",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1185", "1224", "3109"],
	 	language_ID : "5",
	 	id : "1279",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3132"]
 	 }, { 
	 	name : "el médico",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1152", "1186", "1187", "1225"],
	 	language_ID : "5",
	 	id : "1280",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3133"]
 	 }, { 
	 	name : "la trabajadora",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1188", "1226", "3111"],
	 	language_ID : "5",
	 	id : "1281",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3134"]
 	 }, { 
	 	name : "el limpiador",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1154", "1189"],
	 	language_ID : "5",
	 	id : "1282",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3135"]
 	 }, { 
	 	name : "la bombera",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1190", "3112", "3124"],
	 	language_ID : "5",
	 	id : "1283",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3136"]
 	 }, { 
	 	name : "el oficial de policía",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1156", "1191", "1228", "3113", "3125", "3126"],
	 	language_ID : "5",
	 	id : "1284",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3137"]
 	 }, { 
	 	name : "yesterday evening",
	 	categories_IDs : ["39"],
	 	related_IDs : ["880", "881", "913"],
	 	language_ID : "3",
	 	id : "1285",
	 	tags : []
 	 }, { 
	 	name : "suddenly",
	 	categories_IDs : ["39"],
	 	related_IDs : ["1243", "1244", "1794", "Srv3VLlPKcJG"],
	 	language_ID : "3",
	 	id : "1286",
	 	tags : []
 	 }, { 
	 	name : "the weekend",
	 	categories_IDs : ["39"],
	 	related_IDs : ["1288", "1289", "1792"],
	 	language_ID : "3",
	 	id : "1287"
 	 }, { 
	 	name : "das Wochenende",
	 	categories_IDs : ["39"],
	 	related_IDs : ["1287", "1289", "1792"],
	 	language_ID : "2",
	 	id : "1288"
 	 }, { 
	 	name : "en helg",
	 	categories_IDs : ["39"],
	 	related_IDs : ["1287", "1288", "1792"],
	 	language_ID : "1",
	 	id : "1289"
 	 }, { 
	 	name : "soccer",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1291", "1316", "1332"],
	 	language_ID : "3",
	 	id : "1290"
 	 }, { 
	 	name : "der Fußball",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1290", "1316", "1332", "2896"],
	 	language_ID : "2",
	 	id : "1291"
 	 }, { 
	 	name : "tennis",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1293", "1317", "1333"],
	 	language_ID : "3",
	 	id : "1292"
 	 }, { 
	 	name : "Tennis",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1292", "1317", "1333"],
	 	language_ID : "2",
	 	id : "1293"
 	 }, { 
	 	name : "swim",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1295", "1318", "1334", "hzrVmbKR7pQp"],
	 	language_ID : "3",
	 	id : "1294",
	 	tags : []
 	 }, { 
	 	name : "schwimmen",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1294", "1318", "1334"],
	 	language_ID : "2",
	 	id : "1295"
 	 }, { 
	 	name : "play",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1297", "1319", "1336"],
	 	language_ID : "3",
	 	id : "1296"
 	 }, { 
	 	name : "spielen",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1296", "1319", "1336", "2896"],
	 	language_ID : "2",
	 	id : "1297"
 	 }, { 
	 	name : "trainieren",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1320", "1335", "2626"],
	 	language_ID : "2",
	 	id : "1299"
 	 }, { 
	 	name : "hunt",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1301", "1323", "1337"],
	 	language_ID : "3",
	 	id : "1300"
 	 }, { 
	 	name : "jagen",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1300", "1323", "1337"],
	 	language_ID : "2",
	 	id : "1301"
 	 }, { 
	 	name : "win",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1303", "1325", "1338"],
	 	language_ID : "3",
	 	id : "1302"
 	 }, { 
	 	name : "gewinnen",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1302", "1325", "1338"],
	 	language_ID : "2",
	 	id : "1303"
 	 }, { 
	 	name : "verlieren",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1305", "1326", "1339"],
	 	language_ID : "2",
	 	id : "1304"
 	 }, { 
	 	name : "lose",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1304", "1326", "1339", "4vyQrhoTvwyQ"],
	 	language_ID : "3",
	 	id : "1305",
	 	tags : []
 	 }, { 
	 	name : "fight",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1307", "1327", "1340", "1341"],
	 	language_ID : "3",
	 	id : "1306"
 	 }, { 
	 	name : "kämpfen",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1306", "1327", "1340", "1341"],
	 	language_ID : "2",
	 	id : "1307"
 	 }, { 
	 	name : "surrender",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1309", "1328", "1342", "3138"],
	 	language_ID : "3",
	 	id : "1308"
 	 }, { 
	 	name : "aufgeben",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1308", "1328", "1342", "3138"],
	 	language_ID : "2",
	 	id : "1309"
 	 }, { 
	 	name : "the ball",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1311", "1329", "1343"],
	 	language_ID : "3",
	 	id : "1310"
 	 }, { 
	 	name : "der Ball",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1310", "1329", "1343"],
	 	language_ID : "2",
	 	id : "1311"
 	 }, { 
	 	name : "the goal",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1313", "1330", "1344", "eWTbPSMJpkMF"],
	 	language_ID : "3",
	 	id : "1312",
	 	tags : []
 	 }, { 
	 	name : "das Ziel",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1312", "1330", "1344"],
	 	language_ID : "2",
	 	id : "1313"
 	 }, { 
	 	name : "run",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1315", "1331", "1345", "3011"],
	 	language_ID : "3",
	 	id : "1314"
 	 }, { 
	 	name : "laufen",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1314", "1331", "1345", "3011"],
	 	language_ID : "2",
	 	id : "1315"
 	 }, { 
	 	name : "el fútbol",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1290", "1291", "1332"],
	 	language_ID : "5",
	 	id : "1316"
 	 }, { 
	 	name : "el tenis",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1292", "1293", "1333"],
	 	language_ID : "5",
	 	id : "1317"
 	 }, { 
	 	name : "nadar",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1294", "1295", "1334"],
	 	language_ID : "5",
	 	id : "1318"
 	 }, { 
	 	name : "jugar",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1296", "1297", "1336"],
	 	language_ID : "5",
	 	id : "1319"
 	 }, { 
	 	name : "entrenarse",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1299", "1335", "2626"],
	 	language_ID : "5",
	 	id : "1320"
 	 }, { 
	 	name : "la copiadora",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1322", "2589", "2632"],
	 	language_ID : "5",
	 	id : "1321"
 	 }, { 
	 	name : "der Kopierer",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1321", "2589", "2632"],
	 	language_ID : "2",
	 	id : "1322"
 	 }, { 
	 	name : "cazar",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1300", "1301", "1337"],
	 	language_ID : "5",
	 	id : "1323"
 	 }, { 
	 	name : "ganar",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1302", "1303", "1338"],
	 	language_ID : "5",
	 	id : "1325"
 	 }, { 
	 	name : "perder",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1304", "1305", "1339"],
	 	language_ID : "5",
	 	id : "1326"
 	 }, { 
	 	name : "luchar",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1306", "1307", "1340", "1341"],
	 	language_ID : "5",
	 	id : "1327"
 	 }, { 
	 	name : "abandonar",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1308", "1309", "1342", "3138"],
	 	language_ID : "5",
	 	id : "1328"
 	 }, { 
	 	name : "la pelota",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1310", "1311", "1343"],
	 	language_ID : "5",
	 	id : "1329"
 	 }, { 
	 	name : "la meta",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1312", "1313", "1344"],
	 	language_ID : "5",
	 	id : "1330"
 	 }, { 
	 	name : "correr",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1314", "1315", "1345", "3011"],
	 	language_ID : "5",
	 	id : "1331"
 	 }, { 
	 	name : "en fotball",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1290", "1291", "1316"],
	 	language_ID : "1",
	 	id : "1332"
 	 }, { 
	 	name : "en tennis",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1292", "1293", "1317"],
	 	language_ID : "1",
	 	id : "1333"
 	 }, { 
	 	name : "simma",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1294", "1295", "1318"],
	 	language_ID : "1",
	 	id : "1334"
 	 }, { 
	 	name : "träna",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1299", "1320", "2626"],
	 	language_ID : "1",
	 	id : "1335"
 	 }, { 
	 	name : "spela",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1296", "1297", "1319"],
	 	language_ID : "1",
	 	id : "1336"
 	 }, { 
	 	name : "jaga",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1300", "1301", "1323"],
	 	language_ID : "1",
	 	id : "1337"
 	 }, { 
	 	name : "vinna",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1302", "1303", "1325"],
	 	language_ID : "1",
	 	id : "1338"
 	 }, { 
	 	name : "förlora",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1304", "1305", "1326"],
	 	language_ID : "1",
	 	id : "1339"
 	 }, { 
	 	name : "kämpa",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1306", "1307", "1327", "1341"],
	 	language_ID : "1",
	 	id : "1340"
 	 }, { 
	 	name : "tävla",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1306", "1307", "1327", "1340"],
	 	language_ID : "1",
	 	id : "1341"
 	 }, { 
	 	name : "ge upp",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1308", "1309", "1328", "3138"],
	 	language_ID : "1",
	 	id : "1342",
	 	tags : []
 	 }, { 
	 	name : "en boll",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1310", "1311", "1329"],
	 	language_ID : "1",
	 	id : "1343"
 	 }, { 
	 	name : "ett mål",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1312", "1313", "1330"],
	 	language_ID : "1",
	 	id : "1344"
 	 }, { 
	 	name : "springa",
	 	categories_IDs : ["42"],
	 	related_IDs : ["1314", "1315", "1331", "3011"],
	 	language_ID : "1",
	 	id : "1345"
 	 }, { 
	 	name : "das Gefühl",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1362", "1378", "1394"],
	 	language_ID : "2",
	 	id : "1346"
 	 }, { 
	 	name : "fröhlich",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1363", "1379", "1395", "2993"],
	 	language_ID : "2",
	 	id : "1347"
 	 }, { 
	 	name : "traurig",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1364", "1380", "1397", "3139"],
	 	language_ID : "2",
	 	id : "1348"
 	 }, { 
	 	name : "verärgert",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1365", "1381", "1398", "3142"],
	 	language_ID : "2",
	 	id : "1349"
 	 }, { 
	 	name : "wütend",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1366", "1382", "1399", "3140", "3143"],
	 	language_ID : "2",
	 	id : "1350"
 	 }, { 
	 	name : "ruhig",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1367", "1383", "1400", "1401", "2999", "3144", "3145"],
	 	language_ID : "2",
	 	id : "1351"
 	 }, { 
	 	name : "aufgeregt",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1368", "1384", "1402", "3146"],
	 	language_ID : "2",
	 	id : "1352"
 	 }, { 
	 	name : "die Freude",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1369", "1385", "1403", "3141"],
	 	language_ID : "2",
	 	id : "1353"
 	 }, { 
	 	name : "der Ärger",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1370", "1386", "1404", "3147"],
	 	language_ID : "2",
	 	id : "1354"
 	 }, { 
	 	name : "die Trauer",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1371", "1387", "1396"],
	 	language_ID : "2",
	 	id : "1355"
 	 }, { 
	 	name : "der Ekel",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1372", "1388", "1405"],
	 	language_ID : "2",
	 	id : "1356"
 	 }, { 
	 	name : "der Schock",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1373", "1389", "1406", "3148"],
	 	language_ID : "2",
	 	id : "1357"
 	 }, { 
	 	name : "die Hoffnung",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1374", "1390", "1407"],
	 	language_ID : "2",
	 	id : "1358"
 	 }, { 
	 	name : "die Enttäuschung",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1375", "1391", "1408"],
	 	language_ID : "2",
	 	id : "1359"
 	 }, { 
	 	name : "enttäuscht",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1376", "1392", "1409", "3149"],
	 	language_ID : "2",
	 	id : "1360"
 	 }, { 
	 	name : "gelangweilt",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1377", "1393", "1410", "3150"],
	 	language_ID : "2",
	 	id : "1361"
 	 }, { 
	 	name : "en känsel",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1346", "1378", "1394"],
	 	language_ID : "1",
	 	id : "1362"
 	 }, { 
	 	name : "glad",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1347", "1379", "1395", "2993"],
	 	language_ID : "1",
	 	id : "1363"
 	 }, { 
	 	name : "sorglig",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1348", "1380", "1397", "3139"],
	 	language_ID : "1",
	 	id : "1364",
	 	tags : []
 	 }, { 
	 	name : "förargad",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1349", "1381", "1398", "3142"],
	 	language_ID : "1",
	 	id : "1365"
 	 }, { 
	 	name : "rasande",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1350", "1382", "1399", "3140", "3143"],
	 	language_ID : "1",
	 	id : "1366",
	 	tags : []
 	 }, { 
	 	name : "lugn",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1351", "1383", "1400", "1401", "2999", "3144", "3145"],
	 	language_ID : "1",
	 	id : "1367"
 	 }, { 
	 	name : "upprörd",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1352", "1384", "1402", "3146"],
	 	language_ID : "1",
	 	id : "1368"
 	 }, { 
	 	name : "en glädje",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1353", "1385", "1403", "3141"],
	 	language_ID : "1",
	 	id : "1369",
	 	tags : []
 	 }, { 
	 	name : "en förargelse",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1354", "1386", "1404", "3147"],
	 	language_ID : "1",
	 	id : "1370"
 	 }, { 
	 	name : "en sorg",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1355", "1387", "1396"],
	 	language_ID : "1",
	 	id : "1371"
 	 }, { 
	 	name : "ett äckel",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1356", "1388", "1405"],
	 	language_ID : "1",
	 	id : "1372"
 	 }, { 
	 	name : "en chock",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1357", "1389", "1406", "3148"],
	 	language_ID : "1",
	 	id : "1373"
 	 }, { 
	 	name : "ett hopp",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1358", "1390", "1407"],
	 	language_ID : "1",
	 	id : "1374"
 	 }, { 
	 	name : "en besvikelse",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1359", "1391", "1408"],
	 	language_ID : "1",
	 	id : "1375"
 	 }, { 
	 	name : "besviken",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1360", "1392", "1409", "3149"],
	 	language_ID : "1",
	 	id : "1376"
 	 }, { 
	 	name : "uttråkad",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1361", "1393", "1410", "3150"],
	 	language_ID : "1",
	 	id : "1377"
 	 }, { 
	 	name : "the feeling",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1346", "1362", "1394"],
	 	language_ID : "3",
	 	id : "1378"
 	 }, { 
	 	name : "happy",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1347", "1363", "1395", "2993", "xeNAtBikFlUR"],
	 	language_ID : "3",
	 	id : "1379",
	 	tags : []
 	 }, { 
	 	name : "sad",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1348", "1364", "1397", "3139", "CozUsivICGV7"],
	 	language_ID : "3",
	 	id : "1380",
	 	tags : []
 	 }, { 
	 	name : "angered",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1349", "1365", "1398", "3142"],
	 	language_ID : "3",
	 	id : "1381"
 	 }, { 
	 	name : "mad",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1350", "1366", "1399", "3140", "3143"],
	 	language_ID : "3",
	 	id : "1382",
	 	tags : []
 	 }, { 
	 	name : "calm",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1351", "1367", "1400", "1401", "2999", "3144", "3145", "DMaBnj7oS5mz", "UEbo6iw4rkOn"],
	 	language_ID : "3",
	 	id : "1383",
	 	tags : []
 	 }, { 
	 	name : "agitated",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1352", "1368", "1402", "3146"],
	 	language_ID : "3",
	 	id : "1384"
 	 }, { 
	 	name : "the happiness",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1353", "1369", "1403", "3141"],
	 	language_ID : "3",
	 	id : "1385"
 	 }, { 
	 	name : "the anger",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1354", "1370", "1404", "3147", "sR85kNXY8xR3"],
	 	language_ID : "3",
	 	id : "1386",
	 	tags : []
 	 }, { 
	 	name : "the grief",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1355", "1371", "1396", "uieOnYFRF3s5"],
	 	language_ID : "3",
	 	id : "1387",
	 	tags : []
 	 }, { 
	 	name : "the disgust",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1356", "1372", "1405"],
	 	language_ID : "3",
	 	id : "1388"
 	 }, { 
	 	name : "the shock",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1357", "1373", "1406", "3148"],
	 	language_ID : "3",
	 	id : "1389"
 	 }, { 
	 	name : "the hope",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1358", "1374", "1407", "OV3l2opFocOy"],
	 	language_ID : "3",
	 	id : "1390",
	 	tags : []
 	 }, { 
	 	name : "the disappointment",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1359", "1375", "1408", "fxxEdDvOuEBQ"],
	 	language_ID : "3",
	 	id : "1391",
	 	tags : []
 	 }, { 
	 	name : "disappointed",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1360", "1376", "1409", "3149"],
	 	language_ID : "3",
	 	id : "1392"
 	 }, { 
	 	name : "bored",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1361", "1377", "1410", "3150", "4eMh9C0aOsyk"],
	 	language_ID : "3",
	 	id : "1393",
	 	tags : []
 	 }, { 
	 	name : "el sentimiento",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1346", "1362", "1378"],
	 	language_ID : "5",
	 	id : "1394"
 	 }, { 
	 	name : "alegre",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1347", "1363", "1379", "2993"],
	 	language_ID : "5",
	 	id : "1395"
 	 }, { 
	 	name : "el luto",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1355", "1371", "1387"],
	 	language_ID : "5",
	 	id : "1396"
 	 }, { 
	 	name : "triste",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1348", "1364", "1380", "3139"],
	 	language_ID : "5",
	 	id : "1397"
 	 }, { 
	 	name : "enfadado",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1349", "1365", "1381"],
	 	language_ID : "5",
	 	id : "1398",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3142"]
 	 }, { 
	 	name : "furioso",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1350", "1366", "1382", "3140"],
	 	language_ID : "5",
	 	id : "1399",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3143"]
 	 }, { 
	 	name : "tranquila",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1351", "1367", "1383", "1401", "2999", "3145"],
	 	language_ID : "5",
	 	id : "1400",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3144"]
 	 }, { 
	 	categories_IDs : ["43"],
	 	related_IDs : ["1351", "1367", "1383", "1400", "2999", "3144"],
	 	language_ID : "5",
	 	name : "calmada",
	 	id : "1401",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3145"]
 	 }, { 
	 	name : "excitado",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1352", "1368", "1384"],
	 	language_ID : "5",
	 	id : "1402",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3146"]
 	 }, { 
	 	name : "la alegría",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1353", "1369", "1385", "3141"],
	 	language_ID : "5",
	 	id : "1403"
 	 }, { 
	 	name : "el disgusto",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1354", "1370", "1386", "3147"],
	 	language_ID : "5",
	 	id : "1404",
	 	tags : []
 	 }, { 
	 	name : "el asco",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1356", "1372", "1388"],
	 	language_ID : "5",
	 	id : "1405"
 	 }, { 
	 	name : "el shock",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1357", "1373", "1389", "3148"],
	 	language_ID : "5",
	 	id : "1406",
	 	tags : []
 	 }, { 
	 	name : "la esperanza",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1358", "1374", "1390"],
	 	language_ID : "5",
	 	id : "1407"
 	 }, { 
	 	name : "la decepción",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1359", "1375", "1391"],
	 	language_ID : "5",
	 	id : "1408"
 	 }, { 
	 	name : "decepcionado",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1360", "1376", "1392"],
	 	language_ID : "5",
	 	id : "1409",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3149"]
 	 }, { 
	 	name : "aburrida",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1361", "1377", "1393"],
	 	language_ID : "5",
	 	id : "1410",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3150"]
 	 }, { 
	 	related_IDs : ["1412", "1413", "1414"],
	 	categories_IDs : ["36"],
	 	name : "en kategori",
	 	language_ID : "1",
	 	id : "1411"
 	 }, { 
	 	categories_IDs : ["36"],
	 	related_IDs : ["1411", "1413", "1414"],
	 	language_ID : "2",
	 	name : "die Kategorie",
	 	id : "1412"
 	 }, { 
	 	categories_IDs : ["36"],
	 	related_IDs : ["1411", "1412", "1414"],
	 	name : "the category",
	 	language_ID : "3",
	 	id : "1413"
 	 }, { 
	 	categories_IDs : ["36"],
	 	related_IDs : ["1411", "1412", "1413"],
	 	name : "la categoría",
	 	language_ID : "5",
	 	id : "1414"
 	 }, { 
	 	name : "die Hand",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1427", "1446", "1461"],
	 	language_ID : "2",
	 	id : "1415"
 	 }, { 
	 	name : "der Fuß",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1428", "1447", "1462", "2819"],
	 	language_ID : "2",
	 	id : "1416"
 	 }, { 
	 	name : "der Arm",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1429", "1448", "1463"],
	 	language_ID : "2",
	 	id : "1417"
 	 }, { 
	 	name : "der Kopf",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1430", "1449", "1464"],
	 	language_ID : "2",
	 	id : "1418"
 	 }, { 
	 	name : "das Bein",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1431", "1450", "1465"],
	 	language_ID : "2",
	 	id : "1419"
 	 }, { 
	 	name : "das Gesicht",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1432", "1451", "1466"],
	 	language_ID : "2",
	 	id : "1420"
 	 }, { 
	 	name : "der Magen",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1434", "1452", "1467"],
	 	language_ID : "2",
	 	id : "1421"
 	 }, { 
	 	name : "die Nase",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1435", "1453", "1468"],
	 	language_ID : "2",
	 	id : "1422"
 	 }, { 
	 	name : "das Auge",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1437", "1454", "1469"],
	 	language_ID : "2",
	 	id : "1423"
 	 }, { 
	 	name : "krank",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1438", "1455", "1470", "3151"],
	 	language_ID : "2",
	 	id : "1424"
 	 }, { 
	 	name : "gesund",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1439", "1456", "1471", "3152"],
	 	language_ID : "2",
	 	id : "1425"
 	 }, { 
	 	name : "der Schmerz",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1440", "1457", "1472"],
	 	language_ID : "2",
	 	id : "1426"
 	 }, { 
	 	name : "the hand",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1415", "1446", "1461", "yc8Ra2mqxIHK"],
	 	language_ID : "3",
	 	id : "1427"
 	 }, { 
	 	name : "the foot",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1416", "1447", "1462", "2820"],
	 	language_ID : "3",
	 	id : "1428"
 	 }, { 
	 	name : "the arm",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1417", "1448", "1463", "fovqnxLUd0X5"],
	 	language_ID : "3",
	 	id : "1429",
	 	tags : []
 	 }, { 
	 	name : "the head",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1418", "1449", "1464", "bIcMkJoAKeec"],
	 	language_ID : "3",
	 	id : "1430"
 	 }, { 
	 	name : "the leg",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1419", "1450", "1465", "oBClSiAajLmh"],
	 	language_ID : "3",
	 	id : "1431",
	 	tags : []
 	 }, { 
	 	name : "the face",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1420", "1451", "1466", "xMoFW80sLB1J"],
	 	language_ID : "3",
	 	id : "1432",
	 	tags : []
 	 }, { 
	 	name : "der Rücken",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1441", "1458", "1473"],
	 	language_ID : "2",
	 	id : "1433"
 	 }, { 
	 	name : "the stomach",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1421", "1452", "1467"],
	 	language_ID : "3",
	 	id : "1434"
 	 }, { 
	 	name : "the nose",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1422", "1453", "1468"],
	 	language_ID : "3",
	 	id : "1435"
 	 }, { 
	 	name : "der Mund",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1442", "1459", "1474"],
	 	language_ID : "2",
	 	id : "1436"
 	 }, { 
	 	name : "the eye",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1423", "1454", "1469"],
	 	language_ID : "3",
	 	id : "1437"
 	 }, { 
	 	name : "sick",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1424", "1455", "1470", "3151", "6plnLSArTzq4"],
	 	language_ID : "3",
	 	id : "1438",
	 	tags : []
 	 }, { 
	 	name : "healthy",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1425", "1456", "1471", "3152"],
	 	language_ID : "3",
	 	id : "1439"
 	 }, { 
	 	name : "the pain",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1426", "1457", "1472"],
	 	language_ID : "3",
	 	id : "1440"
 	 }, { 
	 	name : "the back",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1433", "1458", "1473"],
	 	language_ID : "3",
	 	id : "1441"
 	 }, { 
	 	name : "the mouth",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1436", "1459", "1474"],
	 	language_ID : "3",
	 	id : "1442"
 	 }, { 
	 	name : "das Ohr",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1445", "1460", "1475"],
	 	language_ID : "2",
	 	id : "1444"
 	 }, { 
	 	name : "the ear",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1444", "1460", "1475"],
	 	language_ID : "3",
	 	id : "1445"
 	 }, { 
	 	name : "la mano",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1415", "1427", "1461"],
	 	language_ID : "5",
	 	id : "1446"
 	 }, { 
	 	name : "el pie",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1416", "1428", "1462", "2821"],
	 	language_ID : "5",
	 	id : "1447"
 	 }, { 
	 	name : "el brazo",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1417", "1429", "1463"],
	 	language_ID : "5",
	 	id : "1448"
 	 }, { 
	 	name : "la cabeza",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1418", "1430", "1464", "2849"],
	 	language_ID : "5",
	 	id : "1449"
 	 }, { 
	 	name : "la pierna",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1419", "1431", "1465"],
	 	language_ID : "5",
	 	id : "1450"
 	 }, { 
	 	name : "la cara",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1420", "1432", "1466"],
	 	language_ID : "5",
	 	id : "1451"
 	 }, { 
	 	name : "el estómago",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1421", "1434", "1467"],
	 	language_ID : "5",
	 	id : "1452"
 	 }, { 
	 	name : "la naríz",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1422", "1435", "1468"],
	 	language_ID : "5",
	 	id : "1453"
 	 }, { 
	 	name : "el ojo",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1423", "1437", "1469"],
	 	language_ID : "5",
	 	id : "1454"
 	 }, { 
	 	name : "enfermo",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1424", "1438", "1470"],
	 	language_ID : "5",
	 	id : "1455",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3151"]
 	 }, { 
	 	name : "sana",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1425", "1439", "1471"],
	 	language_ID : "5",
	 	id : "1456",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3152"]
 	 }, { 
	 	name : "el dolor",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1426", "1440", "1472", "2849"],
	 	language_ID : "5",
	 	id : "1457"
 	 }, { 
	 	name : "la espalda",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1433", "1441", "1473"],
	 	language_ID : "5",
	 	id : "1458"
 	 }, { 
	 	name : "la boca",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1436", "1442", "1474"],
	 	language_ID : "5",
	 	id : "1459"
 	 }, { 
	 	name : "la oreja",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1444", "1445", "1475"],
	 	language_ID : "5",
	 	id : "1460"
 	 }, { 
	 	name : "en hand",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1415", "1427", "1446"],
	 	language_ID : "1",
	 	id : "1461"
 	 }, { 
	 	name : "en fot",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1416", "1428", "1447", "2822"],
	 	language_ID : "1",
	 	id : "1462"
 	 }, { 
	 	name : "en arm",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1417", "1429", "1448"],
	 	language_ID : "1",
	 	id : "1463"
 	 }, { 
	 	name : "ett huvud",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1418", "1430", "1449"],
	 	language_ID : "1",
	 	id : "1464"
 	 }, { 
	 	name : "ett ben",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1419", "1431", "1450"],
	 	language_ID : "1",
	 	id : "1465"
 	 }, { 
	 	name : "ett ansikte",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1420", "1432", "1451"],
	 	language_ID : "1",
	 	id : "1466"
 	 }, { 
	 	name : "en mage",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1421", "1434", "1452"],
	 	language_ID : "1",
	 	id : "1467"
 	 }, { 
	 	name : "en näsa",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1422", "1435", "1453"],
	 	language_ID : "1",
	 	id : "1468"
 	 }, { 
	 	name : "ett öga",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1423", "1437", "1454"],
	 	language_ID : "1",
	 	id : "1469"
 	 }, { 
	 	name : "sjuk",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1424", "1438", "1455", "3151"],
	 	language_ID : "1",
	 	id : "1470"
 	 }, { 
	 	name : "frisk",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1425", "1439", "1456", "3152"],
	 	language_ID : "1",
	 	id : "1471"
 	 }, { 
	 	name : "en smärta",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1426", "1440", "1457"],
	 	language_ID : "1",
	 	id : "1472"
 	 }, { 
	 	name : "en rygg",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1433", "1441", "1458"],
	 	language_ID : "1",
	 	id : "1473"
 	 }, { 
	 	name : "en mun",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1436", "1442", "1459"],
	 	language_ID : "1",
	 	id : "1474"
 	 }, { 
	 	name : "ett öra",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1444", "1445", "1460"],
	 	language_ID : "1",
	 	id : "1475"
 	 }, { 
	 	name : "die Beziehung",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1497", "1518", "1537"],
	 	language_ID : "2",
	 	id : "1476"
 	 }, { 
	 	name : "die Liebe",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1498", "1519", "1538"],
	 	language_ID : "2",
	 	id : "1477"
 	 }, { 
	 	name : "der Streit",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1499", "1539", "2975", "2976", "2977", "2979"],
	 	language_ID : "2",
	 	id : "1478"
 	 }, { 
	 	name : "die Umarmung",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1500", "1521", "1540", "3157"],
	 	language_ID : "2",
	 	id : "1479"
 	 }, { 
	 	name : "die Freundschaft",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1501", "1522", "1541", "2916"],
	 	language_ID : "2",
	 	id : "1480"
 	 }, { 
	 	name : "umarmen",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1502", "1523", "1542", "3010"],
	 	language_ID : "2",
	 	id : "1482"
 	 }, { 
	 	name : "küssen",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1503", "1524", "1543"],
	 	language_ID : "2",
	 	id : "1483"
 	 }, { 
	 	name : "vermeiden",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1506", "1525", "1544", "3009"],
	 	language_ID : "2",
	 	id : "1484"
 	 }, { 
	 	name : "sich anfreunden",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1507", "1526", "1545", "3159", "3162"],
	 	language_ID : "2",
	 	id : "1486"
 	 }, { 
	 	name : "der Bruder",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1508", "1527", "1546", "3160"],
	 	language_ID : "2",
	 	id : "1487"
 	 }, { 
	 	name : "die Schwester",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1509", "1528", "1547"],
	 	language_ID : "2",
	 	id : "1488"
 	 }, { 
	 	name : "der Vater",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1510", "1529", "1548"],
	 	language_ID : "2",
	 	id : "1489"
 	 }, { 
	 	name : "die Mutter",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1511", "1530", "1549", "3161"],
	 	language_ID : "2",
	 	id : "1490"
 	 }, { 
	 	name : "der Onkel",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1512", "1531", "1550", "3090"],
	 	language_ID : "2",
	 	id : "1491"
 	 }, { 
	 	name : "die Tante",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1514", "1532", "1551", "3088", "3089"],
	 	language_ID : "2",
	 	id : "1492"
 	 }, { 
	 	name : "der Enkel",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1515", "1533", "3154"],
	 	language_ID : "2",
	 	id : "1493",
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3153"]
 	 }, { 
	 	name : "die Familie",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1516", "1534", "1553"],
	 	language_ID : "2",
	 	id : "1495"
 	 }, { 
	 	name : "der Partner",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1517", "1535", "1554", "3156"],
	 	language_ID : "2",
	 	id : "1496",
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3155"]
 	 }, { 
	 	name : "en förbindelse",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1476", "1518", "1537"],
	 	language_ID : "1",
	 	id : "1497"
 	 }, { 
	 	name : "en kärlek",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1477", "1519", "1538"],
	 	language_ID : "1",
	 	id : "1498"
 	 }, { 
	 	name : "en strid",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1478", "1539", "2976", "2977", "2978", "2979"],
	 	language_ID : "1",
	 	id : "1499"
 	 }, { 
	 	name : "en umfangning",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1479", "1521", "1540", "3157"],
	 	language_ID : "1",
	 	id : "1500",
	 	tags : []
 	 }, { 
	 	name : "en vänskap",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1480", "1522", "1541"],
	 	language_ID : "1",
	 	id : "1501"
 	 }, { 
	 	name : "omfamna",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1482", "1523", "1542", "3010"],
	 	language_ID : "1",
	 	id : "1502",
	 	tag : "verb"
 	 }, { 
	 	name : "kyssa",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1483", "1524", "1543"],
	 	language_ID : "1",
	 	id : "1503",
	 	tags : []
 	 }, { 
	 	name : "sich kuscheln",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1505", "1536", "1555", "3158"],
	 	language_ID : "2",
	 	id : "1504"
 	 }, { 
	 	name : "smyga sig intill",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1504", "1536", "1555", "3158"],
	 	language_ID : "1",
	 	id : "1505",
	 	tags : []
 	 }, { 
	 	name : "undvika",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1484", "1525", "1544", "3009"],
	 	language_ID : "1",
	 	id : "1506"
 	 }, { 
	 	name : "göra sig vän med ngn.",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1486", "1526", "3159"],
	 	language_ID : "1",
	 	sentence : true,
	 	id : "1507",
	 	tags : []
 	 }, { 
	 	name : "en bror",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1487", "1527", "1546", "3160"],
	 	language_ID : "1",
	 	id : "1508",
	 	tags : []
 	 }, { 
	 	name : "en syster",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1488", "1528", "1547"],
	 	language_ID : "1",
	 	id : "1509"
 	 }, { 
	 	name : "en far",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1489", "1529", "1548"],
	 	language_ID : "1",
	 	id : "1510"
 	 }, { 
	 	name : "en mor",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1490", "1530", "1549", "3161"],
	 	language_ID : "1",
	 	id : "1511",
	 	tags : []
 	 }, { 
	 	name : "en farbror",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1491", "1531", "1550", "3090"],
	 	language_ID : "1",
	 	id : "1512",
	 	tags : [],
	 	formPrimaryName : "bror till fadern",
	 	formsPrimary_IDs : ["3090"]
 	 }, { 
	 	name : "en faster",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1492", "1532", "1551", "3089"],
	 	language_ID : "1",
	 	id : "1514",
	 	tags : [],
	 	formPrimaryName : "syster till fadern",
	 	formsPrimary_IDs : ["3089"]
 	 }, { 
	 	name : "en barnbarn",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1493", "1533", "1552", "3153", "3154"],
	 	language_ID : "1",
	 	id : "1515"
 	 }, { 
	 	name : "en familj",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1495", "1534", "1553"],
	 	language_ID : "1",
	 	id : "1516"
 	 }, { 
	 	name : "en partner",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1496", "1535", "1554", "3155", "3156"],
	 	language_ID : "1",
	 	id : "1517"
 	 }, { 
	 	name : "the relationship",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1476", "1497", "1537"],
	 	language_ID : "3",
	 	id : "1518"
 	 }, { 
	 	name : "the love",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1477", "1498", "1538", "4kQ4o4ms0lcK"],
	 	language_ID : "3",
	 	id : "1519",
	 	tags : []
 	 }, { 
	 	name : "the hug",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1479", "1500", "1540", "3157"],
	 	language_ID : "3",
	 	id : "1521"
 	 }, { 
	 	name : "the friendship",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1480", "1501", "1541"],
	 	language_ID : "3",
	 	id : "1522"
 	 }, { 
	 	name : "hug",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1482", "1502", "1542", "3010"],
	 	language_ID : "3",
	 	id : "1523"
 	 }, { 
	 	name : "kiss",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1483", "1503", "1543"],
	 	language_ID : "3",
	 	id : "1524"
 	 }, { 
	 	name : "avoid",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1484", "1506", "1544", "3009"],
	 	language_ID : "3",
	 	id : "1525"
 	 }, { 
	 	name : "become friends",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1486", "1507", "1545", "3159", "3162"],
	 	language_ID : "3",
	 	id : "1526"
 	 }, { 
	 	name : "the brother",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1487", "1508", "1546", "3160"],
	 	language_ID : "3",
	 	id : "1527"
 	 }, { 
	 	name : "the sister",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1488", "1509", "1547"],
	 	language_ID : "3",
	 	id : "1528"
 	 }, { 
	 	name : "the father",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1489", "1510", "1548"],
	 	language_ID : "3",
	 	id : "1529"
 	 }, { 
	 	name : "the mother",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1490", "1511", "1549", "3161"],
	 	language_ID : "3",
	 	id : "1530"
 	 }, { 
	 	name : "the uncle",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1491", "1512", "1550", "3090"],
	 	language_ID : "3",
	 	id : "1531"
 	 }, { 
	 	name : "the aunt",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1492", "1514", "1551", "3088", "3089"],
	 	language_ID : "3",
	 	id : "1532"
 	 }, { 
	 	name : "the grandchild",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1493", "1515", "1552", "3153", "3154"],
	 	language_ID : "3",
	 	id : "1533"
 	 }, { 
	 	name : "the family",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1495", "1516", "1553"],
	 	language_ID : "3",
	 	id : "1534"
 	 }, { 
	 	name : "the partner",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1496", "1517", "1554", "3155", "3156"],
	 	language_ID : "3",
	 	id : "1535"
 	 }, { 
	 	name : "cuddle",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1504", "1505", "1555", "3158"],
	 	language_ID : "3",
	 	id : "1536"
 	 }, { 
	 	name : "la relación",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1476", "1497", "1518"],
	 	language_ID : "5",
	 	id : "1537"
 	 }, { 
	 	name : "el amor",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1477", "1498", "1519"],
	 	language_ID : "5",
	 	id : "1538"
 	 }, { 
	 	name : "la disputa",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1478", "1499", "2976", "2977", "2979"],
	 	language_ID : "5",
	 	id : "1539"
 	 }, { 
	 	name : "el abrazo",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1479", "1500", "1521", "3157"],
	 	language_ID : "5",
	 	id : "1540"
 	 }, { 
	 	name : "la amistad",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1480", "1501", "1522"],
	 	language_ID : "5",
	 	id : "1541"
 	 }, { 
	 	name : "abrazarse",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1482", "1502", "1523", "3010"],
	 	language_ID : "5",
	 	id : "1542"
 	 }, { 
	 	name : "besar",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1483", "1503", "1524"],
	 	language_ID : "5",
	 	id : "1543"
 	 }, { 
	 	name : "evitar",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1484", "1506", "1525", "3009"],
	 	language_ID : "5",
	 	id : "1544"
 	 }, { 
	 	name : "hacerse amigo de alguien",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1486", "1526", "3159"],
	 	language_ID : "5",
	 	id : "1545",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3162"]
 	 }, { 
	 	name : "el hermano",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1487", "1508", "1527", "3160"],
	 	language_ID : "5",
	 	id : "1546"
 	 }, { 
	 	name : "la hermana",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1488", "1509", "1528"],
	 	language_ID : "5",
	 	id : "1547"
 	 }, { 
	 	name : "el padre",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1489", "1510", "1529"],
	 	language_ID : "5",
	 	id : "1548"
 	 }, { 
	 	name : "la madre",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1490", "1511", "1530", "3161"],
	 	language_ID : "5",
	 	id : "1549"
 	 }, { 
	 	name : "el tio",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1491", "1512", "1531", "3090"],
	 	language_ID : "5",
	 	id : "1550"
 	 }, { 
	 	name : "la tia",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1492", "1514", "1532", "3088", "3089"],
	 	language_ID : "5",
	 	id : "1551"
 	 }, { 
	 	name : "la nieta",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1515", "1533", "3153"],
	 	language_ID : "5",
	 	id : "1552",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3154"]
 	 }, { 
	 	name : "la familia",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1495", "1516", "1534"],
	 	language_ID : "5",
	 	id : "1553"
 	 }, { 
	 	name : "la compañera",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1517", "1535", "3155"],
	 	language_ID : "5",
	 	id : "1554",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3156"]
 	 }, { 
	 	name : "hacerse mimos",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1504", "1505", "1536", "3158"],
	 	language_ID : "5",
	 	id : "1555"
 	 }, { 
	 	name : "die Ruhe",
	 	language_ID : "2",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1575", "1595", "1614", "2847", "3164"],
	 	id : "1556"
 	 }, { 
	 	name : "die Feier",
	 	language_ID : "2",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1559", "1577", "1579", "1580", "1597", "1616", "1617"],
	 	id : "1558"
 	 }, { 
	 	name : "das Fest",
	 	language_ID : "2",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1558", "1577", "1579", "1580", "1597", "1616", "1617"],
	 	id : "1559"
 	 }, { 
	 	name : "feiern",
	 	language_ID : "2",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1578", "1598", "1599", "1618"],
	 	id : "1560"
 	 }, { 
	 	name : "sich ausruhen",
	 	language_ID : "2",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1581", "1600", "1619"],
	 	id : "1561"
 	 }, { 
	 	name : "schlafen",
	 	language_ID : "2",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1582", "1601", "1620"],
	 	id : "1562"
 	 }, { 
	 	name : "spazieren gehen",
	 	language_ID : "2",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1583", "1602", "1621", "2851"],
	 	id : "1563"
 	 }, { 
	 	name : "tanzen",
	 	language_ID : "2",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1584", "1603", "1623"],
	 	id : "1564"
 	 }, { 
	 	name : "das Buch",
	 	language_ID : "2",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1586", "1605", "1625"],
	 	id : "1566"
 	 }, { 
	 	name : "lesen",
	 	language_ID : "2",
	 	categories_IDs : ["35", "46", "55"],
	 	related_IDs : ["1606", "398", "485"],
	 	id : "1567"
 	 }, { 
	 	name : "der Spaß",
	 	language_ID : "2",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1588", "1607", "1622"],
	 	id : "1568"
 	 }, { 
	 	name : "die CD",
	 	language_ID : "2",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1589", "1608", "1626"],
	 	id : "1569"
 	 }, { 
	 	name : "das Radio",
	 	language_ID : "2",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1590", "1609", "1627"],
	 	id : "1570"
 	 }, { 
	 	name : "der Fernseher",
	 	language_ID : "2",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1591", "1610", "1628", "3163", "3165"],
	 	id : "1571"
 	 }, { 
	 	name : "jemanden einladen",
	 	language_ID : "2",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1592", "1611", "1629", "2913"],
	 	id : "1572"
 	 }, { 
	 	name : "absagen",
	 	language_ID : "2",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1593", "1612", "1630"],
	 	id : "1573"
 	 }, { 
	 	name : "zusagen",
	 	language_ID : "2",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1594", "1613", "1631"],
	 	id : "1574"
 	 }, { 
	 	name : "the quiet",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1556", "1595", "1614", "3164"],
	 	language_ID : "3",
	 	id : "1575"
 	 }, { 
	 	name : "the celebration",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1558", "1559", "1579", "1580", "1597", "1616", "1617"],
	 	language_ID : "3",
	 	id : "1577"
 	 }, { 
	 	name : "celebrate",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1560", "1598", "1599", "1618"],
	 	language_ID : "3",
	 	id : "1578"
 	 }, { 
	 	name : "the party",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1558", "1559", "1577", "1580", "1597", "1616", "1617", "jm2iiEMOvfHO"],
	 	language_ID : "3",
	 	id : "1579"
 	 }, { 
	 	name : "die Party",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1558", "1559", "1577", "1579", "1597", "1616", "1617"],
	 	language_ID : "2",
	 	id : "1580"
 	 }, { 
	 	name : "relax",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1561", "1600", "1619"],
	 	language_ID : "3",
	 	id : "1581"
 	 }, { 
	 	name : "sleep",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1562", "1601", "1620"],
	 	language_ID : "3",
	 	id : "1582"
 	 }, { 
	 	name : "go for a walk",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1563", "1602", "1621", "2852"],
	 	language_ID : "3",
	 	id : "1583"
 	 }, { 
	 	name : "dance",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1564", "1603", "1623"],
	 	language_ID : "3",
	 	id : "1584"
 	 }, { 
	 	name : "the book",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1566", "1605", "1625", "ppT8EIICVdVs"],
	 	language_ID : "3",
	 	id : "1586"
 	 }, { 
	 	name : "the fun",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1568", "1607", "1622"],
	 	language_ID : "3",
	 	id : "1588"
 	 }, { 
	 	name : "the CD",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1569", "1608", "1626"],
	 	language_ID : "3",
	 	id : "1589"
 	 }, { 
	 	name : "the radio",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1570", "1609", "1627"],
	 	language_ID : "3",
	 	id : "1590"
 	 }, { 
	 	name : "the TV",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1571", "1610", "1628", "3163", "3165"],
	 	language_ID : "3",
	 	id : "1591"
 	 }, { 
	 	name : "invite someone",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1572", "1611", "1629"],
	 	language_ID : "3",
	 	id : "1592"
 	 }, { 
	 	name : "decline",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1573", "1612", "1630"],
	 	language_ID : "3",
	 	id : "1593"
 	 }, { 
	 	name : "accept",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1574", "1613", "1631"],
	 	language_ID : "3",
	 	id : "1594"
 	 }, { 
	 	name : "la calma",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1556", "1575", "1614", "3164"],
	 	language_ID : "5",
	 	id : "1595"
 	 }, { 
	 	name : "la fiesta",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1558", "1559", "1577", "1579", "1580", "1616", "1617"],
	 	language_ID : "5",
	 	id : "1597"
 	 }, { 
	 	name : "celebrar",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1560", "1578", "1599", "1618"],
	 	language_ID : "5",
	 	id : "1598"
 	 }, { 
	 	name : "estar de fiesta",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1560", "1578", "1598", "1618"],
	 	language_ID : "5",
	 	id : "1599"
 	 }, { 
	 	name : "descansar",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1561", "1581", "1619"],
	 	language_ID : "5",
	 	id : "1600"
 	 }, { 
	 	name : "dormir",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1562", "1582", "1620"],
	 	language_ID : "5",
	 	id : "1601"
 	 }, { 
	 	name : "pasear",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1563", "1583", "1621", "2853"],
	 	language_ID : "5",
	 	id : "1602"
 	 }, { 
	 	name : "bailar",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1564", "1584", "1623"],
	 	language_ID : "5",
	 	id : "1603"
 	 }, { 
	 	name : "el libro",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1566", "1586", "1625"],
	 	language_ID : "5",
	 	id : "1605"
 	 }, { 
	 	name : "leer",
	 	categories_IDs : ["35", "46", "55"],
	 	related_IDs : ["1567", "398", "485"],
	 	language_ID : "5",
	 	id : "1606"
 	 }, { 
	 	name : "la diversión",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1568", "1588", "1622"],
	 	language_ID : "5",
	 	id : "1607"
 	 }, { 
	 	name : "el CD",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1569", "1589", "1626"],
	 	language_ID : "5",
	 	id : "1608"
 	 }, { 
	 	name : "la radio",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1570", "1590", "1627"],
	 	language_ID : "5",
	 	id : "1609"
 	 }, { 
	 	name : "el televisor",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1571", "1591", "1628", "3163", "3165"],
	 	language_ID : "5",
	 	id : "1610",
	 	tags : []
 	 }, { 
	 	name : "invitar a alguien",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1572", "1592", "1629"],
	 	language_ID : "5",
	 	id : "1611"
 	 }, { 
	 	name : "cancelar",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1573", "1593", "1630"],
	 	language_ID : "5",
	 	id : "1612",
	 	tags : []
 	 }, { 
	 	name : "aceptar",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1574", "1594", "1631"],
	 	language_ID : "5",
	 	id : "1613"
 	 }, { 
	 	name : "ett lugn",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1556", "1575", "1595", "3164"],
	 	language_ID : "1",
	 	id : "1614",
	 	tags : []
 	 }, { 
	 	name : "ett firande",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1558", "1559", "1577", "1579", "1580", "1597", "1617"],
	 	language_ID : "1",
	 	id : "1616"
 	 }, { 
	 	name : "en fest",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1558", "1559", "1577", "1579", "1580", "1597", "1616"],
	 	language_ID : "1",
	 	id : "1617"
 	 }, { 
	 	name : "festa",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1560", "1578", "1598", "1599"],
	 	language_ID : "1",
	 	id : "1618"
 	 }, { 
	 	name : "vila ut sig",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1561", "1581", "1600"],
	 	language_ID : "1",
	 	id : "1619"
 	 }, { 
	 	name : "sova",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1562", "1582", "1601"],
	 	language_ID : "1",
	 	id : "1620"
 	 }, { 
	 	name : "promenera",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1563", "1583", "1602", "2854"],
	 	language_ID : "1",
	 	id : "1621"
 	 }, { 
	 	name : "ett skämt",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1568", "1588", "1607"],
	 	language_ID : "1",
	 	id : "1622"
 	 }, { 
	 	name : "dansa",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1564", "1584", "1603"],
	 	language_ID : "1",
	 	id : "1623"
 	 }, { 
	 	name : "en bok",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1566", "1586", "1605"],
	 	language_ID : "1",
	 	id : "1625"
 	 }, { 
	 	name : "en cd-skiva",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1569", "1589", "1608"],
	 	language_ID : "1",
	 	id : "1626"
 	 }, { 
	 	name : "en radio",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1570", "1590", "1609"],
	 	language_ID : "1",
	 	id : "1627"
 	 }, { 
	 	name : "en tv-apparat",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1571", "1591", "1610", "3163", "3165"],
	 	language_ID : "1",
	 	id : "1628",
	 	tags : []
 	 }, { 
	 	name : "bjuda in",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1572", "1592", "1611"],
	 	language_ID : "1",
	 	id : "1629"
 	 }, { 
	 	name : "lämna återbud",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1573", "1593", "1612"],
	 	language_ID : "1",
	 	id : "1630"
 	 }, { 
	 	name : "lova",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1574", "1594", "1613"],
	 	language_ID : "1",
	 	id : "1631"
 	 }, { 
	 	name : "schmal",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1641", "1652", "1662", "2995", "3166"],
	 	language_ID : "2",
	 	id : "1632"
 	 }, { 
	 	name : "kantig",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1642", "1653", "1663", "3167"],
	 	language_ID : "2",
	 	id : "1633"
 	 }, { 
	 	name : "mager",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1643", "1654", "1664", "2994", "3168"],
	 	language_ID : "2",
	 	id : "1634"
 	 }, { 
	 	name : "dick",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1644", "1655", "1665", "3169"],
	 	language_ID : "2",
	 	id : "1635"
 	 }, { 
	 	name : "behaart",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1645", "1656", "1666", "3170"],
	 	language_ID : "2",
	 	id : "1636"
 	 }, { 
	 	name : "glatt",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1646", "1657", "1667", "3171"],
	 	language_ID : "2",
	 	id : "1637"
 	 }, { 
	 	name : "gepflegt",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1648", "1658", "1668", "3001", "3002"],
	 	language_ID : "2",
	 	id : "1638"
 	 }, { 
	 	name : "glänzend",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1649", "1659", "1669", "3004"],
	 	language_ID : "2",
	 	id : "1639"
 	 }, { 
	 	name : "matt",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1650", "1660", "1670", "3173"],
	 	language_ID : "2",
	 	id : "1640",
	 	tags : []
 	 }, { 
	 	name : "delgado",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1632", "1652", "1662", "2995"],
	 	language_ID : "5",
	 	id : "1641",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3166"]
 	 }, { 
	 	name : "angulosa",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1633", "1653", "1663"],
	 	language_ID : "5",
	 	id : "1642",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3167"]
 	 }, { 
	 	name : "flaco",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1634", "1654", "1664", "2994"],
	 	language_ID : "5",
	 	id : "1643",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3168"]
 	 }, { 
	 	name : "gruesa",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1635", "1655", "1665"],
	 	language_ID : "5",
	 	id : "1644",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3169"]
 	 }, { 
	 	name : "velluda",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1636", "1656", "1666"],
	 	language_ID : "5",
	 	id : "1645",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3170"]
 	 }, { 
	 	name : "liso",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1637", "1657", "1667"],
	 	language_ID : "5",
	 	id : "1646",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3171"]
 	 }, { 
	 	name : "rau",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1651", "1661", "1671", "3005", "3006", "3172"],
	 	language_ID : "2",
	 	id : "1647"
 	 }, { 
	 	name : "elegante",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1638", "1658", "1668", "3001", "3002"],
	 	language_ID : "5",
	 	id : "1648"
 	 }, { 
	 	name : "brillante",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1639", "1659", "1669", "3004"],
	 	language_ID : "5",
	 	id : "1649"
 	 }, { 
	 	name : "mate",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1640", "1660", "1670", "3173"],
	 	language_ID : "5",
	 	id : "1650"
 	 }, { 
	 	name : "áspera",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1647", "1661", "1671", "3005", "3006"],
	 	language_ID : "5",
	 	id : "1651",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3172"]
 	 }, { 
	 	name : "thin",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1632", "1641", "1662", "2995", "3166"],
	 	language_ID : "3",
	 	id : "1652"
 	 }, { 
	 	name : "chiselled",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1633", "1642", "1663", "3167"],
	 	language_ID : "3",
	 	id : "1653"
 	 }, { 
	 	name : "lean",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1634", "1643", "1664", "2994", "3168"],
	 	language_ID : "3",
	 	id : "1654"
 	 }, { 
	 	name : "bulky",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1635", "1644", "1665", "3169"],
	 	language_ID : "3",
	 	id : "1655"
 	 }, { 
	 	name : "hairy",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1636", "1645", "1666", "3170"],
	 	language_ID : "3",
	 	id : "1656"
 	 }, { 
	 	name : "smooth",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1637", "1646", "1667", "3171"],
	 	language_ID : "3",
	 	id : "1657"
 	 }, { 
	 	name : "neat",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1638", "1648", "1668", "3001", "3002"],
	 	language_ID : "3",
	 	id : "1658"
 	 }, { 
	 	name : "shiny",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1639", "1649", "1669", "3004"],
	 	language_ID : "3",
	 	id : "1659"
 	 }, { 
	 	name : "dull",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1640", "1650", "1670", "3173"],
	 	language_ID : "3",
	 	id : "1660",
	 	tags : []
 	 }, { 
	 	name : "rough",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1647", "1651", "1671", "3005", "3006", "3172"],
	 	language_ID : "3",
	 	id : "1661"
 	 }, { 
	 	name : "smal",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1632", "1641", "1652", "2995", "3166"],
	 	language_ID : "1",
	 	id : "1662"
 	 }, { 
	 	name : "kantig",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1633", "1642", "1653", "3167"],
	 	language_ID : "1",
	 	id : "1663"
 	 }, { 
	 	name : "mager",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1634", "1643", "1654", "2994", "3168"],
	 	language_ID : "1",
	 	id : "1664"
 	 }, { 
	 	name : "tjock",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1635", "1644", "1655", "3169"],
	 	language_ID : "1",
	 	id : "1665"
 	 }, { 
	 	name : "luden",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1636", "1645", "1656", "3170"],
	 	language_ID : "1",
	 	id : "1666"
 	 }, { 
	 	name : "glatt",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1637", "1646", "1657", "3171"],
	 	language_ID : "1",
	 	id : "1667"
 	 }, { 
	 	name : "vårdad",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1638", "1648", "1658", "3001", "3002"],
	 	language_ID : "1",
	 	id : "1668"
 	 }, { 
	 	name : "glänsande",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1639", "1649", "1659", "3004"],
	 	language_ID : "1",
	 	id : "1669"
 	 }, { 
	 	name : "matt",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1640", "1650", "1660", "3173"],
	 	language_ID : "1",
	 	id : "1670"
 	 }, { 
	 	name : "ojämn",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1647", "1651", "1661", "3005", "3006", "3172"],
	 	language_ID : "1",
	 	id : "1671"
 	 }, { 
	 	name : "blond",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1673", "1674", "1675", "2997", "3174"],
	 	language_ID : "2",
	 	id : "1672"
 	 }, { 
	 	name : "blond",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1672", "1674", "1675", "2997", "3174"],
	 	language_ID : "1",
	 	id : "1673"
 	 }, { 
	 	name : "blond",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1672", "1673", "1675", "2997", "3174"],
	 	language_ID : "3",
	 	id : "1674"
 	 }, { 
	 	name : "rubia",
	 	categories_IDs : ["45"],
	 	related_IDs : ["1672", "1673", "1674", "2997"],
	 	language_ID : "5",
	 	id : "1675",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3174"]
 	 }, { 
	 	name : "kochen",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1683", "1692", "2546"],
	 	language_ID : "2",
	 	id : "1676"
 	 }, { 
	 	name : "backen",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1684", "1693", "2543"],
	 	language_ID : "2",
	 	id : "1677"
 	 }, { 
	 	name : "braten",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1685", "1694", "2544"],
	 	language_ID : "2",
	 	id : "1678"
 	 }, { 
	 	name : "der Backofen",
	 	categories_IDs : ["40", "35", "63"],
	 	related_IDs : ["1686", "1695", "2545", "JNLqTZiSXA0q"],
	 	language_ID : "2",
	 	id : "1679",
	 	tags : []
 	 }, { 
	 	name : "die Mikrowelle",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1687", "1696", "2548"],
	 	language_ID : "2",
	 	id : "1680"
 	 }, { 
	 	name : "die Pfanne",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1688", "1697", "2549", "3175", "Hm3FWQI2OsIB"],
	 	language_ID : "2",
	 	id : "1681",
	 	tags : []
 	 }, { 
	 	name : "der Topf",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1689", "1698", "2547"],
	 	language_ID : "2",
	 	id : "1682"
 	 }, { 
	 	name : "cocinar",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1676", "1692", "2546"],
	 	language_ID : "5",
	 	id : "1683"
 	 }, { 
	 	name : "cocer en el horno",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1677", "1693", "2543"],
	 	language_ID : "5",
	 	id : "1684",
	 	tags : []
 	 }, { 
	 	name : "freír",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1678", "1694", "2544"],
	 	language_ID : "5",
	 	id : "1685"
 	 }, { 
	 	name : "el horno",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1679", "1695", "2545"],
	 	language_ID : "5",
	 	id : "1686"
 	 }, { 
	 	name : "el microondas",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1680", "1696", "2548"],
	 	language_ID : "5",
	 	id : "1687"
 	 }, { 
	 	name : "la sartén",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1681", "1697", "2549", "3175"],
	 	language_ID : "5",
	 	id : "1688",
	 	tags : []
 	 }, { 
	 	name : "la olla",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1682", "1698", "2547"],
	 	language_ID : "5",
	 	id : "1689",
	 	tags : []
 	 }, { 
	 	name : "the meat",
	 	categories_IDs : ["40"],
	 	related_IDs : ["124", "148", "170"],
	 	language_ID : "3",
	 	id : "1690"
 	 }, { 
	 	name : "the water",
	 	categories_IDs : ["40", "59"],
	 	related_IDs : ["130", "154", "25"],
	 	language_ID : "3",
	 	id : "1691"
 	 }, { 
	 	name : "cook",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1676", "1683", "2546"],
	 	language_ID : "3",
	 	id : "1692"
 	 }, { 
	 	name : "bake",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1677", "1684", "2543"],
	 	language_ID : "3",
	 	id : "1693"
 	 }, { 
	 	name : "fry",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1678", "1685", "2544"],
	 	language_ID : "3",
	 	id : "1694"
 	 }, { 
	 	name : "the oven",
	 	categories_IDs : ["40", "35"],
	 	related_IDs : ["1679", "1686", "2545", "QfiYJZGxINJX"],
	 	language_ID : "3",
	 	id : "1695",
	 	tags : []
 	 }, { 
	 	name : "the microwave",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1680", "1687", "2548"],
	 	language_ID : "3",
	 	id : "1696"
 	 }, { 
	 	name : "the pan",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1681", "1688", "2549", "3175", "OJOAyfCGJ0nu"],
	 	language_ID : "3",
	 	id : "1697",
	 	tags : []
 	 }, { 
	 	name : "the pot",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1682", "1689", "2547"],
	 	language_ID : "3",
	 	id : "1698"
 	 }, { 
	 	name : "die Beerdigung",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1700", "1701", "1702", "3176", "ZP7BeHypF9r2"],
	 	language_ID : "2",
	 	id : "1699",
	 	tags : []
 	 }, { 
	 	name : "the funeral",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1699", "1701", "1702", "3176", "7HgGEv1vZ0NQ"],
	 	language_ID : "3",
	 	id : "1700",
	 	tags : []
 	 }, { 
	 	name : "en begravning",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1699", "1700", "1702", "3176"],
	 	language_ID : "1",
	 	id : "1701",
	 	tags : []
 	 }, { 
	 	name : "el entierro",
	 	categories_IDs : ["63"],
	 	related_IDs : ["1699", "1700", "1701", "3176"],
	 	language_ID : "5",
	 	id : "1702"
 	 }, { 
	 	name : "el",
	 	categories_IDs : ["33"],
	 	related_IDs : ["101", "3178", "3179", "3180", "62", "83"],
	 	language_ID : "5",
	 	id : "1703",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3177"]
 	 }, { 
	 	name : "quíen",
	 	categories_IDs : ["33"],
	 	related_IDs : ["103", "64", "84"],
	 	language_ID : "5",
	 	id : "1704"
 	 }, { 
	 	name : "que",
	 	categories_IDs : ["33"],
	 	related_IDs : ["104", "66", "86"],
	 	language_ID : "5",
	 	id : "1705"
 	 }, { 
	 	name : "por qué",
	 	categories_IDs : ["33"],
	 	related_IDs : ["106", "2996", "69", "87"],
	 	language_ID : "5",
	 	id : "1706"
 	 }, { 
	 	name : "dónde",
	 	categories_IDs : ["33"],
	 	related_IDs : ["70", "88"],
	 	language_ID : "5",
	 	id : "1707"
 	 }, { 
	 	name : "cuándo",
	 	categories_IDs : ["33"],
	 	related_IDs : ["105", "71", "89"],
	 	language_ID : "5",
	 	id : "1708"
 	 }, { 
	 	name : "a quién",
	 	categories_IDs : ["33"],
	 	related_IDs : ["72", "84", "90"],
	 	language_ID : "5",
	 	id : "1709"
 	 }, { 
	 	name : "sí",
	 	categories_IDs : ["33"],
	 	related_IDs : ["73", "91"],
	 	language_ID : "5",
	 	id : "1710"
 	 }, { 
	 	name : "no",
	 	categories_IDs : ["33"],
	 	related_IDs : ["2157", "74", "92"],
	 	language_ID : "5",
	 	id : "1711"
 	 }, { 
	 	name : "pero",
	 	categories_IDs : ["33"],
	 	related_IDs : ["109", "3181", "75", "93"],
	 	language_ID : "5",
	 	id : "1712",
	 	tags : []
 	 }, { 
	 	name : "probablemente",
	 	categories_IDs : ["33"],
	 	related_IDs : ["184", "2877", "76", "94"],
	 	language_ID : "5",
	 	id : "1713"
 	 }, { 
	 	name : "y",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1247", "77", "95"],
	 	language_ID : "5",
	 	id : "1714"
 	 }, { 
	 	name : "o",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1248", "78", "96"],
	 	language_ID : "5",
	 	id : "1715"
 	 }, { 
	 	name : "como si",
	 	categories_IDs : ["33"],
	 	related_IDs : ["81"],
	 	language_ID : "5",
	 	id : "1716"
 	 }, { 
	 	name : "también",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1257", "1258", "2859"],
	 	language_ID : "5",
	 	id : "1717"
 	 }, { 
	 	name : "entonces",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1249", "80", "98"],
	 	language_ID : "5",
	 	id : "1718"
 	 }, { 
	 	name : "pues",
	 	categories_IDs : ["33"],
	 	related_IDs : ["181", "182"],
	 	language_ID : "5",
	 	id : "1719"
 	 }, { 
	 	name : "cuanto más",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1128", "1721", "181"],
	 	language_ID : "5",
	 	id : "1720"
 	 }, { 
	 	name : "Cuanto más, mejor.",
	 	related_IDs : ["1127", "1720"],
	 	language_ID : "5",
	 	sentence : true,
	 	categories_IDs : [],
	 	id : "1721"
 	 }, { 
	 	name : "con",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1255", "1256", "2865"],
	 	language_ID : "5",
	 	id : "1722"
 	 }, { 
	 	name : "sin",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1263", "1264", "2866"],
	 	language_ID : "5",
	 	id : "1723"
 	 }, { 
	 	name : "de",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1252", "1253", "1254", "2878", "3128"],
	 	language_ID : "5",
	 	id : "1724"
 	 }, { 
	 	name : "por",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1250", "1251"],
	 	language_ID : "5",
	 	id : "1725"
 	 }, { 
	 	name : "porque",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1245", "1246", "QUBj22S8UA2i"],
	 	language_ID : "5",
	 	id : "1726"
 	 }, { 
	 	name : "si",
	 	categories_IDs : ["33"],
	 	related_IDs : ["105", "79", "89", "97"],
	 	language_ID : "5",
	 	id : "1727"
 	 }, { 
	 	name : "como",
	 	categories_IDs : ["33"],
	 	related_IDs : ["100", "107", "108", "82", "85"],
	 	language_ID : "5",
	 	id : "1728"
 	 }, { 
	 	name : "Muchas gracias.",
	 	categories_IDs : ["34"],
	 	related_IDs : ["2875", "935"],
	 	language_ID : "5",
	 	sentence : true,
	 	id : "1729"
 	 }, { 
	 	name : "ver",
	 	categories_IDs : ["35"],
	 	related_IDs : ["401", "436", "470"],
	 	language_ID : "5",
	 	id : "1732"
 	 }, { 
	 	name : "ayudar",
	 	categories_IDs : ["35"],
	 	related_IDs : ["392", "427", "461"],
	 	language_ID : "5",
	 	id : "1733"
 	 }, { 
	 	name : "comprar",
	 	categories_IDs : ["35"],
	 	related_IDs : ["2817", "408", "443", "475"],
	 	language_ID : "5",
	 	id : "1734"
 	 }, { 
	 	name : "contener",
	 	categories_IDs : ["35"],
	 	related_IDs : ["393", "428", "462"],
	 	language_ID : "5",
	 	id : "1735"
 	 }, { 
	 	name : "necesitar",
	 	categories_IDs : ["35"],
	 	related_IDs : ["382", "417", "454"],
	 	language_ID : "5",
	 	id : "1737"
 	 }, { 
	 	name : "deletrear",
	 	categories_IDs : ["35"],
	 	related_IDs : ["406", "441", "486"],
	 	language_ID : "5",
	 	id : "1738"
 	 }, { 
	 	name : "pensar",
	 	categories_IDs : ["35"],
	 	related_IDs : ["414", "449", "481"],
	 	language_ID : "5",
	 	id : "1739"
 	 }, { 
	 	name : "comer",
	 	categories_IDs : ["35"],
	 	related_IDs : ["402", "437", "469"],
	 	language_ID : "5",
	 	id : "1740"
 	 }, { 
	 	name : "ir",
	 	categories_IDs : ["35"],
	 	related_IDs : ["387", "413", "422", "448", "457", "479"],
	 	language_ID : "5",
	 	id : "1741"
 	 }, { 
	 	name : "tener",
	 	categories_IDs : ["35"],
	 	related_IDs : ["2841", "388", "423", "459"],
	 	language_ID : "5",
	 	id : "1742"
 	 }, { 
	 	name : "llamarse",
	 	categories_IDs : ["35"],
	 	related_IDs : ["391", "426", "483"],
	 	language_ID : "5",
	 	id : "1743"
 	 }, { 
	 	name : "poder",
	 	categories_IDs : ["35"],
	 	related_IDs : ["2837", "2857", "395", "430", "463"],
	 	language_ID : "5",
	 	id : "1744"
 	 }, { 
	 	categories_IDs : ["35", "43"],
	 	related_IDs : ["1746", "2538", "394"],
	 	name : "fühlen",
	 	language_ID : "2",
	 	id : "1745"
 	 }, { 
	 	name : "sentir",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1745", "2538", "394"],
	 	language_ID : "5",
	 	id : "1746"
 	 }, { 
	 	name : "conocer",
	 	categories_IDs : ["35"],
	 	related_IDs : ["2867", "394", "429"],
	 	language_ID : "5",
	 	id : "1747"
 	 }, { 
	 	name : "aprender",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1749", "2627", "397", "433"],
	 	language_ID : "5",
	 	id : "1748"
 	 }, { 
	 	name : "estudiar",
	 	categories_IDs : ["35", "55"],
	 	related_IDs : ["1748", "1961", "2576", "2627", "2628", "397", "433"],
	 	language_ID : "5",
	 	id : "1749"
 	 }, { 
	 	name : "estar echado",
	 	categories_IDs : ["35"],
	 	related_IDs : ["396", "431", "464"],
	 	language_ID : "5",
	 	id : "1750",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3182"]
 	 }, { 
	 	name : "hacer",
	 	categories_IDs : ["35"],
	 	related_IDs : ["381", "416", "453"],
	 	language_ID : "5",
	 	id : "1751",
	 	tags : []
 	 }, { 
	 	name : "tener que",
	 	categories_IDs : ["35"],
	 	related_IDs : ["399", "434", "466"],
	 	language_ID : "5",
	 	id : "1753"
 	 }, { 
	 	name : "tomar",
	 	categories_IDs : ["35"],
	 	related_IDs : ["409", "444", "476"],
	 	language_ID : "5",
	 	id : "1754"
 	 }, { 
	 	name : "escribir",
	 	categories_IDs : ["35"],
	 	related_IDs : ["404", "439", "472"],
	 	language_ID : "5",
	 	id : "1755"
 	 }, { 
	 	name : "deber",
	 	categories_IDs : ["35"],
	 	related_IDs : ["403", "438", "471"],
	 	language_ID : "5",
	 	id : "1756"
 	 }, { 
	 	name : "ser cierto",
	 	categories_IDs : ["35"],
	 	related_IDs : ["2868", "390", "425"],
	 	language_ID : "5",
	 	id : "1759"
 	 }, { 
	 	name : "buscar",
	 	categories_IDs : ["35"],
	 	related_IDs : ["410", "445", "477"],
	 	language_ID : "5",
	 	id : "1760"
 	 }, { 
	 	name : "vender",
	 	categories_IDs : ["35"],
	 	related_IDs : ["389", "424", "460"],
	 	language_ID : "5",
	 	id : "1761"
 	 }, { 
	 	name : "entender",
	 	categories_IDs : ["35"],
	 	related_IDs : ["3183", "385", "420", "456"],
	 	language_ID : "5",
	 	id : "1762",
	 	tags : []
 	 }, { 
	 	name : "hacerse",
	 	categories_IDs : ["35"],
	 	related_IDs : ["3184", "383", "418", "455"],
	 	language_ID : "5",
	 	id : "1763",
	 	tags : []
 	 }, { 
	 	name : "allí",
	 	categories_IDs : ["38"],
	 	related_IDs : ["505", "525", "853"],
	 	language_ID : "5",
	 	id : "1767"
 	 }, { 
	 	name : "casi",
	 	categories_IDs : ["38"],
	 	related_IDs : ["503", "523", "850", "851"],
	 	language_ID : "5",
	 	id : "1768"
 	 }, { 
	 	name : "lejano",
	 	categories_IDs : ["38"],
	 	related_IDs : ["3186", "502", "522", "849"],
	 	language_ID : "5",
	 	id : "1769",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3185"]
 	 }, { 
	 	name : "derecho",
	 	categories_IDs : ["38"],
	 	related_IDs : ["3012", "487", "507", "529"],
	 	language_ID : "5",
	 	id : "1770"
 	 }, { 
	 	name : "aquí",
	 	categories_IDs : ["38"],
	 	related_IDs : ["504", "524", "852"],
	 	language_ID : "5",
	 	id : "1771"
 	 }, { 
	 	name : "detrás",
	 	categories_IDs : ["38"],
	 	related_IDs : ["496", "538", "516"],
	 	language_ID : "5",
	 	id : "1772"
 	 }, { 
	 	name : "detrás de",
	 	categories_IDs : ["38"],
	 	related_IDs : ["498", "518", "540"],
	 	language_ID : "5",
	 	id : "1773"
 	 }, { 
	 	name : "a la izquierda",
	 	categories_IDs : ["38"],
	 	related_IDs : ["489", "509", "531"],
	 	language_ID : "5",
	 	id : "1774"
 	 }, { 
	 	name : "aquí cerca",
	 	categories_IDs : ["38"],
	 	related_IDs : ["3187", "501", "521", "848"],
	 	language_ID : "5",
	 	id : "1775",
	 	tags : []
 	 }, { 
	 	name : "el Norte",
	 	categories_IDs : ["38"],
	 	related_IDs : ["492", "512", "534"],
	 	language_ID : "5",
	 	id : "1776"
 	 }, { 
	 	name : "arriba",
	 	categories_IDs : ["38"],
	 	related_IDs : ["490", "510", "532"],
	 	language_ID : "5",
	 	id : "1777"
 	 }, { 
	 	name : "el Este",
	 	categories_IDs : ["38"],
	 	related_IDs : ["493", "513", "535"],
	 	language_ID : "5",
	 	id : "1778"
 	 }, { 
	 	name : "a la derecha",
	 	categories_IDs : ["38"],
	 	related_IDs : ["488", "508", "530"],
	 	language_ID : "5",
	 	id : "1779"
 	 }, { 
	 	name : "el Sur",
	 	categories_IDs : ["38"],
	 	related_IDs : ["494", "514", "536"],
	 	language_ID : "5",
	 	id : "1780"
 	 }, { 
	 	name : "über",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1783", "2871", "2872"],
	 	language_ID : "2",
	 	id : "1781"
 	 }, { 
	 	name : "encima de",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1781", "2871", "2872"],
	 	language_ID : "5",
	 	id : "1783"
 	 }, { 
	 	name : "unter",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1786", "2869", "3188"],
	 	language_ID : "2",
	 	id : "1784",
	 	tags : []
 	 }, { 
	 	name : "bajo",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1784", "2869", "3188"],
	 	language_ID : "5",
	 	id : "1786",
	 	tags : []
 	 }, { 
	 	name : "delante",
	 	categories_IDs : ["38"],
	 	related_IDs : ["497", "517", "539"],
	 	language_ID : "5",
	 	id : "1788"
 	 }, { 
	 	name : "otra",
	 	categories_IDs : ["38"],
	 	related_IDs : ["506", "526", "854"],
	 	language_ID : "5",
	 	id : "1789",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3189"]
 	 }, { 
	 	name : "el Oeste",
	 	categories_IDs : ["38"],
	 	related_IDs : ["495", "515", "537"],
	 	language_ID : "5",
	 	id : "1790"
 	 }, { 
	 	name : "el fin de semana",
	 	categories_IDs : ["39"],
	 	related_IDs : ["1287", "1288", "1289"],
	 	language_ID : "5",
	 	id : "1792"
 	 }, { 
	 	name : "antes",
	 	categories_IDs : ["39"],
	 	related_IDs : ["918", "919", "921"],
	 	language_ID : "5",
	 	id : "1793"
 	 }, { 
	 	name : "de repente",
	 	categories_IDs : ["39"],
	 	related_IDs : ["1243", "1244", "1286"],
	 	language_ID : "5",
	 	id : "1794"
 	 }, { 
	 	name : "más tarde",
	 	categories_IDs : ["39"],
	 	related_IDs : ["916", "917", "920"],
	 	language_ID : "5",
	 	id : "1795"
 	 }, { 
	 	name : "viejo",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["319", "320", "346"],
	 	language_ID : "5",
	 	id : "1796",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3190"]
 	 }, { 
	 	name : "azul",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["329", "330", "351"],
	 	language_ID : "5",
	 	id : "1797"
 	 }, { 
	 	name : "marrón",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["335", "336", "354"],
	 	language_ID : "5",
	 	id : "1798"
 	 }, { 
	 	name : "de color",
	 	categories_IDs : ["60"],
	 	related_IDs : ["3087", "317", "318", "345"],
	 	language_ID : "5",
	 	id : "1799"
 	 }, { 
	 	name : "grande",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["310", "311", "341", "343"],
	 	language_ID : "5",
	 	id : "1800"
 	 }, { 
	 	name : "verde",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["325", "326", "349"],
	 	language_ID : "5",
	 	id : "1801"
 	 }, { 
	 	name : "joven",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["321", "322", "347"],
	 	language_ID : "5",
	 	id : "1802"
 	 }, { 
	 	name : "pequeño",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["314", "316", "344"],
	 	language_ID : "5",
	 	id : "1803",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3191"]
 	 }, { 
	 	name : "corto",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["312", "313", "342"],
	 	language_ID : "5",
	 	id : "1804",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3192"]
 	 }, { 
	 	name : "larga",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["339", "340", "356", "358"],
	 	language_ID : "5",
	 	id : "1805",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3193"]
 	 }, { 
	 	name : "nueva",
	 	categories_IDs : ["60"],
	 	related_IDs : ["323", "324", "348"],
	 	language_ID : "5",
	 	id : "1806",
	 	tags : [],
	 	formPrimaryName : "femenino",
	 	formsPrimary_IDs : ["3194"]
 	 }, { 
	 	name : "roja",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["327", "328", "350"],
	 	language_ID : "5",
	 	id : "1807",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3195"]
 	 }, { 
	 	name : "negra",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["333", "334", "353"],
	 	language_ID : "5",
	 	id : "1808",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3196"]
 	 }, { 
	 	name : "bonita",
	 	categories_IDs : ["60"],
	 	related_IDs : ["3085", "3086", "337", "338", "355"],
	 	language_ID : "5",
	 	id : "1809",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3197"]
 	 }, { 
	 	name : "blanco",
	 	categories_IDs : ["45", "60"],
	 	related_IDs : ["331", "332", "352"],
	 	language_ID : "5",
	 	id : "1810",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3198"]
 	 }, { 
	 	name : "la India",
	 	categories_IDs : ["41"],
	 	related_IDs : ["1025", "1071", "982"],
	 	language_ID : "5",
	 	id : "1811"
 	 }, { 
	 	name : "die Demokratie",
	 	language_ID : "2",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1836", "1868", "1885"],
	 	id : "1812",
	 	hasAudio : true
 	 }, { 
	 	name : "die Diktatur",
	 	language_ID : "2",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1837", "1861", "1879"],
	 	id : "1813",
	 	hasAudio : true
 	 }, { 
	 	name : "die Diktatorin",
	 	language_ID : "2",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1830", "1860", "1878", "3219", "pdNokAEmCxfT"],
	 	id : "1814",
	 	hasAudio : true,
	 	tags : [],
	 	formPrimaryName : "weiblich",
	 	formsPrimary_IDs : ["3199"]
 	 }, { 
	 	name : "die Regierung",
	 	language_ID : "2",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1842", "1873", "1890"],
	 	id : "1815",
	 	hasAudio : true
 	 }, { 
	 	name : "das Parlament",
	 	language_ID : "2",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1829", "1862", "1877"],
	 	id : "1816",
	 	hasAudio : true
 	 }, { 
	 	name : "die Wahl",
	 	language_ID : "2",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1844", "1875", "1892"],
	 	id : "1817",
	 	hasAudio : true
 	 }, { 
	 	name : "die Mehrheit",
	 	language_ID : "2",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1838", "1869", "1886", "2923"],
	 	id : "1818",
	 	hasAudio : true
 	 }, { 
	 	name : "die Minderheit",
	 	language_ID : "2",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1839", "1870", "1887"],
	 	id : "1819",
	 	hasAudio : true
 	 }, { 
	 	name : "wählen",
	 	language_ID : "2",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1876", "1893", "2980", "3222"],
	 	id : "1820",
	 	hasAudio : true
 	 }, { 
	 	name : "die Partei",
	 	language_ID : "2",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1841", "1872", "1889"],
	 	id : "1821",
	 	hasAudio : true
 	 }, { 
	 	name : "der Präsident",
	 	language_ID : "2",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1835", "1867", "1884", "3201"],
	 	id : "1822",
	 	hasAudio : true,
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3200"]
 	 }, { 
	 	name : "der Kanzler",
	 	language_ID : "2",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1831", "1863", "1880", "3203"],
	 	id : "1823",
	 	hasAudio : true,
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3202"]
 	 }, { 
	 	name : "die Ministerpräsidentin",
	 	language_ID : "2",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1833", "1882", "3220", "3221", "uNJMzO9YJyOH"],
	 	id : "1824",
	 	hasAudio : true,
	 	tags : [],
	 	formPrimaryName : "weiblich",
	 	formsPrimary_IDs : ["3204"]
 	 }, { 
	 	name : "der König",
	 	language_ID : "2",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1832", "1864", "1881", "lpY98YE99xW0"],
	 	id : "1825",
	 	hasAudio : true,
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3061"]
 	 }, { 
	 	name : "die Prinzessin",
	 	language_ID : "2",
	 	categories_IDs : ["48"],
	 	related_IDs : ["3206", "3207", "3208"],
	 	id : "1826",
	 	hasAudio : true,
	 	tags : [],
	 	formPrimaryName : "weiblich",
	 	formsPrimary_IDs : ["3205"]
 	 }, { 
	 	name : "die Monarchie",
	 	language_ID : "2",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1840", "1871", "1888"],
	 	id : "1827",
	 	hasAudio : true
 	 }, { 
	 	name : "die Republik",
	 	language_ID : "2",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1843", "1874", "1891"],
	 	id : "1828",
	 	hasAudio : true
 	 }, { 
	 	name : "the parliament",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1816", "1862", "1877"],
	 	language_ID : "3",
	 	id : "1829"
 	 }, { 
	 	name : "the dictator",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1814", "1860", "1878", "3199", "3219", "9VYkmrFhtn5P"],
	 	language_ID : "3",
	 	id : "1830",
	 	tags : []
 	 }, { 
	 	name : "the chancellor",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1823", "1863", "1880", "3202", "3203"],
	 	language_ID : "3",
	 	id : "1831"
 	 }, { 
	 	name : "the king",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1825", "1864", "1881", "c9aGBVPoxAqs"],
	 	language_ID : "3",
	 	id : "1832",
	 	tags : [],
	 	formPrimaryName : "male",
	 	formsPrimary_IDs : ["3062"]
 	 }, { 
	 	name : "the prime minister",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1824", "1865", "1882", "3204", "3220", "3221", "6o92AqKCnvrx"],
	 	language_ID : "3",
	 	id : "1833",
	 	tags : []
 	 }, { 
	 	name : "the prince",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1866", "1883", "3205"],
	 	language_ID : "3",
	 	id : "1834",
	 	tags : [],
	 	formPrimaryName : "male",
	 	formsPrimary_IDs : ["3208"]
 	 }, { 
	 	name : "the president",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1822", "1867", "1884", "3200", "3201"],
	 	language_ID : "3",
	 	id : "1835"
 	 }, { 
	 	name : "the democracy",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1812", "1868", "1885"],
	 	language_ID : "3",
	 	id : "1836"
 	 }, { 
	 	name : "the dictatorship",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1813", "1861", "1879"],
	 	language_ID : "3",
	 	id : "1837"
 	 }, { 
	 	name : "the majority",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1818", "1869", "1886"],
	 	language_ID : "3",
	 	id : "1838"
 	 }, { 
	 	name : "the minority",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1819", "1870", "1887"],
	 	language_ID : "3",
	 	id : "1839"
 	 }, { 
	 	name : "the monarchy",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1827", "1871", "1888"],
	 	language_ID : "3",
	 	id : "1840"
 	 }, { 
	 	name : "the party",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1821", "1872", "1889"],
	 	language_ID : "3",
	 	id : "1841"
 	 }, { 
	 	name : "the government",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1815", "1873", "1890"],
	 	language_ID : "3",
	 	id : "1842"
 	 }, { 
	 	name : "the republic",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1828", "1874", "1891"],
	 	language_ID : "3",
	 	id : "1843"
 	 }, { 
	 	name : "the election",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1817", "1875", "1892"],
	 	language_ID : "3",
	 	id : "1844"
 	 }, { 
	 	name : "el sueldo",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1167", "1202", "1203", "1239"],
	 	language_ID : "5",
	 	id : "1846"
 	 }, { 
	 	name : "la cuidadora de ancianos",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1193", "3115"],
	 	language_ID : "5",
	 	id : "1847",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3209"]
 	 }, { 
	 	name : "la abogada",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1159", "1194", "1230", "3116"],
	 	language_ID : "5",
	 	id : "1848",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3210"]
 	 }, { 
	 	name : "el funcionario del gobierno",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1170", "1205", "1241", "3123"],
	 	language_ID : "5",
	 	id : "1849",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3211"]
 	 }, { 
	 	name : "el visitante",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1165", "1200", "1236", "3121"],
	 	language_ID : "5",
	 	id : "1850",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3212"]
 	 }, { 
	 	name : "el periodista",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1161", "1196", "1232", "3118"],
	 	language_ID : "5",
	 	id : "1851",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3213"]
 	 }, { 
	 	name : "el cliente",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1147", "1181", "1219"],
	 	language_ID : "5",
	 	id : "1852",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3130"]
 	 }, { 
	 	name : "la enfermera",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1201", "1237", "3122"],
	 	language_ID : "5",
	 	id : "1853",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3214"]
 	 }, { 
	 	name : "la mecánica",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1157", "1192", "1229", "3114"],
	 	language_ID : "5",
	 	id : "1854",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3215"]
 	 }, { 
	 	name : "la silla",
	 	categories_IDs : ["19", "61"],
	 	related_IDs : ["1164", "1199", "1235"],
	 	language_ID : "5",
	 	id : "1855"
 	 }, { 
	 	name : "la sacerdotisa",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1160", "1195", "1231", "3117"],
	 	language_ID : "5",
	 	id : "1856",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3216"]
 	 }, { 
	 	name : "la política",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1163", "1198", "1234", "3120"],
	 	language_ID : "5",
	 	id : "1857",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3217"]
 	 }, { 
	 	name : "el taxista",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1162", "1197", "1233"],
	 	language_ID : "5",
	 	id : "1858",
	 	tags : [],
	 	formsPrimary_IDs : ["3218"],
	 	formPrimaryName : "masculino"
 	 }, { 
	 	name : "la jubilación",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1168", "1204", "1240"],
	 	language_ID : "5",
	 	id : "1859"
 	 }, { 
	 	name : "la dictadora",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1814", "1830", "1878"],
	 	language_ID : "5",
	 	id : "1860",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3219"]
 	 }, { 
	 	name : "la dictadura",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1813", "1837", "1879"],
	 	language_ID : "5",
	 	id : "1861"
 	 }, { 
	 	name : "el parlamento",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1816", "1829", "1877"],
	 	language_ID : "5",
	 	id : "1862"
 	 }, { 
	 	name : "el cancillero",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1823", "1831", "1880"],
	 	language_ID : "5",
	 	id : "1863",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3203"]
 	 }, { 
	 	name : "el rey",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1825", "1832", "1881"],
	 	language_ID : "5",
	 	id : "1864",
	 	tags : [],
	 	formsPrimary_IDs : ["3064"]
 	 }, { 
	 	name : "el presidente del gobierno",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1824", "1833", "1882", "3204", "3221"],
	 	language_ID : "5",
	 	id : "1865",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3220"]
 	 }, { 
	 	name : "el principe",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1834", "1883", "3205"],
	 	language_ID : "5",
	 	id : "1866",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3207"]
 	 }, { 
	 	name : "la presidenta",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1835", "1884", "3200"],
	 	language_ID : "5",
	 	id : "1867",
	 	tags : [],
	 	formPrimaryName : "femenina",
	 	formsPrimary_IDs : ["3201"]
 	 }, { 
	 	name : "la democracia",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1812", "1836", "1885"],
	 	language_ID : "5",
	 	id : "1868"
 	 }, { 
	 	name : "la mayoría",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1818", "1838", "1886"],
	 	language_ID : "5",
	 	id : "1869"
 	 }, { 
	 	name : "la minoría",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1819", "1839", "1887"],
	 	language_ID : "5",
	 	id : "1870"
 	 }, { 
	 	name : "la monarquía",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1827", "1840", "1888"],
	 	language_ID : "5",
	 	id : "1871"
 	 }, { 
	 	name : "el partido",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1821", "1841", "1889"],
	 	language_ID : "5",
	 	id : "1872"
 	 }, { 
	 	name : "el gobierno",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1815", "1842", "1890"],
	 	language_ID : "5",
	 	id : "1873"
 	 }, { 
	 	name : "la república",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1828", "1843", "1891"],
	 	language_ID : "5",
	 	id : "1874"
 	 }, { 
	 	name : "la elección",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1817", "1844", "1892"],
	 	language_ID : "5",
	 	id : "1875"
 	 }, { 
	 	name : "votar",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1820", "1893", "2980", "3222"],
	 	language_ID : "5",
	 	id : "1876"
 	 }, { 
	 	name : "ett parlament",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1816", "1829", "1862"],
	 	language_ID : "1",
	 	id : "1877",
	 	hasAudio : true
 	 }, { 
	 	name : "en diktator",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1814", "1830", "1860", "3199", "3219"],
	 	language_ID : "1",
	 	id : "1878",
	 	hasAudio : true
 	 }, { 
	 	name : "en diktatur",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1813", "1837", "1861"],
	 	language_ID : "1",
	 	id : "1879",
	 	hasAudio : true
 	 }, { 
	 	name : "en kansler",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1823", "1831", "1863", "3202", "3203"],
	 	language_ID : "1",
	 	id : "1880",
	 	hasAudio : true
 	 }, { 
	 	name : "en kung",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1825", "1832", "1864"],
	 	language_ID : "1",
	 	id : "1881",
	 	hasAudio : true,
	 	tags : [],
	 	formPrimaryName : "manlig",
	 	formsPrimary_IDs : ["3063"]
 	 }, { 
	 	name : "en statsminister",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1824", "1833", "1865", "3204", "3220", "3221"],
	 	language_ID : "1",
	 	id : "1882",
	 	hasAudio : true,
	 	tags : []
 	 }, { 
	 	name : "en prins",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1834", "1866", "3205"],
	 	language_ID : "1",
	 	id : "1883",
	 	hasAudio : true,
	 	tags : [],
	 	formPrimaryName : "manlig",
	 	formsPrimary_IDs : ["3206"]
 	 }, { 
	 	name : "en president",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1822", "1835", "1867", "3200", "3201"],
	 	language_ID : "1",
	 	id : "1884",
	 	hasAudio : true
 	 }, { 
	 	name : "en demokrati",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1812", "1836", "1868"],
	 	language_ID : "1",
	 	id : "1885",
	 	hasAudio : true
 	 }, { 
	 	name : "ett flertal",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1818", "1838", "1869"],
	 	language_ID : "1",
	 	id : "1886",
	 	hasAudio : true
 	 }, { 
	 	name : "en minoritet",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1819", "1839", "1870"],
	 	language_ID : "1",
	 	id : "1887",
	 	hasAudio : true
 	 }, { 
	 	name : "en monarki",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1827", "1840", "1871"],
	 	language_ID : "1",
	 	id : "1888",
	 	hasAudio : true
 	 }, { 
	 	name : "ett parti",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1821", "1841", "1872"],
	 	language_ID : "1",
	 	id : "1889",
	 	hasAudio : true
 	 }, { 
	 	name : "en regering",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1815", "1842", "1873"],
	 	language_ID : "1",
	 	id : "1890",
	 	hasAudio : true
 	 }, { 
	 	name : "en republik",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1828", "1843", "1874"],
	 	language_ID : "1",
	 	id : "1891",
	 	hasAudio : true
 	 }, { 
	 	name : "en val",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1817", "1844", "1875"],
	 	language_ID : "1",
	 	id : "1892",
	 	hasAudio : true
 	 }, { 
	 	name : "välja",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1820", "1876", "2980", "3222"],
	 	language_ID : "1",
	 	id : "1893",
	 	hasAudio : true,
	 	tags : []
 	 }, { 
	 	name : "das Internet",
	 	categories_IDs : ["52"],
	 	related_IDs : ["2554", "2598", "2671"],
	 	language_ID : "2",
	 	id : "1894"
 	 }, { 
	 	name : "die Mail",
	 	categories_IDs : ["52"],
	 	related_IDs : ["2559", "2601", "2674", "3261"],
	 	language_ID : "2",
	 	id : "1895"
 	 }, { 
	 	name : "der Anruf",
	 	categories_IDs : ["52"],
	 	related_IDs : ["2553", "2600", "2673"],
	 	language_ID : "2",
	 	id : "1896"
 	 }, { 
	 	name : "das Telefon",
	 	categories_IDs : ["52"],
	 	related_IDs : ["2562", "2599", "2672"],
	 	language_ID : "2",
	 	id : "1897"
 	 }, { 
	 	name : "telefonieren",
	 	categories_IDs : ["52"],
	 	related_IDs : ["2563", "2607", "2680", "2998"],
	 	language_ID : "2",
	 	id : "1898"
 	 }, { 
	 	name : "chatten",
	 	categories_IDs : ["52"],
	 	related_IDs : ["2551", "2596", "2669"],
	 	language_ID : "2",
	 	id : "1899"
 	 }, { 
	 	name : "antworten",
	 	categories_IDs : ["52"],
	 	related_IDs : ["2550", "2595", "2668", "3262", "3269"],
	 	language_ID : "2",
	 	id : "1900"
 	 }, { 
	 	name : "fragen",
	 	categories_IDs : ["52"],
	 	related_IDs : ["2555", "2602", "2675"],
	 	language_ID : "2",
	 	id : "1901"
 	 }, { 
	 	name : "rufen",
	 	categories_IDs : ["52"],
	 	related_IDs : ["2557", "2605", "2678", "3000"],
	 	language_ID : "2",
	 	id : "1902"
 	 }, { 
	 	name : "schweigen",
	 	categories_IDs : ["52"],
	 	related_IDs : ["2558", "2606", "2679", "3272"],
	 	language_ID : "2",
	 	id : "1903"
 	 }, { 
	 	name : "reden",
	 	categories_IDs : ["52"],
	 	related_IDs : ["2556", "2604", "2677"],
	 	language_ID : "2",
	 	id : "1904"
 	 }, { 
	 	name : "das Handy",
	 	categories_IDs : ["52"],
	 	related_IDs : ["2552", "2597", "2670", "3263", "3273"],
	 	language_ID : "2",
	 	id : "1905"
 	 }, { 
	 	name : "der Glaube",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2106", "2131", "2153", "3247"],
	 	language_ID : "2",
	 	id : "1906"
 	 }, { 
	 	name : "der Beweis",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2103", "2123", "2151", "JQ0PzyPqyFk3"],
	 	language_ID : "2",
	 	id : "1907",
	 	tags : []
 	 }, { 
	 	name : "das Gebet",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2102", "2132", "2152"],
	 	language_ID : "2",
	 	id : "1908"
 	 }, { 
	 	name : "der Monismus",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2108", "2135", "2159"],
	 	language_ID : "2",
	 	id : "1909"
 	 }, { 
	 	name : "der Dualismus",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2104", "2129", "2149"],
	 	language_ID : "2",
	 	id : "1910"
 	 }, { 
	 	name : "die Seele",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2113", "2138", "2164", "D1esrQgxMQZt"],
	 	language_ID : "2",
	 	id : "1911",
	 	tags : []
 	 }, { 
	 	name : "die Ewigkeit",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2110", "2134", "2158"],
	 	language_ID : "2",
	 	id : "1912"
 	 }, { 
	 	name : "das Argument",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2101", "2122", "2148", "2979"],
	 	language_ID : "2",
	 	id : "1913"
 	 }, { 
	 	name : "christlich",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2099", "2120", "2146", "3248"],
	 	language_ID : "2",
	 	id : "1914"
 	 }, { 
	 	name : "jüdisch",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2116", "2141", "2166"],
	 	language_ID : "2",
	 	id : "1915"
 	 }, { 
	 	name : "islamisch",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2115", "2142", "2165"],
	 	language_ID : "2",
	 	id : "1916"
 	 }, { 
	 	name : "buddhistisch",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2098", "2119", "2145"],
	 	language_ID : "2",
	 	id : "1917"
 	 }, { 
	 	name : "hinduistisch",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2114", "2140", "2167"],
	 	language_ID : "2",
	 	id : "1918"
 	 }, { 
	 	name : "daoistisch",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2100", "2121", "2147"],
	 	language_ID : "2",
	 	id : "1919"
 	 }, { 
	 	name : "der Extremist",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2105", "2130", "2150", "3250"],
	 	language_ID : "2",
	 	id : "1920",
	 	tags : [],
	 	formsPrimary_IDs : ["3223"]
 	 }, { 
	 	name : "der Gott",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2107", "2133", "2154", "3251", "3252", "3253"],
	 	language_ID : "2",
	 	id : "1921",
	 	tags : [],
	 	formsPrimary_IDs : ["3224"]
 	 }, { 
	 	name : "der Teufel",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2109", "2139", "2160"],
	 	language_ID : "2",
	 	id : "1922",
	 	tags : [],
	 	formsPrimary_IDs : ["3225"]
 	 }, { 
	 	name : "die Schöpfung",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2112", "2137", "2163"],
	 	language_ID : "2",
	 	id : "1923"
 	 }, { 
	 	name : "die Mythologie",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2111", "2136", "2161"],
	 	language_ID : "2",
	 	id : "1924"
 	 }, { 
	 	name : "beten",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2097", "2118", "2144"],
	 	language_ID : "2",
	 	id : "1925"
 	 }, { 
	 	name : "meditieren",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2117", "2143", "2162", "vfurY0lsSL6h"],
	 	language_ID : "2",
	 	id : "1926",
	 	tags : []
 	 }, { 
	 	name : "das Geld",
	 	categories_IDs : ["51"],
	 	related_IDs : ["2172", "2187", "2655", "2934"],
	 	language_ID : "2",
	 	id : "1927"
 	 }, { 
	 	name : "die Schulden",
	 	categories_IDs : ["51"],
	 	related_IDs : ["2180", "2198", "2663"],
	 	language_ID : "2",
	 	id : "1929"
 	 }, { 
	 	name : "die Bank",
	 	categories_IDs : ["51"],
	 	related_IDs : ["2173", "2194", "2661", "2827"],
	 	language_ID : "2",
	 	id : "1930"
 	 }, { 
	 	name : "der Kredit",
	 	categories_IDs : ["51"],
	 	related_IDs : ["2175", "2189", "2657", "2928", "3050"],
	 	language_ID : "2",
	 	id : "1931"
 	 }, { 
	 	name : "die Kreditkarte",
	 	categories_IDs : ["51"],
	 	related_IDs : ["2176", "2195", "2662"],
	 	language_ID : "2",
	 	id : "1932"
 	 }, { 
	 	name : "das Konto",
	 	categories_IDs : ["51"],
	 	related_IDs : ["2174", "2188", "2656"],
	 	language_ID : "2",
	 	id : "1933"
 	 }, { 
	 	name : "der Zoll",
	 	categories_IDs : ["51"],
	 	related_IDs : ["2185", "2197", "2660", "3081"],
	 	language_ID : "2",
	 	id : "1934"
 	 }, { 
	 	name : "der Zins",
	 	categories_IDs : ["51"],
	 	related_IDs : ["2184", "2190", "2659"],
	 	language_ID : "2",
	 	id : "1935"
 	 }, { 
	 	name : "die Überweisung",
	 	categories_IDs : ["51"],
	 	related_IDs : ["2182", "2196", "2665"],
	 	language_ID : "2",
	 	id : "1936"
 	 }, { 
	 	name : "überweisen",
	 	categories_IDs : ["51"],
	 	related_IDs : ["2186", "2193", "2666"],
	 	language_ID : "2",
	 	id : "1937"
 	 }, { 
	 	name : "schenken",
	 	categories_IDs : ["51"],
	 	related_IDs : ["2183", "2192", "2667"],
	 	language_ID : "2",
	 	id : "1938"
 	 }, { 
	 	name : "die Steuer",
	 	categories_IDs : ["51"],
	 	related_IDs : ["2181", "2191", "2664", "2934"],
	 	language_ID : "2",
	 	id : "1939"
 	 }, { 
	 	name : "das Gemälde",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2417", "2435", "2506"],
	 	language_ID : "2",
	 	id : "1940"
 	 }, { 
	 	name : "die Skulptur",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2424", "2476", "2527"],
	 	language_ID : "2",
	 	id : "1941"
 	 }, { 
	 	name : "der Künstler",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2420", "2439", "2515", "3227", "3228"],
	 	language_ID : "2",
	 	id : "1942",
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3226"]
 	 }, { 
	 	name : "die Farbe",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2422", "2467", "2519", "3282"],
	 	language_ID : "2",
	 	id : "1943"
 	 }, { 
	 	name : "der Pinsel",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2421", "2441", "2517"],
	 	language_ID : "2",
	 	id : "1944"
 	 }, { 
	 	name : "das Material",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2418", "2436", "2509"],
	 	language_ID : "2",
	 	id : "1945"
 	 }, { 
	 	name : "klassisch",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2427", "2480", "2532", "3278"],
	 	language_ID : "2",
	 	id : "1947"
 	 }, { 
	 	name : "modern",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2428", "2482", "2534", "3277"],
	 	language_ID : "2",
	 	id : "1948"
 	 }, { 
	 	name : "erschaffen",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2425", "2479", "2530"],
	 	language_ID : "2",
	 	id : "1949"
 	 }, { 
	 	name : "zerstören",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2429", "2485", "2537"],
	 	language_ID : "2",
	 	id : "1950"
 	 }, { 
	 	name : "die Kultur",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2423", "2473", "2524"],
	 	language_ID : "2",
	 	id : "1951"
 	 }, { 
	 	name : "der Autor",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2419", "2438", "2512", "3275", "3276"],
	 	language_ID : "2",
	 	id : "1952",
	 	tags : [],
	 	formsPrimary_IDs : ["3229"]
 	 }, { 
	 	name : "die Ampel",
	 	categories_IDs : ["54"],
	 	related_IDs : ["2565", "2611", "2684"],
	 	language_ID : "2",
	 	id : "1953"
 	 }, { 
	 	name : "die Straße",
	 	categories_IDs : ["54"],
	 	related_IDs : ["2569", "2613", "2686"],
	 	language_ID : "2",
	 	id : "1954"
 	 }, { 
	 	name : "der Gehweg",
	 	categories_IDs : ["54"],
	 	related_IDs : ["2564", "2609", "2683", "3268"],
	 	language_ID : "2",
	 	id : "1955"
 	 }, { 
	 	name : "der Zebrastreifen",
	 	categories_IDs : ["54"],
	 	related_IDs : ["2567", "2610", "2688", "3267"],
	 	language_ID : "2",
	 	id : "1956"
 	 }, { 
	 	name : "die Vorfahrt",
	 	categories_IDs : ["54"],
	 	related_IDs : ["2568", "2614", "2687"],
	 	language_ID : "2",
	 	id : "1957"
 	 }, { 
	 	name : "der Unfall",
	 	categories_IDs : ["54"],
	 	related_IDs : ["2566", "2608", "2681"],
	 	language_ID : "2",
	 	id : "1958"
 	 }, { 
	 	name : "die Polizei",
	 	categories_IDs : ["54"],
	 	related_IDs : ["2574", "2612", "2685"],
	 	language_ID : "2",
	 	id : "1959"
 	 }, { 
	 	name : "einen Unfall bauen",
	 	categories_IDs : ["54"],
	 	related_IDs : ["2615", "2682"],
	 	language_ID : "2",
	 	id : "1960"
 	 }, { 
	 	name : "studieren",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1749", "2576", "2628"],
	 	language_ID : "2",
	 	id : "1961"
 	 }, { 
	 	name : "eine Ausbildung machen",
	 	categories_IDs : ["55", "61"],
	 	related_IDs : ["2578", "2626", "2698", "mBwuIiIsWcBC"],
	 	language_ID : "2",
	 	id : "1962",
	 	tags : []
 	 }, { 
	 	name : "zur Schule gehen",
	 	categories_IDs : ["55"],
	 	related_IDs : ["2579", "2629"],
	 	language_ID : "2",
	 	id : "1963"
 	 }, { 
	 	name : "üben",
	 	categories_IDs : ["55"],
	 	related_IDs : ["2575", "2630", "2697"],
	 	language_ID : "2",
	 	id : "1964"
 	 }, { 
	 	name : "die Hausaufgabe",
	 	categories_IDs : ["55"],
	 	related_IDs : ["2587", "2623", "2693"],
	 	language_ID : "2",
	 	id : "1965"
 	 }, { 
	 	name : "der Unterricht",
	 	categories_IDs : ["55"],
	 	related_IDs : ["2586", "2618", "2696", "3258", "3271"],
	 	language_ID : "2",
	 	id : "1966"
 	 }, { 
	 	name : "das Fach",
	 	categories_IDs : ["55"],
	 	related_IDs : ["2582", "2619", "2689"],
	 	language_ID : "2",
	 	id : "1967"
 	 }, { 
	 	name : "das Studium",
	 	categories_IDs : ["55"],
	 	related_IDs : ["2583", "2621", "2690"],
	 	language_ID : "2",
	 	id : "1968"
 	 }, { 
	 	name : "die Universität",
	 	categories_IDs : ["55"],
	 	related_IDs : ["2588", "2625", "2695"],
	 	language_ID : "2",
	 	id : "1969"
 	 }, { 
	 	name : "die Ausbildung",
	 	categories_IDs : ["55"],
	 	related_IDs : ["2577", "2622", "2692", "2945", "3266"],
	 	language_ID : "2",
	 	id : "1970"
 	 }, { 
	 	name : "der Lehrer",
	 	categories_IDs : ["55"],
	 	related_IDs : ["2584", "2616", "2691", "3232"],
	 	language_ID : "2",
	 	id : "1971",
	 	tags : [],
	 	formsPrimary_IDs : ["3230"]
 	 }, { 
	 	name : "die Professorin",
	 	categories_IDs : ["55"],
	 	related_IDs : ["2585", "2617", "2691", "3232"],
	 	language_ID : "2",
	 	id : "1972",
	 	tags : [],
	 	formsPrimary_IDs : ["3231"]
 	 }, { 
	 	name : "die Blume",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1975", "2388", "2411", "2735", "61PXgQhVZZop"],
	 	language_ID : "2",
	 	id : "1973"
 	 }, { 
	 	name : "die Sonnenblume",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2394", "2416", "2739"],
	 	language_ID : "2",
	 	id : "1974"
 	 }, { 
	 	name : "die Blüte",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1973", "2388", "2389", "2411", "2735"],
	 	language_ID : "2",
	 	id : "1975"
 	 }, { 
	 	name : "das Blatt",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2381", "2402", "2727", "TZ4FOvESTSij"],
	 	language_ID : "2",
	 	id : "1976",
	 	tags : []
 	 }, { 
	 	name : "die Nadel",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2392", "2414", "2737"],
	 	language_ID : "2",
	 	id : "1977"
 	 }, { 
	 	name : "der Stamm",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2386", "2408", "2733", "inbvtBsa6dyU"],
	 	language_ID : "2",
	 	id : "1978",
	 	tags : []
 	 }, { 
	 	name : "das Holz",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2382", "2403", "2728", "3288"],
	 	language_ID : "2",
	 	id : "1979",
	 	tags : []
 	 }, { 
	 	name : "die Wurzel",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2396", "2702", "2741"],
	 	language_ID : "2",
	 	id : "1980"
 	 }, { 
	 	name : "wachsen",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2400", "2706", "2745"],
	 	language_ID : "2",
	 	id : "1981"
 	 }, { 
	 	name : "verrotten",
	 	categories_IDs : ["56", "39"],
	 	related_IDs : ["2398", "2705", "2744", "61PXgQhVZZop"],
	 	language_ID : "2",
	 	id : "1982",
	 	tags : []
 	 }, { 
	 	name : "der Baum",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2383", "2405", "2729", "2950"],
	 	language_ID : "2",
	 	id : "1983"
 	 }, { 
	 	name : "die Eiche",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2390", "2412", "2736"],
	 	language_ID : "2",
	 	id : "1984"
 	 }, { 
	 	name : "die Birke",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2387", "2410", "2734"],
	 	language_ID : "2",
	 	id : "1985"
 	 }, { 
	 	name : "die Tanne",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2395", "2701", "2740"],
	 	language_ID : "2",
	 	id : "1986"
 	 }, { 
	 	name : "der Kirschbaum",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2384", "2407", "2731", "KXZVUnLiAC8G"],
	 	language_ID : "2",
	 	id : "1987",
	 	tags : []
 	 }, { 
	 	name : "die Palme",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2393", "2415", "2738"],
	 	language_ID : "2",
	 	id : "1988"
 	 }, { 
	 	name : "sprießen",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2399", "2704", "2743"],
	 	language_ID : "2",
	 	id : "1989"
 	 }, { 
	 	name : "gießen",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2397", "2703", "2742", "3008", "61PXgQhVZZop"],
	 	language_ID : "2",
	 	id : "1990"
 	 }, { 
	 	name : "der Dünger",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2401", "2406", "2730"],
	 	language_ID : "2",
	 	id : "1991"
 	 }, { 
	 	name : "die Löwin",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2338", "2352", "2379", "3234", "3235", "3236"],
	 	language_ID : "2",
	 	id : "1992",
	 	tags : [],
	 	formsPrimary_IDs : ["3233"]
 	 }, { 
	 	name : "der Affe",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2329", "2348", "2367"],
	 	language_ID : "2",
	 	id : "1993"
 	 }, { 
	 	name : "der Hund",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2333", "2349", "2365"],
	 	language_ID : "2",
	 	id : "1994"
 	 }, { 
	 	name : "die Katze",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2337", "2357", "2364", "lYRdtSvrVPD1"],
	 	language_ID : "2",
	 	id : "1995"
 	 }, { 
	 	name : "der Vogel",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2325", "2354", "2368", "3284"],
	 	language_ID : "2",
	 	id : "1996"
 	 }, { 
	 	name : "der Wolf",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2340", "2355", "2369"],
	 	language_ID : "2",
	 	id : "1997"
 	 }, { 
	 	name : "das Kaninchen",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2332", "2346", "2376"],
	 	language_ID : "2",
	 	id : "1998"
 	 }, { 
	 	name : "die Spinne",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2342", "2359", "2371", "2839"],
	 	language_ID : "2",
	 	id : "1999"
 	 }, { 
	 	name : "die Maus",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2341", "2358", "2372"],
	 	language_ID : "2",
	 	id : "2000"
 	 }, { 
	 	name : "füttern",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2343", "2362", "2374", "3283"],
	 	language_ID : "2",
	 	id : "2001"
 	 }, { 
	 	name : "die Ameise",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2336", "2356", "2370"],
	 	language_ID : "2",
	 	id : "2002"
 	 }, { 
	 	name : "der Kater",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2334", "2350", "2378"],
	 	language_ID : "2",
	 	id : "2003",
	 	tags : [],
	 	formPrimaryName : "männlich"
 	 }, { 
	 	name : "der Kranich",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2335", "2351", "2377"],
	 	language_ID : "2",
	 	id : "2004"
 	 }, { 
	 	name : "der Storch",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2339", "2353", "2380"],
	 	language_ID : "2",
	 	id : "2005"
 	 }, { 
	 	name : "die Bakterie",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2322", "2709", "2803", "3053"],
	 	language_ID : "2",
	 	id : "2006"
 	 }, { 
	 	name : "der Virus",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2324", "2714", "2801"],
	 	language_ID : "2",
	 	id : "2007"
 	 }, { 
	 	name : "der Außerirdische",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2707", "2799", "3257"],
	 	language_ID : "2",
	 	id : "2008",
	 	tags : [],
	 	formPrimaryName : "männlich",
	 	formsPrimary_IDs : ["3237", "3238"]
 	 }, { 
	 	name : "der Stein",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2253", "2277", "2300"],
	 	language_ID : "2",
	 	id : "2009"
 	 }, { 
	 	name : "das Metall",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2266", "2294", "3080"],
	 	language_ID : "2",
	 	id : "2010"
 	 }, { 
	 	name : "die Treppe",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2057", "2086", "2224", "2947"],
	 	id : "2011"
 	 }, { 
	 	name : "der Fahrstuhl",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2050", "2072", "2211", "2947", "3241", "3242", "VxiTvowqQjvs"],
	 	id : "2012",
	 	tags : []
 	 }, { 
	 	name : "das Zimmer",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2014", "2048", "2073", "2203", "3243"],
	 	id : "2013",
	 	tags : []
 	 }, { 
	 	name : "der Raum",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2013", "2048", "2073", "2203", "3243"],
	 	id : "2014"
 	 }, { 
	 	name : "das Stockwerk",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2046", "2067", "2220", "2947", "3239", "3240"],
	 	id : "2015"
 	 }, { 
	 	name : "die Tür",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2058", "2087", "2222"],
	 	id : "2016"
 	 }, { 
	 	name : "das Fenster",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2043", "2064", "2210"],
	 	id : "2017"
 	 }, { 
	 	name : "der Balkon",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2070", "2093", "2206", "3WlY5YCZhmaR"],
	 	id : "2018",
	 	tags : []
 	 }, { 
	 	name : "das Wohnzimmer",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2047", "2068", "2228"],
	 	id : "2019"
 	 }, { 
	 	name : "das Schlafzimmer",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2045", "2066", "2219"],
	 	id : "2020"
 	 }, { 
	 	name : "das Esszimmer",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2042", "2063", "2209"],
	 	id : "2021"
 	 }, { 
	 	name : "das Bad",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2041", "2062", "2205"],
	 	id : "2022"
 	 }, { 
	 	name : "die Dusche",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2055", "2082", "2217", "ndpCqAhkwZk9"],
	 	id : "2023",
	 	tags : []
 	 }, { 
	 	name : "die Badewanne",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2052", "2079", "2214"],
	 	id : "2024"
 	 }, { 
	 	name : "die Spüle",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2084", "2221", "3244", "2056"],
	 	id : "2025"
 	 }, { 
	 	name : "die Waschmaschine",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2060", "2091", "2226", "IoDXXSiw9jyE"],
	 	id : "2026",
	 	tags : []
 	 }, { 
	 	name : "die Decke",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2054", "2216", "2077", "ZLBieKuJasVT"],
	 	id : "2027",
	 	tags : []
 	 }, { 
	 	name : "die Wand",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2059", "2088", "2227"],
	 	id : "2028"
 	 }, { 
	 	name : "der Boden",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2071", "2207", "3239"],
	 	id : "2029"
 	 }, { 
	 	name : "die Baustelle",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2053", "2080", "2215"],
	 	id : "2030"
 	 }, { 
	 	name : "bauen",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2040", "2061", "2204"],
	 	id : "2031"
 	 }, { 
	 	name : "das Haus",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2044", "2065", "2213"],
	 	id : "2032"
 	 }, { 
	 	name : "der Turm",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2051", "2085", "2223"],
	 	id : "2033"
 	 }, { 
	 	name : "die Höhle",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2259", "2281", "2292", "3054"],
	 	language_ID : "2",
	 	id : "2034"
 	 }, { 
	 	name : "der Planet",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2250", "2272", "2296", "lsdS23Q8DMEo"],
	 	language_ID : "2",
	 	id : "2035",
	 	tags : []
 	 }, { 
	 	name : "der Mond",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2249", "2271", "2295", "ijF0Fp6Fu3UZ"],
	 	language_ID : "2",
	 	id : "2036",
	 	tags : []
 	 }, { 
	 	name : "der Fluß",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2247", "2269", "2289", "2290", "D53WkvETV72d"],
	 	language_ID : "2",
	 	id : "2037",
	 	tags : []
 	 }, { 
	 	name : "der Gletscher",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2248", "2270", "2291", "3055"],
	 	language_ID : "2",
	 	id : "2039"
 	 }, { 
	 	name : "construct",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2031", "2061", "2204"],
	 	language_ID : "3",
	 	id : "2040"
 	 }, { 
	 	name : "the bath",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2022", "2062", "2205"],
	 	language_ID : "3",
	 	id : "2041"
 	 }, { 
	 	name : "the dining room",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2021", "2063", "2209"],
	 	language_ID : "3",
	 	id : "2042"
 	 }, { 
	 	name : "the window",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2017", "2064", "2210"],
	 	language_ID : "3",
	 	id : "2043"
 	 }, { 
	 	name : "the house",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2032", "2065", "2213"],
	 	language_ID : "3",
	 	id : "2044"
 	 }, { 
	 	name : "the sleeping room",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2020", "2066", "2219"],
	 	language_ID : "3",
	 	id : "2045"
 	 }, { 
	 	name : "the storey",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2015", "2067", "2220", "3239", "3240"],
	 	language_ID : "3",
	 	id : "2046",
	 	tags : []
 	 }, { 
	 	name : "the living room",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2019", "2068", "2228"],
	 	language_ID : "3",
	 	id : "2047"
 	 }, { 
	 	name : "the room",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2013", "2014", "2073", "2203", "3243"],
	 	language_ID : "3",
	 	id : "2048"
 	 }, { 
	 	name : "the elevator",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2012", "2072", "2211", "3241", "3242", "HEOP6EdhUHO3"],
	 	language_ID : "3",
	 	id : "2050",
	 	tags : []
 	 }, { 
	 	name : "the tower",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2033", "2085", "2223", "FEbJ48RFZ6gq"],
	 	language_ID : "3",
	 	id : "2051"
 	 }, { 
	 	name : "the bathtub",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2024", "2079", "2214"],
	 	language_ID : "3",
	 	id : "2052"
 	 }, { 
	 	name : "the construction site",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2030", "2080", "2215"],
	 	language_ID : "3",
	 	id : "2053"
 	 }, { 
	 	name : "the ceiling",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2027", "2216", "2077", "gvQCP0ZoJYKO"],
	 	language_ID : "3",
	 	id : "2054",
	 	tags : []
 	 }, { 
	 	name : "the shower",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2023", "2082", "2217", "RQzsMLLhxorg"],
	 	language_ID : "3",
	 	id : "2055",
	 	tags : []
 	 }, { 
	 	name : "the stairs",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2011", "2086", "2224"],
	 	language_ID : "3",
	 	id : "2057"
 	 }, { 
	 	name : "the door",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2016", "2087", "2222"],
	 	language_ID : "3",
	 	id : "2058"
 	 }, { 
	 	name : "the wall",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2028", "2088", "2227"],
	 	language_ID : "3",
	 	id : "2059"
 	 }, { 
	 	name : "the washing machine",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2026", "2091", "2226", "rVjomyVzwWfq"],
	 	language_ID : "3",
	 	id : "2060",
	 	tags : []
 	 }, { 
	 	name : "construir",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2031", "2040", "2204"],
	 	language_ID : "5",
	 	id : "2061"
 	 }, { 
	 	name : "el baño",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2022", "2041", "2205"],
	 	language_ID : "5",
	 	id : "2062"
 	 }, { 
	 	name : "el comedor",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2021", "2042", "2209"],
	 	language_ID : "5",
	 	id : "2063"
 	 }, { 
	 	name : "la ventana",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2017", "2043", "2210"],
	 	language_ID : "5",
	 	id : "2064"
 	 }, { 
	 	name : "la casa",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2032", "2044", "2213"],
	 	language_ID : "5",
	 	id : "2065"
 	 }, { 
	 	name : "el dormitorio",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2020", "2045", "2219"],
	 	language_ID : "5",
	 	id : "2066"
 	 }, { 
	 	name : "el piso",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2015", "2046", "2220", "3239", "3240"],
	 	language_ID : "5",
	 	id : "2067"
 	 }, { 
	 	name : "el salón",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2019", "2047", "2228"],
	 	language_ID : "5",
	 	id : "2068"
 	 }, { 
	 	name : "el balcón",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2018", "2093", "2206"],
	 	language_ID : "5",
	 	id : "2070"
 	 }, { 
	 	name : "el suelo",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2029", "2207", "3239"],
	 	language_ID : "5",
	 	id : "2071"
 	 }, { 
	 	name : "el ascensor",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2012", "2050", "2211", "3241", "3242"],
	 	language_ID : "5",
	 	id : "2072",
	 	tags : []
 	 }, { 
	 	name : "la habitación",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2013", "2014", "2048", "3243"],
	 	language_ID : "5",
	 	sentence : false,
	 	id : "2073",
	 	tags : [],
	 	formsPrimary_IDs : ["3243"]
 	 }, { 
	 	name : "der Flur",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2078", "2095", "2212"],
	 	language_ID : "2",
	 	id : "2074"
 	 }, { 
	 	name : "das Dach",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2092", "2208", "2077"],
	 	language_ID : "2",
	 	id : "2075"
 	 }, { 
	 	name : "der Keller",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2083", "2096", "2218", "3246", "IyyVIyENYeZp"],
	 	language_ID : "2",
	 	id : "2076",
	 	tags : []
 	 }, { 
	 	name : "el pasillo",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2074", "2095", "2212"],
	 	language_ID : "5",
	 	id : "2078"
 	 }, { 
	 	name : "la bañera",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2024", "2052", "2214"],
	 	language_ID : "5",
	 	id : "2079"
 	 }, { 
	 	name : "la obra",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2030", "2053", "2215"],
	 	language_ID : "5",
	 	id : "2080"
 	 }, { 
	 	name : "la ducha",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2023", "2055", "2217"],
	 	language_ID : "5",
	 	id : "2082"
 	 }, { 
	 	name : "el sótano",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2076", "2096", "2218", "3246"],
	 	language_ID : "5",
	 	id : "2083"
 	 }, { 
	 	name : "el fregadero",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2025", "2221", "3244", "2056"],
	 	language_ID : "5",
	 	id : "2084",
	 	tags : []
 	 }, { 
	 	name : "la torre",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2033", "2051", "2223"],
	 	language_ID : "5",
	 	id : "2085"
 	 }, { 
	 	name : "las escaleras",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2011", "2057", "2224"],
	 	language_ID : "5",
	 	id : "2086"
 	 }, { 
	 	name : "la puerta",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2016", "2058", "2222"],
	 	language_ID : "5",
	 	id : "2087"
 	 }, { 
	 	name : "la pared",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2028", "2059", "2227"],
	 	language_ID : "5",
	 	id : "2088"
 	 }, { 
	 	name : "das Waschbecken",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2090", "2225", "3244", "3245", "2056", "ak716rhFNG5w"],
	 	language_ID : "2",
	 	id : "2089"
 	 }, { 
	 	name : "el lavabo",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2089", "2225", "3244", "3245", "2056"],
	 	language_ID : "5",
	 	id : "2090"
 	 }, { 
	 	name : "la lavadora",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2026", "2060", "2226"],
	 	language_ID : "5",
	 	id : "2091"
 	 }, { 
	 	name : "the roof",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2075", "2208", "2077"],
	 	language_ID : "3",
	 	id : "2092"
 	 }, { 
	 	name : "the balcony",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2018", "2070", "2206", "1rbf73hSeW6u"],
	 	language_ID : "3",
	 	id : "2093",
	 	tags : []
 	 }, { 
	 	name : "the corridor",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2074", "2078", "2212"],
	 	language_ID : "3",
	 	id : "2095"
 	 }, { 
	 	name : "the cellar",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2076", "2083", "2218", "3246", "H8oIVybyMP3Z"],
	 	language_ID : "3",
	 	id : "2096",
	 	tags : []
 	 }, { 
	 	name : "pray",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1925", "2118", "2144"],
	 	language_ID : "3",
	 	id : "2097"
 	 }, { 
	 	name : "Buddhist",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1917", "2119", "2145"],
	 	language_ID : "3",
	 	id : "2098"
 	 }, { 
	 	name : "Christian",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1914", "2120", "2146", "3248"],
	 	language_ID : "3",
	 	id : "2099"
 	 }, { 
	 	name : "Daoist",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1919", "2121", "2147"],
	 	language_ID : "3",
	 	id : "2100"
 	 }, { 
	 	name : "the point",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1913", "2122", "2148", "2979"],
	 	language_ID : "3",
	 	id : "2101"
 	 }, { 
	 	name : "the prayer",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1908", "2132", "2152"],
	 	language_ID : "3",
	 	id : "2102"
 	 }, { 
	 	name : "the proof",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1907", "2123", "2151", "WY5qRPNuUXuI"],
	 	language_ID : "3",
	 	id : "2103",
	 	tags : []
 	 }, { 
	 	name : "the dualism",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1910", "2129", "2149"],
	 	language_ID : "3",
	 	id : "2104"
 	 }, { 
	 	name : "the extremist",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1920", "2130", "2150", "3223", "3250"],
	 	language_ID : "3",
	 	id : "2105"
 	 }, { 
	 	name : "the belief",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1906", "2131", "2153", "3247"],
	 	language_ID : "3",
	 	id : "2106",
	 	tags : []
 	 }, { 
	 	name : "the God",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1921", "2133", "2154"],
	 	language_ID : "3",
	 	id : "2107",
	 	tags : [],
	 	formPrimaryName : "male",
	 	formsPrimary_IDs : ["3252"]
 	 }, { 
	 	name : "the Monism",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1909", "2135", "2159"],
	 	language_ID : "3",
	 	id : "2108"
 	 }, { 
	 	name : "the devil",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1922", "2139", "2160", "3225"],
	 	language_ID : "3",
	 	id : "2109"
 	 }, { 
	 	name : "the eternity",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1912", "2134", "2158"],
	 	language_ID : "3",
	 	id : "2110"
 	 }, { 
	 	name : "the mythology",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1924", "2136", "2161"],
	 	language_ID : "3",
	 	id : "2111"
 	 }, { 
	 	name : "the creation",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1923", "2137", "2163"],
	 	language_ID : "3",
	 	id : "2112"
 	 }, { 
	 	name : "the soul",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1911", "2138", "2164", "QTMYs7O8wUH7"],
	 	language_ID : "3",
	 	id : "2113",
	 	tags : []
 	 }, { 
	 	name : "Hindu",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1918", "2140", "2167"],
	 	language_ID : "3",
	 	id : "2114"
 	 }, { 
	 	name : "Islamic",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1916", "2142", "2165"],
	 	language_ID : "3",
	 	id : "2115"
 	 }, { 
	 	name : "Jewish",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1915", "2141", "2166"],
	 	language_ID : "3",
	 	id : "2116"
 	 }, { 
	 	name : "meditate",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1926", "2143", "2162", "Uncv9gwaavb1"],
	 	language_ID : "3",
	 	id : "2117",
	 	tags : []
 	 }, { 
	 	name : "rezar",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1925", "2097", "2144"],
	 	language_ID : "5",
	 	id : "2118"
 	 }, { 
	 	name : "budista",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1917", "2098", "2145"],
	 	language_ID : "5",
	 	id : "2119"
 	 }, { 
	 	name : "cristiano",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1914", "2099", "2146"],
	 	language_ID : "5",
	 	id : "2120",
	 	tags : [],
	 	formsPrimary_IDs : ["3248"]
 	 }, { 
	 	name : "taoista",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1919", "2100", "2147"],
	 	language_ID : "5",
	 	id : "2121"
 	 }, { 
	 	name : "el argumento",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1913", "2101", "2148"],
	 	language_ID : "5",
	 	id : "2122"
 	 }, { 
	 	name : "la prueba",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1907", "2103", "2151"],
	 	language_ID : "5",
	 	id : "2123"
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["2126", "2127", "2128", "3249"],
	 	language_ID : "2",
	 	name : "die Haut",
	 	id : "2125"
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["2125", "2127", "2128", "3249"],
	 	language_ID : "3",
	 	name : "the skin",
	 	id : "2126"
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["2125", "2126", "2128", "3249"],
	 	language_ID : "1",
	 	name : "en hud",
	 	id : "2127",
	 	tags : []
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["2125", "2126", "2127", "3249"],
	 	name : "la piel",
	 	language_ID : "5",
	 	id : "2128"
 	 }, { 
	 	name : "el dualismo",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1910", "2104", "2149"],
	 	language_ID : "5",
	 	id : "2129"
 	 }, { 
	 	name : "el extremista",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1920", "2105", "2150"],
	 	language_ID : "5",
	 	id : "2130",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3250"]
 	 }, { 
	 	name : "la fe",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1906", "2106", "2153", "3247"],
	 	language_ID : "5",
	 	id : "2131"
 	 }, { 
	 	name : "la oración",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1908", "2102", "2152"],
	 	language_ID : "5",
	 	id : "2132"
 	 }, { 
	 	name : "el dios",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1921", "2107", "2154", "3224", "3252", "3253"],
	 	language_ID : "5",
	 	id : "2133",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3251", "3252"]
 	 }, { 
	 	name : "la eternidad",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1912", "2110", "2158"],
	 	language_ID : "5",
	 	id : "2134"
 	 }, { 
	 	name : "el monismo",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1909", "2108", "2159"],
	 	language_ID : "5",
	 	id : "2135"
 	 }, { 
	 	name : "la mitología",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1924", "2111", "2161"],
	 	language_ID : "5",
	 	id : "2136"
 	 }, { 
	 	name : "la creación",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1923", "2112", "2163"],
	 	language_ID : "5",
	 	id : "2137"
 	 }, { 
	 	name : "el alma",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1911", "2113", "2164"],
	 	language_ID : "5",
	 	id : "2138"
 	 }, { 
	 	name : "el diablo, la diabla",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1922", "2109", "2160", "3225"],
	 	language_ID : "5",
	 	id : "2139"
 	 }, { 
	 	name : "hindú",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1918", "2114", "2167"],
	 	language_ID : "5",
	 	id : "2140"
 	 }, { 
	 	name : "judío, judía",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1915", "2116", "2166"],
	 	language_ID : "5",
	 	id : "2141"
 	 }, { 
	 	name : "islámico, islámica",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1916", "2115", "2165"],
	 	language_ID : "5",
	 	id : "2142"
 	 }, { 
	 	name : "meditar",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1926", "2117", "2162"],
	 	language_ID : "5",
	 	id : "2143"
 	 }, { 
	 	name : "be",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1925", "2097", "2118"],
	 	language_ID : "1",
	 	id : "2144"
 	 }, { 
	 	name : "buddist",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1917", "2098", "2119"],
	 	language_ID : "1",
	 	id : "2145"
 	 }, { 
	 	name : "kristen",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1914", "2099", "2120", "3248"],
	 	language_ID : "1",
	 	id : "2146"
 	 }, { 
	 	name : "taoistiska",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1919", "2100", "2121"],
	 	language_ID : "1",
	 	id : "2147"
 	 }, { 
	 	name : "ett argument",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1913", "2101", "2122", "2979"],
	 	language_ID : "1",
	 	id : "2148"
 	 }, { 
	 	name : "ett dualitet",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1910", "2104", "2129"],
	 	language_ID : "1",
	 	id : "2149"
 	 }, { 
	 	name : "en extremist",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1920", "2105", "2130", "3223", "3250"],
	 	language_ID : "1",
	 	id : "2150"
 	 }, { 
	 	name : "ett bevis",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1907", "2103", "2123"],
	 	language_ID : "1",
	 	id : "2151"
 	 }, { 
	 	name : "en bön",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1908", "2102", "2132"],
	 	language_ID : "1",
	 	id : "2152"
 	 }, { 
	 	name : "en tro",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1906", "2106", "2131", "3247"],
	 	language_ID : "1",
	 	id : "2153"
 	 }, { 
	 	name : "en gud",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1921", "2107", "2133"],
	 	language_ID : "1",
	 	id : "2154",
	 	tags : [],
	 	formPrimaryName : "manlig",
	 	formsPrimary_IDs : ["3253"]
 	 }, { 
	 	name : "etwas",
	 	categories_IDs : ["33"],
	 	related_IDs : ["2156", "2876"],
	 	language_ID : "2",
	 	id : "2155"
 	 }, { 
	 	name : "något",
	 	categories_IDs : ["33"],
	 	related_IDs : ["2155", "2876"],
	 	language_ID : "1",
	 	id : "2156"
 	 }, { 
	 	name : "nej",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1711", "74", "92"],
	 	language_ID : "1",
	 	id : "2157"
 	 }, { 
	 	name : "en evighet",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1912", "2110", "2134"],
	 	language_ID : "1",
	 	id : "2158"
 	 }, { 
	 	name : "en monismen",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1909", "2108", "2135"],
	 	language_ID : "1",
	 	id : "2159"
 	 }, { 
	 	name : "en djävul, en hon-djävul",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1922", "2109", "2139", "3225"],
	 	language_ID : "1",
	 	id : "2160"
 	 }, { 
	 	name : "en mytologi",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1924", "2111", "2136"],
	 	language_ID : "1",
	 	id : "2161"
 	 }, { 
	 	name : "meditera",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1926", "2117", "2143"],
	 	language_ID : "1",
	 	id : "2162"
 	 }, { 
	 	name : "en skapelse, en kreation",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1923", "2112", "2137"],
	 	language_ID : "1",
	 	id : "2163"
 	 }, { 
	 	name : "en själ",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1911", "2113", "2138"],
	 	language_ID : "1",
	 	id : "2164"
 	 }, { 
	 	name : "islamisk",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1916", "2115", "2142"],
	 	language_ID : "1",
	 	id : "2165"
 	 }, { 
	 	name : "judisk",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1915", "2116", "2141"],
	 	language_ID : "1",
	 	id : "2166"
 	 }, { 
	 	name : "hindu",
	 	categories_IDs : ["49"],
	 	related_IDs : ["1918", "2114", "2140"],
	 	language_ID : "1",
	 	id : "2167"
 	 }, { 
	 	name : "der Himmel",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2170", "2592", "2653"],
	 	language_ID : "2",
	 	id : "2168"
 	 }, { 
	 	name : "die Wiedergeburt",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2171", "2593", "2654"],
	 	language_ID : "2",
	 	id : "2169"
 	 }, { 
	 	name : "en himmel",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2168", "2592", "2653"],
	 	language_ID : "1",
	 	id : "2170"
 	 }, { 
	 	name : "en pånyttfödelse",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2169", "2593", "2654"],
	 	language_ID : "1",
	 	id : "2171"
 	 }, { 
	 	name : "pengar",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1927", "2187", "2655"],
	 	language_ID : "1",
	 	id : "2172"
 	 }, { 
	 	name : "en bank",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1930", "2194", "2661", "2834"],
	 	language_ID : "1",
	 	id : "2173"
 	 }, { 
	 	name : "ett konto",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1933", "2188", "2656"],
	 	language_ID : "1",
	 	id : "2174"
 	 }, { 
	 	name : "ett kredit",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1931", "2189", "2657", "3050"],
	 	language_ID : "1",
	 	id : "2175"
 	 }, { 
	 	name : "ett kontokort",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1932", "2195", "2662"],
	 	language_ID : "1",
	 	id : "2176"
 	 }, { 
	 	name : "en cirkulation",
	 	categories_IDs : ["51"],
	 	related_IDs : ["2199", "2658", "1928"],
	 	language_ID : "1",
	 	id : "2177"
 	 }, { 
	 	name : "ett kretslopp",
	 	categories_IDs : ["44"],
	 	related_IDs : ["2540", "1928"],
	 	language_ID : "1",
	 	id : "2179"
 	 }, { 
	 	name : "en skuld",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1929", "2198", "2663"],
	 	language_ID : "1",
	 	id : "2180"
 	 }, { 
	 	name : "en skatt",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1939", "2191", "2664"],
	 	language_ID : "1",
	 	id : "2181"
 	 }, { 
	 	name : "en girering",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1936", "2196", "2665"],
	 	language_ID : "1",
	 	id : "2182"
 	 }, { 
	 	name : "skänka",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1938", "2192", "2667"],
	 	language_ID : "1",
	 	id : "2183"
 	 }, { 
	 	name : "en ränta",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1935", "2190", "2659"],
	 	language_ID : "1",
	 	id : "2184"
 	 }, { 
	 	name : "en tull",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1934", "2660", "3081"],
	 	language_ID : "1",
	 	id : "2185"
 	 }, { 
	 	name : "girera, överföra",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1937", "2193", "2666"],
	 	language_ID : "1",
	 	id : "2186"
 	 }, { 
	 	name : "the money",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1927", "2172", "2655"],
	 	language_ID : "3",
	 	id : "2187"
 	 }, { 
	 	name : "the bank account",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1933", "2174", "2656"],
	 	language_ID : "3",
	 	id : "2188"
 	 }, { 
	 	name : "the credit",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1931", "2175", "2657", "3050"],
	 	language_ID : "3",
	 	id : "2189"
 	 }, { 
	 	name : "the interest",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1935", "2184", "2659"],
	 	language_ID : "3",
	 	id : "2190"
 	 }, { 
	 	name : "the tax",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1939", "2181", "2664"],
	 	language_ID : "3",
	 	id : "2191"
 	 }, { 
	 	name : "give as a gift",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1938", "2183", "2667"],
	 	language_ID : "3",
	 	id : "2192"
 	 }, { 
	 	name : "transfer",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1937", "2186", "2666"],
	 	language_ID : "3",
	 	id : "2193"
 	 }, { 
	 	name : "the bank",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1930", "2173", "2661", "2828"],
	 	language_ID : "3",
	 	id : "2194"
 	 }, { 
	 	name : "the credit card",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1932", "2176", "2662"],
	 	language_ID : "3",
	 	id : "2195"
 	 }, { 
	 	name : "the transfer",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1936", "2182", "2665"],
	 	language_ID : "3",
	 	id : "2196"
 	 }, { 
	 	name : "the customs",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1934", "2660"],
	 	language_ID : "3",
	 	id : "2197"
 	 }, { 
	 	name : "the debt",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1929", "2180", "2663"],
	 	language_ID : "3",
	 	id : "2198"
 	 }, { 
	 	name : "the cycle",
	 	categories_IDs : ["51"],
	 	related_IDs : ["2177", "2658", "1928"],
	 	language_ID : "3",
	 	id : "2199"
 	 }, { 
	 	related_IDs : ["2201", "2202", "2594"],
	 	categories_IDs : ["51"],
	 	name : "la estadística",
	 	language_ID : "5",
	 	id : "2200"
 	 }, { 
	 	categories_IDs : ["51"],
	 	related_IDs : ["2200", "2202", "2594"],
	 	language_ID : "2",
	 	name : "die Statistik",
	 	id : "2201"
 	 }, { 
	 	categories_IDs : ["51"],
	 	related_IDs : ["2200", "2201", "2594"],
	 	name : "en statistik",
	 	language_ID : "1",
	 	id : "2202"
 	 }, { 
	 	name : "en rum",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2013", "2014", "2048", "3243"],
	 	language_ID : "1",
	 	id : "2203"
 	 }, { 
	 	name : "bygga",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2031", "2040", "2061"],
	 	language_ID : "1",
	 	id : "2204"
 	 }, { 
	 	name : "ett bad",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2022", "2041", "2062"],
	 	language_ID : "1",
	 	id : "2205"
 	 }, { 
	 	name : "en balkong",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2018", "2070", "2093"],
	 	language_ID : "1",
	 	id : "2206"
 	 }, { 
	 	name : "en golv, en mark",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2029", "2071", "3239"],
	 	language_ID : "1",
	 	id : "2207"
 	 }, { 
	 	name : "ett tak",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2075", "2092", "2077"],
	 	language_ID : "1",
	 	id : "2208"
 	 }, { 
	 	name : "ett matrum",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2021", "2042", "2063"],
	 	language_ID : "1",
	 	id : "2209"
 	 }, { 
	 	name : "en fänster",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2017", "2043", "2064"],
	 	language_ID : "1",
	 	id : "2210"
 	 }, { 
	 	name : "en hiss",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2012", "2050", "2072", "3241", "3242"],
	 	language_ID : "1",
	 	id : "2211"
 	 }, { 
	 	name : "en hall, en korridor",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2074", "2078", "2095"],
	 	language_ID : "1",
	 	id : "2212"
 	 }, { 
	 	name : "ett hus",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2032", "2044", "2065"],
	 	language_ID : "1",
	 	id : "2213"
 	 }, { 
	 	name : "ett badkar",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2024", "2052", "2079"],
	 	language_ID : "1",
	 	id : "2214"
 	 }, { 
	 	name : "en byggplats",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2030", "2053", "2080"],
	 	language_ID : "1",
	 	id : "2215"
 	 }, { 
	 	name : "ett lock",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2027", "2054", "2077"],
	 	language_ID : "1",
	 	id : "2216"
 	 }, { 
	 	name : "en dusch",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2023", "2055", "2082"],
	 	language_ID : "1",
	 	id : "2217"
 	 }, { 
	 	name : "en källare",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2076", "2083", "2096", "3246"],
	 	language_ID : "1",
	 	id : "2218"
 	 }, { 
	 	name : "en sovrum",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2020", "2045", "2066"],
	 	language_ID : "1",
	 	id : "2219"
 	 }, { 
	 	name : "en våning",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2015", "2046", "2067", "3239", "3240"],
	 	language_ID : "1",
	 	id : "2220"
 	 }, { 
	 	name : "en diskbänk",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2025", "2084", "3244", "2056"],
	 	language_ID : "1",
	 	id : "2221",
	 	tags : []
 	 }, { 
	 	name : "en dörr",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2016", "2058", "2087"],
	 	language_ID : "1",
	 	id : "2222"
 	 }, { 
	 	name : "ett torn",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2033", "2051", "2085"],
	 	language_ID : "1",
	 	id : "2223"
 	 }, { 
	 	name : "en trappa",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2011", "2057", "2086"],
	 	language_ID : "1",
	 	id : "2224"
 	 }, { 
	 	name : "ett handfat",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2089", "2090", "3244", "3245", "2056"],
	 	language_ID : "1",
	 	id : "2225",
	 	tags : []
 	 }, { 
	 	name : "ett tvättmaskin",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2026", "2060", "2091"],
	 	language_ID : "1",
	 	id : "2226"
 	 }, { 
	 	name : "en vägg",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2028", "2059", "2088"],
	 	language_ID : "1",
	 	id : "2227"
 	 }, { 
	 	name : "ett vardagsrum",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2019", "2047", "2068"],
	 	language_ID : "1",
	 	id : "2228"
 	 }, { 
	 	name : "das Eis",
	 	categories_IDs : ["40", "59"],
	 	related_IDs : ["2242", "2263", "2286", "N8FalBSGX5Fs", "jXDkH1ZKGzc6", "tt0O0lDWgN7U"],
	 	language_ID : "2",
	 	id : "2229",
	 	tags : []
 	 }, { 
	 	name : "der Schnee",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2252", "2276", "2299"],
	 	language_ID : "2",
	 	id : "2230"
 	 }, { 
	 	name : "das Wetter",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2246", "2267", "2305"],
	 	language_ID : "2",
	 	id : "2232"
 	 }, { 
	 	name : "die Sonne",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2260", "2282", "2302", "3285"],
	 	language_ID : "2",
	 	id : "2233",
	 	tags : []
 	 }, { 
	 	name : "der Vulkan",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2256", "2279", "2307"],
	 	language_ID : "2",
	 	id : "2234"
 	 }, { 
	 	name : "das Erdbeben",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2243", "2264", "2287", "YOsylFESuhFx"],
	 	language_ID : "2",
	 	id : "2235",
	 	tags : []
 	 }, { 
	 	name : "die Wolke",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2261", "2283", "2306"],
	 	language_ID : "2",
	 	id : "2236"
 	 }, { 
	 	name : "regnen",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2262", "2284", "2308", "2823"],
	 	language_ID : "2",
	 	id : "2238"
 	 }, { 
	 	name : "die Ebbe",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2257", "2280", "2288"],
	 	language_ID : "2",
	 	id : "2239"
 	 }, { 
	 	name : "die Flut",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2258", "2268", "2289"],
	 	language_ID : "2",
	 	id : "2240"
 	 }, { 
	 	name : "der Sturm",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2254", "2278", "2301", "2950"],
	 	language_ID : "2",
	 	id : "2241"
 	 }, { 
	 	name : "the ice",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2229", "2263", "2286"],
	 	language_ID : "3",
	 	id : "2242"
 	 }, { 
	 	name : "the earthquake",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2235", "2264", "2287", "59qyaMIYRPGd"],
	 	language_ID : "3",
	 	id : "2243",
	 	tags : []
 	 }, { 
	 	name : "the weather",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2232", "2267", "2305"],
	 	language_ID : "3",
	 	id : "2246"
 	 }, { 
	 	name : "the river",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2037", "2269", "2289", "2290", "dnZOw8kVJUfy"],
	 	language_ID : "3",
	 	id : "2247",
	 	tags : []
 	 }, { 
	 	name : "the glacier",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2039", "2270", "2291", "3055"],
	 	language_ID : "3",
	 	id : "2248"
 	 }, { 
	 	name : "the moon",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2036", "2271", "2295", "l1C49RprteKJ"],
	 	language_ID : "3",
	 	id : "2249",
	 	tags : []
 	 }, { 
	 	name : "the planet",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2035", "2272", "2296", "LQ6iPKnNG6yA"],
	 	language_ID : "3",
	 	id : "2250",
	 	tags : []
 	 }, { 
	 	name : "the snow",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2230", "2276", "2299"],
	 	language_ID : "3",
	 	id : "2252"
 	 }, { 
	 	name : "the stone",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2009", "2277", "2300"],
	 	language_ID : "3",
	 	id : "2253"
 	 }, { 
	 	name : "the storm",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2241", "2278", "2301"],
	 	language_ID : "3",
	 	id : "2254"
 	 }, { 
	 	name : "the volcano",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2234", "2279", "2307"],
	 	language_ID : "3",
	 	id : "2256"
 	 }, { 
	 	name : "the ebb",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2239", "2280", "2288"],
	 	language_ID : "3",
	 	id : "2257"
 	 }, { 
	 	name : "the flood",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2240", "2268", "2289"],
	 	language_ID : "3",
	 	id : "2258"
 	 }, { 
	 	name : "the cave",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2034", "2281", "2292", "3054"],
	 	language_ID : "3",
	 	id : "2259",
	 	tags : []
 	 }, { 
	 	name : "the sun",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2233", "2282", "2302", "3287"],
	 	language_ID : "3",
	 	id : "2260",
	 	tags : []
 	 }, { 
	 	name : "the cloud",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2236", "2283", "2306"],
	 	language_ID : "3",
	 	id : "2261"
 	 }, { 
	 	name : "rain",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2238", "2284", "2308", "2824"],
	 	language_ID : "3",
	 	id : "2262"
 	 }, { 
	 	name : "el hielo",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2229", "2242", "2286"],
	 	language_ID : "5",
	 	id : "2263"
 	 }, { 
	 	name : "el terremoto",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2235", "2243", "2287"],
	 	language_ID : "5",
	 	id : "2264"
 	 }, { 
	 	name : "el metal",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2010", "2294", "3080"],
	 	language_ID : "5",
	 	id : "2266"
 	 }, { 
	 	name : "el tiempo",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2232", "2246", "2305"],
	 	language_ID : "5",
	 	id : "2267"
 	 }, { 
	 	name : "la marea alta",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2240", "2258", "2289"],
	 	language_ID : "5",
	 	id : "2268"
 	 }, { 
	 	name : "el río",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2037", "2247", "2289", "2290"],
	 	language_ID : "5",
	 	id : "2269"
 	 }, { 
	 	name : "el glaciar",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2039", "2248", "2291", "3055"],
	 	language_ID : "5",
	 	id : "2270"
 	 }, { 
	 	name : "la luna",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2036", "2249", "2295"],
	 	language_ID : "5",
	 	id : "2271"
 	 }, { 
	 	name : "el planeta",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2035", "2250", "2296"],
	 	language_ID : "5",
	 	id : "2272"
 	 }, { 
	 	name : "la lluvia",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2297", "2771", "2792"],
	 	language_ID : "5",
	 	id : "2273"
 	 }, { 
	 	name : "der Regenwald",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2275", "2385", "2732"],
	 	language_ID : "2",
	 	id : "2274"
 	 }, { 
	 	name : "la selva",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2274", "2385", "2732"],
	 	language_ID : "5",
	 	id : "2275"
 	 }, { 
	 	name : "la nieve",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2230", "2252", "2299"],
	 	language_ID : "5",
	 	id : "2276"
 	 }, { 
	 	name : "la piedra",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2009", "2253", "2300"],
	 	language_ID : "5",
	 	id : "2277"
 	 }, { 
	 	name : "la tempestad, el temporal",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2241", "2254", "2301"],
	 	language_ID : "5",
	 	id : "2278"
 	 }, { 
	 	name : "el volcán",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2234", "2256", "2307"],
	 	language_ID : "5",
	 	id : "2279"
 	 }, { 
	 	name : "la marea baja",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2239", "2257", "2288"],
	 	language_ID : "5",
	 	id : "2280"
 	 }, { 
	 	name : "la cueva",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2034", "2259", "2292", "3054"],
	 	language_ID : "5",
	 	id : "2281"
 	 }, { 
	 	name : "el sol",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2233", "2260", "2302"],
	 	language_ID : "5",
	 	id : "2282"
 	 }, { 
	 	name : "la nube",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2236", "2261", "2306"],
	 	language_ID : "5",
	 	id : "2283"
 	 }, { 
	 	name : "llover",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2238", "2262", "2308", "2825"],
	 	language_ID : "5",
	 	id : "2284"
 	 }, { 
	 	name : "der Regenbogen",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2298", "2715", "2717"],
	 	language_ID : "2",
	 	id : "2285"
 	 }, { 
	 	name : "en is",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2229", "2242", "2263"],
	 	language_ID : "1",
	 	id : "2286"
 	 }, { 
	 	name : "en jordbävning",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2235", "2243", "2264"],
	 	language_ID : "1",
	 	id : "2287"
 	 }, { 
	 	name : "en ebb",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2239", "2257", "2280"],
	 	language_ID : "1",
	 	id : "2288"
 	 }, { 
	 	name : "en flod",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2037", "2240", "2247", "2258", "2268", "2269", "2290"],
	 	language_ID : "1",
	 	id : "2289"
 	 }, { 
	 	name : "en älv",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2037", "2247", "2269", "2289"],
	 	language_ID : "1",
	 	id : "2290"
 	 }, { 
	 	name : "en jökel",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2039", "2248", "2270", "3055"],
	 	language_ID : "1",
	 	id : "2291"
 	 }, { 
	 	name : "en håla",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2034", "2259", "2281", "3054"],
	 	language_ID : "1",
	 	id : "2292"
 	 }, { 
	 	name : "ett hav",
	 	categories_IDs : ["59", "2756", "41"],
	 	related_IDs : ["1057", "964", "1007", "1097"],
	 	language_ID : "1",
	 	id : "2293"
 	 }, { 
	 	name : "en metall",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2010", "2266", "3080"],
	 	language_ID : "1",
	 	id : "2294"
 	 }, { 
	 	name : "en måne",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2036", "2249", "2271"],
	 	language_ID : "1",
	 	id : "2295"
 	 }, { 
	 	name : "en planet",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2035", "2250", "2272"],
	 	language_ID : "1",
	 	id : "2296"
 	 }, { 
	 	name : "ett regn",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2273", "2771", "2792"],
	 	language_ID : "1",
	 	id : "2297"
 	 }, { 
	 	name : "en regnbåge",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2285", "2715", "2717"],
	 	language_ID : "1",
	 	id : "2298"
 	 }, { 
	 	name : "en snö",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2230", "2252", "2276"],
	 	language_ID : "1",
	 	id : "2299"
 	 }, { 
	 	name : "en sten",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2009", "2253", "2277"],
	 	language_ID : "1",
	 	id : "2300"
 	 }, { 
	 	name : "en storm",
	 	categories_IDs : ["2757", "59"],
	 	related_IDs : ["2241", "2254", "2278"],
	 	language_ID : "1",
	 	id : "2301"
 	 }, { 
	 	name : "en sol",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2233", "2260", "2282"],
	 	language_ID : "1",
	 	id : "2302"
 	 }, { 
	 	name : "sonnig",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2304", "2716", "2718", "3256"],
	 	language_ID : "2",
	 	id : "2303"
 	 }, { 
	 	name : "solig",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2303", "2716", "2718", "3256"],
	 	language_ID : "1",
	 	id : "2304"
 	 }, { 
	 	name : "ett väder",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2232", "2246", "2267"],
	 	language_ID : "1",
	 	id : "2305"
 	 }, { 
	 	name : "ett moln",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2236", "2261", "2283"],
	 	language_ID : "1",
	 	id : "2306"
 	 }, { 
	 	name : "en vulkan",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2234", "2256", "2279"],
	 	language_ID : "1",
	 	id : "2307"
 	 }, { 
	 	name : "regna",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2238", "2262", "2284", "2826"],
	 	language_ID : "1",
	 	id : "2308"
 	 }, { 
	 	name : "die Infektion",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2323", "2710", "2804"],
	 	language_ID : "2",
	 	id : "2309"
 	 }, { 
	 	name : "der Pilz",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2321", "2713", "2800", "3051"],
	 	language_ID : "2",
	 	id : "2310"
 	 }, { 
	 	name : "die Alge",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2316", "2708", "2807"],
	 	language_ID : "2",
	 	id : "2311"
 	 }, { 
	 	name : "das Protozoon",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2313", "2802"],
	 	language_ID : "2",
	 	id : "2312"
 	 }, { 
	 	name : "en urdjur",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2312", "2802"],
	 	language_ID : "1",
	 	id : "2313"
 	 }, { 
	 	name : "sich anstecken",
	 	categories_IDs : ["44", "58"],
	 	related_IDs : ["2315", "2711", "2805", "3052"],
	 	language_ID : "2",
	 	id : "2314"
 	 }, { 
	 	name : "bli smittad",
	 	categories_IDs : ["44", "59"],
	 	related_IDs : ["2314", "2711", "2805", "3052"],
	 	language_ID : "1",
	 	id : "2315"
 	 }, { 
	 	name : "alger",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2311", "2708", "2807"],
	 	language_ID : "1",
	 	id : "2316"
 	 }, { 
	 	name : "sich ausbreiten",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2318", "2712", "2806"],
	 	language_ID : "2",
	 	id : "2317"
 	 }, { 
	 	name : "utbreda sig",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2317", "2712", "2806"],
	 	language_ID : "1",
	 	id : "2318"
 	 }, { 
	 	name : "die Allergie",
	 	categories_IDs : ["44"],
	 	related_IDs : ["2320", "2539"],
	 	language_ID : "2",
	 	id : "2319"
 	 }, { 
	 	name : "en allergi",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2319", "2539"],
	 	language_ID : "1",
	 	id : "2320"
 	 }, { 
	 	name : "en svamp",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2310", "2713", "2800", "3051"],
	 	language_ID : "1",
	 	id : "2321"
 	 }, { 
	 	name : "en bakterie",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2006", "2709", "2803", "3053"],
	 	language_ID : "1",
	 	id : "2322"
 	 }, { 
	 	name : "en infektion",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2309", "2710", "2804"],
	 	language_ID : "1",
	 	id : "2323"
 	 }, { 
	 	name : "ett virus",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2007", "2714", "2801"],
	 	language_ID : "1",
	 	id : "2324"
 	 }, { 
	 	name : "en fågel",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1996", "2354", "2368", "3284"],
	 	language_ID : "1",
	 	id : "2325"
 	 }, { 
	 	name : "reiten",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2344", "2363", "2373"],
	 	language_ID : "2",
	 	id : "2326"
 	 }, { 
	 	name : "das Haustier",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2328", "2345", "2375"],
	 	language_ID : "2",
	 	id : "2327"
 	 }, { 
	 	name : "ett husdjur",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2327", "2345", "2375"],
	 	language_ID : "1",
	 	id : "2328"
 	 }, { 
	 	name : "en apa",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1993", "2348", "2367"],
	 	language_ID : "1",
	 	id : "2329"
 	 }, { 
	 	name : "en hästkrake",
	 	categories_IDs : ["57"],
	 	related_IDs : ["176", "178", "179", "2331", "2347"],
	 	language_ID : "1",
	 	id : "2330"
 	 }, { 
	 	name : "en häst",
	 	categories_IDs : ["57"],
	 	related_IDs : ["176", "177", "178", "179", "2330", "2347"],
	 	language_ID : "1",
	 	id : "2331"
 	 }, { 
	 	name : "en kanin",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1998", "2346", "2376"],
	 	language_ID : "1",
	 	id : "2332"
 	 }, { 
	 	name : "en hund",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1994", "2349", "2365"],
	 	language_ID : "1",
	 	id : "2333"
 	 }, { 
	 	name : "en hankatt",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2003", "2350", "2378"],
	 	language_ID : "1",
	 	id : "2334"
 	 }, { 
	 	name : "en trana",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2004", "2351", "2377"],
	 	language_ID : "1",
	 	id : "2335"
 	 }, { 
	 	name : "en myra",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2002", "2356", "2370"],
	 	language_ID : "1",
	 	id : "2336"
 	 }, { 
	 	name : "en katt",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1995", "2357", "2364"],
	 	language_ID : "1",
	 	id : "2337"
 	 }, { 
	 	name : "ett lejon",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1992", "2352", "2379", "3233", "3234", "3235"],
	 	language_ID : "1",
	 	id : "2338",
	 	tags : [],
	 	formPrimaryName : "manlig",
	 	formsPrimary_IDs : ["3236"]
 	 }, { 
	 	name : "en stork",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2005", "2353", "2380"],
	 	language_ID : "1",
	 	id : "2339"
 	 }, { 
	 	name : "en varg",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1997", "2355", "2369"],
	 	language_ID : "1",
	 	id : "2340"
 	 }, { 
	 	name : "en mus",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2000", "2358", "2372"],
	 	language_ID : "1",
	 	id : "2341"
 	 }, { 
	 	name : "en spindel",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1999", "2359", "2371", "2842"],
	 	language_ID : "1",
	 	id : "2342"
 	 }, { 
	 	name : "mata",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2001", "2362", "2374", "3283"],
	 	language_ID : "1",
	 	id : "2343"
 	 }, { 
	 	name : "rida",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2326", "2363", "2373"],
	 	language_ID : "1",
	 	id : "2344"
 	 }, { 
	 	name : "el animal doméstico",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2327", "2328", "2375"],
	 	language_ID : "5",
	 	id : "2345"
 	 }, { 
	 	name : "el conejo",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1998", "2332", "2376"],
	 	language_ID : "5",
	 	id : "2346"
 	 }, { 
	 	name : "el caballo",
	 	categories_IDs : ["57"],
	 	related_IDs : ["176", "177", "178", "179", "2330", "2331"],
	 	language_ID : "5",
	 	id : "2347"
 	 }, { 
	 	name : "el mono",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1993", "2329", "2367"],
	 	language_ID : "5",
	 	id : "2348"
 	 }, { 
	 	name : "el perro",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1994", "2333", "2365"],
	 	language_ID : "5",
	 	id : "2349"
 	 }, { 
	 	name : "el gato",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2003", "2334", "2378"],
	 	language_ID : "5",
	 	id : "2350"
 	 }, { 
	 	name : "la grulla",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2004", "2335", "2377"],
	 	language_ID : "5",
	 	id : "2351"
 	 }, { 
	 	name : "la leona",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1992", "2338", "2379", "3233", "3235", "3236"],
	 	language_ID : "5",
	 	id : "2352",
	 	tags : [],
	 	formsPrimary_IDs : ["3234"]
 	 }, { 
	 	name : "la cigüeña",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2005", "2339", "2380"],
	 	language_ID : "5",
	 	id : "2353"
 	 }, { 
	 	name : "el ave",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1996", "2325", "2368", "3284"],
	 	language_ID : "5",
	 	id : "2354",
	 	tags : []
 	 }, { 
	 	name : "el lobo, la loba",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1997", "2340", "2369"],
	 	language_ID : "5",
	 	id : "2355"
 	 }, { 
	 	name : "la hormiga",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2002", "2336", "2370"],
	 	language_ID : "5",
	 	id : "2356"
 	 }, { 
	 	name : "el gato",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1995", "2337", "2364"],
	 	language_ID : "5",
	 	id : "2357"
 	 }, { 
	 	name : "el ratón",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2000", "2341", "2372"],
	 	language_ID : "5",
	 	id : "2358"
 	 }, { 
	 	name : "la araña",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1999", "2342", "2371", "2841", "2842"],
	 	language_ID : "5",
	 	id : "2359"
 	 }, { 
	 	name : "der Frosch",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2361", "2366", "2746"],
	 	language_ID : "2",
	 	id : "2360"
 	 }, { 
	 	name : "la rana",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2360", "2366", "2746"],
	 	language_ID : "5",
	 	id : "2361"
 	 }, { 
	 	name : "alimentar",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2001", "2343", "2374", "3283"],
	 	language_ID : "5",
	 	id : "2362",
	 	tags : []
 	 }, { 
	 	name : "montar a caballo",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2326", "2344", "2373"],
	 	language_ID : "5",
	 	id : "2363"
 	 }, { 
	 	name : "the cat",
	 	categories_IDs : ["57", "61"],
	 	related_IDs : ["1995", "2337", "2357", "fOXJ9imgXcTk", "2ezg3h3q4ELz"],
	 	language_ID : "3",
	 	id : "2364",
	 	tags : []
 	 }, { 
	 	name : "the dog",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1994", "2333", "2349", "Z88QNnIMQEuJ"],
	 	language_ID : "3",
	 	id : "2365"
 	 }, { 
	 	name : "the frog",
	 	categories_IDs : ["57", "61"],
	 	related_IDs : ["2360", "2361", "2746", "2ezg3h3q4ELz"],
	 	language_ID : "3",
	 	id : "2366",
	 	tags : []
 	 }, { 
	 	name : "the ape",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1993", "2329", "2348", "qmStIaSRiD67"],
	 	language_ID : "3",
	 	id : "2367",
	 	tags : []
 	 }, { 
	 	name : "the bird",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1996", "2325", "2354", "3284"],
	 	language_ID : "3",
	 	id : "2368"
 	 }, { 
	 	name : "the wolf",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1997", "2340", "2355", "KK0YhKt8Y192"],
	 	language_ID : "3",
	 	id : "2369"
 	 }, { 
	 	name : "the ant",
	 	categories_IDs : ["46", "57"],
	 	related_IDs : ["2002", "2336", "2356", "nPLkBStJOvMZ"],
	 	language_ID : "3",
	 	id : "2370",
	 	tags : []
 	 }, { 
	 	name : "the spider",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1999", "2342", "2359", "2840"],
	 	language_ID : "3",
	 	id : "2371"
 	 }, { 
	 	name : "the mouse",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2000", "2341", "2358", "1sNbclk4GeC1"],
	 	language_ID : "3",
	 	id : "2372",
	 	tags : []
 	 }, { 
	 	name : "ride",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2326", "2344", "2363"],
	 	language_ID : "3",
	 	id : "2373"
 	 }, { 
	 	name : "feed",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2001", "2343", "2362", "3283"],
	 	language_ID : "3",
	 	id : "2374"
 	 }, { 
	 	name : "the pet",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2327", "2328", "2345"],
	 	language_ID : "3",
	 	id : "2375"
 	 }, { 
	 	name : "the rabbit",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1998", "2332", "2346", "9TB9SsfO0fFs"],
	 	language_ID : "3",
	 	id : "2376"
 	 }, { 
	 	name : "the crane",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2004", "2335", "2351", "LqO4ccvCU8EG"],
	 	language_ID : "3",
	 	id : "2377"
 	 }, { 
	 	name : "the tomcat",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2003", "2334", "2350"],
	 	language_ID : "3",
	 	id : "2378"
 	 }, { 
	 	name : "the lion",
	 	categories_IDs : ["57"],
	 	related_IDs : ["1992", "2338", "2352", "3233", "3234", "3236"],
	 	language_ID : "3",
	 	id : "2379",
	 	tags : [],
	 	formsPrimary_IDs : ["3235"]
 	 }, { 
	 	name : "the stork",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2005", "2339", "2353"],
	 	language_ID : "3",
	 	id : "2380"
 	 }, { 
	 	name : "the leaf",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1976", "2402", "2727", "WddRjLh6YU3w"],
	 	language_ID : "3",
	 	id : "2381",
	 	tags : []
 	 }, { 
	 	name : "the wood",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1979", "2403", "2728", "3289"],
	 	language_ID : "3",
	 	id : "2382",
	 	tags : []
 	 }, { 
	 	name : "the tree",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1983", "2405", "2729", "qmStIaSRiD67"],
	 	language_ID : "3",
	 	id : "2383"
 	 }, { 
	 	name : "the cherry tree",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1987", "2407", "2731", "IAZDDIKTey8V"],
	 	language_ID : "3",
	 	id : "2384",
	 	tags : []
 	 }, { 
	 	name : "the rain forest",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2274", "2275", "2732"],
	 	language_ID : "3",
	 	id : "2385",
	 	tags : []
 	 }, { 
	 	name : "the trunk",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1978", "2408", "2733", "G8NRbc3anvrn"],
	 	language_ID : "3",
	 	id : "2386",
	 	tags : []
 	 }, { 
	 	name : "the birch",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1985", "2410", "2734"],
	 	language_ID : "3",
	 	id : "2387"
 	 }, { 
	 	name : "the flower",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1973", "1975", "2411", "2735"],
	 	language_ID : "3",
	 	id : "2388"
 	 }, { 
	 	name : "the blossom",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1975", "2735"],
	 	language_ID : "3",
	 	id : "2389"
 	 }, { 
	 	name : "the oak",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1984", "2412", "2736"],
	 	language_ID : "3",
	 	id : "2390"
 	 }, { 
	 	name : "the pine tree",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2413", "357", "359"],
	 	language_ID : "3",
	 	id : "2391"
 	 }, { 
	 	name : "the needle",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1977", "2414", "2737"],
	 	language_ID : "3",
	 	id : "2392"
 	 }, { 
	 	name : "the palm tree",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1988", "2415", "2738"],
	 	language_ID : "3",
	 	id : "2393"
 	 }, { 
	 	name : "the sun flower",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1974", "2416", "2739"],
	 	language_ID : "3",
	 	id : "2394"
 	 }, { 
	 	name : "the fir tree",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1986", "2701", "2740"],
	 	language_ID : "3",
	 	id : "2395"
 	 }, { 
	 	name : "the root",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1980", "2702", "2741"],
	 	language_ID : "3",
	 	id : "2396"
 	 }, { 
	 	name : "water",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1990", "2703", "2742", "3008"],
	 	language_ID : "3",
	 	id : "2397"
 	 }, { 
	 	name : "rot",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1982", "2705", "2744", "JGUg7eMxzptN"],
	 	language_ID : "3",
	 	id : "2398",
	 	tags : []
 	 }, { 
	 	name : "sprout",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1989", "2704", "2743"],
	 	language_ID : "3",
	 	id : "2399"
 	 }, { 
	 	name : "grow",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1981", "2706", "2745", "v0H2LsfblYNz"],
	 	language_ID : "3",
	 	id : "2400",
	 	tags : []
 	 }, { 
	 	name : "manure",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1991", "2406", "2730"],
	 	language_ID : "3",
	 	id : "2401"
 	 }, { 
	 	name : "la hoja",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1976", "2381", "2727"],
	 	language_ID : "5",
	 	id : "2402"
 	 }, { 
	 	name : "la madera",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1979", "2382", "2728"],
	 	language_ID : "5",
	 	id : "2403"
 	 }, { 
	 	name : "el árbol",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1983", "2383", "2729"],
	 	language_ID : "5",
	 	id : "2405"
 	 }, { 
	 	name : "el abono",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1991", "2401", "2730"],
	 	language_ID : "5",
	 	id : "2406"
 	 }, { 
	 	name : "el cerezo",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1987", "2384", "2731"],
	 	language_ID : "5",
	 	id : "2407"
 	 }, { 
	 	name : "el tronco",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1978", "2386", "2733"],
	 	language_ID : "5",
	 	id : "2408"
 	 }, { 
	 	name : "el bosque",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["10", "13", "8", "9", "lbotmEgCymz8"],
	 	language_ID : "5",
	 	id : "2409"
 	 }, { 
	 	name : "el abedul",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1985", "2387", "2734"],
	 	language_ID : "5",
	 	id : "2410"
 	 }, { 
	 	name : "la flor",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1973", "1975", "2388", "2735"],
	 	language_ID : "5",
	 	id : "2411"
 	 }, { 
	 	name : "el roble",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1984", "2390", "2736"],
	 	language_ID : "5",
	 	id : "2412"
 	 }, { 
	 	name : "el pino",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2391", "357", "359"],
	 	language_ID : "5",
	 	id : "2413"
 	 }, { 
	 	name : "la aguja",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1977", "2392", "2737"],
	 	language_ID : "5",
	 	id : "2414"
 	 }, { 
	 	name : "la palma",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1988", "2393", "2738"],
	 	language_ID : "5",
	 	id : "2415"
 	 }, { 
	 	name : "el girasol",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1974", "2394", "2739"],
	 	language_ID : "5",
	 	id : "2416"
 	 }, { 
	 	name : "the painting",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1940", "2435", "2506", "T0EkiFW0NdZn"],
	 	language_ID : "3",
	 	id : "2417"
 	 }, { 
	 	name : "the material",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1945", "2436", "2509"],
	 	language_ID : "3",
	 	id : "2418"
 	 }, { 
	 	name : "the author",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1952", "2438", "2512", "3229", "3275", "3276"],
	 	language_ID : "3",
	 	id : "2419"
 	 }, { 
	 	name : "the artist",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1942", "2439", "2515", "3226", "3227", "3228"],
	 	language_ID : "3",
	 	id : "2420"
 	 }, { 
	 	name : "the brush",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1944", "2441", "2517", "uD4zfeN3DfEA"],
	 	language_ID : "3",
	 	id : "2421"
 	 }, { 
	 	name : "the color",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1943", "2467", "2519", "3282"],
	 	language_ID : "3",
	 	id : "2422",
	 	tags : []
 	 }, { 
	 	name : "the culture",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1951", "2473", "2524"],
	 	language_ID : "3",
	 	id : "2423"
 	 }, { 
	 	name : "the sculpture",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1941", "2476", "2527"],
	 	language_ID : "3",
	 	id : "2424"
 	 }, { 
	 	name : "create",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1949", "2479", "2530"],
	 	language_ID : "3",
	 	id : "2425"
 	 }, { 
	 	name : "impressionist",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2504", "2531"],
	 	language_ID : "3",
	 	id : "2426"
 	 }, { 
	 	name : "classic",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1947", "2480", "2532", "3278"],
	 	language_ID : "3",
	 	id : "2427"
 	 }, { 
	 	name : "modern",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1948", "2482", "2534", "3277"],
	 	language_ID : "3",
	 	id : "2428"
 	 }, { 
	 	name : "destroy",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1950", "2485", "2537"],
	 	language_ID : "3",
	 	id : "2429"
 	 }, { 
	 	name : "paint",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2432", "2481", "2533", "uD4zfeN3DfEA"],
	 	language_ID : "3",
	 	id : "2431"
 	 }, { 
	 	name : "malen",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2431", "2481", "2533"],
	 	language_ID : "2",
	 	id : "2432"
 	 }, { 
	 	name : "zeichnen",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2434", "2484", "2536"],
	 	language_ID : "2",
	 	id : "2433"
 	 }, { 
	 	name : "draw",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2433", "2484", "2536"],
	 	language_ID : "3",
	 	id : "2434"
 	 }, { 
	 	name : "el cuadro",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1940", "2417", "2506"],
	 	language_ID : "5",
	 	id : "2435"
 	 }, { 
	 	name : "el material",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1945", "2418", "2509"],
	 	language_ID : "5",
	 	id : "2436"
 	 }, { 
	 	name : "der Maler",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2440", "2493", "2516", "3280"],
	 	language_ID : "2",
	 	id : "2437",
	 	tags : [],
	 	formsPrimary_IDs : ["3281"]
 	 }, { 
	 	name : "el autor",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1952", "2419", "2512", "3229", "3275"],
	 	language_ID : "5",
	 	id : "2438",
	 	tags : [],
	 	formsPrimary_IDs : ["3276"]
 	 }, { 
	 	name : "el artista",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1942", "2420", "2515", "3226", "3228"],
	 	language_ID : "5",
	 	id : "2439",
	 	tags : [],
	 	formPrimaryName : "masculino",
	 	formsPrimary_IDs : ["3227"]
 	 }, { 
	 	name : "el pintor",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2437", "2493", "2516", "3281"],
	 	language_ID : "5",
	 	id : "2440",
	 	tags : [],
	 	formsPrimary_IDs : ["3280"]
 	 }, { 
	 	name : "el pincel",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1944", "2421", "2517"],
	 	language_ID : "5",
	 	id : "2441"
 	 }, { 
	 	name : "das Klavier",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2454", "2487", "2507"],
	 	language_ID : "2",
	 	id : "2442"
 	 }, { 
	 	name : "die Geige",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2471", "2496", "2521", "3279"],
	 	language_ID : "2",
	 	id : "2443",
	 	tags : []
 	 }, { 
	 	name : "die Gitarre",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2472", "2497", "2522"],
	 	language_ID : "2",
	 	id : "2444"
 	 }, { 
	 	name : "die Flöte",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2470", "2495", "2520"],
	 	language_ID : "2",
	 	id : "2445"
 	 }, { 
	 	name : "das Mikrofon",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2456", "2490", "2510"],
	 	language_ID : "2",
	 	id : "2446"
 	 }, { 
	 	name : "die Band",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2466", "2494", "2518", "3274"],
	 	language_ID : "2",
	 	id : "2447"
 	 }, { 
	 	name : "das Orchester",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2462", "2489", "2511"],
	 	language_ID : "2",
	 	id : "2448"
 	 }, { 
	 	name : "die Musik",
	 	categories_IDs : ["53", "46"],
	 	related_IDs : ["2474", "2499", "2525"],
	 	language_ID : "2",
	 	id : "2449"
 	 }, { 
	 	name : "die Trommel",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2477", "2501", "2528"],
	 	language_ID : "2",
	 	id : "2450"
 	 }, { 
	 	name : "der Klang",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2465", "2492", "2514"],
	 	language_ID : "2",
	 	id : "2451"
 	 }, { 
	 	name : "trommeln",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2478", "2503", "2529"],
	 	language_ID : "2",
	 	id : "2452"
 	 }, { 
	 	name : "singen",
	 	categories_IDs : ["53", "46"],
	 	related_IDs : ["2483", "2502", "2535"],
	 	language_ID : "2",
	 	id : "2453"
 	 }, { 
	 	name : "el piano",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2442", "2487", "2507"],
	 	language_ID : "5",
	 	id : "2454"
 	 }, { 
	 	name : "der Chor",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2463", "2491", "2513"],
	 	language_ID : "2",
	 	id : "2455"
 	 }, { 
	 	name : "el micrófono",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2446", "2490", "2510"],
	 	language_ID : "5",
	 	id : "2456"
 	 }, { 
	 	name : "das Konzert",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2461", "2488", "2508"],
	 	language_ID : "2",
	 	id : "2457"
 	 }, { 
	 	name : "die Oper",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2475", "2500", "2526"],
	 	language_ID : "2",
	 	id : "2458"
 	 }, { 
	 	name : "auftreten",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2460", "2486", "2505"],
	 	language_ID : "2",
	 	id : "2459"
 	 }, { 
	 	name : "entrar en escena",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2459", "2486", "2505"],
	 	language_ID : "5",
	 	id : "2460"
 	 }, { 
	 	name : "el concierto",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2457", "2488", "2508"],
	 	language_ID : "5",
	 	id : "2461"
 	 }, { 
	 	name : "la orquesta",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2448", "2489", "2511"],
	 	language_ID : "5",
	 	id : "2462"
 	 }, { 
	 	name : "el coro",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2455", "2464", "2491", "2513"],
	 	language_ID : "5",
	 	id : "2463"
 	 }, { 
	 	name : "Me gusta mucho cantar a coro.",
	 	related_IDs : ["2463"],
	 	language_ID : "5",
	 	sentence : true,
	 	categories_IDs : [],
	 	id : "2464"
 	 }, { 
	 	name : "el sonido",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2451", "2492", "2514"],
	 	language_ID : "5",
	 	id : "2465"
 	 }, { 
	 	name : "el grupo",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2447", "2494", "2518", "3274"],
	 	language_ID : "5",
	 	id : "2466"
 	 }, { 
	 	name : "el color",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1943", "2422", "2519", "3282"],
	 	language_ID : "5",
	 	id : "2467"
 	 }, { 
	 	name : "die Gänsehaut",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2469", "2498", "2523"],
	 	language_ID : "2",
	 	id : "2468"
 	 }, { 
	 	name : "la carne de gallina",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2468", "2498", "2523"],
	 	language_ID : "5",
	 	id : "2469"
 	 }, { 
	 	name : "la flauta",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2445", "2495", "2520"],
	 	language_ID : "5",
	 	id : "2470"
 	 }, { 
	 	name : "el violín",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2443", "2496", "2521", "3279"],
	 	language_ID : "5",
	 	id : "2471"
 	 }, { 
	 	name : "la guitarra",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2444", "2497", "2522"],
	 	language_ID : "5",
	 	id : "2472"
 	 }, { 
	 	name : "la cultura",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1951", "2423", "2524"],
	 	language_ID : "5",
	 	id : "2473"
 	 }, { 
	 	name : "la música",
	 	categories_IDs : ["53", "46"],
	 	related_IDs : ["2449", "2499", "2525", "1576"],
	 	language_ID : "5",
	 	id : "2474"
 	 }, { 
	 	name : "la ópera",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2458", "2500", "2526"],
	 	language_ID : "5",
	 	id : "2475"
 	 }, { 
	 	name : "la escultura",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1941", "2424", "2527"],
	 	language_ID : "5",
	 	id : "2476"
 	 }, { 
	 	name : "el tambor",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2450", "2501", "2528"],
	 	language_ID : "5",
	 	id : "2477"
 	 }, { 
	 	name : "tocar el tambor",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2452", "2503", "2529"],
	 	language_ID : "5",
	 	id : "2478"
 	 }, { 
	 	name : "crear",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1949", "2425", "2530"],
	 	language_ID : "5",
	 	id : "2479"
 	 }, { 
	 	name : "clásico",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1947", "2427", "2532"],
	 	language_ID : "5",
	 	id : "2480",
	 	tags : [],
	 	formsPrimary_IDs : ["3278"]
 	 }, { 
	 	name : "pintar",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2431", "2432", "2533"],
	 	language_ID : "5",
	 	id : "2481"
 	 }, { 
	 	name : "moderno",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1948", "2428", "2534"],
	 	language_ID : "5",
	 	id : "2482",
	 	tags : [],
	 	formsPrimary_IDs : ["3277"]
 	 }, { 
	 	name : "cantar",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2453", "2464", "2502", "2535"],
	 	language_ID : "5",
	 	id : "2483"
 	 }, { 
	 	name : "dibujar",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2433", "2434", "2536"],
	 	language_ID : "5",
	 	id : "2484"
 	 }, { 
	 	name : "destruir",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1950", "2429", "2537"],
	 	language_ID : "5",
	 	id : "2485"
 	 }, { 
	 	name : "perform",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2459", "2460", "2505"],
	 	language_ID : "3",
	 	id : "2486"
 	 }, { 
	 	name : "the piano",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2442", "2454", "2507", "5K5gh1Zz3feC"],
	 	language_ID : "3",
	 	id : "2487"
 	 }, { 
	 	name : "the concert",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2457", "2461", "2508"],
	 	language_ID : "3",
	 	id : "2488"
 	 }, { 
	 	name : "the orchestra",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2448", "2462", "2511"],
	 	language_ID : "3",
	 	id : "2489"
 	 }, { 
	 	name : "the microphone",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2446", "2456", "2510"],
	 	language_ID : "3",
	 	id : "2490"
 	 }, { 
	 	name : "the choir",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2455", "2463", "2513"],
	 	language_ID : "3",
	 	id : "2491"
 	 }, { 
	 	name : "the sound",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2451", "2465", "2514"],
	 	language_ID : "3",
	 	id : "2492"
 	 }, { 
	 	name : "the painter",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2437", "2440", "2516", "3280", "3281"],
	 	language_ID : "3",
	 	id : "2493"
 	 }, { 
	 	name : "the band",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2447", "2466", "2518", "3274"],
	 	language_ID : "3",
	 	id : "2494"
 	 }, { 
	 	name : "the flute",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2445", "2470", "2520"],
	 	language_ID : "3",
	 	id : "2495"
 	 }, { 
	 	name : "the violin",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2443", "2471", "2521", "3279"],
	 	language_ID : "3",
	 	id : "2496"
 	 }, { 
	 	name : "the guitar",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2444", "2472", "2522"],
	 	language_ID : "3",
	 	id : "2497"
 	 }, { 
	 	name : "the goose bumps",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2468", "2469", "2523"],
	 	language_ID : "3",
	 	id : "2498"
 	 }, { 
	 	name : "the music",
	 	categories_IDs : ["53", "46"],
	 	related_IDs : ["2449", "2474", "2525"],
	 	language_ID : "3",
	 	id : "2499"
 	 }, { 
	 	name : "the opera",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2458", "2475", "2526"],
	 	language_ID : "3",
	 	id : "2500"
 	 }, { 
	 	name : "the drum",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2450", "2477", "2528"],
	 	language_ID : "3",
	 	id : "2501"
 	 }, { 
	 	name : "sing",
	 	categories_IDs : ["53", "46"],
	 	related_IDs : ["2453", "2483", "2535"],
	 	language_ID : "3",
	 	id : "2502"
 	 }, { 
	 	name : "drum",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2452", "2478", "2529"],
	 	language_ID : "3",
	 	id : "2503"
 	 }, { 
	 	name : "impressionistisch",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2426", "2531"],
	 	language_ID : "2",
	 	id : "2504"
 	 }, { 
	 	name : "uppträda",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2459", "2460", "2486"],
	 	language_ID : "1",
	 	id : "2505"
 	 }, { 
	 	name : "en målning",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1940", "2417", "2435"],
	 	language_ID : "1",
	 	id : "2506"
 	 }, { 
	 	name : "ett piano",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2442", "2454", "2487"],
	 	language_ID : "1",
	 	id : "2507"
 	 }, { 
	 	name : "en konsert",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2457", "2461", "2488"],
	 	language_ID : "1",
	 	id : "2508"
 	 }, { 
	 	name : "ett material",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1945", "2418", "2436"],
	 	language_ID : "1",
	 	id : "2509"
 	 }, { 
	 	name : "en mikrofon",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2446", "2456", "2490"],
	 	language_ID : "1",
	 	id : "2510"
 	 }, { 
	 	name : "en orkester",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2448", "2462", "2489"],
	 	language_ID : "1",
	 	id : "2511"
 	 }, { 
	 	name : "en författare",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1952", "2419", "2438", "3229", "3276"],
	 	language_ID : "1",
	 	id : "2512",
	 	tags : [],
	 	formPrimaryName : "manlig",
	 	formsPrimary_IDs : ["3275"]
 	 }, { 
	 	name : "en sångkör",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2455", "2463", "2491"],
	 	language_ID : "1",
	 	id : "2513"
 	 }, { 
	 	name : "en klang",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2451", "2465", "2492"],
	 	language_ID : "1",
	 	id : "2514"
 	 }, { 
	 	name : "en konstnar",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1942", "2420", "2439", "3226", "3227"],
	 	language_ID : "1",
	 	id : "2515",
	 	tags : [],
	 	formPrimaryName : "manlig",
	 	formsPrimary_IDs : ["3228"]
 	 }, { 
	 	name : "en målare",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2437", "2440", "2493", "3280", "3281"],
	 	language_ID : "1",
	 	id : "2516"
 	 }, { 
	 	name : "en pensel",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1944", "2421", "2441"],
	 	language_ID : "1",
	 	id : "2517"
 	 }, { 
	 	name : "ett band",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2447", "2466", "2494", "3274"],
	 	language_ID : "1",
	 	id : "2518",
	 	tags : []
 	 }, { 
	 	name : "en färg",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1943", "2422", "2467", "3282"],
	 	language_ID : "1",
	 	id : "2519"
 	 }, { 
	 	name : "en flöjt",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2445", "2470", "2495"],
	 	language_ID : "1",
	 	id : "2520"
 	 }, { 
	 	name : "en fiol",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2443", "2471", "2496", "3279"],
	 	language_ID : "1",
	 	id : "2521"
 	 }, { 
	 	name : "en gitarr",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2444", "2472", "2497"],
	 	language_ID : "1",
	 	id : "2522"
 	 }, { 
	 	name : "en gåshud",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2468", "2469", "2498"],
	 	language_ID : "1",
	 	id : "2523"
 	 }, { 
	 	name : "en kultur",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1951", "2423", "2473"],
	 	language_ID : "1",
	 	id : "2524"
 	 }, { 
	 	name : "en musik",
	 	categories_IDs : ["53", "46"],
	 	related_IDs : ["2449", "2474", "2499", "1576"],
	 	language_ID : "1",
	 	id : "2525"
 	 }, { 
	 	name : "en opera",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2458", "2475", "2500"],
	 	language_ID : "1",
	 	id : "2526"
 	 }, { 
	 	name : "en skulptur",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1941", "2424", "2476"],
	 	language_ID : "1",
	 	id : "2527"
 	 }, { 
	 	name : "en trumma",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2450", "2477", "2501"],
	 	language_ID : "1",
	 	id : "2528"
 	 }, { 
	 	name : "trumma",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2452", "2478", "2503"],
	 	language_ID : "1",
	 	id : "2529"
 	 }, { 
	 	name : "skapa",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1949", "2425", "2479"],
	 	language_ID : "1",
	 	id : "2530"
 	 }, { 
	 	name : "impressionistisk",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2426", "2504"],
	 	language_ID : "1",
	 	id : "2531"
 	 }, { 
	 	name : "klassisk",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1947", "2427", "2480", "3278"],
	 	language_ID : "1",
	 	id : "2532"
 	 }, { 
	 	name : "måla",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2431", "2432", "2481"],
	 	language_ID : "1",
	 	id : "2533"
 	 }, { 
	 	name : "modern",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1948", "2428", "2482", "3277"],
	 	language_ID : "1",
	 	id : "2534"
 	 }, { 
	 	name : "sjunga",
	 	categories_IDs : ["53", "46"],
	 	related_IDs : ["2453", "2483", "2502", "1585"],
	 	language_ID : "1",
	 	id : "2535"
 	 }, { 
	 	name : "teckna",
	 	categories_IDs : ["53"],
	 	related_IDs : ["2433", "2434", "2484"],
	 	language_ID : "1",
	 	id : "2536"
 	 }, { 
	 	name : "förstöra",
	 	categories_IDs : ["53"],
	 	related_IDs : ["1950", "2429", "2485"],
	 	language_ID : "1",
	 	id : "2537"
 	 }, { 
	 	name : "feel",
	 	categories_IDs : ["43"],
	 	related_IDs : ["1745", "1746", "394"],
	 	language_ID : "3",
	 	id : "2538"
 	 }, { 
	 	name : "the allergy",
	 	categories_IDs : ["44"],
	 	related_IDs : ["2319", "2320"],
	 	language_ID : "3",
	 	id : "2539"
 	 }, { 
	 	name : "the circulation",
	 	categories_IDs : ["44"],
	 	related_IDs : ["2179", "1928"],
	 	language_ID : "3",
	 	id : "2540"
 	 }, { 
	 	name : "en dal",
	 	categories_IDs : ["2756", "46"],
	 	related_IDs : ["1011", "1101", "968"],
	 	language_ID : "1",
	 	id : "2541"
 	 }, { 
	 	name : "ett Australien",
	 	categories_IDs : ["46"],
	 	related_IDs : ["1018", "1109", "975"],
	 	language_ID : "1",
	 	id : "2542"
 	 }, { 
	 	name : "baka",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1677", "1684", "1693"],
	 	language_ID : "1",
	 	id : "2543"
 	 }, { 
	 	name : "steka",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1678", "1685", "1694"],
	 	language_ID : "1",
	 	id : "2544"
 	 }, { 
	 	name : "en bakugn",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1679", "1686", "1695"],
	 	language_ID : "1",
	 	id : "2545"
 	 }, { 
	 	name : "koka",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1676", "1683", "1692"],
	 	language_ID : "1",
	 	id : "2546"
 	 }, { 
	 	name : "en kastrull",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1682", "1689", "1698"],
	 	language_ID : "1",
	 	id : "2547"
 	 }, { 
	 	name : "en mikrovågsugn",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1680", "1687", "1696"],
	 	language_ID : "1",
	 	id : "2548"
 	 }, { 
	 	name : "en stekpanna",
	 	categories_IDs : ["40"],
	 	related_IDs : ["1681", "1688", "1697", "3175"],
	 	language_ID : "1",
	 	id : "2549"
 	 }, { 
	 	name : "svara",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1900", "2595", "2668", "3262", "3269"],
	 	language_ID : "1",
	 	id : "2550"
 	 }, { 
	 	name : "chatta",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1899", "2596", "2669"],
	 	language_ID : "1",
	 	id : "2551"
 	 }, { 
	 	name : "en mobil",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1905", "2597", "2670", "3263", "3273"],
	 	language_ID : "1",
	 	id : "2552",
	 	tags : []
 	 }, { 
	 	name : "ett anrop",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1896", "2600", "2673"],
	 	language_ID : "1",
	 	id : "2553"
 	 }, { 
	 	name : "ett internet",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1894", "2598", "2671"],
	 	language_ID : "1",
	 	id : "2554"
 	 }, { 
	 	name : "fråga",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1901", "2602", "2675"],
	 	language_ID : "1",
	 	id : "2555"
 	 }, { 
	 	name : "tala",
	 	categories_IDs : ["35", "52"],
	 	related_IDs : ["1904", "2604", "2677", "451"],
	 	language_ID : "1",
	 	id : "2556"
 	 }, { 
	 	name : "kala",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1902", "2605", "2678", "3000"],
	 	language_ID : "1",
	 	id : "2557"
 	 }, { 
	 	name : "tiga",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1903", "2606", "2679", "3272"],
	 	language_ID : "1",
	 	id : "2558",
	 	tags : []
 	 }, { 
	 	name : "en e-post",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1895", "2601", "2674", "3261"],
	 	language_ID : "1",
	 	id : "2559"
 	 }, { 
	 	name : "mailen",
	 	categories_IDs : ["52"],
	 	related_IDs : ["2561", "2603", "2676"],
	 	language_ID : "2",
	 	id : "2560"
 	 }, { 
	 	name : "e-posta",
	 	categories_IDs : ["52"],
	 	related_IDs : ["2560", "2603", "2676"],
	 	language_ID : "1",
	 	id : "2561"
 	 }, { 
	 	name : "en telefon",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1897", "2599", "2672"],
	 	language_ID : "1",
	 	id : "2562"
 	 }, { 
	 	name : "ringa",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1898", "2607", "2680", "2998"],
	 	language_ID : "1",
	 	id : "2563"
 	 }, { 
	 	name : "en gångbana",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1955", "2609", "2683", "3268"],
	 	language_ID : "1",
	 	id : "2564"
 	 }, { 
	 	name : "ett trafikljus",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1953", "2611", "2684"],
	 	language_ID : "1",
	 	id : "2565"
 	 }, { 
	 	name : "ett olycka",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1958", "2608", "2681"],
	 	language_ID : "1",
	 	id : "2566"
 	 }, { 
	 	name : "ett övergångsställe",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1956", "2610", "2688", "3267"],
	 	language_ID : "1",
	 	id : "2567"
 	 }, { 
	 	name : "en förkörsrätt",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1957", "2614", "2687"],
	 	language_ID : "1",
	 	id : "2568"
 	 }, { 
	 	name : "en gata",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1954", "2613", "2686"],
	 	language_ID : "1",
	 	id : "2569"
 	 }, { 
	 	name : "kasta",
	 	categories_IDs : ["42"],
	 	related_IDs : ["2571", "2572", "2573", "3007"],
	 	language_ID : "1",
	 	id : "2570"
 	 }, { 
	 	name : "werfen",
	 	categories_IDs : ["42"],
	 	related_IDs : ["2570", "2572", "2573", "3007"],
	 	language_ID : "2",
	 	id : "2571"
 	 }, { 
	 	name : "throw",
	 	categories_IDs : ["42"],
	 	related_IDs : ["2570", "2571", "2573", "3007"],
	 	language_ID : "3",
	 	id : "2572"
 	 }, { 
	 	name : "tirar",
	 	categories_IDs : ["42"],
	 	related_IDs : ["2570", "2571", "2572", "3007"],
	 	language_ID : "5",
	 	id : "2573"
 	 }, { 
	 	name : "en polis",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1959", "2612", "2685"],
	 	language_ID : "1",
	 	id : "2574"
 	 }, { 
	 	name : "öva",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1964", "2630", "2697"],
	 	language_ID : "1",
	 	id : "2575"
 	 }, { 
	 	name : "studera",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1749", "1961", "2628"],
	 	language_ID : "1",
	 	id : "2576"
 	 }, { 
	 	name : "en utbildning",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1970", "2622", "2692", "3266"],
	 	language_ID : "1",
	 	id : "2577"
 	 }, { 
	 	name : "utbilda sig",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1962", "2626", "2698"],
	 	language_ID : "1",
	 	id : "2578"
 	 }, { 
	 	name : "gå i skolan",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1963", "2629"],
	 	language_ID : "1",
	 	id : "2579"
 	 }, { 
	 	name : "die Schule",
	 	categories_IDs : ["55"],
	 	related_IDs : ["2581", "2624", "2694", "3259", "3260"],
	 	language_ID : "2",
	 	id : "2580"
 	 }, { 
	 	name : "en skola",
	 	categories_IDs : ["55"],
	 	related_IDs : ["2580", "2624", "2694", "3259", "3260"],
	 	language_ID : "1",
	 	id : "2581"
 	 }, { 
	 	name : "ett skolämne",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1967", "2619", "2689"],
	 	language_ID : "1",
	 	id : "2582"
 	 }, { 
	 	name : "ett studium",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1968", "2621", "2690"],
	 	language_ID : "1",
	 	id : "2583"
 	 }, { 
	 	name : "en lärare",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1971", "2616", "2691", "3230", "3232"],
	 	language_ID : "1",
	 	id : "2584"
 	 }, { 
	 	name : "en professor",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1972", "2617", "2691", "3231", "3232"],
	 	language_ID : "1",
	 	id : "2585"
 	 }, { 
	 	name : "en undervisning",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1966", "2618", "2696", "3258", "3271"],
	 	language_ID : "1",
	 	id : "2586",
	 	tags : []
 	 }, { 
	 	name : "läxor",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1965", "2623", "2693"],
	 	language_ID : "1",
	 	id : "2587"
 	 }, { 
	 	name : "ett universitet",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1969", "2625", "2695"],
	 	language_ID : "1",
	 	id : "2588"
 	 }, { 
	 	name : "en kopieringsapparat",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1321", "1322", "2632"],
	 	language_ID : "1",
	 	id : "2589"
 	 }, { 
	 	name : "der Drucker",
	 	categories_IDs : ["61"],
	 	related_IDs : ["2591", "2631", "2700", "3270"],
	 	language_ID : "2",
	 	id : "2590"
 	 }, { 
	 	name : "en printer",
	 	categories_IDs : ["61"],
	 	related_IDs : ["2590", "2631", "2700", "3270"],
	 	language_ID : "1",
	 	id : "2591",
	 	tags : []
 	 }, { 
	 	name : "the heaven",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2168", "2170", "2653"],
	 	language_ID : "3",
	 	id : "2592"
 	 }, { 
	 	name : "the reincarnation",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2169", "2171", "2654"],
	 	language_ID : "3",
	 	id : "2593"
 	 }, { 
	 	name : "the statistic",
	 	categories_IDs : ["51"],
	 	related_IDs : ["2200", "2201", "2202"],
	 	language_ID : "3",
	 	id : "2594"
 	 }, { 
	 	name : "respond",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1900", "2550", "2668", "3262", "3269"],
	 	language_ID : "3",
	 	id : "2595",
	 	tags : []
 	 }, { 
	 	name : "chat",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1899", "2551", "2669"],
	 	language_ID : "3",
	 	id : "2596"
 	 }, { 
	 	name : "the mobile phone",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1905", "2552", "2670", "3263", "3273"],
	 	language_ID : "3",
	 	id : "2597"
 	 }, { 
	 	name : "the internet",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1894", "2554", "2671"],
	 	language_ID : "3",
	 	id : "2598"
 	 }, { 
	 	name : "the telephone",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1897", "2562", "2672"],
	 	language_ID : "3",
	 	id : "2599"
 	 }, { 
	 	name : "the call",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1896", "2553", "2673"],
	 	language_ID : "3",
	 	id : "2600"
 	 }, { 
	 	name : "the e-mail",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1895", "2559", "2674", "3261"],
	 	language_ID : "3",
	 	id : "2601"
 	 }, { 
	 	name : "ask",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1901", "2555", "2675"],
	 	language_ID : "3",
	 	id : "2602"
 	 }, { 
	 	name : "e-mail",
	 	categories_IDs : ["52"],
	 	related_IDs : ["2560", "2561", "2676"],
	 	language_ID : "3",
	 	id : "2603"
 	 }, { 
	 	name : "talk",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1904", "2556", "2677"],
	 	language_ID : "3",
	 	id : "2604"
 	 }, { 
	 	name : "call",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1902", "2557", "2678", "3000"],
	 	language_ID : "3",
	 	id : "2605"
 	 }, { 
	 	name : "be silent",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1903", "2558", "2679", "3272"],
	 	language_ID : "3",
	 	id : "2606"
 	 }, { 
	 	name : "phone",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1898", "2563", "2680", "2998"],
	 	language_ID : "3",
	 	id : "2607",
	 	tag : "verb"
 	 }, { 
	 	name : "the accident",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1958", "2566", "2681"],
	 	language_ID : "3",
	 	id : "2608"
 	 }, { 
	 	name : "the pavement",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1955", "2564", "2683", "3268"],
	 	language_ID : "3",
	 	id : "2609",
	 	tags : []
 	 }, { 
	 	name : "the pedestrian crossing",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1956", "2567", "2688", "3267"],
	 	language_ID : "3",
	 	id : "2610",
	 	tags : []
 	 }, { 
	 	name : "the traffic light",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1953", "2565", "2684"],
	 	language_ID : "3",
	 	id : "2611"
 	 }, { 
	 	name : "the police",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1959", "2574", "2685"],
	 	language_ID : "3",
	 	id : "2612"
 	 }, { 
	 	name : "the street",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1954", "2569", "2686"],
	 	language_ID : "3",
	 	id : "2613"
 	 }, { 
	 	name : "right of way",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1957", "2568", "2687"],
	 	language_ID : "3",
	 	id : "2614"
 	 }, { 
	 	name : "cause an accident",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1960", "2682"],
	 	language_ID : "3",
	 	id : "2615"
 	 }, { 
	 	name : "the teacher",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1971", "2584", "2691", "3230", "3232"],
	 	language_ID : "3",
	 	id : "2616"
 	 }, { 
	 	name : "the professor",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1972", "2585", "2691", "3231", "3232"],
	 	language_ID : "3",
	 	id : "2617"
 	 }, { 
	 	name : "the class",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1966", "2586", "2696", "3258", "3271"],
	 	language_ID : "3",
	 	id : "2618"
 	 }, { 
	 	name : "the subject",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1967", "2582", "2689"],
	 	language_ID : "3",
	 	id : "2619"
 	 }, { 
	 	name : "the learning",
	 	categories_IDs : ["55"],
	 	related_IDs : ["678"],
	 	language_ID : "3",
	 	id : "2620"
 	 }, { 
	 	name : "the studies",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1968", "2583", "2690"],
	 	language_ID : "3",
	 	id : "2621"
 	 }, { 
	 	name : "the apprenticeship",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1970", "2577", "2692", "3266"],
	 	language_ID : "3",
	 	id : "2622",
	 	tags : []
 	 }, { 
	 	name : "the homework",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1965", "2587", "2693"],
	 	language_ID : "3",
	 	id : "2623"
 	 }, { 
	 	name : "the school",
	 	categories_IDs : ["55"],
	 	related_IDs : ["2580", "2581", "2694", "3259", "3260"],
	 	language_ID : "3",
	 	id : "2624"
 	 }, { 
	 	name : "the university",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1969", "2588", "2695"],
	 	language_ID : "3",
	 	id : "2625"
 	 }, { 
	 	name : "train",
	 	categories_IDs : ["55", "42"],
	 	related_IDs : ["1962", "2578", "2698", "1299", "1320", "1335", "DNSJVCqKCQa2"],
	 	language_ID : "3",
	 	id : "2626",
	 	tags : []
 	 }, { 
	 	name : "learn",
	 	categories_IDs : ["35", "55"],
	 	related_IDs : ["1748", "1749", "397", "433"],
	 	language_ID : "3",
	 	id : "2627"
 	 }, { 
	 	name : "study",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1749", "1961", "2576"],
	 	language_ID : "3",
	 	id : "2628"
 	 }, { 
	 	name : "attend school",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1963", "2579"],
	 	language_ID : "3",
	 	id : "2629"
 	 }, { 
	 	name : "practice",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1964", "2575", "2697"],
	 	language_ID : "3",
	 	id : "2630"
 	 }, { 
	 	name : "the printer",
	 	categories_IDs : ["61"],
	 	related_IDs : ["2590", "2591", "2700", "3270"],
	 	language_ID : "3",
	 	id : "2631"
 	 }, { 
	 	name : "the copying machine",
	 	categories_IDs : ["61"],
	 	related_IDs : ["1321", "1322", "2589"],
	 	language_ID : "3",
	 	id : "2632"
 	 }, { 
	 	name : "der Schrank",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2642", "2645", "2650", "nJGSqCM3uGpy"],
	 	language_ID : "2",
	 	id : "2633"
 	 }, { 
	 	name : "das Bett",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2638", "2643", "2648", "3264"],
	 	language_ID : "2",
	 	id : "2634"
 	 }, { 
	 	name : "das Sofa",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2639", "2644", "2649", "3265"],
	 	language_ID : "2",
	 	id : "2635",
	 	tags : []
 	 }, { 
	 	name : "der Sessel",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2640", "2646", "2651"],
	 	language_ID : "2",
	 	id : "2636"
 	 }, { 
	 	name : "der Tisch",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2641", "2647", "2652"],
	 	language_ID : "2",
	 	id : "2637"
 	 }, { 
	 	name : "the bed",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2634", "2643", "2648", "3264"],
	 	language_ID : "3",
	 	id : "2638"
 	 }, { 
	 	name : "the couch",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2635", "2644", "2649", "3265"],
	 	language_ID : "3",
	 	id : "2639"
 	 }, { 
	 	name : "the armchair",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2636", "2646", "2651"],
	 	language_ID : "3",
	 	id : "2640"
 	 }, { 
	 	name : "the table",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2637", "2647", "2652"],
	 	language_ID : "3",
	 	id : "2641"
 	 }, { 
	 	name : "the closet",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2633", "2645", "2650"],
	 	language_ID : "3",
	 	id : "2642",
	 	tags : []
 	 }, { 
	 	name : "la cama",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2634", "2638", "2648", "3264"],
	 	language_ID : "5",
	 	id : "2643"
 	 }, { 
	 	name : "el sofá",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2635", "2639", "2649", "3265"],
	 	language_ID : "5",
	 	id : "2644"
 	 }, { 
	 	name : "el armario",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2633", "2642", "2650"],
	 	language_ID : "5",
	 	id : "2645"
 	 }, { 
	 	name : "el sillón",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2636", "2640", "2651"],
	 	language_ID : "5",
	 	id : "2646"
 	 }, { 
	 	name : "la mesa",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2637", "2641", "2652"],
	 	language_ID : "5",
	 	id : "2647"
 	 }, { 
	 	name : "en säng",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2634", "2638", "2643", "3264"],
	 	language_ID : "1",
	 	id : "2648",
	 	tags : []
 	 }, { 
	 	name : "en sofa",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2635", "2639", "2644", "3265"],
	 	language_ID : "1",
	 	id : "2649"
 	 }, { 
	 	name : "ett skåp",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2633", "2642", "2645"],
	 	language_ID : "1",
	 	id : "2650"
 	 }, { 
	 	name : "en fåtölj",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2636", "2640", "2646"],
	 	language_ID : "1",
	 	id : "2651"
 	 }, { 
	 	name : "ett bord",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2637", "2641", "2647"],
	 	language_ID : "1",
	 	id : "2652"
 	 }, { 
	 	name : "el cielo",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2168", "2170", "2592"],
	 	language_ID : "5",
	 	id : "2653"
 	 }, { 
	 	name : "la reencarnación",
	 	categories_IDs : ["49"],
	 	related_IDs : ["2169", "2171", "2593"],
	 	language_ID : "5",
	 	id : "2654"
 	 }, { 
	 	name : "el dinero",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1927", "2172", "2187"],
	 	language_ID : "5",
	 	id : "2655"
 	 }, { 
	 	name : "la cuenta",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1933", "2174", "2188"],
	 	language_ID : "5",
	 	id : "2656"
 	 }, { 
	 	name : "el crédito",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1931", "2175", "2189", "3050"],
	 	language_ID : "5",
	 	id : "2657"
 	 }, { 
	 	name : "el ciclo",
	 	categories_IDs : ["51"],
	 	related_IDs : ["2177", "2199", "1928"],
	 	language_ID : "5",
	 	id : "2658"
 	 }, { 
	 	name : "el interés",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1935", "2184", "2190"],
	 	language_ID : "5",
	 	id : "2659"
 	 }, { 
	 	name : "la aduana",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1934", "2185", "2197", "3081"],
	 	language_ID : "5",
	 	id : "2660"
 	 }, { 
	 	name : "el banco",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1930", "2173", "2194", "2833"],
	 	language_ID : "5",
	 	id : "2661"
 	 }, { 
	 	name : "ta tarjeta de crédito",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1932", "2176", "2195"],
	 	language_ID : "5",
	 	id : "2662"
 	 }, { 
	 	name : "la deuda",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1929", "2180", "2198"],
	 	language_ID : "5",
	 	id : "2663"
 	 }, { 
	 	name : "el impuesto",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1939", "2181", "2191"],
	 	language_ID : "5",
	 	id : "2664"
 	 }, { 
	 	name : "la transferencia",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1936", "2182", "2196"],
	 	language_ID : "5",
	 	id : "2665"
 	 }, { 
	 	name : "transferir",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1937", "2186", "2193"],
	 	language_ID : "5",
	 	id : "2666"
 	 }, { 
	 	name : "regalar",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1938", "2183", "2192"],
	 	language_ID : "5",
	 	id : "2667"
 	 }, { 
	 	name : "contestar",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1900", "2550", "2595", "3262", "3269"],
	 	language_ID : "5",
	 	id : "2668",
	 	tags : []
 	 }, { 
	 	name : "chatear",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1899", "2551", "2596"],
	 	language_ID : "5",
	 	id : "2669"
 	 }, { 
	 	name : "el celular",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1905", "2552", "2597", "3263", "3273"],
	 	language_ID : "5",
	 	id : "2670",
	 	tags : []
 	 }, { 
	 	name : "la red",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1894", "2554", "2598"],
	 	language_ID : "5",
	 	id : "2671"
 	 }, { 
	 	name : "el teléfono",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1897", "2562", "2599"],
	 	language_ID : "5",
	 	id : "2672"
 	 }, { 
	 	name : "la llamada",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1896", "2553", "2600"],
	 	language_ID : "5",
	 	id : "2673"
 	 }, { 
	 	name : "el mail",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1895", "2559", "2601", "3261"],
	 	language_ID : "5",
	 	id : "2674",
	 	tags : []
 	 }, { 
	 	name : "preguntar",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1901", "2555", "2602"],
	 	language_ID : "5",
	 	id : "2675"
 	 }, { 
	 	name : "enviar un correo electrónico",
	 	categories_IDs : ["52"],
	 	related_IDs : ["2560", "2561", "2603"],
	 	language_ID : "5",
	 	id : "2676"
 	 }, { 
	 	name : "hablar",
	 	categories_IDs : ["52", "35"],
	 	related_IDs : ["1904", "2556", "2604", "400", "435", "467"],
	 	language_ID : "5",
	 	id : "2677"
 	 }, { 
	 	name : "llamar",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1902", "2557", "2605", "3000"],
	 	language_ID : "5",
	 	id : "2678"
 	 }, { 
	 	name : "callarse",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1903", "2558", "2606", "3272"],
	 	language_ID : "5",
	 	id : "2679"
 	 }, { 
	 	name : "llamar por teléfono",
	 	categories_IDs : ["52"],
	 	related_IDs : ["1898", "2563", "2607", "2998"],
	 	language_ID : "5",
	 	id : "2680"
 	 }, { 
	 	name : "el accidente",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1958", "2566", "2608"],
	 	language_ID : "5",
	 	id : "2681"
 	 }, { 
	 	name : "causar un accidente",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1960", "2615"],
	 	language_ID : "5",
	 	id : "2682"
 	 }, { 
	 	name : "la acera",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1955", "2564", "2609", "3268"],
	 	language_ID : "5",
	 	id : "2683"
 	 }, { 
	 	name : "el semáforo",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1953", "2565", "2611"],
	 	language_ID : "5",
	 	id : "2684"
 	 }, { 
	 	name : "la policía",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1959", "2574", "2612"],
	 	language_ID : "5",
	 	id : "2685"
 	 }, { 
	 	name : "la calle",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1954", "2569", "2613"],
	 	language_ID : "5",
	 	id : "2686"
 	 }, { 
	 	name : "la preferencia de paso",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1957", "2568", "2614"],
	 	language_ID : "5",
	 	id : "2687"
 	 }, { 
	 	name : "el paso de cebra",
	 	categories_IDs : ["54"],
	 	related_IDs : ["1956", "2567", "2610", "3267"],
	 	language_ID : "5",
	 	id : "2688"
 	 }, { 
	 	name : "la asignatura",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1967", "2582", "2619"],
	 	language_ID : "5",
	 	id : "2689"
 	 }, { 
	 	name : "los estudios",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1968", "2583", "2621"],
	 	language_ID : "5",
	 	id : "2690"
 	 }, { 
	 	name : "el profesor",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1971", "2584", "2585", "2616", "2617", "3231"],
	 	language_ID : "5",
	 	id : "2691",
	 	tags : [],
	 	formsPrimary_IDs : ["3232"]
 	 }, { 
	 	name : "la formación",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1970", "2577", "2622", "3266"],
	 	language_ID : "5",
	 	id : "2692"
 	 }, { 
	 	name : "los deberes",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1965", "2587", "2623"],
	 	language_ID : "5",
	 	id : "2693"
 	 }, { 
	 	name : "el colegio",
	 	categories_IDs : ["55"],
	 	related_IDs : ["2580", "2581", "2624", "3259", "3260"],
	 	language_ID : "5",
	 	id : "2694",
	 	tags : []
 	 }, { 
	 	name : "la universidad",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1969", "2588", "2625"],
	 	language_ID : "5",
	 	id : "2695"
 	 }, { 
	 	name : "la clase",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1966", "2586", "2618", "3258", "3271"],
	 	language_ID : "5",
	 	id : "2696",
	 	tags : []
 	 }, { 
	 	name : "practicar",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1964", "2575", "2630"],
	 	language_ID : "5",
	 	id : "2697"
 	 }, { 
	 	name : "recibir una formación",
	 	categories_IDs : ["55"],
	 	related_IDs : ["1962", "2578", "2626"],
	 	language_ID : "5",
	 	id : "2698"
 	 }, { 
	 	name : "la impresora",
	 	categories_IDs : ["61"],
	 	related_IDs : ["2590", "2591", "2631", "3270"],
	 	language_ID : "5",
	 	id : "2700"
 	 }, { 
	 	name : "el abeto",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1986", "2395", "2740"],
	 	language_ID : "5",
	 	id : "2701"
 	 }, { 
	 	name : "la raíz",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1980", "2396", "2741"],
	 	language_ID : "5",
	 	id : "2702"
 	 }, { 
	 	name : "regar las plantas",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1990", "2397", "2742", "3008"],
	 	language_ID : "5",
	 	id : "2703"
 	 }, { 
	 	name : "brotar",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1989", "2399", "2743"],
	 	language_ID : "5",
	 	id : "2704"
 	 }, { 
	 	name : "pudrirse",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1982", "2398", "2744"],
	 	language_ID : "5",
	 	id : "2705"
 	 }, { 
	 	name : "crecer",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1981", "2400", "2745"],
	 	language_ID : "5",
	 	id : "2706"
 	 }, { 
	 	name : "el extraterrestre",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2008", "2799", "3237", "3238"],
	 	language_ID : "5",
	 	id : "2707",
	 	tags : [],
	 	formsPrimary_IDs : ["3257"]
 	 }, { 
	 	name : "el alga",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2311", "2316", "2807"],
	 	language_ID : "5",
	 	id : "2708"
 	 }, { 
	 	name : "la bacteria",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2006", "2322", "2803", "3053"],
	 	language_ID : "5",
	 	id : "2709"
 	 }, { 
	 	name : "el contagio",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2309", "2323", "2804"],
	 	language_ID : "5",
	 	id : "2710"
 	 }, { 
	 	name : "contagirse",
	 	categories_IDs : ["44", "58"],
	 	related_IDs : ["2314", "2315", "2805", "3052"],
	 	language_ID : "5",
	 	id : "2711"
 	 }, { 
	 	name : "propagarse",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2317", "2318", "2806"],
	 	language_ID : "5",
	 	id : "2712"
 	 }, { 
	 	name : "el hongo",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2310", "2321", "2800", "3051"],
	 	language_ID : "5",
	 	id : "2713"
 	 }, { 
	 	name : "el virus",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2007", "2324", "2801"],
	 	language_ID : "5",
	 	id : "2714"
 	 }, { 
	 	name : "el arco iris",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2285", "2298", "2717"],
	 	language_ID : "5",
	 	id : "2715"
 	 }, { 
	 	name : "soleado",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2303", "2304", "2718"],
	 	language_ID : "5",
	 	id : "2716",
	 	tags : [],
	 	formsPrimary_IDs : ["3256"]
 	 }, { 
	 	name : "the rainbow",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2285", "2298", "2715"],
	 	language_ID : "3",
	 	id : "2717"
 	 }, { 
	 	name : "sunny",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2303", "2304", "2716", "3256"],
	 	language_ID : "3",
	 	id : "2718"
 	 }, { 
	 	name : "the peace",
	 	categories_IDs : ["48"],
	 	related_IDs : ["2721", "2724", "2726"],
	 	language_ID : "3",
	 	id : "2719"
 	 }, { 
	 	name : "the war",
	 	categories_IDs : ["48"],
	 	related_IDs : ["2722", "2723", "2725"],
	 	language_ID : "3",
	 	id : "2720"
 	 }, { 
	 	name : "der Frieden",
	 	categories_IDs : ["48"],
	 	related_IDs : ["2719", "2724", "2726", "2923"],
	 	language_ID : "2",
	 	id : "2721",
	 	hasAudio : true
 	 }, { 
	 	name : "der Krieg",
	 	categories_IDs : ["48"],
	 	related_IDs : ["2720", "2723", "2725"],
	 	language_ID : "2",
	 	id : "2722",
	 	hasAudio : true
 	 }, { 
	 	name : "la guerra",
	 	categories_IDs : ["48"],
	 	related_IDs : ["2720", "2722", "2725"],
	 	language_ID : "5",
	 	id : "2723"
 	 }, { 
	 	name : "la paz",
	 	categories_IDs : ["48"],
	 	related_IDs : ["2719", "2721", "2726"],
	 	language_ID : "5",
	 	id : "2724"
 	 }, { 
	 	name : "ett krig",
	 	categories_IDs : ["48"],
	 	related_IDs : ["2720", "2722", "2723"],
	 	language_ID : "1",
	 	id : "2725",
	 	hasAudio : true
 	 }, { 
	 	name : "en frid",
	 	categories_IDs : ["48"],
	 	related_IDs : ["2719", "2721", "2724"],
	 	language_ID : "1",
	 	id : "2726",
	 	hasAudio : true,
	 	tags : []
 	 }, { 
	 	name : "ett blad",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1976", "2381", "2402"],
	 	language_ID : "1",
	 	id : "2727"
 	 }, { 
	 	name : "ett trä",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1979", "2382", "2403"],
	 	language_ID : "1",
	 	id : "2728"
 	 }, { 
	 	name : "en träd",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1983", "2383", "2405"],
	 	language_ID : "1",
	 	id : "2729"
 	 }, { 
	 	name : "en gödsling",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1991", "2401", "2406"],
	 	language_ID : "1",
	 	id : "2730"
 	 }, { 
	 	name : "ett körsbärstred",
	 	categories_IDs : ["48"],
	 	related_IDs : ["1987", "2384", "2407"],
	 	language_ID : "1",
	 	id : "2731"
 	 }, { 
	 	name : "en regnskog",
	 	categories_IDs : ["48"],
	 	related_IDs : ["2274", "2275", "2385"],
	 	language_ID : "1",
	 	id : "2732"
 	 }, { 
	 	name : "en stamm",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1978", "2386", "2408"],
	 	language_ID : "1",
	 	id : "2733"
 	 }, { 
	 	name : "en björk",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1985", "2387", "2410"],
	 	language_ID : "1",
	 	id : "2734"
 	 }, { 
	 	name : "en blomma",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1973", "1975", "2388", "2389", "2411"],
	 	language_ID : "1",
	 	id : "2735"
 	 }, { 
	 	name : "en ek",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1984", "2390", "2412"],
	 	language_ID : "1",
	 	id : "2736"
 	 }, { 
	 	name : "ett bar",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1977", "2392", "2414"],
	 	language_ID : "1",
	 	id : "2737"
 	 }, { 
	 	name : "en palm",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1988", "2393", "2415"],
	 	language_ID : "1",
	 	id : "2738"
 	 }, { 
	 	name : "en solros",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1974", "2394", "2416"],
	 	language_ID : "1",
	 	id : "2739"
 	 }, { 
	 	name : "en ädelgran",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1986", "2395", "2701"],
	 	language_ID : "1",
	 	id : "2740"
 	 }, { 
	 	name : "en rot",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1980", "2396", "2702"],
	 	language_ID : "1",
	 	id : "2741"
 	 }, { 
	 	name : "hälla",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1990", "2397", "2703", "3008"],
	 	language_ID : "1",
	 	id : "2742"
 	 }, { 
	 	name : "spira",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1989", "2399", "2704"],
	 	language_ID : "1",
	 	id : "2743"
 	 }, { 
	 	name : "ruttna",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1982", "2398", "2705"],
	 	language_ID : "1",
	 	id : "2744"
 	 }, { 
	 	name : "växa",
	 	categories_IDs : ["56"],
	 	related_IDs : ["1981", "2400", "2706"],
	 	language_ID : "1",
	 	id : "2745"
 	 }, { 
	 	name : "en groda",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2360", "2361", "2366"],
	 	language_ID : "1",
	 	id : "2746"
 	 }, { 
	 	name : "die Rose",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2752", "2754", "2808"],
	 	language_ID : "2",
	 	id : "2747"
 	 }, { 
	 	name : "das Gras",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2751", "2753", "2809"],
	 	language_ID : "2",
	 	id : "2748"
 	 }, { 
	 	name : "vertrocknen",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2750", "2755", "2810", "3013"],
	 	language_ID : "2",
	 	id : "2749"
 	 }, { 
	 	name : "förtorka",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2749", "2755", "2810", "3013"],
	 	language_ID : "1",
	 	id : "2750"
 	 }, { 
	 	name : "ett gräs",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2748", "2753", "2809"],
	 	language_ID : "1",
	 	id : "2751"
 	 }, { 
	 	name : "en ros",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2747", "2754", "2808"],
	 	language_ID : "1",
	 	id : "2752"
 	 }, { 
	 	name : "the grass",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2748", "2751", "2809", "UEbo6iw4rkOn"],
	 	language_ID : "3",
	 	id : "2753",
	 	tags : []
 	 }, { 
	 	name : "the rose",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2747", "2752", "2808"],
	 	language_ID : "3",
	 	id : "2754"
 	 }, { 
	 	name : "wither",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2749", "2750", "2810", "3013"],
	 	language_ID : "3",
	 	id : "2755"
 	 }, { 
	 	name : "the meadow",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2759", "2763", "2765"],
	 	language_ID : "3",
	 	id : "2758"
 	 }, { 
	 	name : "die Wiese",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2758", "2763", "2765"],
	 	language_ID : "2",
	 	id : "2759"
 	 }, { 
	 	name : "das Feld",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2761", "2762", "2764"],
	 	language_ID : "2",
	 	id : "2760"
 	 }, { 
	 	name : "the field",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2760", "2762", "2764"],
	 	language_ID : "3",
	 	id : "2761"
 	 }, { 
	 	name : "ett fält",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2760", "2761", "2764"],
	 	language_ID : "1",
	 	id : "2762"
 	 }, { 
	 	name : "en äng",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2758", "2759", "2765"],
	 	language_ID : "1",
	 	id : "2763"
 	 }, { 
	 	name : "el campo",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2760", "2761", "2762"],
	 	language_ID : "5",
	 	id : "2764"
 	 }, { 
	 	name : "el prado",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2758", "2759", "2763"],
	 	language_ID : "5",
	 	id : "2765"
 	 }, { 
	 	name : "die Hitze",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2777", "2786", "2794"],
	 	language_ID : "2",
	 	id : "2766"
 	 }, { 
	 	name : "die Kälte",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2778", "2787", "2795"],
	 	language_ID : "2",
	 	id : "2767"
 	 }, { 
	 	name : "die Dürre",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2776", "2785", "2793", "3254"],
	 	language_ID : "2",
	 	id : "2768"
 	 }, { 
	 	name : "die Luftfeuchtigkeit",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2779", "2788", "2796"],
	 	language_ID : "2",
	 	id : "2769"
 	 }, { 
	 	name : "der Hagel",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2774", "2782", "2791"],
	 	language_ID : "2",
	 	id : "2770"
 	 }, { 
	 	name : "der Regen",
	 	categories_IDs : ["2757", "59"],
	 	related_IDs : ["2775", "2783", "2792", "2273", "2297"],
	 	language_ID : "2",
	 	id : "2771"
 	 }, { 
	 	name : "frieren",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2780", "2789", "2797"],
	 	language_ID : "2",
	 	id : "2772"
 	 }, { 
	 	name : "schwitzen",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2781", "2790", "2798", "3255"],
	 	language_ID : "2",
	 	id : "2773"
 	 }, { 
	 	name : "el granizo",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2770", "2782", "2791"],
	 	language_ID : "5",
	 	id : "2774"
 	 }, { 
	 	name : "la lluvia",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2771", "2783", "2792"],
	 	language_ID : "5",
	 	id : "2775"
 	 }, { 
	 	name : "la sequía",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2768", "2785", "2793", "3254"],
	 	language_ID : "5",
	 	id : "2776"
 	 }, { 
	 	name : "el calor",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2766", "2786", "2794"],
	 	language_ID : "5",
	 	id : "2777"
 	 }, { 
	 	name : "el frío",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2767", "2787", "2795"],
	 	language_ID : "5",
	 	id : "2778"
 	 }, { 
	 	name : "la humedad",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2769", "2788", "2796"],
	 	language_ID : "5",
	 	id : "2779"
 	 }, { 
	 	name : "tener frío",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2772", "2789", "2797"],
	 	language_ID : "5",
	 	id : "2780"
 	 }, { 
	 	name : "sudar",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2773", "2790", "2798", "3255"],
	 	language_ID : "5",
	 	id : "2781",
	 	tags : []
 	 }, { 
	 	name : "ett hagel",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2770", "2774", "2791"],
	 	language_ID : "1",
	 	id : "2782"
 	 }, { 
	 	name : "ett regn",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2771", "2775", "2792"],
	 	language_ID : "1",
	 	id : "2783"
 	 }, { 
	 	name : "en torrhet",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2768", "2776", "2793", "3254"],
	 	language_ID : "1",
	 	id : "2785",
	 	tags : []
 	 }, { 
	 	name : "en hetta",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2766", "2777", "2794"],
	 	language_ID : "1",
	 	id : "2786"
 	 }, { 
	 	name : "en köld",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2767", "2778", "2795"],
	 	language_ID : "1",
	 	id : "2787"
 	 }, { 
	 	name : "en luftfuktighet",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2769", "2779", "2796"],
	 	language_ID : "1",
	 	id : "2788"
 	 }, { 
	 	name : "frysa",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2772", "2780", "2797"],
	 	language_ID : "1",
	 	id : "2789"
 	 }, { 
	 	name : "svettas",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2773", "2781", "2798", "3255"],
	 	language_ID : "1",
	 	id : "2790"
 	 }, { 
	 	name : "the hail",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2770", "2774", "2782"],
	 	language_ID : "3",
	 	id : "2791"
 	 }, { 
	 	name : "the rain",
	 	categories_IDs : ["2757", "59"],
	 	related_IDs : ["2771", "2775", "2783", "2273", "2297"],
	 	language_ID : "3",
	 	id : "2792"
 	 }, { 
	 	name : "the drought",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2768", "2776", "2785", "3254"],
	 	language_ID : "3",
	 	id : "2793"
 	 }, { 
	 	name : "the heat",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2766", "2777", "2786"],
	 	language_ID : "3",
	 	id : "2794"
 	 }, { 
	 	name : "the cold",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2767", "2778", "2787"],
	 	language_ID : "3",
	 	id : "2795"
 	 }, { 
	 	name : "the humidity",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2769", "2779", "2788"],
	 	language_ID : "3",
	 	id : "2796"
 	 }, { 
	 	name : "freeze",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2772", "2780", "2789"],
	 	language_ID : "3",
	 	id : "2797"
 	 }, { 
	 	name : "sweat",
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2773", "2781", "2790", "3255"],
	 	language_ID : "3",
	 	id : "2798"
 	 }, { 
	 	name : "the alien",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2008", "2707", "3237", "3238", "3257"],
	 	language_ID : "3",
	 	id : "2799"
 	 }, { 
	 	name : "the mushroom",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2310", "2321", "2713", "3051"],
	 	language_ID : "3",
	 	id : "2800"
 	 }, { 
	 	name : "the virus",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2007", "2324", "2714"],
	 	language_ID : "3",
	 	id : "2801"
 	 }, { 
	 	name : "the protozoon",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2312", "2313"],
	 	language_ID : "3",
	 	id : "2802"
 	 }, { 
	 	name : "the bacterium",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2006", "2322", "2709", "3053", "eFxkoO2Q9BB3"],
	 	language_ID : "3",
	 	id : "2803"
 	 }, { 
	 	name : "the infection",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2309", "2323", "2710"],
	 	language_ID : "3",
	 	id : "2804"
 	 }, { 
	 	name : "get infected",
	 	categories_IDs : ["44", "58"],
	 	related_IDs : ["2314", "2315", "2711", "3052"],
	 	language_ID : "3",
	 	id : "2805"
 	 }, { 
	 	name : "spread",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2317", "2318", "2712"],
	 	language_ID : "3",
	 	id : "2806"
 	 }, { 
	 	name : "the alga",
	 	categories_IDs : ["58"],
	 	related_IDs : ["2311", "2316", "2708"],
	 	language_ID : "3",
	 	id : "2807"
 	 }, { 
	 	name : "la rosa",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2747", "2752", "2754"],
	 	language_ID : "5",
	 	id : "2808"
 	 }, { 
	 	name : "la hierba",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2748", "2751", "2753"],
	 	language_ID : "5",
	 	id : "2809"
 	 }, { 
	 	name : "secarse",
	 	categories_IDs : ["56"],
	 	related_IDs : ["2749", "2750", "2755", "3013"],
	 	language_ID : "5",
	 	id : "2810"
 	 }, { 
	 	name : "der Bär",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2812", "2813", "2814"],
	 	language_ID : "2",
	 	id : "2811"
 	 }, { 
	 	name : "en björn",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2811", "2813", "2814"],
	 	language_ID : "1",
	 	id : "2812"
 	 }, { 
	 	name : "the bear",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2811", "2812", "2814"],
	 	language_ID : "3",
	 	id : "2813"
 	 }, { 
	 	name : "el oso",
	 	categories_IDs : ["57"],
	 	related_IDs : ["2811", "2812", "2813"],
	 	language_ID : "5",
	 	id : "2814"
 	 }, { 
	 	related_IDs : ["2817", "2818", "3084", "372", "475", "741"],
	 	categories_IDs : ["34", "40", "51"],
	 	name : "Yesterday we bought some fresh vegetables on the market.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2816"
 	 }, { 
	 	categories_IDs : ["34", "40", "51"],
	 	related_IDs : ["123", "1734", "2816", "2818", "3084", "863"],
	 	name : "Ayer hemos comprado algunas verduras frescas en el mercado.",
	 	language_ID : "5",
	 	sentence : true,
	 	id : "2817"
 	 }, { 
	 	categories_IDs : ["34", "40", "51"],
	 	related_IDs : ["169", "2816", "2817", "3084", "408"],
	 	name : "Vi köpte några färska grönsaker på torget i går.",
	 	language_ID : "1",
	 	sentence : true,
	 	id : "2818"
 	 }, { 
	 	categories_IDs : ["34", "44"],
	 	related_IDs : ["1416", "2820", "2821", "2822"],
	 	name : "Du brauchst zu Fuß etwa fünf Minuten von hier zur Post.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "2819"
 	 }, { 
	 	categories_IDs : ["34", "44"],
	 	related_IDs : ["1428", "2819", "2821", "2822"],
	 	name : "By foot you need roughly five minutes from here to the post office.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2820"
 	 }, { 
	 	categories_IDs : ["34", "44"],
	 	related_IDs : ["1447", "2819", "2820", "2822"],
	 	name : "A pie sólo te necesitas cinco minutos de aquí a la oficina de correos.",
	 	language_ID : "5",
	 	sentence : true,
	 	id : "2821"
 	 }, { 
	 	categories_IDs : ["34", "44"],
	 	related_IDs : ["1462", "2819", "2820", "2821"],
	 	name : "Till fots du behöver bara fem minuter härifrån till postkontoret.",
	 	language_ID : "1",
	 	sentence : true,
	 	id : "2822"
 	 }, { 
	 	categories_IDs : ["2757", "39"],
	 	related_IDs : ["2238", "2824", "2825", "2826"],
	 	name : "Als er morgens aufwachte, regnete es immer noch.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "2823"
 	 }, { 
	 	categories_IDs : ["2757", "34"],
	 	related_IDs : ["2262", "2823", "2825", "2826", "756"],
	 	name : "When he woke up in the morning it was still raining.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2824"
 	 }, { 
	 	categories_IDs : ["2757", "34"],
	 	related_IDs : ["2284", "2823", "2824", "2826", "877"],
	 	name : "Seguía lloviendo cuando se despertó por la mañana.",
	 	sentence : true,
	 	language_ID : "5",
	 	id : "2825"
 	 }, { 
	 	categories_IDs : ["2757", "34"],
	 	related_IDs : ["2308", "2823", "2824", "2825", "812"],
	 	name : "Det regnade fortfarande när han vaknade upp på morgonen.",
	 	language_ID : "1",
	 	sentence : true,
	 	id : "2826"
 	 }, { 
	 	categories_IDs : ["19", "34"],
	 	related_IDs : ["1930", "2828", "2830", "2833", "2834"],
	 	name : "Die Bank öffnet um 14 Uhr wieder.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "2827"
 	 }, { 
	 	categories_IDs : ["19", "34"],
	 	related_IDs : ["2194", "249", "2827", "2831", "2833", "2834"],
	 	name : "The bank will open again at 2 p.m..",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2828"
 	 }, { 
	 	categories_IDs : ["35"],
	 	related_IDs : ["2830", "2831", "2832", "2834"],
	 	name : "öppna",
	 	language_ID : "1",
	 	id : "2829"
 	 }, { 
	 	categories_IDs : ["35"],
	 	related_IDs : ["2827", "2829", "2831", "2832", "943"],
	 	name : "öffnen",
	 	language_ID : "2",
	 	id : "2830"
 	 }, { 
	 	categories_IDs : ["35"],
	 	related_IDs : ["2828", "2829", "2830", "2832"],
	 	name : "open",
	 	language_ID : "3",
	 	id : "2831"
 	 }, { 
	 	categories_IDs : ["35"],
	 	related_IDs : ["2829", "2830", "2831", "2833"],
	 	language_ID : "5",
	 	name : "abrir",
	 	id : "2832"
 	 }, { 
	 	categories_IDs : [],
	 	related_IDs : ["2661", "280", "2827", "2828", "2832", "2834"],
	 	name : "El banco se abrirá de nuevo en dos.",
	 	language_ID : "5",
	 	sentence : true,
	 	id : "2833"
 	 }, { 
	 	categories_IDs : ["19", "34"],
	 	related_IDs : ["214", "2173", "2827", "2828", "2829", "2833"],
	 	name : "Banken kommer att öppna igen vid 14.",
	 	language_ID : "1",
	 	sentence : true,
	 	id : "2834"
 	 }, { 
	 	categories_IDs : ["34", "40"],
	 	related_IDs : ["2836", "2837", "2838", "430"],
	 	name : "Kannst du mir bitte die Butter geben?",
	 	sentence : true,
	 	language_ID : "2",
	 	id : "2835"
 	 }, { 
	 	categories_IDs : ["34", "40"],
	 	related_IDs : ["2835", "2837", "2838", "463", "Pr0Vi7H8fpo5"],
	 	name : "Could you pass me the butter, please?",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2836"
 	 }, { 
	 	categories_IDs : ["34", "40"],
	 	related_IDs : ["1744", "2835", "2836", "2838"],
	 	name : "Pordrías darme la mantequilla, por favor?",
	 	language_ID : "5",
	 	sentence : true,
	 	id : "2837"
 	 }, { 
	 	categories_IDs : ["34", "40"],
	 	related_IDs : ["2835", "2836", "2837"],
	 	name : "Kan du ge mig smör, tack?",
	 	language_ID : "1",
	 	id : "2838"
 	 }, { 
	 	categories_IDs : ["34", "57"],
	 	related_IDs : ["1999", "2840", "2841", "2842", "2843", "423"],
	 	name : "Sie hatte keine Angst vor Spinnen, er schon.",
	 	sentence : true,
	 	language_ID : "2",
	 	id : "2839"
 	 }, { 
	 	categories_IDs : ["34", "57"],
	 	related_IDs : ["2371", "2839", "2841", "2842", "452"],
	 	name : "She was not afraid of spiders, but he was.",
	 	sentence : true,
	 	language_ID : "3",
	 	id : "2840"
 	 }, { 
	 	categories_IDs : ["34", "57"],
	 	related_IDs : ["1742", "2359", "2839", "2840"],
	 	name : "No tenía miedo a las arañas, pero que era.",
	 	language_ID : "5",
	 	sentence : true,
	 	id : "2841"
 	 }, { 
	 	categories_IDs : ["34", "57"],
	 	related_IDs : ["109", "2342", "2359", "2839", "2840", "380"],
	 	name : "Hon var inte rädd för spindlar, men han var.",
	 	language_ID : "1",
	 	sentence : true,
	 	id : "2842"
 	 }, { 
	 	categories_IDs : ["43"],
	 	related_IDs : ["2839", "2844", "2845", "2846"],
	 	name : "die Angst",
	 	language_ID : "2",
	 	id : "2843"
 	 }, { 
	 	categories_IDs : ["43"],
	 	related_IDs : ["2843", "2845", "2846"],
	 	name : "the fear",
	 	language_ID : "3",
	 	id : "2844"
 	 }, { 
	 	categories_IDs : ["43"],
	 	related_IDs : ["2843", "2844", "2846"],
	 	name : "el miedo",
	 	language_ID : "5",
	 	id : "2845"
 	 }, { 
	 	categories_IDs : ["43"],
	 	related_IDs : ["2843", "2844", "2845"],
	 	name : "en rädsla",
	 	language_ID : "1",
	 	id : "2846"
 	 }, { 
	 	categories_IDs : ["34", "44"],
	 	related_IDs : ["1556", "2848", "2849", "2850"],
	 	name : "Dank der Ruhe waren die Kopfschmerzen schnell wieder weg.",
	 	sentence : true,
	 	language_ID : "2",
	 	id : "2847"
 	 }, { 
	 	categories_IDs : ["34", "44"],
	 	related_IDs : ["2847", "2849", "2850"],
	 	name : "Thanks to the quietude the headache vanished quickly.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2848"
 	 }, { 
	 	categories_IDs : ["34", "44"],
	 	related_IDs : ["1449", "1457", "2847", "2848", "2850"],
	 	name : "Por la tranquilidad, el dolor de cabeza se marchó rápidamente.",
	 	language_ID : "5",
	 	sentence : true,
	 	id : "2849"
 	 }, { 
	 	categories_IDs : ["34", "44"],
	 	related_IDs : ["2847", "2848", "2849"],
	 	name : "Tack lugnet huvudvärken försvann snabbt.",
	 	language_ID : "1",
	 	sentence : true,
	 	id : "2850"
 	 }, { 
	 	categories_IDs : ["34", "46"],
	 	related_IDs : ["1563", "2852", "2853", "2854", "709"],
	 	name : "Wollen wir am Dienstag im Park spazieren gehen?",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "2851"
 	 }, { 
	 	categories_IDs : ["34", "46"],
	 	related_IDs : ["1583", "2851", "2853", "2854", "763"],
	 	name : "Shall we go for a walk in the park on Tuesday?",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2852"
 	 }, { 
	 	categories_IDs : ["34", "46"],
	 	related_IDs : ["1602", "2851", "2852", "2854", "887"],
	 	name : "¿Vamos a dar un paseo el martes en el parque?",
	 	language_ID : "5",
	 	sentence : true,
	 	id : "2853"
 	 }, { 
	 	categories_IDs : ["34", "46"],
	 	related_IDs : ["1621", "2851", "2852", "2853", "820"],
	 	name : "Ska vi ta en promenad på tisdagen i parken?",
	 	language_ID : "1",
	 	sentence : true,
	 	id : "2854"
 	 }, { 
	 	categories_IDs : ["19", "34"],
	 	related_IDs : ["2856", "2857", "2858", "430", "882", "951"],
	 	name : "Kannst du Markus nachher vom Bahnhof abholen?",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "2855"
 	 }, { 
	 	categories_IDs : ["19", "34"],
	 	related_IDs : ["2855", "2857", "2858", "463", "920", "993"],
	 	name : "Can you fetch Markus from the train station later?",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2856"
 	 }, { 
	 	categories_IDs : ["19", "34"],
	 	related_IDs : ["1085", "1744", "2855", "2856", "2858", "883"],
	 	name : "¿Puedes recoger Markus de la estación después?",
	 	language_ID : "5",
	 	sentence : true,
	 	id : "2857"
 	 }, { 
	 	categories_IDs : ["19", "34"],
	 	related_IDs : ["2855", "2856", "2857", "914"],
	 	name : "Kan du komma och mäta Markus efteråt vid stationen?",
	 	language_ID : "1",
	 	sentence : true,
	 	id : "2858"
 	 }, { 
	 	name : "too",
	 	categories_IDs : ["3"],
	 	related_IDs : ["1257", "1258", "1717"],
	 	language_ID : "3",
	 	id : "2859"
 	 }, { 
	 	name : "orientation",
	 	categories_IDs : ["38"],
	 	related_IDs : ["676", "677"],
	 	language_ID : "3",
	 	id : "2860"
 	 }, { 
	 	name : "Excuse me, what is the name of this street?",
	 	categories_IDs : ["34"],
	 	related_IDs : ["946"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2861"
 	 }, { 
	 	name : "Bye",
	 	categories_IDs : ["34"],
	 	related_IDs : ["938"],
	 	language_ID : "3",
	 	id : "2862"
 	 }, { 
	 	name : "Hi",
	 	categories_IDs : ["34"],
	 	related_IDs : ["931"],
	 	language_ID : "3",
	 	id : "2863"
 	 }, { 
	 	name : "The more, the merrier.",
	 	categories_IDs : ["34"],
	 	related_IDs : ["1127", "1128"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2864"
 	 }, { 
	 	name : "with",
	 	categories_IDs : ["3"],
	 	related_IDs : ["1255", "1256", "1722"],
	 	language_ID : "3",
	 	id : "2865"
 	 }, { 
	 	name : "without",
	 	categories_IDs : ["3"],
	 	related_IDs : ["1263", "1264", "1723"],
	 	language_ID : "3",
	 	id : "2866"
 	 }, { 
	 	name : "know",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1747", "394", "411", "429", "446", "ugTZyT2iO2vq"],
	 	language_ID : "3",
	 	id : "2867"
 	 }, { 
	 	name : "be correct",
	 	categories_IDs : ["35"],
	 	related_IDs : ["1759", "390", "425"],
	 	language_ID : "3",
	 	id : "2868"
 	 }, { 
	 	name : "under",
	 	categories_IDs : ["33", "38"],
	 	related_IDs : ["1784", "1786", "3188", "491"],
	 	language_ID : "3",
	 	id : "2869"
 	 }, { 
	 	name : "over",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1781", "1783", "2872"],
	 	language_ID : "3",
	 	id : "2871"
 	 }, { 
	 	name : "above",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1781", "1783", "2871"],
	 	language_ID : "3",
	 	id : "2872"
 	 }, { 
	 	name : "Show me, how you do that.",
	 	categories_IDs : ["34"],
	 	related_IDs : ["1123", "82"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2873"
 	 }, { 
	 	name : "Where is the train station?",
	 	categories_IDs : ["34"],
	 	related_IDs : ["941", "942", "4BN0TINFgaDM"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2874"
 	 }, { 
	 	name : "Thank you.",
	 	categories_IDs : ["34"],
	 	related_IDs : ["1729", "934", "935", "936"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2875"
 	 }, { 
	 	name : "some",
	 	categories_IDs : ["33"],
	 	related_IDs : ["2155", "2156"],
	 	language_ID : "3",
	 	id : "2876"
 	 }, { 
	 	name : "likely",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1713", "183", "184"],
	 	language_ID : "3",
	 	id : "2877"
 	 }, { 
	 	name : "from",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1252", "1253", "1254", "1724", "3128"],
	 	language_ID : "3",
	 	id : "2878"
 	 }, { 
	 	name : "Det är bra, tack.",
	 	language_ID : "1",
	 	sentence : true,
	 	categories_IDs : ["34"],
	 	related_IDs : [],
	 	id : "2879"
 	 }, { 
	 	name : "Ja, ich kann schon mal die Tomaten waschen.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["40"],
	 	related_IDs : [],
	 	id : "2880"
 	 }, { 
	 	related_IDs : ["140", "703"],
	 	categories_IDs : ["40"],
	 	responses : [2880,2882],
	 	language_ID : "2",
	 	name : "Wollen wir einen Salat aus Tomaten und Gurken zu Mittag zubereiten?",
	 	sentence : true,
	 	id : "2881"
 	 }, { 
	 	name : "Find ich super. Wir können auch noch ein bisschen Feta hinzufügen!",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["40"],
	 	related_IDs : [],
	 	id : "2882"
 	 }, { 
	 	name : "Eine Kartoffelsuppe. Bringe ich Ihnen sofort.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["40"],
	 	related_IDs : [],
	 	id : "2883"
 	 }, { 
	 	categories_IDs : ["40"],
	 	related_IDs : ["143"],
	 	sentence : true,
	 	language_ID : "2",
	 	name : "Kann ich bitte eine Kartoffelsuppe haben?",
	 	responses : [2883,2885,2886],
	 	id : "2884"
 	 }, { 
	 	name : "Die Kartoffelsuppe haben wir heute leider nicht mehr. Kann ich Ihnen einen Möhrenauflauf anbieten?",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["40"],
	 	related_IDs : ["2951"],
	 	id : "2885"
 	 }, { 
	 	name : "Gerne. Möchten Sie noch etwas zu trinken?",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["40"],
	 	related_IDs : [],
	 	id : "2886"
 	 }, { 
	 	name : "Sie können den Zug über Kopenhagen nehmen. Das dauert etwa fünfzehn Stunden.",
	 	id : "2887",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["41"],
	 	related_IDs : []
 	 }, { 
	 	related_IDs : ["2962"],
	 	categories_IDs : ["41"],
	 	responses : [2887],
	 	language_ID : "2",
	 	name : "Wie komme ich von Berlin nach Stockholm?",
	 	sentence : true,
	 	id : "2888"
 	 }, { 
	 	name : "Ja, er ist im Fach vorne ganz unten.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["41"],
	 	related_IDs : [],
	 	id : "2889"
 	 }, { 
	 	related_IDs : ["2892"],
	 	categories_IDs : ["41"],
	 	responses : [2889,2891],
	 	language_ID : "2",
	 	name : "Hast du deinen Reisepass in den Rucksack eingepackt?",
	 	sentence : true,
	 	id : "2890"
 	 }, { 
	 	name : "Oh, danke für die Erinnerung, den hätte ich fast vergessen!",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["41"],
	 	related_IDs : [],
	 	id : "2891"
 	 }, { 
	 	categories_IDs : ["41"],
	 	related_IDs : ["2890", "vGdOuUzBgsNR"],
	 	language_ID : "2",
	 	name : "der Rucksack",
	 	id : "2892"
 	 }, { 
	 	categories_IDs : ["41", "54"],
	 	related_IDs : ["2894", "2939", "KZSpx1Rjk5yM"],
	 	language_ID : "2",
	 	name : "der Bus",
	 	id : "2893"
 	 }, { 
	 	name : "Am billigsten wäre es, mit dem Bus zu fahren.",
	 	related_IDs : ["2893", "tD4yqlaoo4p8"],
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["41", "54"],
	 	id : "2894"
 	 }, { 
	 	name : "Klar! Da freuen wir uns schon seit Tagen darauf.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["42", "46"],
	 	related_IDs : [],
	 	id : "2895"
 	 }, { 
	 	categories_IDs : ["42", "46"],
	 	related_IDs : ["1291", "1297", "882", "lan2bBZgKtvY"],
	 	language_ID : "2",
	 	name : "Wollt ihr nachher Fußball spielen?",
	 	sentence : true,
	 	responses : [2895,2897],
	 	id : "2896"
 	 }, { 
	 	name : "Vielleicht, ich rede mal mit den anderen.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["42", "46"],
	 	related_IDs : ["b6Gedd1tQgk1"],
	 	id : "2897"
 	 }, { 
	 	name : "Ich komme ein bisschen später heute.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["42"],
	 	related_IDs : ["yQyIwTeXnsHE"],
	 	id : "2898"
 	 }, { 
	 	categories_IDs : ["42"],
	 	related_IDs : ["705"],
	 	language_ID : "2",
	 	name : "Das Team trifft sich heute Nachmittag um 15 Uhr zum Aufwärmen in der Halle.",
	 	sentence : true,
	 	responses : [2898,2900],
	 	id : "2899"
 	 }, { 
	 	name : "Ich sag meinem Bruder Bescheid, danke.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["42"],
	 	related_IDs : [],
	 	id : "2900"
 	 }, { 
	 	name : "Noch hat die andere Manschaft eine Chance.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["42"],
	 	related_IDs : ["L99nfbw6YwHF"],
	 	id : "2901"
 	 }, { 
	 	categories_IDs : ["42"],
	 	related_IDs : ["2971"],
	 	language_ID : "2",
	 	name : "Die Manschaft aus London hat im Moment fünf Punkte Vorsprung.",
	 	sentence : true,
	 	responses : [2901],
	 	id : "2902"
 	 }, { 
	 	name : "Ich dich auch.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["43"],
	 	related_IDs : [],
	 	id : "2903"
 	 }, { 
	 	categories_IDs : ["43"],
	 	related_IDs : ["2956"],
	 	language_ID : "2",
	 	name : "Ich liebe dich von ganzem Herzen.",
	 	sentence : true,
	 	responses : [2903],
	 	id : "2904"
 	 }, { 
	 	name : "Wenigstens haben die Hände nichts abbekommen.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["44"],
	 	related_IDs : ["2965"],
	 	id : "2905"
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["2960"],
	 	language_ID : "2",
	 	name : "Mein linkes Knie tut mir seit dem Unfall ab und zu weh.",
	 	sentence : true,
	 	responses : [2905],
	 	id : "2906"
 	 }, { 
	 	name : "Ich muss noch für eine Prüfung lernen. Das schaffe ich leider nicht.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["43"],
	 	related_IDs : [],
	 	id : "2907"
 	 }, { 
	 	categories_IDs : ["43"],
	 	related_IDs : [],
	 	language_ID : "2",
	 	name : "Ich freue mich schon auf die Feier nächsten Freitag. Kommst du auch?",
	 	sentence : true,
	 	responses : [2907,2909],
	 	id : "2908"
 	 }, { 
	 	name : "Klar, wollen wir zusammen hinfahren?",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["43"],
	 	related_IDs : ["2958"],
	 	id : "2909"
 	 }, { 
	 	name : "Ja, mir hat er auch sehr gut getan, danke.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["43"],
	 	related_IDs : [],
	 	id : "2910"
 	 }, { 
	 	categories_IDs : ["43"],
	 	related_IDs : [],
	 	language_ID : "2",
	 	name : "Der Spaziergang durch den Wald war beruhigend.",
	 	sentence : true,
	 	responses : [2910],
	 	id : "2911"
 	 }, { 
	 	name : "Leider nein. Derzeit habe ich ziemlich viel vor abends.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["63"],
	 	related_IDs : [],
	 	id : "2912"
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["1572", "586", "689"],
	 	language_ID : "2",
	 	name : "Darf ich dich morgen abend zum Essen einladen?",
	 	sentence : true,
	 	responses : [2912,2914],
	 	id : "2913"
 	 }, { 
	 	name : "Sehr gern. Neunzehn Uhr?",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["63"],
	 	related_IDs : [],
	 	id : "2914"
 	 }, { 
	 	name : "Ja, mir tut unsere Freundschaft auch sehr gut!",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["43", "63"],
	 	related_IDs : [],
	 	id : "2915"
 	 }, { 
	 	categories_IDs : ["43", "63"],
	 	related_IDs : ["1480", "2955"],
	 	language_ID : "2",
	 	name : "Ich bin echt dankbar für unsere Freundschaft.",
	 	sentence : true,
	 	responses : [2915],
	 	id : "2916"
 	 }, { 
	 	name : "Hm, die dunkle Brille passt besser zu deiner Augenfarbe.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["45"],
	 	related_IDs : [],
	 	id : "2917"
 	 }, { 
	 	categories_IDs : ["45"],
	 	related_IDs : ["3014"],
	 	language_ID : "2",
	 	name : "Steht mir eher die dunkle oder die helle Brille? Was meinst du?",
	 	sentence : true,
	 	responses : [2917,2919],
	 	id : "2918"
 	 }, { 
	 	name : "Ich würde eine rote Brille ausprobieren, das passt besser zu deinem Gesicht.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["45"],
	 	related_IDs : ["2966"],
	 	id : "2919"
 	 }, { 
	 	name : "Dann beginnt der Wahlkampf wohl bald wieder.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["48"],
	 	related_IDs : [],
	 	id : "2920"
 	 }, { 
	 	categories_IDs : ["48"],
	 	related_IDs : ["2972"],
	 	language_ID : "2",
	 	name : "Die nächsten Parlamentswahlen finden in zwei Jahren statt.",
	 	sentence : true,
	 	responses : [2920],
	 	id : "2921"
 	 }, { 
	 	categories_IDs : ["48"],
	 	related_IDs : ["970", "986"],
	 	language_ID : "2",
	 	name : "Die französische Revolution von 1789 bis 1799 beeinflusste ganz Europa.",
	 	sentence : true,
	 	id : "2922"
 	 }, { 
	 	categories_IDs : [],
	 	related_IDs : ["1818", "2721"],
	 	language_ID : "2",
	 	name : "In Umfragen gibt es eine deutliche Mehrheit für Friedensgespräche.",
	 	sentence : true,
	 	id : "2923"
 	 }, { 
	 	categories_IDs : ["49"],
	 	related_IDs : [],
	 	language_ID : "2",
	 	name : "Soweit ich weiß, unterscheidet man im Buddhismus zwischen nötigem und unnötigem Leiden?",
	 	sentence : true,
	 	responses : [2924],
	 	id : "2925"
 	 }, { 
	 	categories_IDs : ["49"],
	 	related_IDs : ["2968"],
	 	language_ID : "2",
	 	sentence : true,
	 	name : "Das Judentum, das Christentum und der Islam haben eine gemeinsame Geschichte.",
	 	id : "2926"
 	 }, { 
	 	name : "Da hast du Recht. Es würde lange dauern, ihn zurückzuzahlen.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["51"],
	 	related_IDs : [],
	 	id : "2927"
 	 }, { 
	 	categories_IDs : ["51"],
	 	related_IDs : ["1931", "2974"],
	 	language_ID : "2",
	 	name : "Du solltest dir gut überlegen, ob du wirklich einen Kredit aufnehmen möchtest.",
	 	sentence : true,
	 	responses : [2927],
	 	id : "2928"
 	 }, { 
	 	name : "Ja, aber das heißt ja nicht, dass weniger Menschen religiös sind.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["49"],
	 	related_IDs : [],
	 	id : "2929"
 	 }, { 
	 	categories_IDs : ["49"],
	 	related_IDs : ["577", "595", "976"],
	 	language_ID : "2",
	 	name : "In Deutschland sind immer weniger Menschen offiziell Religionsmitglieder.",
	 	sentence : true,
	 	responses : [2929],
	 	id : "2930"
 	 }, { 
	 	name : "Okay, ich sollte spätestens morgen damit fertig sein.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["52"],
	 	related_IDs : [],
	 	id : "2931"
 	 }, { 
	 	categories_IDs : ["52", "61"],
	 	related_IDs : ["2964", "439"],
	 	language_ID : "2",
	 	name : "Schreib mir einfach eine email, sobald du den Entwurf geschrieben hast.",
	 	sentence : true,
	 	responses : [2931],
	 	id : "2932"
 	 }, { 
	 	name : "Das klingt irgendwie ungerecht.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["51"],
	 	related_IDs : ["2969"],
	 	id : "2933"
 	 }, { 
	 	categories_IDs : ["51"],
	 	related_IDs : ["1927", "1939", "610"],
	 	language_ID : "2",
	 	name : "Wer Geld mit Aktien verdient, zahlt oft weniger Steuern, als jemand der Geld mit Arbeit verdient.",
	 	sentence : true,
	 	responses : [2933],
	 	id : "2934"
 	 }, { 
	 	name : "Können wir das Treffen heute Nachmittag vorbereiten? Ich bin gerade noch anderweitig beschäftigt.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["52"],
	 	related_IDs : [],
	 	id : "2935"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["1255", "447", "709"],
	 	language_ID : "2",
	 	name : "Ich würde gern mit dir den Plan für das Treffen Dienstag absprechen.",
	 	sentence : true,
	 	responses : [2935],
	 	id : "2936"
 	 }, { 
	 	name : "Danke für ihre Hilfe beim Aufhängen der Bilder.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["53"],
	 	related_IDs : ["2967", "zzw3VeF3wO23"],
	 	id : "2937"
 	 }, { 
	 	categories_IDs : ["53"],
	 	related_IDs : ["2952"],
	 	language_ID : "2",
	 	name : "Die Foto-Ausstellung ist ein voller Erfolg!",
	 	sentence : true,
	 	responses : [2937],
	 	id : "2938"
 	 }, { 
	 	categories_IDs : ["54"],
	 	related_IDs : ["207", "2893", "694", "947", "951"],
	 	language_ID : "2",
	 	name : "Mit dem Fahrrad bist du zehn Minuten schneller am Bahnhof als mit dem Bus.",
	 	sentence : true,
	 	id : "2939"
 	 }, { 
	 	name : "Dann bist du wahrscheinlich noch erschöpft, oder?",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["55"],
	 	related_IDs : ["2970"],
	 	id : "2940"
 	 }, { 
	 	categories_IDs : ["39", "55"],
	 	related_IDs : ["2959", "691"],
	 	language_ID : "2",
	 	name : "Letzte Woche hatte ich drei Prüfungen. In Mathe, Physik und Deutsch.",
	 	sentence : true,
	 	responses : [2940,2942],
	 	id : "2941"
 	 }, { 
	 	name : "Ich muss die Deutschprüfung wiederholen, weil ich da krank war.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["44", "55"],
	 	related_IDs : ["2957"],
	 	id : "2942"
 	 }, { 
	 	categories_IDs : ["61"],
	 	related_IDs : [],
	 	language_ID : "2",
	 	name : "Zwei Kollegen werden am Dienstag eine Weiterbildung über Zeitmanagement durchführen.",
	 	sentence : true,
	 	id : "2943"
 	 }, { 
	 	name : "Da verdient man auch schon Geld, oder?",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["55"],
	 	related_IDs : [],
	 	id : "2944"
 	 }, { 
	 	categories_IDs : ["55", "61"],
	 	related_IDs : ["1970", "2953"],
	 	language_ID : "2",
	 	name : "Die Ausbildung in Deutschland beinhaltet schulische und praktische Anteile.",
	 	sentence : true,
	 	responses : [2944],
	 	id : "2945"
 	 }, { 
	 	name : "Die Treppe wäre wohl gesünder für mich.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["19"],
	 	related_IDs : [],
	 	id : "2946"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["2011", "2012", "2015"],
	 	language_ID : "2",
	 	name : "Im dritten Stock ist eine Cafeteria. Du kannst die Treppe oder den Fahrstuhl nehmen.",
	 	sentence : true,
	 	responses : [2948],
	 	id : "2947"
 	 }, { 
	 	name : "Die Treppe zu nehmen wäre wohl gesünder für mich.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["19"],
	 	related_IDs : [],
	 	id : "2948"
 	 }, { 
	 	name : "Einige Dächer sind wohl auch beschädigt worden.",
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : ["19", "2757", "56"],
	 	related_IDs : ["2973"],
	 	id : "2949"
 	 }, { 
	 	categories_IDs : ["2757", "56"],
	 	related_IDs : ["1983", "2241", "2954", "691"],
	 	language_ID : "2",
	 	name : "Der Sturm letzte Woche hat einige Bäume herausgerissen.",
	 	sentence : true,
	 	responses : [2949],
	 	id : "2950"
 	 }, { 
	 	name : "We are out of potatoe soup for today. May I offer you a carrot casserole?",
	 	categories_IDs : [],
	 	related_IDs : ["2885", "Y0xgGfYCeDok"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2951"
 	 }, { 
	 	name : "The photo exhibition is a great success.",
	 	categories_IDs : [],
	 	related_IDs : ["2938"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2952"
 	 }, { 
	 	name : "Apprenticeship in Germany contains school and practice parts.",
	 	categories_IDs : ["61"],
	 	related_IDs : ["2945"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2953"
 	 }, { 
	 	name : "The storm last week pulled out some trees.",
	 	categories_IDs : [],
	 	related_IDs : ["2950"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2954"
 	 }, { 
	 	name : "I am really grateful for our friendship.",
	 	categories_IDs : ["43", "63"],
	 	related_IDs : ["2916"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2955"
 	 }, { 
	 	name : "I love you with all my heart.",
	 	categories_IDs : [],
	 	related_IDs : ["2904", "36rnzzC0aCq9"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2956"
 	 }, { 
	 	name : "I have to repeat the German exam because I was sick.",
	 	categories_IDs : ["44", "55"],
	 	related_IDs : ["2942"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2957"
 	 }, { 
	 	name : "Sure, how about we drive there together?",
	 	categories_IDs : [],
	 	related_IDs : ["2909", "479"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2958"
 	 }, { 
	 	name : "Last week I had three exams. Math, physics and German.",
	 	categories_IDs : ["39", "55"],
	 	related_IDs : ["2941"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2959"
 	 }, { 
	 	name : "Since the accident my left knee is hurting from time to time.",
	 	categories_IDs : [],
	 	related_IDs : ["2906"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2960"
 	 }, { 
	 	name : "When is the post office open?",
	 	categories_IDs : [],
	 	related_IDs : ["943", "944"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2961"
 	 }, { 
	 	name : "How do I get from Berlin to Stockholm?",
	 	categories_IDs : ["41"],
	 	related_IDs : ["2888"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2962"
 	 }, { 
	 	name : "How are you?",
	 	categories_IDs : [],
	 	related_IDs : ["926", "927", "928", "929"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2963"
 	 }, { 
	 	name : "Just write me an email when you are done with the draft.",
	 	categories_IDs : ["34", "52", "61"],
	 	related_IDs : ["2932", "472"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2964",
	 	tags : []
 	 }, { 
	 	name : "At least the hands were left unharmed.",
	 	categories_IDs : ["44"],
	 	related_IDs : ["2905"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2965"
 	 }, { 
	 	name : "I'd try the red glasses, they might suit your face better.",
	 	categories_IDs : ["45"],
	 	related_IDs : ["2919"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2966"
 	 }, { 
	 	name : "Thank you for your help with hanging the pictures.",
	 	categories_IDs : [],
	 	related_IDs : ["2937", "tZzOvlVsRO9t"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2967"
 	 }, { 
	 	name : "Judaism, Christianity and Islam share a common history.",
	 	categories_IDs : [],
	 	related_IDs : ["2926"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2968"
 	 }, { 
	 	name : "Somehow that sounds unfair.",
	 	categories_IDs : [],
	 	related_IDs : ["2933"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2969"
 	 }, { 
	 	name : "Then you are probably still going to be exhausted, no?",
	 	categories_IDs : [],
	 	related_IDs : ["2940"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2970"
 	 }, { 
	 	name : "The team from London is in the lead by five points.",
	 	categories_IDs : [],
	 	related_IDs : ["2902"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2971"
 	 }, { 
	 	name : "The next parliamentary elections will take place in two years.",
	 	categories_IDs : [],
	 	related_IDs : ["2921"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2972"
 	 }, { 
	 	name : "A few roofs seem to have been damaged as well.",
	 	categories_IDs : ["19", "2757"],
	 	related_IDs : ["2949"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2973"
 	 }, { 
	 	name : "You should think about borrowing money very carefully.",
	 	categories_IDs : [],
	 	related_IDs : ["2928"],
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2974",
	 	tags : []
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["1478", "2976", "2977", "2978", "2979"],
	 	language_ID : "1",
	 	name : "ett bråk",
	 	id : "2975"
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["1478", "1499", "1539", "2975", "2978", "2979"],
	 	language_ID : "1",
	 	name : "en tvist",
	 	id : "2976"
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["1478", "1499", "1539", "2975", "2979"],
	 	language_ID : "3",
	 	name : "the fight",
	 	id : "2977"
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["1499", "2975", "2976", "2979"],
	 	language_ID : "5",
	 	name : "la pelea",
	 	id : "2978"
 	 }, { 
	 	categories_IDs : ["52", "63"],
	 	related_IDs : ["1913", "2101", "2148", "1478", "1499", "1539", "2975", "2976", "2977", "2978"],
	 	name : "the argument",
	 	language_ID : "3",
	 	id : "2979"
 	 }, { 
	 	categories_IDs : ["48"],
	 	related_IDs : ["1820", "1876", "1893", "3222"],
	 	name : "elect",
	 	language_ID : "3",
	 	id : "2980",
	 	tags : []
 	 }, { 
	 	related_IDs : ["2982", "2983", "2984"],
	 	categories_IDs : ["45"],
	 	responses : [],
	 	language_ID : "2",
	 	name : "der Spiegel",
	 	id : "2981"
 	 }, { 
	 	categories_IDs : ["45"],
	 	related_IDs : ["2981", "2983", "2984"],
	 	language_ID : "3",
	 	name : "the mirror",
	 	id : "2982"
 	 }, { 
	 	related_IDs : ["2981", "2982", "2984"],
	 	categories_IDs : ["45"],
	 	responses : [],
	 	language_ID : "5",
	 	name : "el espejo",
	 	id : "2983"
 	 }, { 
	 	related_IDs : ["2981", "2982", "2983"],
	 	categories_IDs : ["45"],
	 	responses : [],
	 	language_ID : "1",
	 	name : "en spegel",
	 	id : "2984"
 	 }, { 
	 	related_IDs : ["2986", "2987", "2988"],
	 	categories_IDs : ["59"],
	 	responses : [],
	 	language_ID : "2",
	 	name : "der Diamant",
	 	id : "2985"
 	 }, { 
	 	categories_IDs : ["59"],
	 	related_IDs : ["2985", "2987", "2988"],
	 	language_ID : "3",
	 	name : "the diamond",
	 	id : "2986"
 	 }, { 
	 	categories_IDs : ["59"],
	 	related_IDs : ["2985", "2986", "2988"],
	 	language_ID : "1",
	 	name : "en diamant",
	 	id : "2987"
 	 }, { 
	 	categories_IDs : ["59"],
	 	related_IDs : ["2985", "2986", "2987"],
	 	language_ID : "1",
	 	name : "en diamante",
	 	id : "2988"
 	 }, { 
	 	categories_IDs : ["59"],
	 	related_IDs : ["2990", "2991", "2992"],
	 	language_ID : "2",
	 	name : "der Edelstein",
	 	id : "2989"
 	 }, { 
	 	categories_IDs : ["59"],
	 	related_IDs : ["2989", "2991", "2992"],
	 	language_ID : "3",
	 	name : "the gem",
	 	id : "2990"
 	 }, { 
	 	categories_IDs : ["59"],
	 	related_IDs : ["2989", "2990", "2992"],
	 	language_ID : "5",
	 	name : "la gema",
	 	id : "2991"
 	 }, { 
	 	categories_IDs : ["59"],
	 	related_IDs : ["2989", "2990", "2991"],
	 	language_ID : "1",
	 	name : "en pärla",
	 	id : "2992"
 	 }, { 
	 	categories_IDs : ["43"],
	 	related_IDs : ["1347", "1363", "1379", "1395"],
	 	name : "munter",
	 	language_ID : "1",
	 	id : "2993"
 	 }, { 
	 	categories_IDs : ["45"],
	 	related_IDs : ["1634", "1643", "1654", "1664", "3168"],
	 	language_ID : "3",
	 	name : "skinny",
	 	id : "2994"
 	 }, { 
	 	categories_IDs : ["45"],
	 	related_IDs : ["1632", "1641", "1652", "1662", "3166"],
	 	name : "trång",
	 	language_ID : "1",
	 	id : "2995"
 	 }, { 
	 	categories_IDs : ["33"],
	 	related_IDs : ["106", "1706", "69", "87"],
	 	language_ID : "2",
	 	name : "wieso",
	 	id : "2996"
 	 }, { 
	 	categories_IDs : ["45"],
	 	related_IDs : ["1672", "1673", "1674", "1675", "3174"],
	 	language_ID : "3",
	 	name : "fair",
	 	id : "2997"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["1898", "2563", "2607", "2680"],
	 	language_ID : "3",
	 	name : "talk",
	 	id : "2998"
 	 }, { 
	 	categories_IDs : ["43"],
	 	related_IDs : ["1351", "1367", "1383", "1400", "1401", "3144", "3145"],
	 	name : "stilla",
	 	language_ID : "1",
	 	id : "2999"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["1902", "2557", "2605", "2678"],
	 	language_ID : "1",
	 	name : "ropa",
	 	id : "3000"
 	 }, { 
	 	categories_IDs : ["45"],
	 	related_IDs : ["1638", "1648", "1658", "1668", "3002"],
	 	language_ID : "3",
	 	name : "tended",
	 	id : "3001"
 	 }, { 
	 	categories_IDs : ["45"],
	 	related_IDs : ["1638", "1648", "1658", "1668", "3001"],
	 	language_ID : "1",
	 	name : "välskött",
	 	id : "3002"
 	 }, { 
	 	categories_IDs : ["39"],
	 	related_IDs : ["922", "924", "925", "738", "519"],
	 	name : "innan",
	 	language_ID : "1",
	 	id : "3003"
 	 }, { 
	 	categories_IDs : ["45"],
	 	related_IDs : ["1639", "1649", "1659", "1669"],
	 	language_ID : "3",
	 	name : "glossy",
	 	id : "3004"
 	 }, { 
	 	categories_IDs : ["45"],
	 	related_IDs : ["1647", "1651", "1661", "1671", "3006", "3172"],
	 	language_ID : "3",
	 	name : "coarse",
	 	id : "3005"
 	 }, { 
	 	categories_IDs : ["45"],
	 	related_IDs : ["1647", "1651", "1661", "1671", "3005", "3172"],
	 	language_ID : "1",
	 	name : "skrovlig",
	 	id : "3006"
 	 }, { 
	 	categories_IDs : ["42"],
	 	related_IDs : ["2570", "2571", "2572", "2573"],
	 	language_ID : "5",
	 	name : "lanzar",
	 	id : "3007"
 	 }, { 
	 	categories_IDs : ["56"],
	 	related_IDs : ["1990", "2397", "2703", "2742"],
	 	language_ID : "1",
	 	name : "vattna",
	 	id : "3008",
	 	tag : "verb"
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["1484", "1506", "1525", "1544"],
	 	language_ID : "1",
	 	name : "undgå",
	 	id : "3009"
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["1482", "1502", "1523", "1542"],
	 	language_ID : "1",
	 	name : "krama",
	 	id : "3010"
 	 }, { 
	 	categories_IDs : ["42"],
	 	related_IDs : ["1314", "1315", "1331", "1345"],
	 	language_ID : "5",
	 	name : "trotar",
	 	id : "3011"
 	 }, { 
	 	categories_IDs : ["38"],
	 	related_IDs : ["1770", "487", "507", "529"],
	 	language_ID : "5",
	 	name : "recto",
	 	id : "3012"
 	 }, { 
	 	categories_IDs : ["56"],
	 	related_IDs : ["2749", "2750", "2755", "2810"],
	 	language_ID : "3",
	 	name : "dry up",
	 	id : "3013",
	 	tag : "verb"
 	 }, { 
	 	related_IDs : ["2918", "3015", "3016", "3017"],
	 	categories_IDs : ["45"],
	 	responses : [],
	 	language_ID : "2",
	 	name : "die Brille",
	 	id : "3014"
 	 }, { 
	 	categories_IDs : ["45"],
	 	related_IDs : ["3014", "3016", "3017"],
	 	language_ID : "3",
	 	name : "the glasses",
	 	id : "3015"
 	 }, { 
	 	categories_IDs : ["45"],
	 	related_IDs : ["3014", "3015", "3017"],
	 	language_ID : "5",
	 	name : "las gafas",
	 	tag : "siempre plural",
	 	id : "3016"
 	 }, { 
	 	categories_IDs : ["45"],
	 	related_IDs : ["3014", "3015", "3016"],
	 	language_ID : "1",
	 	name : "glasögon",
	 	id : "3017"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["3019", "3020", "3021"],
	 	language_ID : "2",
	 	name : "sagen",
	 	id : "3018"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["3018", "3020", "3021"],
	 	sentence : false,
	 	language_ID : "3",
	 	name : "say",
	 	id : "3019"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["3018", "3019", "3021"],
	 	language_ID : "5",
	 	name : "decir",
	 	id : "3020"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["3018", "3019", "3020"],
	 	language_ID : "1",
	 	name : "säga",
	 	id : "3021",
	 	tags : [],
	 	formsSecondary : ["sade", "sagt"],
	 	formsSecondaryNames : ["Preteritum", "Supinum"]
 	 }, { 
	 	related_IDs : ["3023", "3024", "3025"],
	 	categories_IDs : ["44", "57"],
	 	responses : [],
	 	language_ID : "2",
	 	name : "die Geburt",
	 	id : "3022"
 	 }, { 
	 	categories_IDs : ["44", "57"],
	 	related_IDs : ["3022", "3024", "3025"],
	 	language_ID : "3",
	 	name : "the birth",
	 	id : "3023"
 	 }, { 
	 	categories_IDs : ["44", "57"],
	 	related_IDs : ["3022", "3023", "3025"],
	 	language_ID : "5",
	 	name : "el nacimiento",
	 	id : "3024"
 	 }, { 
	 	categories_IDs : ["44", "57"],
	 	related_IDs : ["3022", "3023", "3024"],
	 	language_ID : "1",
	 	name : "en födelse",
	 	id : "3025"
 	 }, { 
	 	categories_IDs : ["40", "57"],
	 	related_IDs : ["3027", "3028", "3029"],
	 	language_ID : "2",
	 	name : "das Ei",
	 	id : "3026"
 	 }, { 
	 	categories_IDs : ["40", "57"],
	 	related_IDs : ["3026", "3028", "3029"],
	 	language_ID : "3",
	 	name : "the egg",
	 	id : "3027"
 	 }, { 
	 	categories_IDs : ["40", "57"],
	 	related_IDs : ["3026", "3027", "3029"],
	 	language_ID : "5",
	 	name : "el huevo",
	 	id : "3028"
 	 }, { 
	 	categories_IDs : ["40", "57"],
	 	related_IDs : ["3026", "3027", "3028"],
	 	language_ID : "1",
	 	name : "ett ägg",
	 	id : "3029"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["3031", "3032", "3033"],
	 	language_ID : "2",
	 	name : "das Missverständnis",
	 	id : "3030"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["3030", "3032", "3033"],
	 	language_ID : "3",
	 	name : "the misunderstanding",
	 	id : "3031"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["3030", "3031", "3033"],
	 	language_ID : "5",
	 	name : "el malentendido",
	 	id : "3032"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["3030", "3031", "3032"],
	 	language_ID : "1",
	 	name : "ett missförstånd",
	 	id : "3033"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["3035", "3036", "3037"],
	 	language_ID : "2",
	 	name : "ehrlich",
	 	id : "3034"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["3034", "3036", "3037"],
	 	language_ID : "3",
	 	name : "honest",
	 	id : "3035"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["3034", "3035", "3037"],
	 	language_ID : "5",
	 	name : "honestamente",
	 	id : "3036"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["3034", "3035", "3036"],
	 	language_ID : "1",
	 	name : "ärligt",
	 	id : "3037"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["3039", "3040", "3041"],
	 	language_ID : "2",
	 	name : "die Lüge",
	 	id : "3038"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["3038", "3040", "3041"],
	 	language_ID : "3",
	 	name : "the lie",
	 	id : "3039"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["3038", "3039", "3041"],
	 	language_ID : "5",
	 	name : "la mentira",
	 	id : "3040"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["3038", "3039", "3040"],
	 	language_ID : "1",
	 	name : "en lögn",
	 	id : "3041"
 	 }, { 
	 	categories_IDs : ["54"],
	 	related_IDs : ["3043", "3044", "3045"],
	 	language_ID : "2",
	 	name : "anhalten",
	 	id : "3042",
	 	tags : []
 	 }, { 
	 	categories_IDs : ["54"],
	 	related_IDs : ["3042", "3044", "3045"],
	 	name : "stop",
	 	id : "3043",
	 	language_ID : "3"
 	 }, { 
	 	categories_IDs : ["54"],
	 	related_IDs : ["3042", "3043", "3045"],
	 	language_ID : "5",
	 	name : "parar",
	 	id : "3044"
 	 }, { 
	 	categories_IDs : ["35", "54"],
	 	related_IDs : ["3042", "3043", "3044", "440"],
	 	language_ID : "1",
	 	name : "stanna",
	 	id : "3045",
	 	tags : []
 	 }, { 
	 	categories_IDs : ["2756"],
	 	related_IDs : ["3047", "3048", "3049"],
	 	language_ID : "2",
	 	name : "der Meeresboden",
	 	id : "3046"
 	 }, { 
	 	categories_IDs : ["2756"],
	 	related_IDs : ["3046", "3048", "3049"],
	 	language_ID : "3",
	 	name : "the seabed",
	 	id : "3047"
 	 }, { 
	 	categories_IDs : ["2756"],
	 	related_IDs : ["3046", "3047", "3049"],
	 	language_ID : "1",
	 	name : "en havsbotten",
	 	id : "3048"
 	 }, { 
	 	categories_IDs : ["2756"],
	 	related_IDs : ["3046", "3047", "3048"],
	 	language_ID : "5",
	 	name : "el fondo del mar",
	 	id : "3049"
 	 }, { 
	 	categories_IDs : ["51"],
	 	related_IDs : ["1931", "2175", "2189", "2657"],
	 	language_ID : "3",
	 	name : "the loan",
	 	id : "3050"
 	 }, { 
	 	categories_IDs : ["58"],
	 	related_IDs : ["2310", "2321", "2713", "2800"],
	 	name : "the fungus",
	 	language_ID : "3",
	 	id : "3051"
 	 }, { 
	 	categories_IDs : ["44", "58"],
	 	related_IDs : ["2314", "2315", "2711", "2805"],
	 	language_ID : "3",
	 	name : "contract s.th.",
	 	id : "3052"
 	 }, { 
	 	categories_IDs : ["58"],
	 	related_IDs : ["2006", "2322", "2709", "2803"],
	 	language_ID : "3",
	 	name : "the germ",
	 	id : "3053"
 	 }, { 
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2034", "2259", "2281", "2292"],
	 	language_ID : "5",
	 	name : "la gruta",
	 	id : "3054"
 	 }, { 
	 	categories_IDs : ["2756"],
	 	related_IDs : ["2039", "2248", "2270", "2291"],
	 	language_ID : "1",
	 	name : "en glaciär",
	 	id : "3055"
 	 }, { 
	 	related_IDs : ["3057", "3058"],
	 	categories_IDs : ["59"],
	 	responses : [],
	 	language_ID : "2",
	 	name : "das Feuer",
	 	id : "3056"
 	 }, { 
	 	categories_IDs : ["59"],
	 	related_IDs : ["1190", "3056", "3058"],
	 	language_ID : "3",
	 	name : "the fire",
	 	id : "3057"
 	 }, { 
	 	categories_IDs : ["59"],
	 	related_IDs : ["3056", "3057"],
	 	language_ID : "5",
	 	name : "el fuego",
	 	id : "3058"
 	 }, { 
	 	categories_IDs : ["59"],
	 	related_IDs : ["3060"],
	 	language_ID : "2",
	 	name : "das Eisen",
	 	id : "3059"
 	 }, { 
	 	categories_IDs : ["59"],
	 	related_IDs : ["3059"],
	 	language_ID : "3",
	 	name : "the iron",
	 	id : "3060"
 	 }, { 
	 	name : "die Königin",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["48"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1825"],
	 	related_IDs : ["3062", "3063", "3064"],
	 	id : "3061",
	 	tags : []
 	 }, { 
	 	name : "the queen",
	 	formPrimaryName : "female",
	 	categories_IDs : ["48"],
	 	language_ID : "3",
	 	formsPrimary_IDs : ["1832"],
	 	related_IDs : ["3061", "3063", "3064"],
	 	id : "3062",
	 	tags : []
 	 }, { 
	 	name : "en drottning",
	 	formPrimaryName : "kvinnlig",
	 	categories_IDs : ["48"],
	 	language_ID : "1",
	 	formsPrimary_IDs : ["1881"],
	 	related_IDs : ["3061", "3062", "3064"],
	 	id : "3063",
	 	tags : []
 	 }, { 
	 	name : "la reina",
	 	categories_IDs : ["48"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1864"],
	 	related_IDs : ["3061", "3062", "3063", "3064"],
	 	id : "3064",
	 	tags : []
 	 }, { 
	 	related_IDs : ["3066"],
	 	categories_IDs : ["51"],
	 	responses_IDs : [],
	 	tags : [],
	 	language_ID : "1",
	 	name : "ett företag",
	 	id : "3065"
 	 }, { 
	 	related_IDs : ["3067", "3068", "3069"],
	 	categories_IDs : ["51"],
	 	responses_IDs : [],
	 	tags : [],
	 	language_ID : "2",
	 	name : "die Firma",
	 	id : "3066"
 	 }, { 
	 	categories_IDs : ["51"],
	 	related_IDs : ["3066", "3068", "3069"],
	 	language_ID : "1",
	 	name : "ett företag",
	 	id : "3067"
 	 }, { 
	 	categories_IDs : ["51"],
	 	related_IDs : ["3066", "3067", "3069"],
	 	language_ID : "3",
	 	name : "the company",
	 	id : "3068"
 	 }, { 
	 	categories_IDs : ["51"],
	 	related_IDs : ["3066", "3067", "3068"],
	 	language_ID : "5",
	 	name : "la empresa",
	 	id : "3069"
 	 }, { 
	 	name : "Jag heter Anton.",
	 	language_ID : "1",
	 	sentence : true,
	 	categories_IDs : ["34"],
	 	related_IDs : ["3072", "3082", "3083"],
	 	id : "3070"
 	 }, { 
	 	name : "Ich heiße Anton.",
	 	language_ID : "2",
	 	categories_IDs : ["34"],
	 	sentence : true,
	 	related_IDs : ["3070", "3082", "3083"],
	 	id : "3072",
	 	tags : []
 	 }, { 
	 	name : "Hi!",
	 	language_ID : "3",
	 	categories_IDs : ["34"],
	 	sentence : true,
	 	related_IDs : ["931", "932", "933"],
	 	id : "3073"
 	 }, { 
	 	name : "Bye.",
	 	language_ID : "3",
	 	categories_IDs : ["34"],
	 	sentence : true,
	 	related_IDs : ["937", "938"],
	 	id : "3074"
 	 }, { 
	 	name : "Is there a pharmacy here?",
	 	language_ID : "3",
	 	categories_IDs : ["34"],
	 	sentence : true,
	 	related_IDs : ["939", "940"],
	 	id : "3075"
 	 }, { 
	 	name : "Excuse me, what is the name of this street?",
	 	language_ID : "3",
	 	categories_IDs : ["34"],
	 	sentence : true,
	 	related_IDs : ["945", "946"],
	 	id : "3076"
 	 }, { 
	 	name : "The more the merrier.",
	 	language_ID : "3",
	 	categories_IDs : ["34"],
	 	sentence : true,
	 	related_IDs : ["1125", "1127"],
	 	id : "3078"
 	 }, { 
	 	name : "the metal",
	 	language_ID : "3",
	 	categories_IDs : ["59"],
	 	related_IDs : ["2010", "2266", "2294"],
	 	id : "3080"
 	 }, { 
	 	name : "customs",
	 	language_ID : "3",
	 	categories_IDs : ["51"],
	 	related_IDs : ["1934", "2185", "2660"],
	 	id : "3081"
 	 }, { 
	 	name : "My name is Anton.",
	 	language_ID : "3",
	 	categories_IDs : ["34"],
	 	sentence : true,
	 	related_IDs : ["3070", "3072", "3083"],
	 	id : "3082",
	 	tags : []
 	 }, { 
	 	name : "Me llamo Anton.",
	 	language_ID : "5",
	 	categories_IDs : ["34"],
	 	sentence : true,
	 	related_IDs : ["3070", "3072", "3082"],
	 	id : "3083"
 	 }, { 
	 	name : "Wir haben gestern frisches Gemüse auf dem Markt gekauft.",
	 	language_ID : "2",
	 	categories_IDs : ["34", "40", "51"],
	 	sentence : true,
	 	related_IDs : ["2816", "2817", "2818"],
	 	id : "3084"
 	 }, { 
	 	related_IDs : ["1809", "3086", "3197", "337", "338", "355"],
	 	categories_IDs : ["60"],
	 	responses_IDs : [],
	 	tags : [],
	 	language_ID : "1",
	 	name : "fin",
	 	id : "3085"
 	 }, { 
	 	categories_IDs : ["60"],
	 	related_IDs : ["1809", "3085", "3197", "337", "338", "355"],
	 	language_ID : "1",
	 	name : "skön",
	 	id : "3086"
 	 }, { 
	 	categories_IDs : ["60"],
	 	related_IDs : ["1799", "317", "318", "345"],
	 	language_ID : "1",
	 	name : "färggrann",
	 	id : "3087"
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["1492", "1514", "1532", "1551"],
	 	name : "en moster",
	 	language_ID : "1",
	 	id : "3088",
	 	formPrimaryName : "syster till mor"
 	 }, { 
	 	name : "en moster",
	 	formPrimaryName : "syster till mor",
	 	categories_IDs : ["63"],
	 	language_ID : "1",
	 	formsPrimary_IDs : ["1514"],
	 	related_IDs : ["1492", "1532", "1551"],
	 	id : "3089",
	 	tags : []
 	 }, { 
	 	name : "en morbror",
	 	formPrimaryName : "bror till modern",
	 	categories_IDs : ["63"],
	 	language_ID : "1",
	 	formsPrimary_IDs : ["1512"],
	 	related_IDs : ["1491", "1531", "1550"],
	 	id : "3090",
	 	tags : []
 	 }, { 
	 	categories_IDs : ["38"],
	 	related_IDs : ["500", "520", "542", "3187"],
	 	name : "vid",
	 	language_ID : "1",
	 	id : "3091"
 	 }, { 
	 	categories_IDs : ["35"],
	 	related_IDs : ["3093", "3094", "3096", "384"],
	 	language_ID : "2",
	 	name : "bekommen",
	 	id : "3092"
 	 }, { 
	 	name : "Könnte ich einen Salat bekommen?",
	 	related_IDs : ["3092", "3095"],
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : [],
	 	id : "3093"
 	 }, { 
	 	categories_IDs : ["35"],
	 	related_IDs : ["3092", "3095", "3096", "384"],
	 	language_ID : "3",
	 	name : "get",
	 	id : "3094"
 	 }, { 
	 	name : "May I get a salad?",
	 	related_IDs : ["3094", "3093"],
	 	language_ID : "3",
	 	sentence : true,
	 	categories_IDs : [],
	 	id : "3095"
 	 }, { 
	 	categories_IDs : ["35"],
	 	related_IDs : ["3092", "3094", "3097", "384"],
	 	language_ID : "5",
	 	name : "conseguir",
	 	id : "3096"
 	 }, { 
	 	name : "¿Puedo conseguir una ensalada?",
	 	related_IDs : ["3096"],
	 	language_ID : "5",
	 	sentence : true,
	 	categories_IDs : [],
	 	id : "3097"
 	 }, { 
	 	name : "Kan jag få en sallad?",
	 	related_IDs : ["384"],
	 	language_ID : "1",
	 	sentence : true,
	 	categories_IDs : [],
	 	id : "3098"
 	 }, { 
	 	name : "Darf ich mir das Buch zwei Tage ausleihen?",
	 	related_IDs : ["419"],
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : [],
	 	id : "3099"
 	 }, { 
	 	name : "Sie dürfen hier nicht durch!",
	 	related_IDs : ["419"],
	 	language_ID : "2",
	 	sentence : true,
	 	categories_IDs : [],
	 	id : "3100"
 	 }, { 
	 	categories_IDs : ["35"],
	 	related_IDs : ["384", "419", "484"],
	 	language_ID : "5",
	 	name : "permitir",
	 	id : "3101"
 	 }, { 
	 	name : "die Kundin",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1146"],
	 	related_IDs : ["1181", "3130"],
	 	id : "3102",
	 	tags : []
 	 }, { 
	 	name : "die Klientin",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1147"],
	 	related_IDs : ["1181", "3130"],
	 	id : "3103",
	 	tags : []
 	 }, { 
	 	name : "die Chefin",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1148"],
	 	related_IDs : ["1276"],
	 	id : "3104"
 	 }, { 
	 	name : "la ingeniera",
	 	formPrimaryName : "femenino",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1277"],
	 	related_IDs : ["1183", "1222", "3106"],
	 	id : "3105",
	 	tags : []
 	 }, { 
	 	name : "die Ingenieurin",
	 	id : "3106",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1149"],
	 	related_IDs : ["1183", "1222", "3105"],
	 	tags : []
 	 }, { 
	 	name : "die Psychologin",
	 	id : "3107",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1150"],
	 	related_IDs : ["1184", "1223", "3108"],
	 	tags : []
 	 }, { 
	 	name : "la psicóloga",
	 	id : "3108",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1278"],
	 	related_IDs : ["1184", "1223", "3107"],
	 	tags : []
 	 }, { 
	 	name : "der LKW-Fahrer",
	 	id : "3109",
	 	formPrimaryName : "männlich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1151"],
	 	related_IDs : ["1185", "1224", "3132"],
	 	tags : []
 	 }, { 
	 	name : "die Ärztin",
	 	id : "3110",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1152"],
	 	related_IDs : ["1186", "1187", "1225", "1280", "3133"]
 	 }, { 
	 	name : "die Arbeiterin",
	 	id : "3111",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1153"],
	 	related_IDs : ["1188", "1226", "1281", "3134"]
 	 }, { 
	 	name : "die Feuerwehrfrau",
	 	id : "3112",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1155"],
	 	related_IDs : ["1190", "1227", "1283", "3124", "3136"]
 	 }, { 
	 	name : "der Polizist",
	 	id : "3113",
	 	formPrimaryName : "männlich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1156"],
	 	related_IDs : ["1191", "1228", "1284", "3125", "3126", "3137"]
 	 }, { 
	 	name : "der Mechaniker",
	 	id : "3114",
	 	formPrimaryName : "männlich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1157"],
	 	related_IDs : ["1192", "1229", "1854", "3215"],
	 	tags : []
 	 }, { 
	 	name : "die Altenpflegerin",
	 	id : "3115",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1158"],
	 	related_IDs : ["1193", "1847", "3209"]
 	 }, { 
	 	name : "der Anwalt",
	 	id : "3116",
	 	formPrimaryName : "männlich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1159"],
	 	related_IDs : ["1194", "1230", "1848", "3210"]
 	 }, { 
	 	name : "der Priester",
	 	id : "3117",
	 	formPrimaryName : "männlich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1160"],
	 	related_IDs : ["1195", "1231", "1856", "3216"]
 	 }, { 
	 	name : "der Journalist",
	 	id : "3118",
	 	formPrimaryName : "männlich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1161"],
	 	related_IDs : ["1196", "1232", "1851", "3213"]
 	 }, { 
	 	name : "die Taxifahrerin",
	 	id : "3119",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1162"],
	 	related_IDs : ["1197", "1233", "1858", "3218"]
 	 }, { 
	 	name : "der Politiker",
	 	id : "3120",
	 	formPrimaryName : "männlich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1163"],
	 	related_IDs : ["1198", "1234", "1857", "3217"]
 	 }, { 
	 	name : "die Besucherin",
	 	id : "3121",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1165"],
	 	related_IDs : ["1200", "1236", "1850", "3212"]
 	 }, { 
	 	name : "die Krankenpflegerin",
	 	id : "3122",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1166"],
	 	related_IDs : ["1201", "1237", "1853", "3127", "3214"]
 	 }, { 
	 	name : "der Beamte",
	 	id : "3123",
	 	formPrimaryName : "männlich",
	 	categories_IDs : ["61"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1170"],
	 	related_IDs : ["1205", "1241", "1849", "3211"]
 	 }, { 
	 	name : "en brandkvinna",
	 	id : "3124",
	 	formPrimaryName : "kvinnlig",
	 	categories_IDs : ["61"],
	 	language_ID : "1",
	 	formsPrimary_IDs : ["1227"],
	 	related_IDs : ["1155", "1190", "1283", "3112", "3136"]
 	 }, { 
	 	name : "en kvinnlig polis",
	 	id : "3125",
	 	formPrimaryName : "kvinnlig",
	 	categories_IDs : ["61"],
	 	language_ID : "1",
	 	formsPrimary_IDs : ["1228"],
	 	related_IDs : ["1156", "1191", "1284", "3113", "3137"]
 	 }, { 
	 	name : "en polis",
	 	id : "3126",
	 	formPrimaryName : "neutral",
	 	categories_IDs : ["61"],
	 	language_ID : "1",
	 	formsPrimary_IDs : ["1228"],
	 	related_IDs : ["1156", "1191", "1284", "3113", "3137"]
 	 }, { 
	 	name : "en sjukskötare",
	 	id : "3127",
	 	formPrimaryName : "manlig",
	 	categories_IDs : ["61"],
	 	language_ID : "1",
	 	formsPrimary_IDs : ["1237"],
	 	related_IDs : ["1166", "1201", "1853", "3122", "3214"]
 	 }, { 
	 	categories_IDs : ["33"],
	 	related_IDs : ["1252", "1253", "1254", "1724", "2878"],
	 	language_ID : "2",
	 	name : "aus",
	 	id : "3128"
 	 }, { 
	 	categories_IDs : ["33"],
	 	related_IDs : ["1259", "1260"],
	 	language_ID : "2",
	 	name : "in",
	 	id : "3129"
 	 }, { 
	 	name : "la clienta",
	 	id : "3130",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1852"],
	 	related_IDs : ["1147", "1181", "1219", "3102", "3103"],
	 	tags : []
 	 }, { 
	 	name : "el jefe",
	 	id : "3131",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1276"],
	 	related_IDs : ["1148", "1182", "1220", "1221"]
 	 }, { 
	 	name : "el camionero",
	 	id : "3132",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1279"],
	 	related_IDs : ["1151", "1185", "1224", "3109"]
 	 }, { 
	 	name : "la médica",
	 	id : "3133",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1280"],
	 	related_IDs : ["1186", "1187", "1225", "3110"],
	 	tags : []
 	 }, { 
	 	name : "el trabajador",
	 	id : "3134",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1281"],
	 	related_IDs : ["1153", "1188", "1226"],
	 	tags : []
 	 }, { 
	 	name : "la limpiadora",
	 	id : "3135",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1282"],
	 	related_IDs : ["1154", "1189"]
 	 }, { 
	 	name : "el bombero",
	 	id : "3136",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1283"],
	 	related_IDs : ["1155", "1190", "1227", "3112", "3124"]
 	 }, { 
	 	name : "la oficial de policía",
	 	id : "3137",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1284"],
	 	related_IDs : ["1156", "1191", "1228", "3113", "3125", "3126"]
 	 }, { 
	 	categories_IDs : ["42"],
	 	related_IDs : ["1308", "1309", "1328", "1342"],
	 	name : "lämna in",
	 	language_ID : "1",
	 	id : "3138"
 	 }, { 
	 	categories_IDs : ["43"],
	 	related_IDs : ["1348", "1364", "1380", "1397"],
	 	language_ID : "1",
	 	name : "ledsam",
	 	id : "3139"
 	 }, { 
	 	categories_IDs : ["43"],
	 	related_IDs : ["1350", "1366", "1382", "1399", "3143"],
	 	language_ID : "1",
	 	name : "ursinnig",
	 	id : "3140"
 	 }, { 
	 	categories_IDs : ["43"],
	 	related_IDs : ["1353", "1369", "1385", "1403"],
	 	language_ID : "1",
	 	name : "ett nöje",
	 	id : "3141"
 	 }, { 
	 	name : "enfadada",
	 	id : "3142",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["43"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1398"],
	 	related_IDs : ["1349", "1365", "1381"]
 	 }, { 
	 	name : "furiosa",
	 	id : "3143",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["43"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1399"],
	 	related_IDs : ["1350", "1366", "1382", "3140"]
 	 }, { 
	 	name : "tranquilo",
	 	id : "3144",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["43"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1400"],
	 	related_IDs : ["1351", "1367", "1383", "1401", "2999", "3145"]
 	 }, { 
	 	name : "calmado",
	 	id : "3145",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["43"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1401"],
	 	related_IDs : ["1351", "1367", "1383", "1400", "2999", "3144"]
 	 }, { 
	 	name : "excitada",
	 	id : "3146",
	 	formPrimaryName : "femenino",
	 	categories_IDs : ["43"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1402"],
	 	related_IDs : ["1352", "1368", "1384"]
 	 }, { 
	 	categories_IDs : ["43"],
	 	related_IDs : ["1354", "1370", "1386", "1404"],
	 	language_ID : "5",
	 	name : "la rabia",
	 	id : "3147"
 	 }, { 
	 	categories_IDs : ["43"],
	 	related_IDs : ["1357", "1373", "1389", "1406"],
	 	language_ID : "5",
	 	name : "la conmoción",
	 	id : "3148"
 	 }, { 
	 	name : "decepcionada",
	 	id : "3149",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["43"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1409"],
	 	related_IDs : ["1360", "1376", "1392"]
 	 }, { 
	 	name : "aburrido",
	 	id : "3150",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["43"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1410"],
	 	related_IDs : ["1361", "1377", "1393"]
 	 }, { 
	 	name : "enferma",
	 	id : "3151",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["44"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1455"],
	 	related_IDs : ["1424", "1438", "1470"]
 	 }, { 
	 	name : "sano",
	 	id : "3152",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["44"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1456"],
	 	related_IDs : ["1425", "1439", "1471"]
 	 }, { 
	 	name : "die Enkelin",
	 	id : "3153",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["63"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1493"],
	 	related_IDs : ["1515", "1533", "1552", "3154"]
 	 }, { 
	 	name : "el nieto",
	 	id : "3154",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["63"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1552"],
	 	related_IDs : ["1493", "1515", "1533", "3153"]
 	 }, { 
	 	name : "die Partnerin",
	 	id : "3155",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["63"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1496"],
	 	related_IDs : ["1517", "1535", "1554", "3156"]
 	 }, { 
	 	name : "el compañero",
	 	id : "3156",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["63"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1554"],
	 	related_IDs : ["1496", "1517", "1535"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["1479", "1500", "1521", "1540"],
	 	language_ID : "1",
	 	name : "en kram",
	 	id : "3157"
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["1504", "1505", "1536", "1555"],
	 	language_ID : "1",
	 	name : "kura inhop sig",
	 	id : "3158"
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["1486", "1507", "1526", "1545", "3162"],
	 	language_ID : "1",
	 	name : "blir sig vän med ngn.",
	 	id : "3159",
	 	tags : []
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["1487", "1508", "1527", "1546"],
	 	language_ID : "1",
	 	name : "en broder",
	 	id : "3160"
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["1490", "1511", "1530", "1549"],
	 	name : "en mamma",
	 	language_ID : "1",
	 	id : "3161"
 	 }, { 
	 	name : "hacerse amiga de alguien",
	 	id : "3162",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["63"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1545"],
	 	related_IDs : ["1486", "1526", "3159"]
 	 }, { 
	 	categories_IDs : ["46"],
	 	related_IDs : ["1571", "1591", "1610", "1628", "3165"],
	 	name : "la tele",
	 	language_ID : "5",
	 	id : "3163"
 	 }, { 
	 	categories_IDs : ["46"],
	 	related_IDs : ["1556", "1575", "1595", "1614"],
	 	language_ID : "1",
	 	name : "en ro",
	 	id : "3164"
 	 }, { 
	 	categories_IDs : ["46"],
	 	related_IDs : ["1571", "1591", "1610", "1628", "3163"],
	 	name : "en tv",
	 	language_ID : "1",
	 	id : "3165"
 	 }, { 
	 	name : "delgada",
	 	id : "3166",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["45"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1641"],
	 	related_IDs : ["1632", "1652", "1662", "2995"]
 	 }, { 
	 	name : "anguloso",
	 	id : "3167",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["45"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1642"],
	 	related_IDs : ["1633", "1653", "1663"]
 	 }, { 
	 	name : "flaca",
	 	id : "3168",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["45"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1643"],
	 	related_IDs : ["1634", "1654", "1664", "2994"]
 	 }, { 
	 	name : "grueso",
	 	id : "3169",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["45"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1644"],
	 	related_IDs : ["1635", "1655", "1665"]
 	 }, { 
	 	name : "velludo",
	 	id : "3170",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["45"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1645"],
	 	related_IDs : ["1636", "1656", "1666"]
 	 }, { 
	 	name : "lisa",
	 	id : "3171",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["45"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1646"],
	 	related_IDs : ["1637", "1657", "1667"]
 	 }, { 
	 	name : "áspero",
	 	id : "3172",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["45"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1651"],
	 	related_IDs : ["1647", "1661", "1671", "3005", "3006"]
 	 }, { 
	 	categories_IDs : ["45"],
	 	related_IDs : ["1640", "1650", "1660", "1670"],
	 	name : "tarnished",
	 	language_ID : "3",
	 	id : "3173"
 	 }, { 
	 	name : "rubio",
	 	id : "3174",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["45"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1675"],
	 	related_IDs : ["1672", "1673", "1674", "2997"]
 	 }, { 
	 	categories_IDs : ["40"],
	 	related_IDs : ["1681", "1688", "1697", "2549"],
	 	language_ID : "5",
	 	name : "la paila",
	 	id : "3175"
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["1699", "1700", "1701", "1702"],
	 	language_ID : "1",
	 	name : "en jordfästning",
	 	id : "3176"
 	 }, { 
	 	name : "la",
	 	id : "3177",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["33"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1703"],
	 	related_IDs : ["101", "3178", "3179", "3180", "62", "83"]
 	 }, { 
	 	name : "die",
	 	id : "3178",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["33"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["62"],
	 	related_IDs : ["101", "1703", "3177", "3180", "83"]
 	 }, { 
	 	name : "das",
	 	id : "3179",
	 	formPrimaryName : "neutral",
	 	categories_IDs : ["33"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["62"],
	 	related_IDs : ["101", "1703", "3177", "3180", "83"],
	 	tags : []
 	 }, { 
	 	name : "det",
	 	id : "3180",
	 	formPrimaryName : "neutrum",
	 	categories_IDs : ["33"],
	 	language_ID : "1",
	 	formsPrimary_IDs : ["101"],
	 	related_IDs : ["1703", "3177", "3178", "3179", "62", "83"]
 	 }, { 
	 	categories_IDs : ["33"],
	 	related_IDs : ["109", "1712", "75", "93"],
	 	name : "sin embargo",
	 	language_ID : "5",
	 	id : "3181"
 	 }, { 
	 	name : "estar echada",
	 	id : "3182",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["35"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1750"],
	 	related_IDs : ["396", "431", "464"]
 	 }, { 
	 	categories_IDs : ["35"],
	 	related_IDs : ["1762", "385", "420", "456"],
	 	language_ID : "5",
	 	name : "comprender",
	 	id : "3183"
 	 }, { 
	 	categories_IDs : ["35"],
	 	related_IDs : ["1763", "383", "418", "455"],
	 	name : "ponerse",
	 	language_ID : "5",
	 	id : "3184"
 	 }, { 
	 	name : "lejana",
	 	id : "3185",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["38"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1769"],
	 	related_IDs : ["502", "522", "849"]
 	 }, { 
	 	categories_IDs : ["38"],
	 	related_IDs : ["1769", "502", "522", "849"],
	 	language_ID : "5",
	 	name : "lejos de",
	 	id : "3186"
 	 }, { 
	 	categories_IDs : ["38"],
	 	related_IDs : ["1775", "501", "521", "848", "3091", "500", "520", "542"],
	 	name : "cerca de",
	 	language_ID : "5",
	 	id : "3187"
 	 }, { 
	 	categories_IDs : ["38"],
	 	related_IDs : ["1784", "1786", "2869"],
	 	name : "debajo de",
	 	language_ID : "5",
	 	id : "3188",
	 	tags : []
 	 }, { 
	 	name : "otro",
	 	id : "3189",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["38"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1789"],
	 	related_IDs : ["506", "526", "854"]
 	 }, { 
	 	name : "vieja",
	 	id : "3190",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["45", "60"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1796"],
	 	related_IDs : ["319", "320", "346"]
 	 }, { 
	 	name : "pequeña",
	 	id : "3191",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["45", "60"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1803"],
	 	related_IDs : ["314", "316", "344"]
 	 }, { 
	 	name : "corta",
	 	id : "3192",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["45", "60"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1804"],
	 	related_IDs : ["312", "313", "342"]
 	 }, { 
	 	name : "largo",
	 	id : "3193",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["45", "60"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1805"],
	 	related_IDs : ["339", "340", "356", "358"]
 	 }, { 
	 	name : "nuevo",
	 	id : "3194",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["60"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1806"],
	 	related_IDs : ["323", "324", "348"]
 	 }, { 
	 	name : "rojo",
	 	id : "3195",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["45", "60"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1807"],
	 	related_IDs : ["327", "328", "350"]
 	 }, { 
	 	name : "negro",
	 	id : "3196",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["45", "60"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1808"],
	 	related_IDs : ["333", "334", "353"]
 	 }, { 
	 	name : "bonito",
	 	id : "3197",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["60"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1809"],
	 	related_IDs : ["3085", "3086", "337", "338", "355"]
 	 }, { 
	 	name : "blanca",
	 	id : "3198",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["45", "60"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1810"],
	 	related_IDs : ["331", "332", "352"]
 	 }, { 
	 	name : "der Diktator",
	 	id : "3199",
	 	formPrimaryName : "männlich",
	 	categories_IDs : ["48"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1814"],
	 	related_IDs : ["1830", "1860", "1878", "3219"]
 	 }, { 
	 	name : "die Präsidentin",
	 	id : "3200",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["48"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1822"],
	 	related_IDs : ["1835", "1867", "1884", "3201"]
 	 }, { 
	 	name : "el presidente",
	 	id : "3201",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["48"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1867"],
	 	related_IDs : ["1822", "1835", "1884", "3200"]
 	 }, { 
	 	name : "die Kanzlerin",
	 	id : "3202",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["48"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1823"],
	 	related_IDs : ["1831", "1863", "1880", "3203"]
 	 }, { 
	 	name : "la cancillera",
	 	id : "3203",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["48"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1863"],
	 	related_IDs : ["1831", "1880", "3202"],
	 	tags : []
 	 }, { 
	 	name : "der Ministerpräsident",
	 	id : "3204",
	 	formPrimaryName : "männlich",
	 	categories_IDs : ["48"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1824"],
	 	related_IDs : ["1833", "1865", "1882", "3221"],
	 	tags : []
 	 }, { 
	 	name : "der Prinz",
	 	id : "3205",
	 	formPrimaryName : "männlich",
	 	categories_IDs : ["48"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1826"],
	 	related_IDs : ["1834", "1866", "1883"],
	 	tags : []
 	 }, { 
	 	name : "en prinsessa",
	 	id : "3206",
	 	formPrimaryName : "kvinnlig",
	 	categories_IDs : ["48"],
	 	language_ID : "1",
	 	formsPrimary_IDs : ["1883"],
	 	related_IDs : ["1826", "3207", "3208"],
	 	tags : []
 	 }, { 
	 	name : "la princesa",
	 	id : "3207",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["48"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1866"],
	 	related_IDs : ["1826", "3206", "3208"],
	 	tags : []
 	 }, { 
	 	name : "the princess",
	 	id : "3208",
	 	formPrimaryName : "female",
	 	categories_IDs : ["48"],
	 	language_ID : "3",
	 	formsPrimary_IDs : ["1834"],
	 	related_IDs : ["1826", "3206", "3207"],
	 	tags : []
 	 }, { 
	 	name : "el cuidador de ancianos",
	 	id : "3209",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1847"],
	 	related_IDs : ["1158", "1193", "3115"]
 	 }, { 
	 	name : "el abogado",
	 	id : "3210",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1848"],
	 	related_IDs : ["1159", "1194", "1230", "3116"]
 	 }, { 
	 	name : "la funcionaria del gobierno",
	 	id : "3211",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1849"],
	 	related_IDs : ["1170", "1205", "1241", "3123"]
 	 }, { 
	 	name : "la visitante",
	 	id : "3212",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1850"],
	 	related_IDs : ["1165", "1200", "1236", "3121"]
 	 }, { 
	 	name : "la periodista",
	 	id : "3213",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1851"],
	 	related_IDs : ["1161", "1196", "1232", "3118"]
 	 }, { 
	 	name : "el enfermero",
	 	id : "3214",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1853"],
	 	related_IDs : ["1166", "1201", "3127"],
	 	tags : []
 	 }, { 
	 	name : "el mecánico",
	 	id : "3215",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1854"],
	 	related_IDs : ["1157", "1192", "1229", "3114"]
 	 }, { 
	 	name : "el sacerdote",
	 	id : "3216",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1856"],
	 	related_IDs : ["1160", "1195", "1231", "3117"]
 	 }, { 
	 	name : "el político",
	 	id : "3217",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1857"],
	 	related_IDs : ["1163", "1198", "1234", "3120"]
 	 }, { 
	 	name : "la taxista",
	 	id : "3218",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["61"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1858"],
	 	related_IDs : ["1162", "1197", "1233", "3119"]
 	 }, { 
	 	name : "el dictador",
	 	id : "3219",
	 	formPrimaryName : "masculino",
	 	categories_IDs : ["48"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1860"],
	 	related_IDs : ["1814", "1830", "1878", "3199"]
 	 }, { 
	 	name : "la presidenta del gobierno",
	 	id : "3220",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["48"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["1865"],
	 	related_IDs : ["1824", "1833", "1882", "3204", "3221"]
 	 }, { 
	 	categories_IDs : ["48"],
	 	related_IDs : ["1824", "1833", "1865", "1882", "3204", "3220"],
	 	language_ID : "1",
	 	name : "en ministerpresident",
	 	id : "3221"
 	 }, { 
	 	categories_IDs : ["48"],
	 	related_IDs : ["1820", "1876", "1893", "2980"],
	 	language_ID : "1",
	 	name : "gå och rösta",
	 	id : "3222"
 	 }, { 
	 	name : "die Extremistin",
	 	id : "3223",
	 	categories_IDs : ["49"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1920"],
	 	related_IDs : ["2105", "2130", "2150", "3250"]
 	 }, { 
	 	name : "die Göttin",
	 	id : "3224",
	 	categories_IDs : ["49"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1921"],
	 	related_IDs : ["2107", "2133", "2154", "3251", "3252", "3253"]
 	 }, { 
	 	name : "die Teufelin",
	 	id : "3225",
	 	categories_IDs : ["49"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1922"],
	 	related_IDs : ["2109", "2139", "2160"]
 	 }, { 
	 	name : "die Künstlerin",
	 	id : "3226",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["53"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1942"],
	 	related_IDs : ["2420", "2439", "2515", "3227", "3228"]
 	 }, { 
	 	name : "la artista",
	 	id : "3227",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["53"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["2439"],
	 	related_IDs : ["1942", "2420", "2515", "3226", "3228"]
 	 }, { 
	 	name : "en konstnärinna",
	 	id : "3228",
	 	formPrimaryName : "kvinnlig",
	 	categories_IDs : ["53"],
	 	language_ID : "1",
	 	formsPrimary_IDs : ["2515"],
	 	related_IDs : ["1942", "2420", "2439", "3226", "3227"]
 	 }, { 
	 	name : "die Autorin",
	 	id : "3229",
	 	categories_IDs : ["53"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1952"],
	 	related_IDs : ["2419", "2438", "2512", "3275", "3276"]
 	 }, { 
	 	name : "die Lehrerin",
	 	id : "3230",
	 	categories_IDs : ["55"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1971"],
	 	related_IDs : ["2584", "2616", "2691", "3232"]
 	 }, { 
	 	name : "der Professor",
	 	id : "3231",
	 	categories_IDs : ["55"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1972"],
	 	related_IDs : ["2585", "2617", "2691", "3232"]
 	 }, { 
	 	name : "la profesora",
	 	id : "3232",
	 	categories_IDs : ["55"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["2691"],
	 	related_IDs : ["1972", "2584", "2616", "2617", "3230"],
	 	tags : []
 	 }, { 
	 	name : "der Löwe",
	 	id : "3233",
	 	categories_IDs : ["57"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["1992"],
	 	related_IDs : ["2338", "2352", "2379", "3234", "3235", "3236"]
 	 }, { 
	 	name : "el león",
	 	id : "3234",
	 	categories_IDs : ["57"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["2352"],
	 	related_IDs : ["1992", "2338", "2379", "3233", "3235", "3236"]
 	 }, { 
	 	name : "the lioness",
	 	id : "3235",
	 	categories_IDs : ["57"],
	 	language_ID : "3",
	 	formsPrimary_IDs : ["2379"],
	 	related_IDs : ["1992", "2338", "2352", "3233", "3234", "3236"]
 	 }, { 
	 	name : "en lejonhona",
	 	id : "3236",
	 	formPrimaryName : "kvinnlig",
	 	categories_IDs : ["57"],
	 	language_ID : "1",
	 	formsPrimary_IDs : ["2338"],
	 	related_IDs : ["1992", "2352", "2379", "3233", "3234", "3235"]
 	 }, { 
	 	name : "die Außerirdische",
	 	id : "3237",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["58"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["2008", "3238"],
	 	related_IDs : ["2707", "2799", "3257"],
	 	tags : []
 	 }, { 
	 	name : "das außerirdische Wesen",
	 	id : "3238",
	 	formPrimaryName : "neutral",
	 	categories_IDs : ["58"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["2008", "3237"],
	 	related_IDs : ["2707", "2799", "3257"]
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["2015", "2046", "2067", "2220", "3240", "2029", "2071", "2207"],
	 	name : "the floor",
	 	language_ID : "3",
	 	id : "3239"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["2015", "2046", "2067", "2220", "3239"],
	 	language_ID : "3",
	 	name : "the level",
	 	id : "3240"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["2012", "2050", "2072", "2211", "3242"],
	 	name : "the lift",
	 	language_ID : "3",
	 	id : "3241"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["2012", "2050", "2072", "2211", "3241"],
	 	name : "el elevador",
	 	language_ID : "5",
	 	id : "3242"
 	 }, { 
	 	name : "la pieza",
	 	id : "3243",
	 	formPrimaryName : "lat. am.",
	 	categories_IDs : ["19"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["2073"],
	 	related_IDs : ["2013", "2014", "2048", "2203"],
	 	sentence : false,
	 	tags : []
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["2089", "2090", "2225", "3245", "2056"],
	 	language_ID : "3",
	 	name : "the basin",
	 	id : "3244",
	 	tags : []
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["2089", "2090", "2225", "3244", "2056"],
	 	language_ID : "1",
	 	name : "ett tvättställ",
	 	id : "3245"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["2076", "2083", "2096", "2218"],
	 	name : "the basement",
	 	language_ID : "3",
	 	id : "3246"
 	 }, { 
	 	categories_IDs : ["49"],
	 	related_IDs : ["1906", "2106", "2131", "2153"],
	 	name : "the faith",
	 	language_ID : "3",
	 	id : "3247"
 	 }, { 
	 	name : "cristiana",
	 	id : "3248",
	 	categories_IDs : ["49"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["2120"],
	 	related_IDs : ["1914", "2099", "2146"]
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["2125", "2126", "2127", "2128"],
	 	name : "ett skinn",
	 	language_ID : "1",
	 	id : "3249"
 	 }, { 
	 	name : "la extremista",
	 	id : "3250",
	 	formPrimaryName : "femenino",
	 	categories_IDs : ["49"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["2130"],
	 	related_IDs : ["1920", "2105", "2150", "3223"]
 	 }, { 
	 	name : "la diosa",
	 	id : "3251",
	 	formPrimaryName : "femenina",
	 	categories_IDs : ["49"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["2133"],
	 	related_IDs : ["1921", "2107", "2154", "3224", "3252", "3253"],
	 	tags : []
 	 }, { 
	 	name : "the Goddess",
	 	id : "3252",
	 	formPrimaryName : "female",
	 	categories_IDs : ["49"],
	 	language_ID : "3",
	 	formsPrimary_IDs : ["2107"],
	 	related_IDs : ["3224", "3251", "3253"],
	 	tags : []
 	 }, { 
	 	name : "en gudinna",
	 	id : "3253",
	 	formPrimaryName : "kvinnlig",
	 	categories_IDs : ["49"],
	 	language_ID : "1",
	 	formsPrimary_IDs : ["2154"],
	 	related_IDs : ["3224", "3251", "3252"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2768", "2776", "2785", "2793"],
	 	name : "en torka",
	 	language_ID : "1",
	 	id : "3254"
 	 }, { 
	 	categories_IDs : ["2757"],
	 	related_IDs : ["2773", "2781", "2790", "2798"],
	 	name : "transpirar",
	 	language_ID : "5",
	 	id : "3255"
 	 }, { 
	 	name : "soleada",
	 	id : "3256",
	 	categories_IDs : ["59"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["2716"],
	 	related_IDs : ["2303", "2304", "2718"]
 	 }, { 
	 	name : "la extraterrestre",
	 	id : "3257",
	 	categories_IDs : ["58"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["2707"],
	 	related_IDs : ["2008", "2799", "3237", "3238"]
 	 }, { 
	 	categories_IDs : ["55"],
	 	related_IDs : ["1966", "2586", "2618", "2696", "3271"],
	 	language_ID : "5",
	 	name : "la lección",
	 	id : "3258"
 	 }, { 
	 	categories_IDs : ["55"],
	 	related_IDs : ["2580", "2581", "2624", "2694", "3260"],
	 	language_ID : "5",
	 	name : "la escuela",
	 	id : "3259"
 	 }, { 
	 	categories_IDs : ["55"],
	 	related_IDs : ["2580", "2581", "2624", "2694", "3259"],
	 	language_ID : "5",
	 	name : "el instituto",
	 	id : "3260"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["1895", "2559", "2601", "2674"],
	 	name : "el correo electrónico",
	 	language_ID : "5",
	 	id : "3261"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["1900", "2550", "2595", "2668", "3269"],
	 	name : "responder",
	 	language_ID : "5",
	 	id : "3262"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["1905", "2552", "2597", "2670", "3273"],
	 	name : "el móvil",
	 	language_ID : "5",
	 	id : "3263"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["2634", "2638", "2643", "2648"],
	 	name : "en bädd",
	 	language_ID : "1",
	 	id : "3264"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["2635", "2639", "2644", "2649"],
	 	language_ID : "2",
	 	name : "die Couch",
	 	id : "3265"
 	 }, { 
	 	categories_IDs : ["55"],
	 	related_IDs : ["1970", "2577", "2622", "2692"],
	 	name : "the vocational training",
	 	language_ID : "3",
	 	id : "3266"
 	 }, { 
	 	categories_IDs : ["54"],
	 	related_IDs : ["1956", "2567", "2610", "2688"],
	 	name : "the crosswalk",
	 	language_ID : "3",
	 	id : "3267"
 	 }, { 
	 	categories_IDs : ["54"],
	 	related_IDs : ["1955", "2564", "2609", "2683"],
	 	name : "the sidewalk",
	 	language_ID : "3",
	 	id : "3268"
 	 }, { 
	 	categories_IDs : ["35", "52"],
	 	related_IDs : ["1900", "2550", "2595", "2668", "3262"],
	 	name : "answer",
	 	language_ID : "3",
	 	id : "3269"
 	 }, { 
	 	categories_IDs : ["61"],
	 	related_IDs : ["2590", "2591", "2631", "2700"],
	 	name : "en skrivare",
	 	language_ID : "1",
	 	id : "3270"
 	 }, { 
	 	categories_IDs : ["55"],
	 	related_IDs : ["1966", "2586", "2618", "2696", "3258"],
	 	name : "en lektion",
	 	language_ID : "1",
	 	id : "3271"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["1903", "2558", "2606", "2679"],
	 	name : "vara tyst",
	 	language_ID : "1",
	 	id : "3272"
 	 }, { 
	 	categories_IDs : ["52"],
	 	related_IDs : ["1905", "2552", "2597", "2670", "3263"],
	 	name : "en mobiltelefon",
	 	language_ID : "1",
	 	id : "3273"
 	 }, { 
	 	categories_IDs : ["53"],
	 	related_IDs : ["2447", "2466", "2494", "2518"],
	 	name : "en grupp",
	 	language_ID : "1",
	 	id : "3274"
 	 }, { 
	 	name : "en författarinna",
	 	id : "3275",
	 	formPrimaryName : "kvinnlig",
	 	categories_IDs : ["53"],
	 	language_ID : "1",
	 	formsPrimary_IDs : ["2512"],
	 	related_IDs : ["1952", "2419", "2438", "3229", "3276"]
 	 }, { 
	 	name : "la autora",
	 	id : "3276",
	 	categories_IDs : ["53"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["2438"],
	 	related_IDs : ["1952", "2419", "2512", "3229", "3275"]
 	 }, { 
	 	name : "moderna",
	 	id : "3277",
	 	categories_IDs : ["53"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["2482"],
	 	related_IDs : ["1948", "2428", "2534"]
 	 }, { 
	 	name : "clásica",
	 	id : "3278",
	 	categories_IDs : ["53"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["2480"],
	 	related_IDs : ["1947", "2427", "2532"]
 	 }, { 
	 	categories_IDs : ["53"],
	 	related_IDs : ["2443", "2471", "2496", "2521"],
	 	language_ID : "2",
	 	name : "die Violine",
	 	id : "3279"
 	 }, { 
	 	name : "la pintora",
	 	id : "3280",
	 	categories_IDs : ["53"],
	 	language_ID : "5",
	 	formsPrimary_IDs : ["2440"],
	 	related_IDs : ["2437", "2493", "2516", "3281"]
 	 }, { 
	 	name : "die Malerin",
	 	id : "3281",
	 	categories_IDs : ["53"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["2437"],
	 	related_IDs : ["2440", "2493", "2516", "3280"]
 	 }, { 
	 	categories_IDs : ["53"],
	 	related_IDs : ["1943", "2422", "2467", "2519"],
	 	name : "the colour",
	 	language_ID : "3",
	 	id : "3282"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["2001", "2343", "2362", "2374"],
	 	name : "dar de comer",
	 	language_ID : "5",
	 	id : "3283"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["1996", "2325", "2354", "2368"],
	 	name : "el pájaro",
	 	language_ID : "5",
	 	id : "3284"
 	 }, { 
	 	name : "Gegen Mittag war die Sonne hinter Wolken verschwunden.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "3285",
	 	related_IDs : ["2233", "3287"],
	 	categories_IDs : ["59"],
	 	tags : []
 	 }, { 
	 	name : "Around noon the sun was hidden behind clouds.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "3287",
	 	related_IDs : ["2260", "QPAp1Vim2Qc5"],
	 	categories_IDs : ["59"]
 	 }, { 
	 	name : "Die Holzplatte war über die Jahre matt geworden.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "3288",
	 	related_IDs : ["1979", "3289"],
	 	categories_IDs : ["19", "56"],
	 	tags : []
 	 }, { 
	 	name : "The wooden plate had become dull over the years.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "3289",
	 	related_IDs : ["2382", "3288"],
	 	categories_IDs : ["56"],
	 	tags : []
 	 }, { 
	 	name : "Which country were you born in?",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "3290",
	 	related_IDs : ["990", "NTK4zlTIhBcp"],
	 	categories_IDs : ["41"],
	 	tags : []
 	 }, { 
	 	related_IDs : ["3291"],
	 	categories_IDs : ["54"],
	 	responses_IDs : [],
	 	tags : [],
	 	language_ID : "2",
	 	name : "das Schiff",
	 	id : "3291"
 	 }, { 
	 	name : "Schiffe sind größer als Boote.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "3292",
	 	related_IDs : ["3291"],
	 	categories_IDs : ["54"]
 	 }, { 
	 	name : "sprechen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["400", "450", "467", "2677"],
	 	language_ID : "2",
	 	id : "435"
 	 }, { 
	 	name : "tala",
	 	categories_IDs : ["35"],
	 	related_IDs : ["435", "467"],
	 	language_ID : "1",
	 	id : "450"
 	 }, { 
	 	name : "tretton",
	 	categories_IDs : ["37"],
	 	related_IDs : ["213", "261", "292"],
	 	language_ID : "1",
	 	id : "212"
 	 }, { 
	 	name : "stanna",
	 	language_ID : "1",
	 	categories_IDs : ["35"],
	 	related_IDs : ["440", "473"],
	 	id : "405"
 	 }, { 
	 	name : "stay",
	 	categories_IDs : ["35"],
	 	related_IDs : ["405", "440", "QfiYJZGxINJX"],
	 	language_ID : "3",
	 	id : "473"
 	 }, { 
	 	name : "sprechen",
	 	categories_IDs : ["35"],
	 	related_IDs : ["450", "467"],
	 	language_ID : "2",
	 	id : "451"
 	 }, { 
	 	name : "speak",
	 	categories_IDs : ["35"],
	 	related_IDs : ["400", "435", "450", "451", "2677"],
	 	language_ID : "3",
	 	id : "467"
 	 }, { 
	 	name : "unten",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	related_IDs : ["2869"],
	 	id : "491"
 	 }, { 
	 	name : "back",
	 	categories_IDs : ["38"],
	 	related_IDs : ["1772", "496", "538"],
	 	language_ID : "3",
	 	id : "516"
 	 }, { 
	 	name : "en natur",
	 	categories_IDs : ["36"],
	 	related_IDs : ["576", "612"],
	 	language_ID : "1",
	 	id : "544"
 	 }, { 
	 	name : "die Natur",
	 	categories_IDs : ["36"],
	 	related_IDs : ["544", "612"],
	 	language_ID : "2",
	 	id : "576"
 	 }, { 
	 	name : "the nature",
	 	categories_IDs : ["36"],
	 	related_IDs : ["544", "576"],
	 	language_ID : "3",
	 	id : "612"
 	 }, { 
	 	name : "vor",
	 	language_ID : "2",
	 	categories_IDs : ["38", "39"],
	 	related_IDs : ["541", "794", "861", "738"],
	 	id : "499"
 	 }, { 
	 	name : "before",
	 	categories_IDs : ["38", "39"],
	 	related_IDs : ["861", "922", "924", "925", "3003"],
	 	language_ID : "3",
	 	id : "519"
 	 }, { 
	 	name : "before",
	 	language_ID : "3",
	 	categories_IDs : ["38", "39"],
	 	related_IDs : ["794", "861", "499", "922", "924", "925", "3003"],
	 	id : "738"
 	 }, { 
	 	name : "querer",
	 	categories_IDs : ["35"],
	 	related_IDs : ["412", "447", "480", "386", "421", "458"],
	 	language_ID : "5",
	 	id : "1730"
 	 }, { 
	 	name : "estar",
	 	categories_IDs : ["35"],
	 	related_IDs : ["380", "415", "452", "407", "442", "474"],
	 	language_ID : "5",
	 	id : "1731"
 	 }, { 
	 	name : "la política",
	 	categories_IDs : ["36", "48"],
	 	related_IDs : ["561", "594", "629"],
	 	language_ID : "5",
	 	id : "663"
 	 }, { 
	 	name : "das Meer",
	 	categories_IDs : ["2756", "41", "59"],
	 	related_IDs : ["1053", "1097", "2293"],
	 	language_ID : "2",
	 	id : "964"
 	 }, { 
	 	name : "el techo",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2075", "2092", "2208", "2027", "2054", "2216"],
	 	language_ID : "5",
	 	id : "2077",
	 	tags : []
 	 }, { 
	 	name : "the sink",
	 	categories_IDs : ["19"],
	 	related_IDs : ["2025", "2084", "2221", "3244", "2089", "2090", "2225", "3245"],
	 	language_ID : "3",
	 	id : "2056",
	 	tags : []
 	 }, { 
	 	name : "der Kreislauf",
	 	categories_IDs : ["51", "44"],
	 	related_IDs : ["2177", "2199", "2658", "2179", "2540"],
	 	language_ID : "2",
	 	id : "1928"
 	 }, { 
	 	name : "the sea",
	 	categories_IDs : ["2756", "41", "59"],
	 	related_IDs : ["1057", "1097"],
	 	language_ID : "3",
	 	id : "1007"
 	 }, { 
	 	name : "el mar",
	 	categories_IDs : ["2756", "41", "59"],
	 	related_IDs : ["1057", "2244"],
	 	language_ID : "5",
	 	id : "1097"
 	 }, { 
	 	name : "Some animals like moose mostly prefer the deep forrest.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "pvm6orYu9k11",
	 	related_IDs : ["10", "LFTqo8s1HUEC"],
	 	categories_IDs : ["2756"]
 	 }, { 
	 	name : "Why did you do that?",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "JoG040wLz4sX",
	 	related_IDs : ["87", "x3I4WmfusD39"],
	 	categories_IDs : ["33"]
 	 }, { 
	 	name : "When do you want to meet for a coffee?",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "bbE7NynxaUEi",
	 	related_IDs : ["89", "WIo0BQBbstaE"],
	 	categories_IDs : ["33"]
 	 }, { 
	 	name : "Who is that person with the red scarf on the left of the door?",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "rxYDYG1liI8e",
	 	related_IDs : ["84", "FwcLhf2Dcmw8"],
	 	categories_IDs : ["33"],
	 	tags : []
 	 }, { 
	 	name : "How would you describe that movie in one sentence?",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "jABkXYb127Ht",
	 	related_IDs : ["85", "gAMhmCxrvW04"],
	 	categories_IDs : ["33"]
 	 }, { 
	 	name : "Yes, that sounds like a suitable way to organize our files.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "nxFluqsNeOuP",
	 	related_IDs : ["91", "lj36tyliEc2B"],
	 	categories_IDs : ["33"]
 	 }, { 
	 	name : "No way, I can't believe he said that!",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "cM4heQAHVUQT",
	 	related_IDs : ["92", "8v3ptKjbC4ND"],
	 	categories_IDs : ["33"]
 	 }, { 
	 	name : "Take it or leave it.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "p72T1dwKbnkM",
	 	related_IDs : ["96"],
	 	categories_IDs : ["33"]
 	 }, { 
	 	name : "How about making some fresh apple juice?",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "6kRynX9bmtma",
	 	related_IDs : ["134", "7lIwUOXdMbYa"],
	 	categories_IDs : ["40"]
 	 }, { 
	 	name : "Big bands often have more than ten members.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "9BXJuuT2mYR4",
	 	related_IDs : ["341", "3ATF3zNuVdiD"],
	 	categories_IDs : ["45", "60"]
 	 }, { 
	 	name : "Please tell it in a nutshell, the short version please.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "uiYPoNsP9Orw",
	 	related_IDs : ["342", "NmQRMGzOscAd"],
	 	categories_IDs : ["45", "60"]
 	 }, { 
	 	name : "The banana had turned brown from becoming older.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "Q0JOzDvAItFx",
	 	related_IDs : ["346", "360", "455", "ReqfRTFC50Vv"],
	 	categories_IDs : ["45", "60"]
 	 }, { 
	 	name : "When I'm feeling blue, I try to spend more time with good friends.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "nvoHYN3Whtfl",
	 	related_IDs : ["351", "620", "rCbGzaNw7WzN"],
	 	categories_IDs : ["45", "60"],
	 	tags : []
 	 }, { 
	 	name : "The lake was deep blue due to the cloud free sky in the afternoon.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "ijEgwjqlizr5",
	 	related_IDs : ["351", "sirDuqmxqAV5"],
	 	categories_IDs : ["45", "60"]
 	 }, { 
	 	name : "White wine vinegar can be used for spicing up green salad.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "k1V3SDu9bmft",
	 	related_IDs : ["352", "uB0c4B3PNDj2"],
	 	categories_IDs : ["45", "60"]
 	 }, { 
	 	name : "Some cultures use white clothes to show sadness, others use black.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "n21VuZLdDydW",
	 	related_IDs : ["352", "WdEcBRMb0Fed"],
	 	categories_IDs : ["45", "60"]
 	 }, { 
	 	name : "Fixing the bike took a long time.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "x76cEC3LdZt2",
	 	related_IDs : ["356", "9Fd9fPDmsfcA"],
	 	categories_IDs : ["45", "60"]
 	 }, { 
	 	name : "Cutting onions always makes me cry.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "zZXRu1xNRIbf",
	 	related_IDs : ["363", "TTKxpHw9JTGp", "453", "1Yqhds6QYrsA"],
	 	categories_IDs : ["40"]
 	 }, { 
	 	categories_IDs : ["43", "40"],
	 	related_IDs : ["zZXRu1xNRIbf", "9pYMKyM15bMX", "KfnXYV5hHXmG"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "cry",
	 	id : "TTKxpHw9JTGp"
 	 }, { 
	 	categories_IDs : ["43", "40"],
	 	related_IDs : ["TTKxpHw9JTGp", "cSl2mH33yydT", "KfnXYV5hHXmG"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "weinen",
	 	id : "9pYMKyM15bMX"
 	 }, { 
	 	name : "Wenn ich Zwiebeln schneide, muss ich immer weinen.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "cSl2mH33yydT",
	 	related_IDs : ["9pYMKyM15bMX", "iyk7RNSbYeOX"],
	 	categories_IDs : ["43", "40"]
 	 }, { 
	 	name : "Please make sure that the children get home by sunset.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "COlaiIjNXzag",
	 	related_IDs : ["453", "zjDoEZqfI3P1"],
	 	categories_IDs : ["34", "63"],
	 	tags : []
 	 }, { 
	 	name : "I need to eat some chocolate to sooth my nerves.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "8NZmApTLoDwr",
	 	related_IDs : ["454", "nGq5Q86Vk5X9", "A6YeE8TAEocq"],
	 	categories_IDs : ["34"],
	 	tags : []
 	 }, { 
	 	name : "Do you need a towel or did you bring one?",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "m2SPLXssEXqh",
	 	related_IDs : ["454", "bghilco9jaHj", "Eofy3OVOzm7U"],
	 	categories_IDs : ["34", "46"],
	 	tags : []
 	 }, { 
	 	name : "Sorry, can you help with making supper? I need someone to cut the peppers for the salad.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "1n6nLUTDt5my",
	 	related_IDs : ["461", "KECjuk6zFkce", "u9yH7rIKSeBv"],
	 	categories_IDs : ["34", "35", "40"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["2967", "rGj1ZswfZPpj", "NTOqyo65r2EH", "zzw3VeF3wO23"],
	 	tags : [],
	 	name : "the help",
	 	language_ID : "3",
	 	id : "tZzOvlVsRO9t"
 	 }, { 
	 	categories_IDs : ["63", "53"],
	 	related_IDs : ["2937", "rGj1ZswfZPpj", "NTOqyo65r2EH", "tZzOvlVsRO9t"],
	 	tags : [],
	 	name : "die Hilfe",
	 	language_ID : "2",
	 	id : "zzw3VeF3wO23"
 	 }, { 
	 	categories_IDs : ["63", "53"],
	 	related_IDs : ["zzw3VeF3wO23", "tZzOvlVsRO9t", "NTOqyo65r2EH"],
	 	tags : [],
	 	language_ID : "5",
	 	name : "la ayuda",
	 	id : "rGj1ZswfZPpj"
 	 }, { 
	 	categories_IDs : ["63", "53"],
	 	related_IDs : ["tZzOvlVsRO9t", "zzw3VeF3wO23", "rGj1ZswfZPpj"],
	 	tags : [],
	 	language_ID : "1",
	 	name : "en hjälp",
	 	id : "NTOqyo65r2EH"
 	 }, { 
	 	name : "They lay down in the sand at the beach.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "itaj5phGE7DZ",
	 	related_IDs : ["464", "eFIRRMfO6z3x"],
	 	categories_IDs : ["34", "46"],
	 	tags : []
 	 }, { 
	 	name : "They were about to lay down their weapons when the ceasefire broke.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "eidFO6UeF8eY",
	 	related_IDs : ["PJ3JUUslu99U", "6XRUGmXSLoam"],
	 	categories_IDs : ["34", "61"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["35", "61"],
	 	related_IDs : ["eidFO6UeF8eY", "431"],
	 	tags : [],
	 	name : "lay",
	 	language_ID : "3",
	 	formsSecondary : ["laid", "laid"],
	 	formsSecondaryNames : ["Past Tense", "Past Participle"],
	 	id : "PJ3JUUslu99U"
 	 }, { 
	 	name : "If you look very closely, you can see the pattern.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "icy4IVQ8XUPX",
	 	related_IDs : ["470", "8sMWcM0l2H0C"],
	 	categories_IDs : ["34"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["35"],
	 	related_IDs : ["icy4IVQ8XUPX"],
	 	tags : [],
	 	name : "Wenn Sie genau hinschauen, können Sie das Muster sehen.",
	 	sentence : true,
	 	language_ID : "2",
	 	id : "8sMWcM0l2H0C"
 	 }, { 
	 	name : "Drive straight ahead along the the sea to get to the venue.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "vSdF9cgCICK0",
	 	related_IDs : ["507", "xmZzKp8E8VFa"],
	 	categories_IDs : ["38"]
 	 }, { 
	 	name : "The reasons behind his actions were complex.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "S4zXcilJKrkP",
	 	related_IDs : ["518", "gN2ysnigezWO"],
	 	categories_IDs : ["38"]
 	 }, { 
	 	name : "You can find a broom behind the counter.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "1WlJ9m1EaBRn",
	 	related_IDs : ["518", "q6097vFxIcPQ"],
	 	categories_IDs : ["38"]
 	 }, { 
	 	name : "The journey home had been long and exhausting.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "W1IBRDuzyX5N",
	 	related_IDs : ["622", "ymMhJDK5QJD5"],
	 	categories_IDs : ["34", "41"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["36", "41"],
	 	related_IDs : ["W1IBRDuzyX5N"],
	 	tags : [],
	 	name : "Die Reise nach Hause war lang und erschöpfend gewesen.",
	 	sentence : true,
	 	language_ID : "2",
	 	id : "ymMhJDK5QJD5"
 	 }, { 
	 	name : "I love Japanese food.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "pGhKZWyE0sDp",
	 	related_IDs : ["621", "V7GZmXn0NCMB"],
	 	categories_IDs : ["36"]
 	 }, { 
	 	name : "Try to count the number of birds in the sky.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "GHHIUlIUZPZX",
	 	related_IDs : ["XfNL6yFMQkxJ", "LG44PTYzF387"],
	 	categories_IDs : ["34"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["35"],
	 	related_IDs : ["GHHIUlIUZPZX", "cpPichTvL47T"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "count",
	 	id : "XfNL6yFMQkxJ"
 	 }, { 
	 	categories_IDs : ["35"],
	 	related_IDs : ["GHHIUlIUZPZX", "cpPichTvL47T"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "Versuch, die Zahl an Vögeln am Himmel zu zählen.",
	 	sentence : true,
	 	id : "LG44PTYzF387"
 	 }, { 
	 	categories_IDs : ["35"],
	 	related_IDs : ["XfNL6yFMQkxJ", "LG44PTYzF387"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "zählen",
	 	id : "cpPichTvL47T"
 	 }, { 
	 	related_IDs : ["Phtg3VoiLJLP"],
	 	categories_IDs : ["24"],
	 	responses_IDs : [],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the fairy",
	 	id : "ePJ3kGEJDYJr"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["63H5bE420zCn"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the troll",
	 	id : "V5iOzpfAWKo1"
 	 }, { 
	 	categories_IDs : ["19", "24"],
	 	related_IDs : ["wHrC5OPjnsH8"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the castle",
	 	id : "uHHbZhQTatHH"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["Hzr0IMz9PMVm"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the treasure",
	 	id : "SemqRM4TLqiC"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["OitC7CCM4NYb"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the dragon",
	 	id : "bIvLmjCW2eMg"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["4Hi7tCseSXmc"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the ghost",
	 	id : "GvkWSTKVzLDf"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["4fAgTK1kjoLy"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the space pirate",
	 	id : "VX0DWZDhnWO4"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["wA3sGZ8JFUR7"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the knight",
	 	id : "ZFmdzkGUupUZ"
 	 }, { 
	 	categories_IDs : ["24", "49"],
	 	related_IDs : ["pq8BmW9PVDR2"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the elder",
	 	id : "RCpvgjX70Gc6"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["72rUMbDbsCoj"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the dwarf",
	 	id : "dH7i8MAuxUdj"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["1xpm6qalxURh"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the fortune teller",
	 	id : "u4VJukuMmF6i"
 	 }, { 
	 	related_IDs : ["Z8BGVI3NLMa7"],
	 	categories_IDs : ["24", "61"],
	 	responses_IDs : [],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the huntress",
	 	formPrimaryName : "female",
	 	formsPrimary_IDs : ["ARJUwxtCiAEK"],
	 	id : "ksIKUIr0Vfqu"
 	 }, { 
	 	name : "the hunter",
	 	id : "ARJUwxtCiAEK",
	 	formPrimaryName : "male",
	 	categories_IDs : ["24", "61"],
	 	language_ID : "3",
	 	formsPrimary_IDs : ["ksIKUIr0Vfqu"],
	 	related_IDs : ["f04Lkn5UTD9d"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["XAutbmVdBHbw"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the bandit",
	 	id : "t9dIJ156jLRj"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["HR8QmBvBdNLv"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the vampire",
	 	id : "rOywSaTvrz2e"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["l8wJOAiB7Azq", "FEbJ48RFZ6gq"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the monster",
	 	id : "3jgF6CGJd23y"
 	 }, { 
	 	categories_IDs : ["24", "49"],
	 	related_IDs : ["vCoqhO0KQPlm"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the prophet",
	 	id : "PwbKWvHCYqcH"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["8Bh7qxaTNc5E"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the wizard",
	 	id : "J1ctcnSAqQq1"
 	 }, { 
	 	categories_IDs : ["24", "61"],
	 	related_IDs : ["5EhxIwkWrZn0"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the thief",
	 	id : "x7440eZ5zzUp"
 	 }, { 
	 	categories_IDs : ["24", "49"],
	 	related_IDs : ["fm9xMxat4udn"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the demon",
	 	id : "kQjXfQGKtP4L"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["NqTqkX4mb27R"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the unicorn",
	 	id : "Z1Qw9rS9FQHg"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["lU2rLrTCizXi"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the genie",
	 	id : "taFvNlHKGlLK"
 	 }, { 
	 	categories_IDs : ["24", "49"],
	 	related_IDs : ["4Hi7tCseSXmc"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the spirit",
	 	id : "UWeizO1Yg1WD"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["GpWYT9A9C8Ph"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the elf",
	 	id : "DHqbKYJputrB"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["DMzsutVoQceb"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the werewolf",
	 	id : "Why9eulWFLOO"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["JsfkNDOgMDtx"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the golem",
	 	id : "YBsSa0oRcMeA"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["o7aP7vO0V127"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the giant",
	 	id : "pNXGZiJYOMgQ"
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["5S3Dg95S6hE0"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the orphan",
	 	id : "rXP5XZA7TOQh"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["31JNjKcDIZ0Y"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the elephant",
	 	id : "grXFiBOTMNbd"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["U6Eh7ID6mRmU"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the revenant",
	 	id : "tmQ0dmcge7Dk"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["3rc7hNvMZIWU"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the grim reaper",
	 	id : "pO16WbY3lNHx"
 	 }, { 
	 	name : "The witch cursed the ignorant woman with blindness.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "eInuOquqdKlo",
	 	related_IDs : ["C9zbHQmnCj1q", "DE1XqUpsjFki", "Sq8D4p3mpr4F"],
	 	categories_IDs : ["24", "34"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["24", "35"],
	 	related_IDs : ["eInuOquqdKlo", "eVTGllE0ODw3"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "curse",
	 	id : "C9zbHQmnCj1q"
 	 }, { 
	 	name : "The train to Vienna is going to be late by about fifteen minutes.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "nKlejMrLYmlj",
	 	related_IDs : ["RDdJjt3PGMdJ"],
	 	categories_IDs : ["55", "42"]
 	 }, { 
	 	name : "I can't wait for summer to come again.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "lYM6rEKlMF00",
	 	related_IDs : ["785", "k4mZ64Dh7SAG", "j07sptMYr6Ic", "jZpK6TXtspIf"],
	 	categories_IDs : ["39"]
 	 }, { 
	 	name : "I have seen them earlier on their way to the cinema, why?",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "L3N18n8pPvQy",
	 	related_IDs : ["921", "gSeAfk3PbCOI"],
	 	categories_IDs : ["39"]
 	 }, { 
	 	name : "I think the keyboard may be broken, it does not register any keystrokes since two days ago.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "Csn80o3nIXbR",
	 	related_IDs : ["1175", "CTPSX1Q5LdW8"],
	 	categories_IDs : ["61"],
	 	tags : []
 	 }, { 
	 	name : "Let's have a break. I cannot concentrate on this any longer.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "BIu1cZPihXQy",
	 	related_IDs : ["1177", "xjHXXpsuDdPa"],
	 	categories_IDs : ["61"]
 	 }, { 
	 	name : "My newest client was an elderly man in search of his grey cat.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "fOXJ9imgXcTk",
	 	related_IDs : ["1181", "2364", "lYRdtSvrVPD1"],
	 	categories_IDs : ["61"],
	 	tags : []
 	 }, { 
	 	name : "The doctor asked her to lift her left lower arm.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "I7u5rYQCUvvh",
	 	related_IDs : ["1186", "9fd48tuSVC2a"],
	 	categories_IDs : ["61"]
 	 }, { 
	 	name : "If she really wanted to become a lawyer she would have to dig through a lot of law books.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "6jRmQcUJR1dU",
	 	related_IDs : ["1194", "4UkoAtLzG1s9"],
	 	categories_IDs : ["61"]
 	 }, { 
	 	name : "He suddenly stood up and walked to the fridge.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "Srv3VLlPKcJG",
	 	related_IDs : ["1286", "uGbyAXfZGFvW"],
	 	categories_IDs : ["39"]
 	 }, { 
	 	name : "I really should do more swimming, it just feels so good.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "hzrVmbKR7pQp",
	 	related_IDs : ["1294", "fePBkGSEfwOd"],
	 	categories_IDs : ["42"]
 	 }, { 
	 	name : "You are so going to lose this bet!",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "4vyQrhoTvwyQ",
	 	related_IDs : ["1305", "5KAVGpmlTCDS"],
	 	categories_IDs : ["42"]
 	 }, { 
	 	categories_IDs : ["33", "35", "54", "39"],
	 	related_IDs : ["lYM6rEKlMF00", "gCaXn6VzyRPp", "CAizcdtM795C"],
	 	tags : [],
	 	name : "come",
	 	language_ID : "3",
	 	id : "k4mZ64Dh7SAG"
 	 }, { 
	 	categories_IDs : ["33", "35", "54", "39"],
	 	related_IDs : ["k4mZ64Dh7SAG", "Rmzh5OKh8GW5", "CAizcdtM795C"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "kommen",
	 	id : "gCaXn6VzyRPp"
 	 }, { 
	 	name : "Ich kann es nicht erwarten, dass der Sommer wiederkommt.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "Rmzh5OKh8GW5",
	 	related_IDs : ["gCaXn6VzyRPp"],
	 	categories_IDs : ["33", "35", "54", "39"]
 	 }, { 
	 	categories_IDs : ["33", "35", "54", "39"],
	 	related_IDs : ["k4mZ64Dh7SAG", "gCaXn6VzyRPp"],
	 	tags : [],
	 	name : "komma",
	 	language_ID : "1",
	 	id : "CAizcdtM795C",
	 	formsSecondary : ["kom", "kommit"],
	 	formsSecondaryNames : ["Preteritum", "Supinum"]
 	 }, { 
	 	categories_IDs : ["33", "35", "42"],
	 	related_IDs : ["eWMxP42PCU53", "dqjDaeNLRrNR"],
	 	tags : [],
	 	language_ID : "1",
	 	name : "hålla",
	 	formsSecondary : ["höll", "hållit"],
	 	formsSecondaryNames : ["Preteritum", "Supinum"],
	 	id : "guA1Ony0x0LH"
 	 }, { 
	 	related_IDs : ["dqjDaeNLRrNR", "guA1Ony0x0LH"],
	 	categories_IDs : ["33", "35", "42"],
	 	responses_IDs : [],
	 	tags : [],
	 	language_ID : "2",
	 	name : "halten",
	 	id : "eWMxP42PCU53"
 	 }, { 
	 	categories_IDs : ["33", "35", "42"],
	 	related_IDs : ["eWMxP42PCU53", "guA1Ony0x0LH", "yc8Ra2mqxIHK"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "hold",
	 	id : "dqjDaeNLRrNR"
 	 }, { 
	 	name : "The nurse checked the patient's blood pressure.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "o2MS1I28761L",
	 	related_IDs : ["1201", "L5JrWlhKjuu6"],
	 	categories_IDs : ["61"]
 	 }, { 
	 	name : "The team captain was stopped ten meters from the goal with a foul.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "eWTbPSMJpkMF",
	 	related_IDs : ["1312", "5ZddtCs8tHHM"],
	 	categories_IDs : ["42"]
 	 }, { 
	 	name : "I am really happy about having a few hours of quietude.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "xeNAtBikFlUR",
	 	related_IDs : ["1379", "nYfk7dABntLm"],
	 	categories_IDs : ["43"]
 	 }, { 
	 	name : "The thought of having to leave made her very sad.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "CozUsivICGV7",
	 	related_IDs : ["1380", "jpLRQdULYgIy"],
	 	categories_IDs : ["43"]
 	 }, { 
	 	name : "She could sense the anger boiling within her girlfriend.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "sR85kNXY8xR3",
	 	related_IDs : ["1386", "5u0IjlSNSKgL"],
	 	categories_IDs : ["43"]
 	 }, { 
	 	name : "Despite the shouting he remained completely calm.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "DMaBnj7oS5mz",
	 	related_IDs : ["1383", "dLwg65Q2R9WA", "zWLExIlIWh4h"],
	 	categories_IDs : ["43"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["35", "52", "43"],
	 	related_IDs : ["DMaBnj7oS5mz", "6DvQHin4beDx"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "shout",
	 	id : "dLwg65Q2R9WA"
 	 }, { 
	 	categories_IDs : ["35", "52", "43"],
	 	related_IDs : ["dLwg65Q2R9WA", "zWLExIlIWh4h"],
	 	tags : [],
	 	name : "rufen",
	 	language_ID : "2",
	 	id : "6DvQHin4beDx"
 	 }, { 
	 	name : "Trotz der lauten Rufe blieb er ganz ruhig.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "zWLExIlIWh4h",
	 	related_IDs : ["6DvQHin4beDx", "DMaBnj7oS5mz"],
	 	categories_IDs : ["35", "52", "43"],
	 	tags : []
 	 }, { 
	 	name : "She was overcome by grief after having had lost her husband and her mother in the car crash.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "uieOnYFRF3s5",
	 	related_IDs : ["1387", "LKjENYbxVFNT"],
	 	categories_IDs : ["43"]
 	 }, { 
	 	name : "Hope is the key towards a better future, but only if you act accordingly.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "OV3l2opFocOy",
	 	related_IDs : ["1390", "i0uhLOOQTSDs"],
	 	categories_IDs : ["43"]
 	 }, { 
	 	name : "Losing this game was a huge disappointment for their fans.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "fxxEdDvOuEBQ",
	 	related_IDs : ["1391", "Vtbf0CLgOGfG"],
	 	categories_IDs : ["43"]
 	 }, { 
	 	name : "Having read in that book for an hour she was really becoming quite bored.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "4eMh9C0aOsyk",
	 	related_IDs : ["1393", "tXSPyL4eG0Wg"],
	 	categories_IDs : ["43"]
 	 }, { 
	 	name : "He crossed his arms as he was really not happy about what his mother had just said.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "fovqnxLUd0X5",
	 	related_IDs : ["1429", "YGSokkHPVzyU"],
	 	categories_IDs : ["44"]
 	 }, { 
	 	name : "I am really looking forward to not be sick anymore.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "6plnLSArTzq4",
	 	related_IDs : ["1438", "jy18H8pKBEIj"],
	 	categories_IDs : ["44"]
 	 }, { 
	 	name : "Love helps us overcome pain and suffering.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "4kQ4o4ms0lcK",
	 	related_IDs : ["1519", "z9oFYLlsJ1X7"],
	 	categories_IDs : ["63"]
 	 }, { 
	 	name : "We are planning to marry in autumn, somewhen around September.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "mYXBzmhOitST",
	 	related_IDs : ["95Zyf0S4HfIo", "Ah1rLFCcspFx"],
	 	categories_IDs : ["34", "63"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["35", "63"],
	 	related_IDs : ["mYXBzmhOitST", "vE4ZURwHg6Jw"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "marry",
	 	id : "95Zyf0S4HfIo"
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["2Q9g6vTFQdVM"],
	 	tags : [],
	 	name : "the divorce",
	 	language_ID : "3",
	 	id : "0A3qmf6qbdzn"
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["0A3qmf6qbdzn"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "die Scheidung",
	 	id : "2Q9g6vTFQdVM"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["X4HACz6soAOG", "YJiENY0csahg"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the zombie",
	 	id : "XIDf3pw0Ds3p"
 	 }, { 
	 	related_IDs : ["9pYMKyM15bMX", "TTKxpHw9JTGp"],
	 	categories_IDs : ["43", "40"],
	 	responses_IDs : [],
	 	tags : [],
	 	language_ID : "1",
	 	name : "gråta",
	 	id : "KfnXYV5hHXmG",
	 	formsSecondary : ["grät", "gråtit"],
	 	formsSecondaryNames : ["Preteritum", "Supinum"]
 	 }, { 
	 	name : "I think the cake should stay in the oven for another ten minutes.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "QfiYJZGxINJX",
	 	related_IDs : ["1695", "JNLqTZiSXA0q", "473", "481", "sWURaX0K8Q5K"],
	 	categories_IDs : ["34", "40"],
	 	tags : []
 	 }, { 
	 	name : "Ich glaube, der Kuchen sollte noch zehn Minuten im Backofen bleiben.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "JNLqTZiSXA0q",
	 	related_IDs : ["1679", "QfiYJZGxINJX", "440", "438"],
	 	categories_IDs : ["40", "35", "63"],
	 	tags : []
 	 }, { 
	 	name : "The handle of the pan was covered in oil.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "OJOAyfCGJ0nu",
	 	related_IDs : ["1697", "Hm3FWQI2OsIB", "ots5bXhLHbWo"],
	 	categories_IDs : ["40"]
 	 }, { 
	 	name : "Der Pfannengriff war ölbedeckt.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "Hm3FWQI2OsIB",
	 	related_IDs : ["1681", "OJOAyfCGJ0nu", "sLZL83xnAhAq"],
	 	categories_IDs : ["40"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["40", "54"],
	 	related_IDs : ["Hm3FWQI2OsIB", "ots5bXhLHbWo"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "das Öl",
	 	id : "sLZL83xnAhAq"
 	 }, { 
	 	categories_IDs : ["40", "54"],
	 	related_IDs : ["OJOAyfCGJ0nu", "sLZL83xnAhAq"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the oil",
	 	id : "ots5bXhLHbWo"
 	 }, { 
	 	name : "The daughter of the deceased held a touching eulogoy at his funeral.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "7HgGEv1vZ0NQ",
	 	related_IDs : ["1700", "ZP7BeHypF9r2", "U6q28lU7WGpc"],
	 	categories_IDs : ["63"],
	 	tags : []
 	 }, { 
	 	name : "Die Tochter des Verstorbenes hielt eine berührende Trauerrede auf der Beerdigung.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "ZP7BeHypF9r2",
	 	related_IDs : ["1699", "7HgGEv1vZ0NQ"],
	 	categories_IDs : ["63"],
	 	tags : []
 	 }, { 
	 	name : "Every dictator dies given enough time.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "9VYkmrFhtn5P",
	 	related_IDs : ["1830", "pdNokAEmCxfT"],
	 	categories_IDs : ["48"]
 	 }, { 
	 	name : "Jede Diktatorin stirbt mit der Zeit.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "pdNokAEmCxfT",
	 	related_IDs : ["1814", "9VYkmrFhtn5P"],
	 	categories_IDs : ["48"],
	 	tags : []
 	 }, { 
	 	name : "The king was well loved by his people.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "c9aGBVPoxAqs",
	 	related_IDs : ["1832", "lpY98YE99xW0"],
	 	categories_IDs : ["48"]
 	 }, { 
	 	name : "Der König wurde von seinem Volk geliebt.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "lpY98YE99xW0",
	 	related_IDs : ["1825", "c9aGBVPoxAqs"],
	 	categories_IDs : ["48"],
	 	tags : []
 	 }, { 
	 	name : "The prime minister feared she might lose reelections.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "6o92AqKCnvrx",
	 	related_IDs : ["1833", "uNJMzO9YJyOH"],
	 	categories_IDs : ["48"]
 	 }, { 
	 	name : "Die Ministerpräsidentin fürchtete, die Wiederwahlen zu verlieren.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "uNJMzO9YJyOH",
	 	related_IDs : ["1824", "6o92AqKCnvrx"],
	 	categories_IDs : ["48"],
	 	tags : []
 	 }, { 
	 	name : "To get to the reception just take the elevator to the fifth floor.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "HEOP6EdhUHO3",
	 	related_IDs : ["2050", "VxiTvowqQjvs"],
	 	categories_IDs : ["19"]
 	 }, { 
	 	name : "Um zur Anmeldung zu können können sie einfach den Fahrstuhl in den fünften Stock nehmen.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "VxiTvowqQjvs",
	 	related_IDs : ["2012", "HEOP6EdhUHO3"],
	 	categories_IDs : ["19"],
	 	tags : []
 	 }, { 
	 	name : "All she needed now was a hot shower to relax again.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "RQzsMLLhxorg",
	 	related_IDs : ["2055", "ndpCqAhkwZk9"],
	 	categories_IDs : ["19"]
 	 }, { 
	 	name : "Alles was sie brauchte war eine heiße Dusche, um sich wieder zu entspannen.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "ndpCqAhkwZk9",
	 	related_IDs : ["2023", "RQzsMLLhxorg"],
	 	categories_IDs : ["19"],
	 	tags : []
 	 }, { 
	 	name : "He would require a ladder to replace the defect light bulb under the ceiling.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "gvQCP0ZoJYKO",
	 	related_IDs : ["2054", "ZLBieKuJasVT"],
	 	categories_IDs : ["19"],
	 	tags : []
 	 }, { 
	 	name : "Er würde eine Leiter benötigen, um die kaputte Glühbirne an der Decke zu ersetzen.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "ZLBieKuJasVT",
	 	related_IDs : ["2027", "gvQCP0ZoJYKO"],
	 	categories_IDs : ["19"],
	 	tags : []
 	 }, { 
	 	name : "Wer ist die Person mit dem roten Schal links von der Tür?",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "FwcLhf2Dcmw8",
	 	related_IDs : ["64", "rxYDYG1liI8e"],
	 	categories_IDs : ["33"],
	 	tags : []
 	 }, { 
	 	name : "The washing machine still needs half an hour to finish.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "rVjomyVzwWfq",
	 	related_IDs : ["2060", "IoDXXSiw9jyE"],
	 	categories_IDs : ["19"]
 	 }, { 
	 	name : "Die Waschmaschine braucht noch eine halbe Stunde, um fertig zu werden.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "IoDXXSiw9jyE",
	 	related_IDs : ["2026", "rVjomyVzwWfq"],
	 	categories_IDs : ["19"],
	 	tags : []
 	 }, { 
	 	name : "Sitting on the balcony for breakfast during summer really was a pleasure.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "1rbf73hSeW6u",
	 	related_IDs : ["2093", "3WlY5YCZhmaR"],
	 	categories_IDs : ["19"],
	 	tags : []
 	 }, { 
	 	name : "Auf dem Balkon zu sitzen und zu frühstücken war wirklich eine Freude im Sommer.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "3WlY5YCZhmaR",
	 	related_IDs : ["2018", "1rbf73hSeW6u"],
	 	categories_IDs : ["19"],
	 	tags : []
 	 }, { 
	 	name : "Faith does not require proof but true belief.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "WY5qRPNuUXuI",
	 	related_IDs : ["2103", "JQ0PzyPqyFk3"],
	 	categories_IDs : ["49"],
	 	tags : []
 	 }, { 
	 	name : "Glaube braucht keinen Beweis sondern echten Glauben.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "JQ0PzyPqyFk3",
	 	related_IDs : ["1907", "WY5qRPNuUXuI"],
	 	categories_IDs : ["49"],
	 	tags : []
 	 }, { 
	 	name : "I think we moved the old couch to the cellar.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "H8oIVybyMP3Z",
	 	related_IDs : ["2096", "IyyVIyENYeZp"],
	 	categories_IDs : ["19"],
	 	tags : []
 	 }, { 
	 	name : "Ich glaube, wir haben das alte Sofa in den Keller gebracht.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "IyyVIyENYeZp",
	 	related_IDs : ["2076", "H8oIVybyMP3Z"],
	 	categories_IDs : ["19"],
	 	tags : []
 	 }, { 
	 	name : "May his soul find peace.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "QTMYs7O8wUH7",
	 	related_IDs : ["2113", "D1esrQgxMQZt"],
	 	categories_IDs : ["49"]
 	 }, { 
	 	name : "Möge seine Seele Frieden finden.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "D1esrQgxMQZt",
	 	related_IDs : ["1911", "QTMYs7O8wUH7"],
	 	categories_IDs : ["49"],
	 	tags : []
 	 }, { 
	 	name : "Meditating often helps with blood pressure issues.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "Uncv9gwaavb1",
	 	related_IDs : ["2117", "vfurY0lsSL6h"],
	 	categories_IDs : ["49"]
 	 }, { 
	 	name : "Meditieren hilft oft bei Problemen mit dem Blutdruck.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "vfurY0lsSL6h",
	 	related_IDs : ["1926", "Uncv9gwaavb1"],
	 	categories_IDs : ["49"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["40"],
	 	related_IDs : ["2229", "jXDkH1ZKGzc6", "tt0O0lDWgN7U"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the ice cream",
	 	id : "N8FalBSGX5Fs"
 	 }, { 
	 	categories_IDs : ["40", "59"],
	 	related_IDs : ["N8FalBSGX5Fs", "2229", "tt0O0lDWgN7U"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "die Eiscreme",
	 	id : "jXDkH1ZKGzc6"
 	 }, { 
	 	name : "The earthquake had destroyed more than 100 houses and about 60 people were heavily injured.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "59qyaMIYRPGd",
	 	related_IDs : ["2243", "YOsylFESuhFx"],
	 	categories_IDs : ["59"]
 	 }, { 
	 	name : "Das Erdbeben hatte mehr als einhundert Häuser zerstört und ungefähr 60 Menschen waren schwer verletzt worden.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "YOsylFESuhFx",
	 	related_IDs : ["2235", "59qyaMIYRPGd"],
	 	categories_IDs : ["59"],
	 	tags : []
 	 }, { 
	 	name : "The moon was shining softly onto the lake surface.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "l1C49RprteKJ",
	 	related_IDs : ["2249", "ijF0Fp6Fu3UZ"],
	 	categories_IDs : ["59"]
 	 }, { 
	 	name : "Der Mond schien sanft auf die Oberfläche des Sees.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "ijF0Fp6Fu3UZ",
	 	related_IDs : ["2036", "l1C49RprteKJ"],
	 	categories_IDs : ["59"],
	 	tags : []
 	 }, { 
	 	name : "Our solar system is home to eight planets.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "LQ6iPKnNG6yA",
	 	related_IDs : ["2250", "lsdS23Q8DMEo"],
	 	categories_IDs : ["59"]
 	 }, { 
	 	name : "Unser Sonnensystem ist die Heimat von acht Planeten.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "lsdS23Q8DMEo",
	 	related_IDs : ["2035", "LQ6iPKnNG6yA"],
	 	categories_IDs : ["59"],
	 	tags : []
 	 }, { 
	 	name : "The landscape had been shaped by the river's winding  path.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "dnZOw8kVJUfy",
	 	related_IDs : ["2247", "D53WkvETV72d"],
	 	categories_IDs : ["2756"],
	 	tags : []
 	 }, { 
	 	name : "Die Landschaft war geprägt von dem sich windenden Verlauf des Flusses.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "D53WkvETV72d",
	 	related_IDs : ["2037", "dnZOw8kVJUfy"],
	 	categories_IDs : ["2756"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["57", "61"],
	 	related_IDs : ["1995", "fOXJ9imgXcTk"],
	 	tags : [],
	 	name : "Mein neuester Kunde war ein älterer Mann auf der Suche nach seiner grauen Katze.",
	 	sentence : true,
	 	language_ID : "2",
	 	id : "lYRdtSvrVPD1"
 	 }, { 
	 	name : "The cherry trees were offering a colourful sea of white and pink.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "IAZDDIKTey8V",
	 	related_IDs : ["2384", "KXZVUnLiAC8G"],
	 	categories_IDs : ["56"],
	 	tags : []
 	 }, { 
	 	name : "Die Kirschbäume boten ein farbenfrohes Meer aus Weiß und Rosa.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "KXZVUnLiAC8G",
	 	related_IDs : ["1987", "IAZDDIKTey8V"],
	 	categories_IDs : ["56"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["40"],
	 	related_IDs : ["F0Y6nMQUzfxu"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the cherry",
	 	id : "2KA6aMvf7eaZ"
 	 }, { 
	 	categories_IDs : ["40"],
	 	related_IDs : ["2KA6aMvf7eaZ"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "die Kirsche",
	 	id : "F0Y6nMQUzfxu"
 	 }, { 
	 	name : "The trunk of the tree was covered by dark green moss.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "G8NRbc3anvrn",
	 	related_IDs : ["2386", "inbvtBsa6dyU"],
	 	categories_IDs : ["56"]
 	 }, { 
	 	name : "Der Baumstamm war von dunkelgrünem Moos bedeckt.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "inbvtBsa6dyU",
	 	related_IDs : ["1978", "G8NRbc3anvrn"],
	 	categories_IDs : ["56"],
	 	tags : []
 	 }, { 
	 	name : "He had not watered the flowers for weeks and now they were slowly rotting away.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "JGUg7eMxzptN",
	 	related_IDs : ["2398", "61PXgQhVZZop", "k8IeIwOK4ZWN"],
	 	categories_IDs : ["56"]
 	 }, { 
	 	name : "Er hatte die Blumen seit Wochen nicht gegossen und jetzt verrotteten sie langsam.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "61PXgQhVZZop",
	 	related_IDs : ["1982", "JGUg7eMxzptN", "1990", "1973", "691", "yRmGf3bK5xbd"],
	 	categories_IDs : ["56", "39"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["39"],
	 	related_IDs : ["JGUg7eMxzptN", "yRmGf3bK5xbd", "AgHDXFtWd2U9", "8eYnZ1B2pOCL"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "now",
	 	id : "k8IeIwOK4ZWN"
 	 }, { 
	 	categories_IDs : ["39"],
	 	related_IDs : ["61PXgQhVZZop", "k8IeIwOK4ZWN", "AgHDXFtWd2U9", "8eYnZ1B2pOCL"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "jetzt",
	 	id : "yRmGf3bK5xbd"
 	 }, { 
	 	categories_IDs : ["39"],
	 	related_IDs : ["k8IeIwOK4ZWN", "yRmGf3bK5xbd", "8eYnZ1B2pOCL"],
	 	tags : [],
	 	language_ID : "5",
	 	name : "ahora",
	 	id : "AgHDXFtWd2U9"
 	 }, { 
	 	categories_IDs : ["39"],
	 	related_IDs : ["yRmGf3bK5xbd", "k8IeIwOK4ZWN", "AgHDXFtWd2U9"],
	 	tags : [],
	 	language_ID : "1",
	 	name : "nu",
	 	id : "8eYnZ1B2pOCL"
 	 }, { 
	 	name : "The leaves were ripped off the oaks by strong autumn winds.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "WddRjLh6YU3w",
	 	related_IDs : ["2381", "TZ4FOvESTSij"],
	 	categories_IDs : ["56"]
 	 }, { 
	 	name : "Die Blätter wurden durch starke Herbstwinde von den Eichen abgerissen.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "TZ4FOvESTSij",
	 	related_IDs : ["1976", "WddRjLh6YU3w"],
	 	categories_IDs : ["56"],
	 	tags : []
 	 }, { 
	 	name : "Most of these apples were grown in Italy.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "v0H2LsfblYNz",
	 	related_IDs : ["2400", "CYkhmMrVvP9B"],
	 	categories_IDs : ["56"]
 	 }, { 
	 	categories_IDs : ["35", "40"],
	 	related_IDs : ["SDqafJlyRGBP", "t5Ah5DRRZACn"],
	 	tags : [],
	 	language_ID : "1",
	 	name : "sätta",
	 	formsSecondary : ["satte", "satt"],
	 	formsSecondaryNames : ["Preterium", "Supinum"],
	 	id : "Mn8vx5Fr2rm4"
 	 }, { 
	 	categories_IDs : ["35", "40"],
	 	related_IDs : ["Mn8vx5Fr2rm4", "t5Ah5DRRZACn"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "stellen",
	 	id : "SDqafJlyRGBP"
 	 }, { 
	 	categories_IDs : ["35", "40"],
	 	related_IDs : ["Mn8vx5Fr2rm4", "SDqafJlyRGBP"],
	 	tags : [],
	 	name : "put",
	 	language_ID : "3",
	 	id : "t5Ah5DRRZACn"
 	 }, { 
	 	related_IDs : ["0Nsv7I0CXix6", "15b7wM3O32KO"],
	 	categories_IDs : ["24"],
	 	responses_IDs : [],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the sage",
	 	id : "Z0jXULzwM6tf"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["Z0jXULzwM6tf"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "der Weise",
	 	formsPrimary_IDs : ["15b7wM3O32KO"],
	 	id : "0Nsv7I0CXix6",
	 	formPrimaryName : "männlich"
 	 }, { 
	 	name : "die Weise",
	 	id : "15b7wM3O32KO",
	 	formPrimaryName : "weiblich",
	 	categories_IDs : ["24"],
	 	language_ID : "2",
	 	formsPrimary_IDs : ["0Nsv7I0CXix6"],
	 	related_IDs : ["Z0jXULzwM6tf"]
 	 }, { 
	 	name : "Am Mittag war die Sonne hinter den Wolken versteckt.",
	 	language_ID : "2",
	 	categories_IDs : ["59"],
	 	sentence : true,
	 	related_IDs : ["3287"],
	 	id : "QPAp1Vim2Qc5"
 	 }, { 
	 	name : "In welchem Land bist du geboren?",
	 	language_ID : "2",
	 	categories_IDs : ["41"],
	 	sentence : true,
	 	related_IDs : ["3290"],
	 	id : "NTK4zlTIhBcp"
 	 }, { 
	 	name : "Einige Tiere wie Elche bevorzugen den tiefen Wald.",
	 	language_ID : "2",
	 	categories_IDs : ["2756"],
	 	sentence : true,
	 	related_IDs : ["pvm6orYu9k11"],
	 	id : "LFTqo8s1HUEC"
 	 }, { 
	 	name : "Warum hast du das getan?",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	sentence : true,
	 	related_IDs : ["JoG040wLz4sX"],
	 	id : "x3I4WmfusD39"
 	 }, { 
	 	name : "Wann wollen wir uns auf einen Kaffee treffen?",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	sentence : true,
	 	related_IDs : ["bbE7NynxaUEi"],
	 	id : "WIo0BQBbstaE"
 	 }, { 
	 	name : "Wie würdest du den Film in einem Satz beschreiben?",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	sentence : true,
	 	related_IDs : ["jABkXYb127Ht"],
	 	id : "gAMhmCxrvW04"
 	 }, { 
	 	name : "Ja, das klingt nach einer passenden Möglichkeit, die Dateien zu sortieren.",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	sentence : true,
	 	related_IDs : ["nxFluqsNeOuP"],
	 	id : "lj36tyliEc2B"
 	 }, { 
	 	name : "Nein, ich kann mir nicht vorstellen, dass er das gesagt hat!",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	sentence : true,
	 	related_IDs : ["cM4heQAHVUQT"],
	 	id : "8v3ptKjbC4ND"
 	 }, { 
	 	name : "Big Bands haben oft mehr als zehn Mitglieder.",
	 	language_ID : "2",
	 	categories_IDs : ["45", "60"],
	 	sentence : true,
	 	related_IDs : ["9BXJuuT2mYR4"],
	 	id : "3ATF3zNuVdiD"
 	 }, { 
	 	name : "Gib mir die Kurzform, bitte.",
	 	language_ID : "2",
	 	categories_IDs : ["45", "60"],
	 	sentence : true,
	 	related_IDs : ["uiYPoNsP9Orw"],
	 	id : "NmQRMGzOscAd"
 	 }, { 
	 	name : "Die Banane war vom Altern braun geworden.",
	 	language_ID : "2",
	 	categories_IDs : ["45", "60"],
	 	sentence : true,
	 	related_IDs : ["Q0JOzDvAItFx"],
	 	id : "ReqfRTFC50Vv"
 	 }, { 
	 	name : "Wenn ich traurig bin, versuche ich, zeit mit guten Freunden zu verbringen.",
	 	language_ID : "2",
	 	categories_IDs : ["45", "60"],
	 	sentence : true,
	 	related_IDs : ["nvoHYN3Whtfl"],
	 	id : "rCbGzaNw7WzN"
 	 }, { 
	 	name : "Am Nachmittag war der See tiefblau wegen dem wolkenfreien Himmel.",
	 	language_ID : "2",
	 	categories_IDs : ["45", "60"],
	 	sentence : true,
	 	related_IDs : ["ijEgwjqlizr5"],
	 	id : "sirDuqmxqAV5"
 	 }, { 
	 	name : "Hast du Lust, frischen Apfelsaft zu machen?",
	 	language_ID : "2",
	 	categories_IDs : ["40"],
	 	sentence : true,
	 	related_IDs : ["6kRynX9bmtma"],
	 	id : "7lIwUOXdMbYa"
 	 }, { 
	 	name : "Weißweinessig kann benutzt werden, um grünen Salat anzumachen.",
	 	language_ID : "2",
	 	categories_IDs : ["45", "60"],
	 	sentence : true,
	 	related_IDs : ["k1V3SDu9bmft"],
	 	id : "uB0c4B3PNDj2"
 	 }, { 
	 	name : "Einige Kulturen nutzen weiße Kleidung, um Trauer auszudrücken, andere nutzen Schwarz.",
	 	language_ID : "2",
	 	categories_IDs : ["45", "60"],
	 	sentence : true,
	 	related_IDs : ["n21VuZLdDydW"],
	 	id : "WdEcBRMb0Fed"
 	 }, { 
	 	name : "Das Fahrrad zu reparieren kostete viel Zeit.",
	 	language_ID : "2",
	 	categories_IDs : ["45", "60"],
	 	sentence : true,
	 	related_IDs : ["x76cEC3LdZt2"],
	 	id : "9Fd9fPDmsfcA"
 	 }, { 
	 	name : "Beim Zwiebeln schneiden muss ich immer schneiden.",
	 	language_ID : "2",
	 	categories_IDs : ["40"],
	 	sentence : true,
	 	related_IDs : ["zZXRu1xNRIbf"],
	 	id : "1Yqhds6QYrsA"
 	 }, { 
	 	name : "Bitte stell sicher, dass die Kinder bei Sonnenuntergang wieder zu Hause sind.",
	 	language_ID : "2",
	 	categories_IDs : ["34", "63"],
	 	sentence : true,
	 	related_IDs : ["COlaiIjNXzag"],
	 	id : "zjDoEZqfI3P1",
	 	tags : []
 	 }, { 
	 	name : "Ich muss ein bisschen Schokolade essen um meine Nerven zu beruhigen.",
	 	language_ID : "2",
	 	categories_IDs : ["35"],
	 	sentence : true,
	 	related_IDs : ["8NZmApTLoDwr"],
	 	id : "nGq5Q86Vk5X9"
 	 }, { 
	 	name : "Brauchst du ein Handtuch oder hast du eins mitgebracht?",
	 	language_ID : "2",
	 	categories_IDs : ["35"],
	 	sentence : true,
	 	related_IDs : ["m2SPLXssEXqh"],
	 	id : "bghilco9jaHj"
 	 }, { 
	 	name : "Entschuldigung, kannst du mir beim Abendessen helfen? Ich brauche jemanden, der die Paprika für den Salat schneidet.",
	 	language_ID : "2",
	 	categories_IDs : ["35", "63"],
	 	sentence : true,
	 	related_IDs : ["1n6nLUTDt5my", "iyk7RNSbYeOX"],
	 	id : "KECjuk6zFkce"
 	 }, { 
	 	name : "Sie legten sich am Strand in den Sand.",
	 	language_ID : "2",
	 	categories_IDs : ["35"],
	 	sentence : true,
	 	related_IDs : ["itaj5phGE7DZ"],
	 	id : "eFIRRMfO6z3x"
 	 }, { 
	 	name : "Sie hatten gerade die Waffen niederlegen wollen als der Waffenstillstand gebrochen wurde.",
	 	language_ID : "2",
	 	categories_IDs : ["35", "61"],
	 	sentence : true,
	 	related_IDs : ["eidFO6UeF8eY"],
	 	id : "6XRUGmXSLoam"
 	 }, { 
	 	name : "Fahren Sie geradeaus am Meer entlang, um zum Veranstaltungsort zu kommen.",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	sentence : true,
	 	related_IDs : ["vSdF9cgCICK0"],
	 	id : "xmZzKp8E8VFa"
 	 }, { 
	 	name : "Die Gründe für sein Handeln waren komplex.",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	sentence : true,
	 	related_IDs : ["S4zXcilJKrkP"],
	 	id : "gN2ysnigezWO"
 	 }, { 
	 	name : "Sie können einen Besen hinter der Theke finden.",
	 	language_ID : "2",
	 	categories_IDs : ["38"],
	 	sentence : true,
	 	related_IDs : ["1WlJ9m1EaBRn"],
	 	id : "q6097vFxIcPQ"
 	 }, { 
	 	name : "I liebe japanisches Essen.",
	 	language_ID : "2",
	 	categories_IDs : ["36"],
	 	sentence : true,
	 	related_IDs : ["pGhKZWyE0sDp"],
	 	id : "V7GZmXn0NCMB"
 	 }, { 
	 	name : "die Fee",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["ePJ3kGEJDYJr"],
	 	id : "Phtg3VoiLJLP"
 	 }, { 
	 	name : "der Troll",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["V5iOzpfAWKo1"],
	 	id : "63H5bE420zCn",
	 	tags : []
 	 }, { 
	 	name : "das Schloss",
	 	language_ID : "2",
	 	categories_IDs : ["19", "24"],
	 	related_IDs : ["uHHbZhQTatHH"],
	 	id : "wHrC5OPjnsH8"
 	 }, { 
	 	name : "der Schatz",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["SemqRM4TLqiC"],
	 	id : "Hzr0IMz9PMVm"
 	 }, { 
	 	name : "der Drache",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["bIvLmjCW2eMg"],
	 	id : "OitC7CCM4NYb"
 	 }, { 
	 	name : "der Geist",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["GvkWSTKVzLDf", "UWeizO1Yg1WD"],
	 	id : "4Hi7tCseSXmc"
 	 }, { 
	 	name : "der Weltraumpirat",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["VX0DWZDhnWO4"],
	 	id : "4fAgTK1kjoLy"
 	 }, { 
	 	name : "der Ritter",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["ZFmdzkGUupUZ"],
	 	id : "wA3sGZ8JFUR7"
 	 }, { 
	 	name : "der Älteste",
	 	language_ID : "2",
	 	categories_IDs : ["24", "49"],
	 	related_IDs : ["RCpvgjX70Gc6"],
	 	id : "pq8BmW9PVDR2"
 	 }, { 
	 	name : "die Zwergin",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["dH7i8MAuxUdj"],
	 	id : "72rUMbDbsCoj"
 	 }, { 
	 	name : "die Weisssagerin",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["u4VJukuMmF6i"],
	 	id : "1xpm6qalxURh"
 	 }, { 
	 	name : "die Jägerin",
	 	language_ID : "2",
	 	categories_IDs : ["24", "61"],
	 	related_IDs : ["ksIKUIr0Vfqu"],
	 	id : "Z8BGVI3NLMa7"
 	 }, { 
	 	name : "der Jäger",
	 	language_ID : "2",
	 	categories_IDs : ["24", "61"],
	 	related_IDs : ["ARJUwxtCiAEK"],
	 	id : "f04Lkn5UTD9d"
 	 }, { 
	 	name : "der Räuber",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["t9dIJ156jLRj"],
	 	id : "XAutbmVdBHbw"
 	 }, { 
	 	name : "der Vampir",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["rOywSaTvrz2e"],
	 	id : "HR8QmBvBdNLv"
 	 }, { 
	 	name : "das Monster",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["3jgF6CGJd23y"],
	 	id : "l8wJOAiB7Azq"
 	 }, { 
	 	name : "der Prophet",
	 	language_ID : "2",
	 	categories_IDs : ["24", "49"],
	 	related_IDs : ["PwbKWvHCYqcH"],
	 	id : "vCoqhO0KQPlm"
 	 }, { 
	 	name : "der Zauberer",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["J1ctcnSAqQq1"],
	 	id : "8Bh7qxaTNc5E"
 	 }, { 
	 	name : "der Dieb",
	 	language_ID : "2",
	 	categories_IDs : ["24", "61"],
	 	related_IDs : ["x7440eZ5zzUp"],
	 	id : "5EhxIwkWrZn0"
 	 }, { 
	 	name : "der Dämon",
	 	language_ID : "2",
	 	categories_IDs : ["24", "49"],
	 	related_IDs : ["kQjXfQGKtP4L"],
	 	id : "fm9xMxat4udn"
 	 }, { 
	 	name : "das Einhorn",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["Z1Qw9rS9FQHg"],
	 	id : "NqTqkX4mb27R"
 	 }, { 
	 	name : "der Elf",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["DHqbKYJputrB"],
	 	id : "GpWYT9A9C8Ph"
 	 }, { 
	 	name : "die Werwölfin",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["Why9eulWFLOO"],
	 	id : "DMzsutVoQceb"
 	 }, { 
	 	name : "der Golem",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["YBsSa0oRcMeA"],
	 	id : "JsfkNDOgMDtx"
 	 }, { 
	 	name : "der Riese",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["pNXGZiJYOMgQ"],
	 	id : "o7aP7vO0V127"
 	 }, { 
	 	name : "das Waisenkind",
	 	language_ID : "2",
	 	categories_IDs : ["63"],
	 	related_IDs : ["rXP5XZA7TOQh"],
	 	id : "5S3Dg95S6hE0"
 	 }, { 
	 	name : "der Elefant",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["grXFiBOTMNbd"],
	 	id : "31JNjKcDIZ0Y"
 	 }, { 
	 	name : "der Untote",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["tmQ0dmcge7Dk"],
	 	id : "U6Eh7ID6mRmU"
 	 }, { 
	 	name : "der Sensenmann",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["pO16WbY3lNHx"],
	 	id : "3rc7hNvMZIWU"
 	 }, { 
	 	name : "Die Hexe verfluchte die ignorante Frau, sodass sie blind wurde.",
	 	language_ID : "2",
	 	categories_IDs : ["24", "35"],
	 	sentence : true,
	 	related_IDs : ["eInuOquqdKlo"],
	 	id : "DE1XqUpsjFki"
 	 }, { 
	 	name : "verfluchen",
	 	language_ID : "2",
	 	categories_IDs : ["24", "35"],
	 	related_IDs : ["C9zbHQmnCj1q"],
	 	id : "eVTGllE0ODw3"
 	 }, { 
	 	name : "Der Zug nach Wien wird sich um ungefähr fünfzehn Minuten verspäten.",
	 	language_ID : "2",
	 	categories_IDs : ["55", "42"],
	 	sentence : true,
	 	related_IDs : ["nKlejMrLYmlj"],
	 	id : "RDdJjt3PGMdJ"
 	 }, { 
	 	name : "Der Sommer kann nicht früh genug kommen.",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	sentence : true,
	 	related_IDs : ["lYM6rEKlMF00"],
	 	id : "j07sptMYr6Ic"
 	 }, { 
	 	name : "Ich habe sie vorhin auf ihrem Weg zum Kino gesehen, warum?",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	sentence : true,
	 	related_IDs : ["L3N18n8pPvQy"],
	 	id : "gSeAfk3PbCOI"
 	 }, { 
	 	name : "Ich glaube meine Tastatur ist kapuut, einige Tasten funktionieren seit zwei Tagen nicht mehr.",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	sentence : true,
	 	related_IDs : ["Csn80o3nIXbR"],
	 	id : "CTPSX1Q5LdW8"
 	 }, { 
	 	name : "Lass uns eine Pause machen. Ich kann mich da nicht mehr drauf konzentrieren.",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	sentence : true,
	 	related_IDs : ["BIu1cZPihXQy"],
	 	id : "xjHXXpsuDdPa"
 	 }, { 
	 	name : "Der Arzt bat sie, ihren linken Unterarm anzuheben.",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	sentence : true,
	 	related_IDs : ["I7u5rYQCUvvh"],
	 	id : "9fd48tuSVC2a"
 	 }, { 
	 	name : "Wenn sie wirklich Anwältin werden wollte, würde sie sich durch viele Rechtsbücher durcharbeiten müssen.",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	sentence : true,
	 	related_IDs : ["6jRmQcUJR1dU"],
	 	id : "4UkoAtLzG1s9"
 	 }, { 
	 	name : "Er stand plötzlich auf und ging zum Kühlschrank.",
	 	language_ID : "2",
	 	categories_IDs : ["39"],
	 	sentence : true,
	 	related_IDs : ["Srv3VLlPKcJG"],
	 	id : "uGbyAXfZGFvW"
 	 }, { 
	 	name : "Ich sollte öfters Schwimmen gehen, es fühlt sich einfach so gut an.",
	 	language_ID : "2",
	 	categories_IDs : ["42"],
	 	sentence : true,
	 	related_IDs : ["hzrVmbKR7pQp"],
	 	id : "fePBkGSEfwOd"
 	 }, { 
	 	name : "Diese Wette verlierst du!",
	 	language_ID : "2",
	 	categories_IDs : ["42"],
	 	sentence : true,
	 	related_IDs : ["4vyQrhoTvwyQ"],
	 	id : "5KAVGpmlTCDS"
 	 }, { 
	 	name : "Die Schwester überprüfte den Blutdruck der Patientin.",
	 	language_ID : "2",
	 	categories_IDs : ["61"],
	 	sentence : true,
	 	related_IDs : ["o2MS1I28761L"],
	 	id : "L5JrWlhKjuu6"
 	 }, { 
	 	name : "Der Manschaftskapitän wurde zehn Meter vor dem Tor durch ein Foul gestoppt.",
	 	language_ID : "2",
	 	categories_IDs : ["42"],
	 	sentence : true,
	 	related_IDs : ["eWTbPSMJpkMF"],
	 	id : "5ZddtCs8tHHM"
 	 }, { 
	 	name : "Ich bin wirklich glücklich darüber, ein paar Stunden Ruhe zu haben.",
	 	language_ID : "2",
	 	categories_IDs : ["43"],
	 	sentence : true,
	 	related_IDs : ["xeNAtBikFlUR"],
	 	id : "nYfk7dABntLm"
 	 }, { 
	 	name : "Der Gedanke, gehen zu müssen, machte sie sehr traurig.",
	 	language_ID : "2",
	 	categories_IDs : ["43"],
	 	sentence : true,
	 	related_IDs : ["CozUsivICGV7"],
	 	id : "jpLRQdULYgIy"
 	 }, { 
	 	name : "Sie konnte spüren, wie ihre Freundin vor Wut kochte.",
	 	language_ID : "2",
	 	categories_IDs : ["43"],
	 	sentence : true,
	 	related_IDs : ["sR85kNXY8xR3"],
	 	id : "5u0IjlSNSKgL"
 	 }, { 
	 	name : "Sie war von Trauer übermannt, nachdem sie ihren Mann und ihre Mutter bei dem Autounfall verloren hatte.",
	 	language_ID : "2",
	 	categories_IDs : ["43"],
	 	sentence : true,
	 	related_IDs : ["uieOnYFRF3s5"],
	 	id : "LKjENYbxVFNT"
 	 }, { 
	 	name : "Hoffnung ist der Schlüssel zur Zukunft. Aber nur, wenn man entsprechend handelt.",
	 	language_ID : "2",
	 	categories_IDs : ["43"],
	 	sentence : true,
	 	related_IDs : ["OV3l2opFocOy"],
	 	id : "i0uhLOOQTSDs"
 	 }, { 
	 	name : "Dieses Spiel zu verlieren war für die Fans eine Riesenenttäuschung.",
	 	language_ID : "2",
	 	categories_IDs : ["43"],
	 	sentence : true,
	 	related_IDs : ["fxxEdDvOuEBQ"],
	 	id : "Vtbf0CLgOGfG"
 	 }, { 
	 	name : "Nach einer Stunde in dem Buch lesen wurde ihr wirklich langsam langweilig.",
	 	language_ID : "2",
	 	categories_IDs : ["43"],
	 	sentence : true,
	 	related_IDs : ["4eMh9C0aOsyk"],
	 	id : "tXSPyL4eG0Wg"
 	 }, { 
	 	name : "Er verschränkte seine Arme weil er wirklich nicht glücklich darüber war, was seine Mutter gerade gesagt hatte.",
	 	language_ID : "2",
	 	categories_IDs : ["44"],
	 	sentence : true,
	 	related_IDs : ["fovqnxLUd0X5"],
	 	id : "YGSokkHPVzyU"
 	 }, { 
	 	name : "Ich freue mich schon sehr darauf nicht mehr krank zu sein.",
	 	language_ID : "2",
	 	categories_IDs : ["44"],
	 	sentence : true,
	 	related_IDs : ["6plnLSArTzq4"],
	 	id : "jy18H8pKBEIj"
 	 }, { 
	 	name : "Liebe hilft uns, mit Schmerz und Leiden umzugehen.",
	 	language_ID : "2",
	 	categories_IDs : ["63"],
	 	sentence : true,
	 	related_IDs : ["4kQ4o4ms0lcK"],
	 	id : "z9oFYLlsJ1X7"
 	 }, { 
	 	name : "Wir planen, im Herbst zu heiraten. Irgendwann um September rum.",
	 	language_ID : "2",
	 	categories_IDs : ["35", "63"],
	 	sentence : true,
	 	related_IDs : ["mYXBzmhOitST"],
	 	id : "Ah1rLFCcspFx"
 	 }, { 
	 	name : "heiraten",
	 	language_ID : "2",
	 	categories_IDs : ["35", "63"],
	 	related_IDs : ["95Zyf0S4HfIo"],
	 	id : "vE4ZURwHg6Jw"
 	 }, { 
	 	name : "der Zombie",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["XIDf3pw0Ds3p"],
	 	id : "X4HACz6soAOG"
 	 }, { 
	 	name : "Die meisten dieser Äpfel kommen aus Italien.",
	 	language_ID : "2",
	 	categories_IDs : ["56"],
	 	sentence : true,
	 	related_IDs : ["v0H2LsfblYNz"],
	 	id : "CYkhmMrVvP9B"
 	 }, { 
	 	name : "She was training to become a car mechanic.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "DNSJVCqKCQa2",
	 	related_IDs : ["2626", "mBwuIiIsWcBC"],
	 	categories_IDs : ["55", "61"],
	 	tags : []
 	 }, { 
	 	name : "Sie machte eine Ausbildung zur KFZ-Mechanikerin.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "mBwuIiIsWcBC",
	 	related_IDs : ["1962", "DNSJVCqKCQa2"],
	 	categories_IDs : ["55", "61"],
	 	tags : []
 	 }, { 
	 	name : "Dieser Baum darf nicht gefällt werden.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "ZR8JxU9gnRDL",
	 	related_IDs : ["GYTf3REiIUjV"],
	 	categories_IDs : ["35", "56", "61"],
	 	tags : []
 	 }, { 
	 	related_IDs : ["ZR8JxU9gnRDL", "YPdXGN20tqWz"],
	 	categories_IDs : ["35", "56", "61"],
	 	responses_IDs : [],
	 	tags : [],
	 	language_ID : "2",
	 	name : "fällen",
	 	id : "GYTf3REiIUjV"
 	 }, { 
	 	name : "Die Entscheidung muss bis spätestens nächsten Mittwoch gefällt sein.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "YPdXGN20tqWz",
	 	related_IDs : ["GYTf3REiIUjV"],
	 	categories_IDs : ["35", "56", "61"]
 	 }, { 
	 	categories_IDs : ["35", "61", "43", "40", "63"],
	 	related_IDs : ["cSl2mH33yydT", "KECjuk6zFkce", "u9yH7rIKSeBv"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "schneiden",
	 	id : "iyk7RNSbYeOX"
 	 }, { 
	 	categories_IDs : ["35", "61", "43", "40", "63"],
	 	related_IDs : ["iyk7RNSbYeOX", "1n6nLUTDt5my"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "cut",
	 	formPrimaryName : "",
	 	id : "u9yH7rIKSeBv"
 	 }, { 
	 	name : "el gelato",
	 	language_ID : "1",
	 	categories_IDs : ["40", "59"],
	 	related_IDs : ["jXDkH1ZKGzc6", "N8FalBSGX5Fs", "2229"],
	 	id : "tt0O0lDWgN7U"
 	 }, { 
	 	related_IDs : ["2417", "349", "992"],
	 	categories_IDs : ["53", "45", "60", "41"],
	 	responses_IDs : [],
	 	tags : [],
	 	language_ID : "3",
	 	name : "This painting shows the green hills above an ancient city.",
	 	sentence : true,
	 	id : "T0EkiFW0NdZn"
 	 }, { 
	 	related_IDs : ["2487"],
	 	categories_IDs : ["34", "53"],
	 	responses_IDs : [],
	 	tags : [],
	 	language_ID : "3",
	 	name : "He played the piano remarkably well for someone his age.",
	 	sentence : true,
	 	id : "5K5gh1Zz3feC"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["XIDf3pw0Ds3p"],
	 	tags : [],
	 	name : "Repairing the barricade would mean they were safe from the zombies again.",
	 	sentence : true,
	 	language_ID : "3",
	 	id : "YJiENY0csahg"
 	 }, { 
	 	categories_IDs : ["34", "53", "41", "37"],
	 	related_IDs : ["1001", "257", "264", "4BqMuQn8APNP"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "The museum is open to the public from nine till sixteen o'clock.",
	 	sentence : true,
	 	id : "lzxJwHxP7qNQ"
 	 }, { 
	 	categories_IDs : ["34", "53"],
	 	related_IDs : ["476", "2421", "2431"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "She took the brush and began to paint her next masterpiece.",
	 	sentence : true,
	 	id : "uD4zfeN3DfEA"
 	 }, { 
	 	categories_IDs : ["34", "53", "46"],
	 	related_IDs : ["1586"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "His latest book was a thriller and the story mainly took place within a small town in Denmark.",
	 	sentence : true,
	 	id : "ppT8EIICVdVs"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["9tDcHoMZTZk1"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the story",
	 	id : "ErpPl2jkO4tA"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["ErpPl2jkO4tA"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "die Geschichte",
	 	id : "9tDcHoMZTZk1"
 	 }, { 
	 	categories_IDs : ["39", "40", "46"],
	 	related_IDs : ["367"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "The barbecue tomorrow is going to be great. I'll bring salad.",
	 	id : "hOKBaLEHhhPv"
 	 }, { 
	 	categories_IDs : ["34", "46"],
	 	related_IDs : ["1579"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "Were you at the neighbours party last week?",
	 	id : "jm2iiEMOvfHO",
	 	sentence : true
 	 }, { 
	 	categories_IDs : ["19", "24", "34"],
	 	related_IDs : ["3jgF6CGJd23y", "2051", "jZpK6TXtspIf"],
	 	tags : [],
	 	name : "At the top of the burning tower the monster was waiting for rescue.",
	 	sentence : true,
	 	language_ID : "3",
	 	id : "FEbJ48RFZ6gq"
 	 }, { 
	 	categories_IDs : ["19", "24", "35", "39"],
	 	related_IDs : ["lYM6rEKlMF00", "FEbJ48RFZ6gq", "g1PnAwNBYOTH"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "wait",
	 	id : "jZpK6TXtspIf"
 	 }, { 
	 	categories_IDs : ["35", "19", "24", "39"],
	 	related_IDs : ["jZpK6TXtspIf"],
	 	tags : [],
	 	name : "warten",
	 	language_ID : "2",
	 	id : "g1PnAwNBYOTH"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["ZB2PxFrIphJk"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the snake",
	 	id : "eKpoDG6xUpMH"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["eKpoDG6xUpMH"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "die Schlange",
	 	id : "ZB2PxFrIphJk"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["uIkdLAIDRuF3"],
	 	tags : [],
	 	name : "the dolphin",
	 	language_ID : "3",
	 	id : "8JNZxD1oLOHA"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["1qpeU8tBSi1H"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the whale",
	 	id : "y8KEZuh2TZ1P"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["XZto2JkaPyZf"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the shark",
	 	id : "mTVFNOtooFbz"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["0BlPXU8qKrue"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the lizard",
	 	id : "jKk4EcOCtoLu"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["PKnViN6d7ap4"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the cow",
	 	id : "7uTTQO3gq71F"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["T8z6cnuAbj3H"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the pig",
	 	id : "TIor9xliVUIN"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["6r68nbwfhgal"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the llama",
	 	id : "XpO3C1lgffAA"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["PW5an5clfjgd"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the fox",
	 	id : "sMbdX8CJeuhy"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["Ko0wy64RP07G"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the sloth",
	 	id : "wMeC2pzstGK4"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["ZvYEKSoEaJWg"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the carp",
	 	id : "z5YFakioC9sT"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["D3GQHM9PSnq3"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the fly",
	 	id : "bQUIQlXtqduj"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["X1xdyrpNublk"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the goblin",
	 	id : "qT2wqqBxzov8"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["G9V5Xh0HL9hn"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the goblet",
	 	id : "YDpYVHtYy7km"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["YDpYVHtYy7km"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "der Kelch",
	 	id : "G9V5Xh0HL9hn"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["9GxJO3WiXYaN"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the eagle",
	 	id : "UEMNMUN8TWRn"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["nEvT5o6yEY5C"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the dove",
	 	id : "CZwF3U3aIZBM"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["wV3gFdN6WdA9"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the vulture",
	 	id : "JXLIkQ29YTdR"
 	 }, { 
	 	name : "der Delfin",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["8JNZxD1oLOHA"],
	 	id : "uIkdLAIDRuF3",
	 	tags : []
 	 }, { 
	 	name : "der Wal",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["y8KEZuh2TZ1P"],
	 	id : "1qpeU8tBSi1H",
	 	tags : []
 	 }, { 
	 	name : "der Hai",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["mTVFNOtooFbz"],
	 	id : "XZto2JkaPyZf",
	 	tags : []
 	 }, { 
	 	name : "die Echse",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["jKk4EcOCtoLu"],
	 	id : "0BlPXU8qKrue",
	 	tags : []
 	 }, { 
	 	name : "die Kuh",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["7uTTQO3gq71F"],
	 	id : "PKnViN6d7ap4",
	 	tags : []
 	 }, { 
	 	name : "das Schwein",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["TIor9xliVUIN"],
	 	id : "T8z6cnuAbj3H",
	 	tags : []
 	 }, { 
	 	name : "das Lama",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["XpO3C1lgffAA"],
	 	id : "6r68nbwfhgal",
	 	tags : []
 	 }, { 
	 	name : "der Fuchs",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["sMbdX8CJeuhy"],
	 	id : "PW5an5clfjgd",
	 	tags : []
 	 }, { 
	 	name : "das Faultier",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["wMeC2pzstGK4"],
	 	id : "Ko0wy64RP07G",
	 	tags : []
 	 }, { 
	 	name : "der Karpfen",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["z5YFakioC9sT"],
	 	id : "ZvYEKSoEaJWg",
	 	tags : []
 	 }, { 
	 	name : "die Fliege",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["bQUIQlXtqduj"],
	 	id : "D3GQHM9PSnq3",
	 	tags : []
 	 }, { 
	 	name : "der Goblin",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["qT2wqqBxzov8"],
	 	id : "X1xdyrpNublk",
	 	tags : []
 	 }, { 
	 	name : "der Adler",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["UEMNMUN8TWRn"],
	 	id : "9GxJO3WiXYaN",
	 	tags : []
 	 }, { 
	 	name : "die Taube",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["CZwF3U3aIZBM"],
	 	id : "nEvT5o6yEY5C",
	 	tags : []
 	 }, { 
	 	name : "der Geier",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["JXLIkQ29YTdR"],
	 	id : "wV3gFdN6WdA9",
	 	tags : []
 	 }, { 
	 	name : "Das Museum ist zwischen neun und sechzehn Uhr für die Allgemeinheit geöffnet.",
	 	language_ID : "2",
	 	categories_IDs : ["34", "53", "41", "37"],
	 	sentence : true,
	 	related_IDs : ["lzxJwHxP7qNQ"],
	 	id : "4BqMuQn8APNP",
	 	tags : []
 	 }, { 
	 	name : "What do you mean?",
	 	language_ID : "3",
	 	categories_IDs : ["34"],
	 	sentence : true,
	 	related_IDs : ["1133", "1134"],
	 	id : "vO6qE1RQZaeY"
 	 }, { 
	 	name : "because",
	 	language_ID : "3",
	 	categories_IDs : ["33"],
	 	related_IDs : ["1245", "1246", "1726", "1250"],
	 	id : "QUBj22S8UA2i"
 	 }, { 
	 	name : "the backpack",
	 	language_ID : "3",
	 	categories_IDs : ["41"],
	 	related_IDs : ["2892"],
	 	id : "vGdOuUzBgsNR"
 	 }, { 
	 	name : "the bus",
	 	language_ID : "3",
	 	categories_IDs : ["41", "54"],
	 	related_IDs : ["2893"],
	 	id : "KZSpx1Rjk5yM"
 	 }, { 
	 	name : "The cheapest option would be to take the bus.",
	 	language_ID : "3",
	 	categories_IDs : ["41", "54"],
	 	sentence : true,
	 	related_IDs : ["2894"],
	 	id : "tD4yqlaoo4p8"
 	 }, { 
	 	name : "Do you want to play soccer later?",
	 	language_ID : "3",
	 	categories_IDs : ["42", "46"],
	 	sentence : true,
	 	related_IDs : ["2896"],
	 	id : "lan2bBZgKtvY"
 	 }, { 
	 	name : "Perhaps, I'll talk with the others.",
	 	language_ID : "3",
	 	categories_IDs : ["42", "46"],
	 	sentence : true,
	 	related_IDs : ["2897"],
	 	id : "b6Gedd1tQgk1"
 	 }, { 
	 	name : "I am going to be late today.",
	 	language_ID : "3",
	 	categories_IDs : ["42"],
	 	sentence : true,
	 	related_IDs : ["2898"],
	 	id : "yQyIwTeXnsHE"
 	 }, { 
	 	name : "Until now the other team still has a chance.",
	 	language_ID : "3",
	 	categories_IDs : ["42"],
	 	sentence : true,
	 	related_IDs : ["2901"],
	 	id : "L99nfbw6YwHF"
 	 }, { 
	 	name : "I like apples, water melons, and strawberry icecream.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "D86f3GZ0BG3R",
	 	related_IDs : ["95", "QJhEwgwCZP02"],
	 	categories_IDs : ["33"]
 	 }, { 
	 	name : "The frog jumped back into the pond when the cat showed up.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "2ezg3h3q4ELz",
	 	related_IDs : ["2366", "2364", "CNRKkUruLtZv"],
	 	categories_IDs : ["57", "61"],
	 	tags : []
 	 }, { 
	 	name : "The owner showed them around their new property.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "NdRDtLbgmfQG",
	 	related_IDs : ["jpVZbUhAWUXL", "BBj3qqY0M6QJ"],
	 	categories_IDs : ["34", "41"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["33", "35"],
	 	related_IDs : ["NdRDtLbgmfQG", "qRXUZO4flUwU"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "show",
	 	id : "jpVZbUhAWUXL"
 	 }, { 
	 	categories_IDs : ["33", "35"],
	 	related_IDs : ["jpVZbUhAWUXL"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "zeigen",
	 	id : "qRXUZO4flUwU"
 	 }, { 
	 	categories_IDs : ["34", "56", "57", "43"],
	 	related_IDs : ["179", "1383", "2753", "LsXkfcufkUnx"],
	 	tags : [],
	 	name : "The horses were calmly eating grass.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "UEbo6iw4rkOn"
 	 }, { 
	 	categories_IDs : ["34", "57"],
	 	related_IDs : ["2365", "xhqeyigWqqXP", "3JScP3nIENwa"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "The dog sat by the baby as if to watch over it.",
	 	sentence : true,
	 	id : "Z88QNnIMQEuJ"
 	 }, { 
	 	categories_IDs : ["35", "46", "57"],
	 	related_IDs : ["Z88QNnIMQEuJ", "82dr26HGS0NO", "41oOljsAm8PX"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "watch",
	 	id : "xhqeyigWqqXP"
 	 }, { 
	 	categories_IDs : ["34", "46"],
	 	related_IDs : ["xhqeyigWqqXP", "WodRfvKWKR6Q"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "He was watching way more television than he should.",
	 	sentence : true,
	 	id : "82dr26HGS0NO"
 	 }, { 
	 	name : "The apes were swinging happily back and forth between the tree branches.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "qmStIaSRiD67",
	 	related_IDs : ["2367", "2383", "glcfQHC7b9Lc", "e6h2u0J69waa"],
	 	categories_IDs : ["56", "57"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["36", "56"],
	 	related_IDs : ["qmStIaSRiD67", "xwe1QTv2EDPX"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the branch",
	 	id : "glcfQHC7b9Lc"
 	 }, { 
	 	name : "You can think of twigs as small branches.",
	 	language_ID : "2",
	 	sentence : true,
	 	id : "Ag9g9P7l0vq7",
	 	related_IDs : ["5msuvg4GaULn"],
	 	categories_IDs : ["56"]
 	 }, { 
	 	categories_IDs : ["36", "56"],
	 	related_IDs : ["Ag9g9P7l0vq7"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "the twig",
	 	id : "5msuvg4GaULn"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["2369", "LfSJOAM2PasS", "PhFv9c74tpZB"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "The wolf pack is currently roaming through the forest searching for prey.",
	 	sentence : true,
	 	id : "KK0YhKt8Y192"
 	 }, { 
	 	categories_IDs : ["36", "57"],
	 	related_IDs : ["KK0YhKt8Y192", "V0RUHsDIzIb0"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the prey",
	 	id : "LfSJOAM2PasS"
 	 }, { 
	 	name : "The ants were swarming all over the picnic table.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "nPLkBStJOvMZ",
	 	related_IDs : ["2370", "GzHhPNEwRvfD", "1DnFK9BAHsy6"],
	 	categories_IDs : ["57"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["nPLkBStJOvMZ", "HR5RsJwh0qIP"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "swarm",
	 	id : "GzHhPNEwRvfD"
 	 }, { 
	 	name : "Several mice had eaten from the poisoned cheese and were now feeling sick.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "1sNbclk4GeC1",
	 	related_IDs : ["2372", "xEpbwRRlQtfx", "x2pyfGROkJsC"],
	 	categories_IDs : ["34", "57"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["40", "58"],
	 	related_IDs : ["1sNbclk4GeC1", "eFxkoO2Q9BB3", "4rKIgmGsSK11"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the cheese",
	 	id : "xEpbwRRlQtfx"
 	 }, { 
	 	name : "Cheese is made from milk with the help of bacteria.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "eFxkoO2Q9BB3",
	 	related_IDs : ["xEpbwRRlQtfx", "2803", "T3ez0rejbdRq"],
	 	categories_IDs : ["40", "58"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["34", "57"],
	 	related_IDs : ["2376", "Y0xgGfYCeDok", "MeBy9TpFoPKe", "IAIAAJrut9Ug"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "An hour ago she had fed the rabbit carrots.",
	 	id : "9TB9SsfO0fFs"
 	 }, { 
	 	categories_IDs : ["40", "56"],
	 	related_IDs : ["9TB9SsfO0fFs", "2951", "IAIAAJrut9Ug"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the carrot",
	 	id : "Y0xgGfYCeDok"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["0SuMyl8mqR6W"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the nest",
	 	id : "RRuiBPa3u0MB"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["2377"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "The crane was flying low above the lake.",
	 	sentence : true,
	 	id : "LqO4ccvCU8EG"
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["1427", "dqjDaeNLRrNR", "U6q28lU7WGpc", "uI3lnNQFnc9b"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "With his hands he held her daughter up into the sunlight.",
	 	sentence : true,
	 	id : "yc8Ra2mqxIHK"
 	 }, { 
	 	categories_IDs : ["63"],
	 	related_IDs : ["7HgGEv1vZ0NQ", "yc8Ra2mqxIHK", "YLHkLrC6J2ce"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the daughter",
	 	id : "U6q28lU7WGpc",
	 	formPrimaryName : "Female",
	 	formsPrimary_IDs : ["WZSdfifwiZ5O", "VMcYgbMx0MOq"]
 	 }, { 
	 	name : "the son",
	 	id : "WZSdfifwiZ5O",
	 	formPrimaryName : "male",
	 	categories_IDs : ["63"],
	 	language_ID : "3",
	 	formsPrimary_IDs : ["U6q28lU7WGpc", "VMcYgbMx0MOq"],
	 	related_IDs : ["JPqPWlJtGSwQ"]
 	 }, { 
	 	name : "the child",
	 	id : "VMcYgbMx0MOq",
	 	formPrimaryName : "neutral",
	 	categories_IDs : ["63"],
	 	language_ID : "3",
	 	formsPrimary_IDs : ["U6q28lU7WGpc", "WZSdfifwiZ5O"],
	 	related_IDs : ["sB46uwxmO7d1"]
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["1430", "e6QErW865Psf", "1G01OvKUJu5U"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "His headache got better after he drank some water and lay down for a bit.",
	 	sentence : true,
	 	id : "bIcMkJoAKeec"
 	 }, { 
	 	categories_IDs : ["35", "40", "44"],
	 	related_IDs : ["bIcMkJoAKeec", "ZEkltjUt70XA"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "drink",
	 	id : "e6QErW865Psf"
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["1431", "66tsV4nLK8HX"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "She jumped with her legs high in the air.",
	 	sentence : true,
	 	id : "oBClSiAajLmh"
 	 }, { 
	 	name : "His face was covered by sweat after half an hour of exercising.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "xMoFW80sLB1J",
	 	related_IDs : ["1432", "jI7jrp2RumdS"],
	 	categories_IDs : ["42", "44"],
	 	tags : []
 	 }, { 
	 	related_IDs : ["249", "253", "250", "Y5bxsBJ2Kn6U"],
	 	categories_IDs : ["34", "37"],
	 	responses_IDs : [],
	 	tags : [],
	 	language_ID : "3",
	 	name : "Three times two is six.",
	 	sentence : true,
	 	id : "OU1XNBg6Cz2O"
 	 }, { 
	 	categories_IDs : ["37"],
	 	related_IDs : ["268", "256", "269", "Gr6LMKXwcgGW"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "Twentytwo plus eight is thirtyone.",
	 	sentence : true,
	 	id : "AwyU31OjUJ3k"
 	 }, { 
	 	categories_IDs : ["37"],
	 	related_IDs : ["AwyU31OjUJ3k", "DETFKPrjfZL7"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "plus",
	 	id : "Gr6LMKXwcgGW"
 	 }, { 
	 	name : "Fifteen minus fifty equals minus thirtyfive.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "WKNCxKW80SWV",
	 	related_IDs : ["liK6Z0QGn0N7"],
	 	categories_IDs : []
 	 }, { 
	 	categories_IDs : ["37"],
	 	related_IDs : ["WKNCxKW80SWV", "IImoC4pZWaDq"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "minus",
	 	id : "liK6Z0QGn0N7"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["4XXHRz904Lrs"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the kangaroo",
	 	id : "yLrCLc2jw3Yg"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["yLrCLc2jw3Yg"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "das Känguru",
	 	id : "4XXHRz904Lrs"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["58VvEZjGyu2g"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the octopus",
	 	id : "i5ZqvLd7hshY"
 	 }, { 
	 	categories_IDs : ["40"],
	 	related_IDs : ["2836", "T4IQ8ipQlLE9"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the butter",
	 	id : "Pr0Vi7H8fpo5"
 	 }, { 
	 	categories_IDs : ["57"],
	 	related_IDs : ["XFHb1GID1FNW"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the panda bear",
	 	id : "NXUwG5PIpABN"
 	 }, { 
	 	name : "Der Dschinn",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["taFvNlHKGlLK"],
	 	id : "lU2rLrTCizXi",
	 	tags : []
 	 }, { 
	 	name : "die Butter",
	 	language_ID : "2",
	 	categories_IDs : ["40", "34"],
	 	related_IDs : ["Pr0Vi7H8fpo5"],
	 	id : "T4IQ8ipQlLE9"
 	 }, { 
	 	name : "der Oktopus",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["i5ZqvLd7hshY"],
	 	id : "58VvEZjGyu2g"
 	 }, { 
	 	name : "plus",
	 	language_ID : "2",
	 	categories_IDs : ["37"],
	 	related_IDs : ["Gr6LMKXwcgGW"],
	 	id : "DETFKPrjfZL7"
 	 }, { 
	 	name : "minus",
	 	language_ID : "2",
	 	categories_IDs : ["37"],
	 	related_IDs : ["liK6Z0QGn0N7"],
	 	id : "IImoC4pZWaDq"
 	 }, { 
	 	name : "der Pandabär",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["NXUwG5PIpABN"],
	 	id : "XFHb1GID1FNW"
 	 }, { 
	 	name : "der Ast",
	 	language_ID : "2",
	 	categories_IDs : ["36", "56"],
	 	related_IDs : ["glcfQHC7b9Lc"],
	 	id : "xwe1QTv2EDPX"
 	 }, { 
	 	name : "die Beute",
	 	language_ID : "2",
	 	categories_IDs : ["36", "57"],
	 	related_IDs : ["LfSJOAM2PasS"],
	 	id : "V0RUHsDIzIb0"
 	 }, { 
	 	name : "der Käse",
	 	language_ID : "2",
	 	categories_IDs : ["40", "58"],
	 	related_IDs : ["xEpbwRRlQtfx"],
	 	id : "4rKIgmGsSK11"
 	 }, { 
	 	name : "I'll check the left side and you go to the right.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "r28Z3QvEj2Lv",
	 	related_IDs : ["95"],
	 	categories_IDs : ["33"]
 	 }, { 
	 	name : "How about some soup or would you prefer bread?",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "AqfGdFVxoVeC",
	 	related_IDs : ["96", "Wnyvu7tBA3JZ"],
	 	categories_IDs : ["33"]
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["tldrmKqmg9Bh", "9rVguJaibuIQ"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "itch",
	 	id : "JPYFh6JjZCQ6"
 	 }, { 
	 	name : "His nose was still itching.",
	 	language_ID : "3",
	 	sentence : true,
	 	id : "tldrmKqmg9Bh",
	 	related_IDs : ["JPYFh6JjZCQ6", "SDt760IlRMPy"],
	 	categories_IDs : ["44"],
	 	tags : []
 	 }, { 
	 	categories_IDs : ["36", "44"],
	 	related_IDs : ["2956", "dE3gTs38xpmW"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the heart",
	 	id : "36rnzzC0aCq9"
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["d5U6igiQwfzO"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the lungs",
	 	id : "B8PA4FtfdpTA"
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["SQenxfCPrqph"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the muscle",
	 	id : "0UWAXliAFbIC"
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["Hqu1iZUFuZ6H", "Me1JwIC0CqsF"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the tendon",
	 	id : "1CMb67RCNWnE"
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["1CMb67RCNWnE", "Me1JwIC0CqsF"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the sinews",
	 	id : "Hqu1iZUFuZ6H"
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["3EULMHcwaxYO"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the finger",
	 	id : "N4W5BK0rxuoU"
 	 }, { 
	 	categories_IDs : ["44", "57"],
	 	related_IDs : ["iAv6hSJWiFq3"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the toe",
	 	id : "jSDQUmW0wcoE"
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["I71yl8Xe6a8g"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the ankle",
	 	id : "vSMjCS0b37rE"
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["b5LnAGnLlhYQ"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the joint",
	 	id : "VlzyCxVdOiAD"
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["8FT9ADi5aN8u"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the shoulder",
	 	id : "Y70QZWIkYRDV"
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["o8U8UYNTfFdf"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the hip",
	 	id : "afa6mt7HiAlL"
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["Njn5nG9bWyH3"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the spine",
	 	id : "4fhbAl3VEN5B"
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["QmUoX8QF3bWq"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the belly",
	 	id : "37DlbKRwX6LE"
 	 }, { 
	 	categories_IDs : ["44"],
	 	related_IDs : ["kLoMWjwE0y78"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the breast",
	 	id : "WqGZQKJk2WNm"
 	 }, { 
	 	name : "Ich mag Äpfel, Wassermelonen und Erdbeereis.",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	sentence : true,
	 	related_IDs : ["D86f3GZ0BG3R"],
	 	id : "QJhEwgwCZP02"
 	 }, { 
	 	name : "Der Frosch sprang zurück in den Teich als die Katze kam.",
	 	language_ID : "2",
	 	categories_IDs : ["57", "61"],
	 	sentence : true,
	 	related_IDs : ["2ezg3h3q4ELz"],
	 	id : "CNRKkUruLtZv"
 	 }, { 
	 	name : "Der Eigentümer zeigte ihnen ihren neuen Besitz.",
	 	language_ID : "2",
	 	categories_IDs : ["35", "41"],
	 	sentence : true,
	 	related_IDs : ["NdRDtLbgmfQG"],
	 	id : "BBj3qqY0M6QJ"
 	 }, { 
	 	name : "Die Pferde aßen ruhig Gras.",
	 	language_ID : "2",
	 	categories_IDs : ["34", "56", "57", "43"],
	 	sentence : true,
	 	related_IDs : ["UEbo6iw4rkOn"],
	 	id : "LsXkfcufkUnx"
 	 }, { 
	 	name : "Der Hund saß neben dem Baby als ob er auf es aufpassen würde.",
	 	language_ID : "2",
	 	categories_IDs : ["34", "57"],
	 	sentence : true,
	 	related_IDs : ["Z88QNnIMQEuJ"],
	 	id : "3JScP3nIENwa"
 	 }, { 
	 	name : "beobachten",
	 	language_ID : "2",
	 	categories_IDs : ["35", "46", "34", "57"],
	 	related_IDs : ["xhqeyigWqqXP"],
	 	id : "41oOljsAm8PX"
 	 }, { 
	 	name : "Er sah wesentlich mehr Fernsehen als er sollte.",
	 	language_ID : "2",
	 	categories_IDs : ["34", "46"],
	 	sentence : true,
	 	related_IDs : ["82dr26HGS0NO"],
	 	id : "WodRfvKWKR6Q"
 	 }, { 
	 	name : "Die Affen schwangen fröhlich zwischen den Baumästen hin und her.",
	 	language_ID : "2",
	 	categories_IDs : ["56", "57"],
	 	sentence : true,
	 	related_IDs : ["qmStIaSRiD67"],
	 	id : "e6h2u0J69waa"
 	 }, { 
	 	name : "Das Wolfsrudel durchstreift gerade den Wald auf der Suche nach Beute.",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	sentence : true,
	 	related_IDs : ["KK0YhKt8Y192"],
	 	id : "PhFv9c74tpZB"
 	 }, { 
	 	name : "Die Ameisen schwärmten über den gesamten Picknick-Tisch.",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	sentence : true,
	 	related_IDs : ["nPLkBStJOvMZ"],
	 	id : "1DnFK9BAHsy6"
 	 }, { 
	 	name : "schwärmen",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["GzHhPNEwRvfD"],
	 	id : "HR5RsJwh0qIP"
 	 }, { 
	 	name : "Mehrere Mäuse hatten den vergifteten Käse gegessen und fühlten sich nun schlecht.",
	 	language_ID : "2",
	 	categories_IDs : ["34", "57"],
	 	sentence : true,
	 	related_IDs : ["1sNbclk4GeC1"],
	 	id : "x2pyfGROkJsC"
 	 }, { 
	 	name : "Käse wird aus Milch mit Hilfe von Bakterien gemacht.",
	 	language_ID : "2",
	 	categories_IDs : ["40", "58"],
	 	sentence : true,
	 	related_IDs : ["eFxkoO2Q9BB3"],
	 	id : "T3ez0rejbdRq"
 	 }, { 
	 	name : "Vor einer Stunde hatte sie die Kaninchen mit Karotten gefüttert.",
	 	language_ID : "2",
	 	categories_IDs : ["34", "57"],
	 	related_IDs : ["9TB9SsfO0fFs", "IAIAAJrut9Ug"],
	 	id : "MeBy9TpFoPKe",
	 	tags : []
 	 }, { 
	 	name : "die Karotte",
	 	language_ID : "2",
	 	categories_IDs : ["40", "56", "34", "57"],
	 	related_IDs : ["Y0xgGfYCeDok", "9TB9SsfO0fFs", "MeBy9TpFoPKe"],
	 	id : "IAIAAJrut9Ug",
	 	tags : []
 	 }, { 
	 	name : "das Nest",
	 	language_ID : "2",
	 	categories_IDs : ["57"],
	 	related_IDs : ["RRuiBPa3u0MB"],
	 	id : "0SuMyl8mqR6W"
 	 }, { 
	 	name : "die Tochter",
	 	language_ID : "2",
	 	categories_IDs : ["63"],
	 	related_IDs : ["U6q28lU7WGpc"],
	 	id : "YLHkLrC6J2ce"
 	 }, { 
	 	name : "der Sohn",
	 	language_ID : "2",
	 	categories_IDs : ["63"],
	 	related_IDs : ["WZSdfifwiZ5O"],
	 	id : "JPqPWlJtGSwQ"
 	 }, { 
	 	name : "das Kind",
	 	language_ID : "2",
	 	categories_IDs : ["63"],
	 	related_IDs : ["VMcYgbMx0MOq"],
	 	id : "sB46uwxmO7d1"
 	 }, { 
	 	name : "trinken",
	 	language_ID : "2",
	 	categories_IDs : ["35", "40", "44"],
	 	related_IDs : ["e6QErW865Psf"],
	 	id : "ZEkltjUt70XA"
 	 }, { 
	 	name : "Drei mal Zwei ist Sechs.",
	 	language_ID : "2",
	 	categories_IDs : ["34", "37"],
	 	sentence : true,
	 	related_IDs : ["OU1XNBg6Cz2O"],
	 	id : "Y5bxsBJ2Kn6U"
 	 }, { 
	 	name : "jucken",
	 	language_ID : "2",
	 	categories_IDs : ["44"],
	 	related_IDs : ["JPYFh6JjZCQ6"],
	 	id : "9rVguJaibuIQ"
 	 }, { 
	 	name : "Seine Nase juckte immer noch.",
	 	language_ID : "2",
	 	categories_IDs : ["44"],
	 	sentence : true,
	 	related_IDs : ["tldrmKqmg9Bh"],
	 	id : "SDt760IlRMPy"
 	 }, { 
	 	name : "das Herz",
	 	language_ID : "2",
	 	categories_IDs : ["36", "44"],
	 	related_IDs : ["36rnzzC0aCq9"],
	 	id : "dE3gTs38xpmW"
 	 }, { 
	 	name : "die Lunge",
	 	language_ID : "2",
	 	categories_IDs : ["44"],
	 	related_IDs : ["B8PA4FtfdpTA"],
	 	id : "d5U6igiQwfzO"
 	 }, { 
	 	name : "der Muskel",
	 	language_ID : "2",
	 	categories_IDs : ["44"],
	 	related_IDs : ["0UWAXliAFbIC"],
	 	id : "SQenxfCPrqph"
 	 }, { 
	 	name : "die Sehne",
	 	language_ID : "2",
	 	categories_IDs : ["44"],
	 	related_IDs : ["1CMb67RCNWnE", "Hqu1iZUFuZ6H"],
	 	id : "Me1JwIC0CqsF"
 	 }, { 
	 	name : "der Finger",
	 	language_ID : "2",
	 	categories_IDs : ["44"],
	 	related_IDs : ["N4W5BK0rxuoU"],
	 	id : "3EULMHcwaxYO"
 	 }, { 
	 	name : "der Zeh",
	 	language_ID : "2",
	 	categories_IDs : ["44", "57"],
	 	related_IDs : ["jSDQUmW0wcoE"],
	 	id : "iAv6hSJWiFq3"
 	 }, { 
	 	name : "das Fußgelenk",
	 	language_ID : "2",
	 	categories_IDs : ["44"],
	 	related_IDs : ["vSMjCS0b37rE"],
	 	id : "I71yl8Xe6a8g"
 	 }, { 
	 	name : "das Gelenk",
	 	language_ID : "2",
	 	categories_IDs : ["44"],
	 	related_IDs : ["VlzyCxVdOiAD"],
	 	id : "b5LnAGnLlhYQ"
 	 }, { 
	 	name : "die Schulter",
	 	language_ID : "2",
	 	categories_IDs : ["44"],
	 	related_IDs : ["Y70QZWIkYRDV"],
	 	id : "8FT9ADi5aN8u"
 	 }, { 
	 	name : "die Hüfte",
	 	language_ID : "2",
	 	categories_IDs : ["44"],
	 	related_IDs : ["afa6mt7HiAlL"],
	 	id : "o8U8UYNTfFdf"
 	 }, { 
	 	name : "die Wirbelsäule",
	 	language_ID : "2",
	 	categories_IDs : ["44"],
	 	related_IDs : ["4fhbAl3VEN5B"],
	 	id : "Njn5nG9bWyH3"
 	 }, { 
	 	name : "der Bauch",
	 	language_ID : "2",
	 	categories_IDs : ["44"],
	 	related_IDs : ["37DlbKRwX6LE"],
	 	id : "QmUoX8QF3bWq"
 	 }, { 
	 	name : "die Brust",
	 	language_ID : "2",
	 	categories_IDs : ["44"],
	 	related_IDs : ["WqGZQKJk2WNm"],
	 	id : "kLoMWjwE0y78"
 	 }, { 
	 	related_IDs : ["Ecgj5HaxJA3T"],
	 	categories_IDs : ["60"],
	 	responses_IDs : [],
	 	tags : [],
	 	language_ID : "3",
	 	name : "fast",
	 	id : "oXmrEcrxsuy1"
 	 }, { 
	 	categories_IDs : ["60"],
	 	related_IDs : ["Q646d9NnTZGg"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "slow",
	 	id : "0vTehTg5Ijys"
 	 }, { 
	 	categories_IDs : ["60"],
	 	related_IDs : ["oXmrEcrxsuy1"],
	 	tags : [],
	 	language_ID : "2",
	 	name : "schnell",
	 	id : "Ecgj5HaxJA3T"
 	 }, { 
	 	categories_IDs : ["60"],
	 	related_IDs : ["0vTehTg5Ijys"],
	 	tags : [],
	 	name : "langsam",
	 	language_ID : "2",
	 	id : "Q646d9NnTZGg"
 	 }, { 
	 	related_IDs : ["r9zOECHED0VJ"],
	 	categories_IDs : ["19"],
	 	responses_IDs : [],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the carpet",
	 	id : "JufArNYXxLRM"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["2633"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the cupboard",
	 	id : "nJGSqCM3uGpy"
 	 }, { 
	 	categories_IDs : ["40"],
	 	related_IDs : ["wFfQP1Iqh2jZ"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the plate",
	 	id : "Fdz25apEaX0K"
 	 }, { 
	 	categories_IDs : ["40"],
	 	related_IDs : ["8ENbwDOOoWaG"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the fork",
	 	id : "WjDSBUm5HMXD"
 	 }, { 
	 	categories_IDs : ["40"],
	 	related_IDs : ["fwVUAJT0KkK4"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the knife",
	 	id : "weVYvmFeOZFu"
 	 }, { 
	 	categories_IDs : ["40"],
	 	related_IDs : ["1G5tiKAyEjRb"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the spoon",
	 	id : "pDudYqCGqaSC"
 	 }, { 
	 	categories_IDs : ["40"],
	 	related_IDs : ["gqGJMRzNbszM"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the juice",
	 	id : "wF6hpTT7JGJu"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["ukjJtEPKfBjZ"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the furniture",
	 	id : "fl1YlBRaqzOU"
 	 }, { 
	 	related_IDs : ["6VSuFrZxxX0C"],
	 	categories_IDs : ["19"],
	 	responses_IDs : [],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the curtain",
	 	id : "QBczqHwWahfJ"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["D0ETnrma1p0F"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the air conditioner",
	 	id : "3UpkozRYjvLb"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["FwQbcbxJThsm"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the windowblinds",
	 	id : "uyMyms3S3QdE"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["FXE82XhcW21z"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the attic",
	 	id : "S48vGIy8vOv0"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["WPdnJd6nzhgh"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the drawer",
	 	id : "3sRbIShLAsyc"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["DsPjuI8vInQ8"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the dresser",
	 	id : "vovUPzPxRJAM"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["t0jW9dFXc44Z"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the socket",
	 	id : "4vfytYEkGKKS"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["2089"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the wash basin",
	 	id : "ak716rhFNG5w"
 	 }, { 
	 	categories_IDs : ["24", "35"],
	 	related_IDs : ["V2WjGmwnNSSi"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "kill",
	 	id : "g1zj9yrXDJPA"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["XnMArp6ZqURU"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "flee",
	 	id : "CuyM3B8Zslf3"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["6oELZFXnBPWg"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "steal",
	 	id : "WqBLRmxjrZFf"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["jrvNUfngwlWg"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the trap",
	 	id : "PKcCu9UmDg4o"
 	 }, { 
	 	categories_IDs : ["24", "49"],
	 	related_IDs : ["R2sxBsOgXATd"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the monk",
	 	id : "O8kq8uM0RLIX"
 	 }, { 
	 	categories_IDs : ["24", "49"],
	 	related_IDs : ["DpaSxmEG2qJa"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the nun",
	 	id : "5FftQYGoK442"
 	 }, { 
	 	categories_IDs : ["46"],
	 	related_IDs : ["m2SPLXssEXqh", "qXfxdcORzjAx"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the towel",
	 	id : "Eofy3OVOzm7U"
 	 }, { 
	 	categories_IDs : ["40"],
	 	related_IDs : ["8NZmApTLoDwr", "nFAwnLjY8lfb"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the chocolate",
	 	id : "A6YeE8TAEocq"
 	 }, { 
	 	categories_IDs : ["40"],
	 	related_IDs : ["QfiYJZGxINJX", "jNg0bnFhXqQh"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the cake",
	 	id : "sWURaX0K8Q5K"
 	 }, { 
	 	categories_IDs : ["24"],
	 	related_IDs : ["eInuOquqdKlo", "i3qGYLkMQeVp"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the witch",
	 	id : "Sq8D4p3mpr4F"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["FK53wIXQYuh1"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the bridge",
	 	id : "9HvNTtL39Akq"
 	 }, { 
	 	categories_IDs : ["19"],
	 	related_IDs : ["ARUAzsvsf0Fr"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the tunnel",
	 	id : "xDXlGJrAgLOX"
 	 }, { 
	 	categories_IDs : ["54"],
	 	related_IDs : ["ds6zinOw2A5g"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the train",
	 	id : "cPc4mmWuq2lU"
 	 }, { 
	 	categories_IDs : ["54"],
	 	related_IDs : ["JL0EVBJuJLsh"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the airplane",
	 	id : "y1gkmWJTRGtp"
 	 }, { 
	 	categories_IDs : ["42"],
	 	related_IDs : ["FvzJXQZHvtpx"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the chess",
	 	id : "XRPH1U5SZLBW"
 	 }, { 
	 	categories_IDs : ["42"],
	 	related_IDs : ["2oAdyAh7Dng0"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "baseball",
	 	id : "4TrnKMgc4ybm"
 	 }, { 
	 	categories_IDs : ["42"],
	 	related_IDs : ["maokLuPOhOAb"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "rugby",
	 	id : "hDQWvmvNdzVg"
 	 }, { 
	 	categories_IDs : ["35", "42"],
	 	related_IDs : ["B6dMdGm2lN55"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "score",
	 	id : "e23GnJBg3sV4"
 	 }, { 
	 	categories_IDs : ["43", "60"],
	 	related_IDs : ["FVrauJu5pxao"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "certain",
	 	id : "FAWoHG7TbXui"
 	 }, { 
	 	categories_IDs : ["43", "60"],
	 	related_IDs : ["Nn2J2GIJMSpi"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "uncertain",
	 	id : "2s7B5KtQmhHG"
 	 }, { 
	 	categories_IDs : ["36", "48"],
	 	related_IDs : ["rv6xt76Ap7qf"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the decision",
	 	id : "t6fXi0LcVIi1"
 	 }, { 
	 	categories_IDs : ["35", "48"],
	 	related_IDs : ["Lg5UZppMVXWE"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "decide",
	 	id : "IaVwp8GXdHKE"
 	 }, { 
	 	categories_IDs : ["36", "48"],
	 	related_IDs : ["X5QSLEU3QISp"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "the responsibility",
	 	id : "YSsp88ipRsUx"
 	 }, { 
	 	categories_IDs : ["35", "49"],
	 	related_IDs : ["4Upl55ClzTuY"],
	 	tags : [],
	 	language_ID : "3",
	 	name : "believe",
	 	id : "mEmiFUQqllPf"
 	 }, { 
	 	name : "der Kuchen",
	 	language_ID : "2",
	 	categories_IDs : ["40"],
	 	related_IDs : ["sWURaX0K8Q5K"],
	 	id : "jNg0bnFhXqQh",
	 	tags : []
 	 }, { 
	 	name : "der Mönch",
	 	language_ID : "2",
	 	categories_IDs : ["24", "49"],
	 	related_IDs : ["O8kq8uM0RLIX"],
	 	id : "R2sxBsOgXATd"
 	 }, { 
	 	name : "der Vorhang",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["QBczqHwWahfJ"],
	 	id : "6VSuFrZxxX0C"
 	 }, { 
	 	name : "die Klimaanlage",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["3UpkozRYjvLb"],
	 	id : "D0ETnrma1p0F"
 	 }, { 
	 	name : "das Mobiliar",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["fl1YlBRaqzOU"],
	 	id : "ukjJtEPKfBjZ"
 	 }, { 
	 	name : "der Dachboden",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["S48vGIy8vOv0"],
	 	id : "FXE82XhcW21z"
 	 }, { 
	 	name : "die Schublade",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["3sRbIShLAsyc"],
	 	id : "WPdnJd6nzhgh"
 	 }, { 
	 	name : "die Kommode",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["vovUPzPxRJAM"],
	 	id : "DsPjuI8vInQ8"
 	 }, { 
	 	name : "die Steckdose",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["4vfytYEkGKKS"],
	 	id : "t0jW9dFXc44Z"
 	 }, { 
	 	name : "töten",
	 	language_ID : "2",
	 	categories_IDs : ["24", "35"],
	 	related_IDs : ["g1zj9yrXDJPA"],
	 	id : "V2WjGmwnNSSi"
 	 }, { 
	 	name : "flüchten",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["CuyM3B8Zslf3"],
	 	id : "XnMArp6ZqURU"
 	 }, { 
	 	name : "stehlen",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["WqBLRmxjrZFf"],
	 	id : "6oELZFXnBPWg"
 	 }, { 
	 	name : "die Falle",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["PKcCu9UmDg4o"],
	 	id : "jrvNUfngwlWg"
 	 }, { 
	 	name : "die Nonne",
	 	language_ID : "2",
	 	categories_IDs : ["24", "49"],
	 	related_IDs : ["5FftQYGoK442"],
	 	id : "DpaSxmEG2qJa"
 	 }, { 
	 	name : "das Handtuch",
	 	language_ID : "2",
	 	categories_IDs : ["46"],
	 	related_IDs : ["Eofy3OVOzm7U"],
	 	id : "qXfxdcORzjAx"
 	 }, { 
	 	name : "die Schokolade",
	 	language_ID : "2",
	 	categories_IDs : ["40"],
	 	related_IDs : ["A6YeE8TAEocq"],
	 	id : "nFAwnLjY8lfb"
 	 }, { 
	 	name : "die Hexe",
	 	language_ID : "2",
	 	categories_IDs : ["24"],
	 	related_IDs : ["Sq8D4p3mpr4F"],
	 	id : "i3qGYLkMQeVp"
 	 }, { 
	 	name : "die Brücke",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["9HvNTtL39Akq"],
	 	id : "FK53wIXQYuh1"
 	 }, { 
	 	name : "der Tunnel",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["xDXlGJrAgLOX"],
	 	id : "ARUAzsvsf0Fr"
 	 }, { 
	 	name : "der Zug",
	 	language_ID : "2",
	 	categories_IDs : ["54"],
	 	related_IDs : ["cPc4mmWuq2lU"],
	 	id : "ds6zinOw2A5g"
 	 }, { 
	 	name : "das Flugzeug",
	 	language_ID : "2",
	 	categories_IDs : ["54"],
	 	related_IDs : ["y1gkmWJTRGtp"],
	 	id : "JL0EVBJuJLsh"
 	 }, { 
	 	name : "das Schachspiel",
	 	language_ID : "2",
	 	categories_IDs : ["42"],
	 	related_IDs : ["XRPH1U5SZLBW"],
	 	id : "FvzJXQZHvtpx"
 	 }, { 
	 	name : "Baseball",
	 	language_ID : "2",
	 	categories_IDs : ["42"],
	 	related_IDs : ["4TrnKMgc4ybm"],
	 	id : "2oAdyAh7Dng0"
 	 }, { 
	 	name : "Rugby",
	 	language_ID : "2",
	 	categories_IDs : ["42"],
	 	related_IDs : ["hDQWvmvNdzVg"],
	 	id : "maokLuPOhOAb"
 	 }, { 
	 	name : "punkten",
	 	language_ID : "2",
	 	categories_IDs : ["35", "42"],
	 	related_IDs : ["e23GnJBg3sV4"],
	 	id : "B6dMdGm2lN55"
 	 }, { 
	 	name : "sicher",
	 	language_ID : "2",
	 	categories_IDs : ["43", "60"],
	 	related_IDs : ["FAWoHG7TbXui"],
	 	id : "FVrauJu5pxao"
 	 }, { 
	 	name : "unsicher",
	 	language_ID : "2",
	 	categories_IDs : ["43", "60"],
	 	related_IDs : ["2s7B5KtQmhHG"],
	 	id : "Nn2J2GIJMSpi"
 	 }, { 
	 	name : "die Entscheidung",
	 	language_ID : "2",
	 	categories_IDs : ["36", "48"],
	 	related_IDs : ["t6fXi0LcVIi1"],
	 	id : "rv6xt76Ap7qf"
 	 }, { 
	 	name : "entscheiden",
	 	language_ID : "2",
	 	categories_IDs : ["35", "48"],
	 	related_IDs : ["IaVwp8GXdHKE"],
	 	id : "Lg5UZppMVXWE"
 	 }, { 
	 	name : "die Verantwortung",
	 	language_ID : "2",
	 	categories_IDs : ["36", "48"],
	 	related_IDs : ["YSsp88ipRsUx"],
	 	id : "X5QSLEU3QISp"
 	 }, { 
	 	name : "glauben",
	 	language_ID : "2",
	 	categories_IDs : ["35", "49"],
	 	related_IDs : ["mEmiFUQqllPf"],
	 	id : "4Upl55ClzTuY"
 	 }, { 
	 	name : "Mit seinen Händen hielt er ihre Tochter in das Sonnenlicht.",
	 	language_ID : "2",
	 	categories_IDs : ["44"],
	 	sentence : true,
	 	related_IDs : ["yc8Ra2mqxIHK"],
	 	id : "uI3lnNQFnc9b"
 	 }, { 
	 	name : "Seine Kopfschmerzen besserten sich, nachdem er ein bisschen Wasser getrunken und sich hingelegt hatte.",
	 	language_ID : "2",
	 	categories_IDs : ["44"],
	 	sentence : true,
	 	related_IDs : ["bIcMkJoAKeec"],
	 	id : "1G01OvKUJu5U"
 	 }, { 
	 	name : "Sie sprang mit hren Beinen hoch in die Luft.",
	 	language_ID : "2",
	 	categories_IDs : ["44"],
	 	sentence : true,
	 	related_IDs : ["oBClSiAajLmh"],
	 	id : "66tsV4nLK8HX"
 	 }, { 
	 	name : "Sein Gesicht war schweißbedeckt nach einer halben Stunde Training.",
	 	language_ID : "2",
	 	categories_IDs : ["42", "44"],
	 	sentence : true,
	 	related_IDs : ["xMoFW80sLB1J"],
	 	id : "jI7jrp2RumdS"
 	 }, { 
	 	name : "der Teppich",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["JufArNYXxLRM"],
	 	id : "r9zOECHED0VJ"
 	 }, { 
	 	name : "der Teller",
	 	language_ID : "2",
	 	categories_IDs : ["40"],
	 	related_IDs : ["Fdz25apEaX0K"],
	 	id : "wFfQP1Iqh2jZ"
 	 }, { 
	 	name : "die Gabel",
	 	language_ID : "2",
	 	categories_IDs : ["40"],
	 	related_IDs : ["WjDSBUm5HMXD"],
	 	id : "8ENbwDOOoWaG"
 	 }, { 
	 	name : "das Messer",
	 	language_ID : "2",
	 	categories_IDs : ["40"],
	 	related_IDs : ["weVYvmFeOZFu"],
	 	id : "fwVUAJT0KkK4"
 	 }, { 
	 	name : "der Löffel",
	 	language_ID : "2",
	 	categories_IDs : ["40"],
	 	related_IDs : ["pDudYqCGqaSC"],
	 	id : "1G5tiKAyEjRb"
 	 }, { 
	 	name : "der Saft",
	 	language_ID : "2",
	 	categories_IDs : ["40"],
	 	related_IDs : ["wF6hpTT7JGJu"],
	 	id : "gqGJMRzNbszM"
 	 }, { 
	 	name : "das Rollo",
	 	language_ID : "2",
	 	categories_IDs : ["19"],
	 	related_IDs : ["uyMyms3S3QdE"],
	 	id : "FwQbcbxJThsm"
 	 }, { 
	 	name : "Wie wäre es mit Suppe oder wäre Ihnen Brot lieber?",
	 	language_ID : "2",
	 	categories_IDs : ["33"],
	 	sentence : true,
	 	related_IDs : ["AqfGdFVxoVeC"],
	 	id : "Wnyvu7tBA3JZ"
 	 }, { 
	 	name : "saber",
	 	language_ID : "5",
	 	categories_IDs : ["35"],
	 	related_IDs : ["446", "2867", "411"],
	 	id : "ugTZyT2iO2vq"
 	 }, { 
	 	name : "¿Dónde está la estación?",
	 	language_ID : "5",
	 	categories_IDs : ["34"],
	 	sentence : true,
	 	related_IDs : ["941", "2874", "942"],
	 	id : "4BN0TINFgaDM"
 	 }, { 
	 	name : "la forêt",
	 	language_ID : "4",
	 	categories_IDs : ["2756"],
	 	related_IDs : ["10", "13", "2409", "8", "9"],
	 	id : "lbotmEgCymz8"
 }]; 

this.knowBetter_languages = [{
	 	name : "Svenska",
	 	key : "sv",
	 	id : "1",
	 	chars : ['ä', 'Ä', 'ö', 'Ö', 'å', 'Å'],
	 	articles : ["ett", "en"]
 	 }, { 
	 	name : "Deutsch",
	 	id : "2",
	 	key : "de",
	 	chars : ['ä', 'Ä', 'ö', 'Ö', 'ü', 'Ü', 'ß'],
	 	articles : ["der", "die", "das", "ein", "eine"]
 	 }, { 
	 	name : "English",
	 	key : "en",
	 	id : "3",
	 	articles : ["the", "a", "an"]
 	 }, { 
	 	name : "Français",
	 	key : "fr",
	 	id : "4",
	 	articles : ["le", "la", "les", "un", "une", "des"],
	 	chars : ['À', 'à', 'Â', 'â', 'Æ', 'æ', 'Ç', 'ç', 'É', 'é', 'È', 'è', 'Ê', 'ê', 'Ë', 'ë', 'Î', 'î', 'Ï', 'ï', 'Ô', 'ô', 'Œ', 'œ', 'Ù', 'ù', 'Û', 'û', 'Ü', 'ü', 'Ÿ', 'ÿ']
 	 }, { 
	 	name : "Español",
	 	key : "es",
	 	chars : ['á', 'é', 'í', 'ó', 'ú', 'ü', 'ñ', '¿', '¡'],
	 	id : "5",
	 	articles : ["el", "la", "los", "un", "una", "uno"]
 }];

this.knowBetter_users = [{
	 	name : "Myself",
	 	id : "15",
	 	theme : "emerald",
	 	languageKnown_id : "2",
	 	languageLearn_id : "1",
	 	progress_1 : [],
	 	progress_2 : [],
	 	progress_3 : [],
	 	progress_4 : [],
	 	current_session : []
 }];

this.knowBetter_comments = [{
	 	name : "Only plural.",
	 	language_ID : "[object Object]",
	 	related_IDs : ["B8PA4FtfdpTA"],
	 	isComment : true,
	 	id : "Fi6O4PXKjJCD"
 	 }, { 
	 	name : "Only plural.",
	 	language_ID : "[object Object]",
	 	related_IDs : ["Hqu1iZUFuZ6H"],
	 	isComment : true,
	 	id : "yrnMXeeoZMsl"
 }];

this.knowBetter_tags = [];



this.knowBetter_categories = [{
	 	name : "CATEGORY_STRUCTURAL_WORDS",
	 	id : "33",
	 	group : "BASICS"
 	 }, { 
	 	name : "CATEGORY_PHRASES",
	 	id : "34",
	 	group : "BASICS"
 	 }, { 
	 	name : "CATEGORY_VERBS",
	 	id : "35",
	 	group : "BASICS"
 	 }, { 
	 	name : "CATEGORY_NOUNS",
	 	id : "36",
	 	group : "BASICS"
 	 }, { 
	 	name : "CATEGORY_NUMBERS_SCALES",
	 	id : "37",
	 	group : "BASICS"
 	 }, { 
	 	name : "CATEGORY_ORIENTATION",
	 	id : "38",
	 	group : "BASICS"
 	 }, { 
	 	name : "CATEGORY_TIME",
	 	id : "39",
	 	group : "BASICS"
 	 }, { 
	 	name : "CATEGORY_FOOD",
	 	id : "40",
	 	group : "HUMANS"
 	 }, { 
	 	name : "CATEGORY_TRAVELS",
	 	id : "41",
	 	group : "HUMANS"
 	 }, { 
	 	name : "CATEGORY_SPORTS",
	 	id : "42",
	 	group : "HUMANS"
 	 }, { 
	 	name : "CATEGORY_EMOTIONS",
	 	id : "43",
	 	group : "HUMANS"
 	 }, { 
	 	name : "CATEGORY_BODY",
	 	id : "44",
	 	group : "HUMANS"
 	 }, { 
	 	name : "CATEGORY_RELATIONS",
	 	id : "63",
	 	group : "HUMANS"
 	 }, { 
	 	name : "CATEGORY_APPEARANCE",
	 	id : "45",
	 	group : "HUMANS"
 	 }, { 
	 	name : "CATEGORY_LEISURE",
	 	id : "46",
	 	group : "HUMANS"
 	 }, { 
	 	name : "CATEGORY_POLITICS",
	 	id : "48",
	 	group : "SOCIETY"
 	 }, { 
	 	name : "CATEGORY_RELIGION_AND_PHILOSOPHY",
	 	id : "49",
	 	group : "SOCIETY"
 	 }, { 
	 	name : "CATEGORY_ECONOMY",
	 	id : "51",
	 	group : "SOCIETY"
 	 }, { 
	 	name : "CATEGORY_COMMUNICATION",
	 	id : "52",
	 	group : "SOCIETY"
 	 }, { 
	 	name : "CATEGORY_ART",
	 	id : "53",
	 	group : "SOCIETY"
 	 }, { 
	 	name : "CATEGORY_TRAFFIC",
	 	id : "54",
	 	group : "SOCIETY"
 	 }, { 
	 	name : "CATEGORY_STORIES",
	 	id : "24",
	 	group : "SOCIETY"
 	 }, { 
	 	name : "CATEGORY_LEARNING",
	 	id : "55",
	 	group : "SOCIETY"
 	 }, { 
	 	name : "CATEGORY_PLANTS",
	 	id : "56",
	 	group : "NATURE_AND_ENVIRONMENT"
 	 }, { 
	 	name : "CATEGORY_ANIMALS",
	 	id : "57",
	 	group : "NATURE_AND_ENVIRONMENT"
 	 }, { 
	 	name : "CATEGORY_OTHER_LIFEFORMS",
	 	id : "58",
	 	group : "NATURE_AND_ENVIRONMENT"
 	 }, { 
	 	name : "CATEGORY_INANIMATE_NATURE",
	 	id : "59",
	 	group : "NATURE_AND_ENVIRONMENT"
 	 }, { 
	 	name : "CATEGORY_DESCRIPTORS",
	 	id : "60",
	 	group : "BASICS"
 	 }, { 
	 	name : "CATEGORY_WORK",
	 	id : "61",
	 	group : "SOCIETY"
 	 }, { 
	 	name : "CATEGORY_BUILDINGS",
	 	id : "19",
	 	group : "SOCIETY"
 	 }, { 
	 	name : "CATEGORY_LANDSCAPES",
	 	id : "2756",
	 	group : "NATURE_AND_ENVIRONMENT"
 	 }, { 
	 	name : "CATEGORY_WEATHER",
	 	id : "2757",
	 	group : "NATURE_AND_ENVIRONMENT"
 }];