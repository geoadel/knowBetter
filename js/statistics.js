/**
 This file is part of knowBetter.
 
 knowBetter is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 knowBetter is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with knowBetter.  If not, see <http://www.gnu.org/licenses/>.
 **/


(function () {

    var app = angular.module('statisticsService', ['dataService']);

    app.factory('statisticsService', function (dataService) {

        var stats = {};
        var data = dataService;

        var plot1 = {};
        var plot2 = {};
        var plot3 = {};
        var plot4 = {};

        stats.getWordsPerLanguage = function () {
            var result = [];
            angular.forEach(data.languages, function (language) {
                var wordsForLanguage = data.getWordsForLanguage(language.id);
                result.push(wordsForLanguage.length);
            });
            return result;
        };

        stats.WordsPerLanguage = stats.getWordsPerLanguage();
        stats.WordsPerLanguageLearned = [];
        stats.WordsPerLanguageLearnedPercentage = [];
        stats.WordsTotal = 0;


        stats.calculateWordsPerLanguage = function () {
            var n = 0;
            console.log(JSON.stringify(data.languages));
            angular.forEach(data.languages, function (language) {
                var wordsForLanguage = data.getWordsForLanguage(language.id);
                stats.WordsPerLanguage.push(wordsForLanguage.length);
                stats.WordsPerLanguageLearned.push(data.getUntrainedWords(wordsForLanguage, data.user.id).length);
                console.log("Percentage learned for language " + language.name + ": " + stats.WordsPerLanguageLearned[n] + " / " + stats.WordsPerLanguage[n]);
                stats.WordsPerLanguageLearnedPercentage.push(stats.WordsPerLanguageLearned[n] / stats.WordsPerLanguage[n]);
                stats.WordsTotal = stats.WordsTotal + wordsForLanguage.length;
                n++;
            });
        }

        stats.calculateStats = function (data) {
            console.log("Calculating and displaying stats...");

            var langSv = data.getWordsForCategoryGroups("1", data.user.id, false, false);
            var langDe = data.getWordsForCategoryGroups("2", data.user.id, false, false);
            var langEn = data.getWordsForCategoryGroups("3", data.user.id, false, false);
            var langEs = data.getWordsForCategoryGroups("5", data.user.id, false, false);
            langSv.splice(0, 0, "Svenska");
            langDe.splice(0, 0, "Deutsch");
            langEn.splice(0, 0, "English");
            langEs.splice(0, 0, "Español");
            var langs = [langSv, langDe, langEn, langEs];

            var langSvLearned = data.getWordsForCategoryGroups("1", data.user.id, true, false);
            var langDeLearned = data.getWordsForCategoryGroups("2", data.user.id, true, false);
            var langEnLearned = data.getWordsForCategoryGroups("3", data.user.id, true, false);
            var langEsLearned = data.getWordsForCategoryGroups("5", data.user.id, true, false);
            langSvLearned.splice(0, 0, "Svenska");
            langDeLearned.splice(0, 0, "Deutsch");
            langEnLearned.splice(0, 0, "English");
            langEsLearned.splice(0, 0, "Español");
            var langsLearned = [langSvLearned, langDeLearned, langEnLearned, langEsLearned];


            var langSvSentences = data.getWordsForCategoryGroups("1", data.user.id, false, true);
            var langDeSentences = data.getWordsForCategoryGroups("2", data.user.id, false, true);
            var langEnSentences = data.getWordsForCategoryGroups("3", data.user.id, false, true);
            var langEsSentences = data.getWordsForCategoryGroups("5", data.user.id, false, true);
            langSvSentences.splice(0, 0, "Svenska");
            langDeSentences.splice(0, 0, "Deutsch");
            langEnSentences.splice(0, 0, "English");
            langEsSentences.splice(0, 0, "Español");
            var langsSentences = [langSvSentences, langDeSentences, langEnSentences, langEsSentences];


            var langSvSentencesPerCat = data.getWordsPerCategory("1", true);
            var langDeSentencesPerCat = data.getWordsPerCategory("2", true);
            var langEnSentencesPerCat = data.getWordsPerCategory("3", true);
            var langEsSentencesPerCat = data.getWordsPerCategory("5", true);
            langSvSentencesPerCat.splice(0, 0, "Svenska");
            langDeSentencesPerCat.splice(0, 0, "Deutsch");
            langEnSentencesPerCat.splice(0, 0, "English");
            langEsSentencesPerCat.splice(0, 0, "Español");
            
            var langsSentencesPerCat = [langSvSentencesPerCat, langDeSentencesPerCat, langEnSentencesPerCat, langEsSentencesPerCat];


            if (plot1.load) {
                console.log("Updating existing charts");
                plot1.load({
                    columns: langs
                });
                plot2.load({
                    columns: langsLearned
                });
                plot3.load({
                    columns: langsSentences
                });
                plot4.load({
                   columns: langsSentencesPerCat
                });
            } else {
                // if plot1 does not yet exist, create both plots
                // Can specify a custom tick Array.
                // Ticks should match up one for each y value (category) in the series.
                var ticks = ["Basics", "Humans", "Society", "Nature"];
                
                var ticks_all_cats = [];
                
                for(var i = 0; i< data.categories.length; i++){
                    ticks_all_cats[i] = data.categories[i].name.substring(9);
                }
                
                plot1 = stats.createBarchart("#languageWordsComparison", langs, ticks);
                plot2 = stats.createBarchart("#languageWordsLearned", langsLearned, ticks);
                plot3 = stats.createBarchart("#languageSentences", langsSentences, ticks);
                plot4 = stats.createBarchart("#languageSentencesPerCat", langsSentencesPerCat, ticks_all_cats);
            }
        }


        stats.createBarchart = function (DOM_id, data, ticks) {
            console.log("Creating charts using " + JSON.stringify(data));
            var plot = c3.generate({
                bindto: DOM_id,
                data: {
                    columns: data,
                    type: 'bar'
                },
                bar: {
                    width: {
                        ratio: 0.5 // this makes bar width 50% of length between ticks
                    }
                    // or
                    //width: 100 // this makes bar width 100px
                },
                axis: {
                    x: {
                        type: 'category',
                        categories: ticks
                    }
                }
            });
            return plot;
        }

        stats.nextColor = function (index, max) {
            var val1 = Math.round(255 * (index / max));
            return ("rgb(" + val1 + ", " + (255 - val1) + ", 255)");
        }

        stats.getLanguageNames = function () {
            var names = [];
            for (var i = 0; i < data.languages.length; i++) {
                names.push(data.languages[i].name);
            }
            return names;
        }

        return stats;
    });
})();
