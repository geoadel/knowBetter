/**
 This file is part of knowBetter.
 
 knowBetter is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 knowBetter is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with knowBetter.  If not, see <http://www.gnu.org/licenses/>.
 **/

(function () {

    var app = angular.module('importDataService', ["dataService"]);
    app.factory('importDataService', ["dataService", function (data) {
            var importData = {};


            importData.loadAndMergeDataFromFile = function () {
                var conflicts = [];

                // need to start with languages, as sometimes changes might be necessary to their ids which would affect import_words and import_users as well
                conflicts[0] = importData.mergeLanguagesFromFile();

                conflicts[1] = importData.mergeCategoriesFromFile();

                conflicts[2] = importData.mergeWordsFromFile();

                // conflicts[3] = importData.mergeCommentsFromFile();

                // conflicts[4] = importData.mergeUsersFromFile();

                return conflicts;
            }

            importData.mergeLanguagesFromFile = function () {
                var conflicting_languages = [];
                ﻿if (import_languages) {
                    console.log("Found " + import_languages.length + " languages.");
                    for (var i = 0; i < import_languages.length; i++) {
                        var language = import_languages[i];
                        if (language && language.id) {
                            var existing_object = data.getObjectByID(language.id, "import_languages");

                            if (existing_object) {
                                if (existing_object.key) {
                                    // if existing object is language - only languages have property "key"
                                    if (language.key === existing_object.key && language.name === existing_object.name) {
                                        // no import needed

                                        console.log("NO_IMPORT_NECESSARY: " + JSON.stringify(existing_object) + " <--> " + JSON.stringify(language));
                                        continue;
                                    } else {
                                        // if there is a conflict, meaning the two language entries with same id are not identical, push them into conflicting_languages
                                        var conflict = [];
                                        conflict[0] = existing_object;
                                        conflict[1] = language;
                                        conflicting_languages.push(conflict);
                                        console.log("IMPORT_CONFLICT: " + JSON.stringify(conflict[0] + " (old) <--> (new) " + JSON.stringify(conflict[1])));
                                        continue;
                                    }
                                } else {
                                    // if object is not a language, language needs new id

                                    var new_id = data.getNewID();

                                    // all to be imported words need to update their language id, if it is the same
                                    if (import_words) {
                                        for (var w = 0; w < import_words.length; w++) {
                                            var word = import_words[w];
                                            if (word && word.language_ID === language.id) {
                                                word.language_ID = new_id;
                                            }
                                        }
                                    }
                                    language.id = new_id;
                                    data.languages.push(language);

                                    console.log("NO_CONFLICT (new id): " + JSON.stringify(existing_object) + " (old) <--> (new) " + JSON.stringify(language));
                                    data.updateSession();
                                    continue;
                                }
                            } else {
                                // if there is no existing object with that id, it can safely be imported
                                data.languages.push(language);

                                console.log("NO_CONFLICT: " + JSON.stringify(language));

                                data.updateSession();
                                continue;
                            }
                        }
                    }
                }
                return conflicting_languages;
            }

            importData.mergeWordsFromFile = function () {
                var conflicting = [];

                // store non-conflicting words here temporarily before adding them to the data.words array - necessary to allow for changing ids of related words
                var non_conflicting = [];

                var loadedLanguages_IDs = [];
                var loadedLanguages_WORDS = [];

                ﻿if (import_words) {
                    console.log("Found " + import_words.length + " words.");

                    outer_loop : for (var i = 0; i < import_words.length; i++) {
                        var word = import_words[i];

                        console.log(i + " Looking at word to be imported: " + word.name);

                        var existingWordsOfSameLanguage = [];

                        // store loaded words per language to not have to retrieve them for each new word again
                        if (loadedLanguages_IDs.indexOf(word.language_ID) < 0) {
                            existingWordsOfSameLanguage = data.getWordsForLanguage(word.language_ID, null, true);
                            loadedLanguages_IDs.push(word.language_ID);
                            loadedLanguages_WORDS.push(existingWordsOfSameLanguage);
                        } else {
                            existingWordsOfSameLanguage = loadedLanguages_WORDS[loadedLanguages_IDs.indexOf(word.language_ID)];
                        }

                        // first, check if there is an existing word for that language which has the same name already in the database
                        for (var x = 0; x < existingWordsOfSameLanguage.length; x++) {
                            var wordOfSameLanguage = existingWordsOfSameLanguage[x];
                            // if word has name and that name is equal to the to be imported word and the id is not the same
                            if (wordOfSameLanguage.name && wordOfSameLanguage.name === word.name) {
                                if (wordOfSameLanguage.id !== word.id) {
                                    var conflict = [];
                                    conflict[0] = wordOfSameLanguage;
                                    conflict[1] = word;
                                    conflicting.push(conflict);
                                    console.log("Conflict between " + JSON.stringify(conflict[0]) + " and " + JSON.stringify(conflict[1]));
                                }
                            }
                        }


                        if (word && word.id) {
                            var existing_object = data.getObjectByID(word.id, "importWords");
                            if (existing_object) {
                                if (existing_object.categories_IDs) {
                                    // if existing object is word - only words have property "categories_IDs"
                                    if (data.arraysNumericalEqual(word.categories_IDs, existing_object.categories_IDs)
                                            && word.name === existing_object.name
                                            && word.language_ID === existing_object.language_ID
                                            && data.arraysNumericalEqual(word.related_IDs, existing_object.related_IDs)) {
                                        // no import needed
                                        console.log("To be imported word " + word.name + " already exists in database - no import required.");
                                        continue outer_loop;
                                    } else {
                                        // if there is a conflict, meaning the two word entries with same id are not identical, push them into conflicting_languages
                                        var conflict = [];
                                        conflict[0] = existing_object;
                                        conflict[1] = word;
                                        conflicting.push(conflict);
                                        console.log("CONFLICT between " + JSON.stringify(existing_object) + " and " + JSON.stringify(word));
                                        continue outer_loop;
                                    }
                                } else {
                                    // if existing_object is not of type WORD, word needs new id and relations in import_words need to be checked

                                    var new_id = data.getNewID();

                                    for (var w = 0; w < import_words.length; w++) {
                                        var wordOther = import_words[w];
                                        var indexRelated = wordOther.related_IDs.indexOf(word.id);
                                        if (indexRelated <= 0) {
                                            wordOther.related_IDs.splice(indexRelated, 1);
                                            wordOther.related_IDs.push(new_id);
                                        }
                                    }

                                    // just to be sure, check words which have already been added to non_conflicting and look through their relations as well
                                    for (var w = 0; w < non_conflicting.length; w++) {
                                        var indexRelated = non_conflicting[w].related_IDs.indexOf(word.id);
                                        if (indexRelated <= 0) {
                                            non_conflicting[w].related_IDs.splice(indexRelated, 1);
                                            non_conflicting[w].related_IDs.push(new_id);
                                        }
                                    }

                                    word.id = new_id;
                                    non_conflicting.push(word);
                                    continue outer_loop;
                                }
                            } else {
                                // if there is no existing object with that id, it can safely be imported
                                non_conflicting.push(word);
                                continue;
                            }
                        }
                    }
                }

                console.log("Ended the LONG for loop");

                for (i = 0; i < non_conflicting.length; i++) {
                    data.words.push(non_conflicting[i]);

                    // make sure to add relations to related objects, which might as of yet be unaware of it
                    for (var r = 0; r < non_conflicting[i].related_IDs; r++) {
                        data.addRelationToObject(non_conflicting[i].id, non_conflicting[i].related_IDs[r]);
                    }
                }

                console.log("Ended the short for loop");
                data.updateSession();

                console.log("Updated session, returning " + conflicting.length + " conflicting objects");
                return conflicting;
            }


            importData.mergeCategoriesFromFile = function () {
                var conflicting = [];

                // store non-conflicting words here temporarily before adding them to the data.words array - necessary to allow for changing ids of related words
                var non_conflicting = [];

                ﻿if (import_categories) {
                    console.log("Found " + import_categories.length + " categories.");
                    for (var i = 0; i < import_categories.length; i++) {
                        var category = import_categories[i];
                        if (category && category.id) {
                            var existing_object = data.getObjectByID(category.id, "importCategories");
                            // if another object already exists with the id of category
                            if (existing_object) {
                                // if existing object is of type CATEGORY - only categories have property "group"
                                if (existing_object.group) {
                                    if (category.group === existing_object.group
                                            && category.name === existing_object.name) {
                                        // no import needed if both are identical
                                    } else {
                                        // if there is a conflict, meaning the two word entries with same id are not identical, push them into conflicting_languages
                                        var conflict = [];
                                        conflict[0] = existing_object;
                                        conflict[1] = category;
                                        conflicting.push(conflict);
                                        continue;
                                    }
                                } else {
                                    // if existing_object is not of type CATEGORY, category needs new id and relations in import_words need to be checked

                                    var new_id = data.getNewID();

                                    for (var w = 0; w < import_words.length; w++) {
                                        var word = import_word[w];
                                        var indexRelated = word.categories_IDs.indexOf(category.id);
                                        if (indexRelated <= 0) {
                                            word.categories_IDs.splice(indexRelated, 1);
                                            word.categories_IDs.push(new_id);
                                        }
                                    }

                                    for (var w = 0; w < import_users.length; w++) {
                                        var user = import_users[w];
                                        if (user && user.category === category.id) {
                                            user.category = new_id;
                                        }
                                    }
                                    category.id = new_id;
                                    non_conflicting.push(category);
                                }
                            } else {
                                // if there is no existing object with that id, it can safely be imported
                                data.categories.push(category);
                                data.updateSession();
                            }
                        }
                    }
                }

                for (i = 0; i < non_conflicting.length; i++) {
                    data.categories.push(non_conflicting[i]);
                }
                data.updateSession();

                return conflicting;
            }

            importData.mergeCommentsFromFile = function () {
                var conflicting = [];

                // store non-conflicting words here temporarily before adding them to the data.words array - necessary to allow for changing ids of related words
                var non_conflicting = [];

                ﻿if (import_comments) {
                    console.log("Found " + import_comments.length + " categories.");
                    for (var i = 0; i < import_comments.length; i++) {
                        var comment = import_comments[i];
                        if (comment && comment.id) {
                            var existing_object = data.getObjectByID(comment.id, "importComments");
                            // if another object already exists with the id of category
                            if (existing_object) {

                                // there is currently no way to check if object is a comment or other type as comments do not have unique property fields

                                var new_id = data.getNewID();

                                // go through all words to be imported and replace their relation ids to this comment with the new id for this comment
                                for (var w = 0; w < import_words.length; w++) {
                                    var word = import_word[w];
                                    var indexRelated = word.categories_IDs.indexOf(comment.id);
                                    if (indexRelated <= 0) {
                                        word.categories_IDs.splice(indexRelated, 1);
                                        word.categories_IDs.push(new_id);
                                    }
                                }

                                comment.id = new_id;
                                non_conflicting.push(comment);
                                continue;
                            } else {
                                // if there is no existing object with that id, it can safely be imported
                                data.categories.push(comment);
                                data.updateSession();
                            }
                        }
                    }
                }

                for (i = 0; i < non_conflicting.length; i++) {
                    data.comments.push(non_conflicting[i]);
                }
                data.updateSession();
                return conflicting;
            }


            importData.mergeUsersFromFile = function () {
                var conflicting = [];

                // store non-conflicting words here temporarily before adding them to the data.words array - necessary to allow for changing ids of related words
                var non_conflicting = [];

                ﻿if (import_users) {
                    console.log("Found " + import_users.length + " categories.");
                    for (var i = 0; i < import_users.length; i++) {
                        var user = import_users[i];
                        if (user && user.id) {
                            var existing_object = data.getObjectByID(user.id, "import_users");
                            // if another object already exists with the id of category
                            if (existing_object) {
                                // skip this comment, if the name is identical to the existing object andthe existing object is a user (only users hav field languageKnown_id)
                                if (existing_object.name === user.name && existing_object.languageKnown_id) {
                                    continue;
                                }

                                // there is currently no way to check if object is a comment or other type as comments do not have unique property fields

                                var new_id = data.getNewID();

                                user.id = new_id;
                                non_conflicting.push(user);

                            } else {
                                // if there is no existing object with that id, it can safely be imported
                                data.users.push(user);
                                data.updateSession();
                            }
                        }
                    }
                }

                for (i = 0; i < non_conflicting.length; i++) {
                    data.users.push(non_conflicting[i]);
                }
                data.updateSession();
                return conflicting;
            }


            return importData;
        }]);

})();
