﻿/**
    This file is part of knowBetter.

    knowBetter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    knowBetter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with knowBetter.  If not, see <http://www.gnu.org/licenses/>.
 **/


        (function () {
            app = angular.module('knowBetter', ['dataService', 'importDataService', 'exportDataService', 'testService', 'editService', 'pascalprecht.translate', 'ngAnimate', 'relateService', 'statisticsService', 'ngMaterial', 'LocalStorageModule', 'angular-flippy']);
            

            
            app.controller("dataController", ['$scope', 'dataService', 'importDataService', 'exportDataService', 'testService', 'editService', 'relateService', 'statisticsService' ,function ($scope, dataService, importDataService, exportDataService, testService, editService, relateService, statisticsService) {
                    
                    // try loading session data
                    $scope.data = dataService;
                    $scope.importData = importDataService;
                    $scope.exportData = exportDataService;
                    $scope.data.getPreviousSession();
                    $scope.test = testService;
                    $scope.edit = editService;
                    $scope.relate = relateService;
                    $scope.stats = statisticsService;
                    $scope.translations = knowBetter_UI_translations;
                    $scope.categories = knowBetter_categories;
                                        
                    $scope.$on('$viewContentLoaded', function (event) {
                        statisticsService.calculateStats(dataService);
                    });
                }
            ]);
            

            app.config(function ($translateProvider) {
                $translateProvider.translations('en', knowBetter_UI_en);
                $translateProvider.translations('de', knowBetter_UI_de);
                $translateProvider.translations('sv', knowBetter_UI_sv);
                $translateProvider.translations('es', knowBetter_UI_es);
                $translateProvider.useSanitizeValueStrategy('escaped');
                $translateProvider.preferredLanguage('en');
                $translateProvider.fallbackLanguage('en');
            });
            
            app.config(function($mdThemingProvider) {
                $mdThemingProvider.alwaysWatchTheme(true);
                $mdThemingProvider.theme('default')
                    .primaryPalette('brown')
                    .backgroundPalette('brown')
                    .warnPalette('red')
                    .accentPalette('amber');
                $mdThemingProvider.theme('grey')
                    .primaryPalette('grey')
                    .backgroundPalette('grey')
                    .warnPalette('indigo')
                    .accentPalette('lime');
                $mdThemingProvider.theme('pink')
                    .primaryPalette('pink')
                    .backgroundPalette('pink')
                    .warnPalette('amber')            
                    .accentPalette('indigo');
                $mdThemingProvider.theme('emerald')
                    .primaryPalette('teal')
                    .backgroundPalette('blue-grey')
                    .warnPalette('amber')            
                    .accentPalette('indigo');
                $mdThemingProvider.theme('indigo')
                    .primaryPalette('indigo')
                    .backgroundPalette('indigo')
                    .warnPalette('amber')            
                    .accentPalette('pink'); 
            });
            
            app.controller('dialogController', function($scope, $mdDialog, $mdMedia, $translate) {
                $scope.status = '  ';
                $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');

                $scope.showHelp = function(ev, title, content) {
                    // Appending dialog to document.body to cover sidenav in docs app
                    // Modal dialogs should fully cover application
                    // to prevent interaction outside of dialog
                    $mdDialog.show(
                          $mdDialog.alert()
                            .parent(angular.element(document.querySelector('body')))
                            .clickOutsideToClose(true)
                            .title($translate.instant(title))
                            .textContent($translate.instant(content))
                            .ariaLabel($translate.instant(title))
                            .ok($translate.instant("OKAY"))
                            .targetEvent(ev)
                            .theme($scope.data.getUserTheme())
                    );
                };

                $scope.showTemplateHelp = function(ev, template) {
                    var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
                    $mdDialog.show({
                        controller: DialogController,
                        templateUrl: template,
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose:true,
                        fullscreen: useFullScreen
                    }).then(function(answer) {
                        $scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        $scope.status = 'You cancelled the dialog.';
                    });
                    $scope.$watch(function() {
                        return $mdMedia('xs') || $mdMedia('sm');
                    }, function(wantsFullScreen) {
                        $scope.customFullscreen = (wantsFullScreen === true);
                    });
                };
                
                function DialogController($scope, $mdDialog) {
                    $scope.hide = function() {
                        $mdDialog.hide();
                    };

                    $scope.cancel = function() {
                        $mdDialog.cancel();
                    };
                }
            });
            
            

            app.controller('LanguageController', function ($rootScope, $scope, $translate, dataService) {                             
                $scope.changeLanguage = function (id) {
                    console.log("Trying to change language to "+id);
                    var key = "en";
                    if(id){
                        key = $scope.data.getObjectByID(id, "app.LanguageController").key;                        
                    }

                    $translate.use(key).then(function (key) {
                        $scope.data.user.languageKnown_id = id;
                        $scope.data.updateSession();
                    }, function (key) {
                        console.log("Language key not recognized...");
                    });
                };
                // on startup load the users last selected known language as UI language
                angular.element(document).ready(function () {
                    console.log("Loaded user "+dataService.user.name+" with languages "+dataService.user.languageKnown_id+" , "+dataService.user.languageLearn_id);
                    $scope.changeLanguage(dataService.user.languageKnown_id);
                });              
            });

                      
            app.config(function(localStorageServiceProvider){
               localStorageServiceProvider.setPrefix('knowBetter')
                       .setStorageType('localStorage');
            });
            
            
            app.directive('start', function () {
                return {
                    restrict: 'E',
                    templateUrl: "./templates/start.html"
                };
            });     
            app.directive('editor', function () {
                return {
                    restrict: 'E',
                    templateUrl: "./templates/editor.html"
                };
            });
                        
            app.directive('selectTests', function () {
                return {
                    restrict: 'E',
                    templateUrl: "./templates/select-tests.html"
                };
            });
            
            app.directive('trainPairs', function () {
                return {
                    restrict: 'E',
                    templateUrl: "./templates/train-pairs.html"
                };
            });
            app.directive('trainGrid', function () {
                return {
                    restrict: 'E',
                    templateUrl: "./templates/train-grid.html"
                };
            });
            app.directive('testSimilar', function () {
                return {
                    restrict: 'E',
                    templateUrl: "./templates/test-similar.html"
                };
            });            

            app.directive('testGap', function () {
                return {
                    restrict: 'E',
                    templateUrl: "./templates/test-gap.html"
                };
            });            
            app.directive('testGrid', function () {
                return {
                    restrict: 'E',
                    templateUrl: "./templates/test-grid.html"
                };
            });
            app.directive('testListen', function () {
                return {
                    restrict: 'E',
                    templateUrl: "./templates/test-listen.html"
                };
            });
            app.directive('testEnd', function () {
                return {
                    restrict: 'E',
                    templateUrl: "./templates/test-end.html"
                };
            });
            app.directive('testTranslate', function () {
                return {
                    restrict: 'E',
                    templateUrl: "./templates/test-translate.html"
                };
            });

            app.directive('edit', function () {
                return {
                    restrict: 'E',
                    templateUrl: "./templates/edit-edit.html"
                };
            });
            app.directive('translate', function () {
                return {
                    restrict: 'E',
                    templateUrl: "./templates/edit-translate.html"
                };
            });
            app.directive('relate', function () {
                return {
                    restrict: 'E',
                    templateUrl: "./templates/edit-relate.html"
                };
            });
            app.directive('relateCategory', function () {
                return {
                    restrict: 'E',
                    templateUrl: "./templates/edit-relate-category.html"
                };
            });
            app.directive('statistics', function () {
                return {
                    restrict: 'E',
                    templateUrl: "./templates/statistics.html"
                };
            });
            app.directive('settings', function () {
                return {
                    restrict: 'E',
                    templateUrl: "./templates/edit-settings.html"
                };
            });
            
            app.directive('backgroundImage', function () {
                return function (scope, element, attrs) {
                    restrict : 'A',
                            attrs.$observe('backgroundImage', function (value) {
                                element.css({
                                    'background-image': 'url(' + value + ')'
                                });
                            });
                };
            });
            
            app.directive('backgroundImage', function () {
                return function (scope, element, attrs) {
                    restrict : 'A',
                            attrs.$observe('backgroundImage', function (value) {
                                element.css({
                                    'background-image': 'url(' + value + ')'
                                });
                            });
                };
            });
            
            
            app.filter('categoriesContainsID', function () {
                return function (items, id) {
                    var filtered = [];
                    for (var i = 0; i < items.length; i++) {
                        for (var r = 0; r < items[i].categories.length; r++) {
                            if (items[i].categories[r] === id) {
                                filtered.push(items[i]);
                            }
                        }
                    }
                    return filtered;
                };
            });
            
            /**
             * a custom filter to only let through words with same language as given language_id - if ignoreSentences === true, sentences are excluded from result
             */
            app.filter('sameLanguage', function() {
              return function(input, language_id, ignoreSentences) {
                console.log("Called sameLanguage filter with language_id "+language_id+", ignore sentences: "+ignoreSentences);
                var output = [];

                if(!input || !language_id){
                    console.log("Could not parse input " + input.length + " or language_id "+language_id+".");
                    return input;
                } else {
                    for(var i = 0; i < input.length; i++){
                        if(input[i].language === language_id){
                            if(ignoreSentences){
                                if(input[i].sentence){   
                                    // console.log(input[i].name + " is a sentence...");
                                } else {
                                    output.push(input[i]);
                                }
                            } else {
                                output.push(input[i]);
                            }

                        } else {
                            // console.log(input[i].name +" with language id "+input[i].language + " was not of language id "+language_id);
                        }
                    }
                }

                return output;

              }

            });

            
            app.filter('untranslatedWords', function () {
                return function (items, language2_id, filterIsActive, data) {
                    var filtered = [];
                    if (filterIsActive && items !== undefined) {
                        console.log("Filter is active");
                        for (var i = 0; i < items.length; i++) {
                            var isTranslated = false;
                            for (var r = 0; r < items[i].related.length; r++) {
                                console.log("Tryng to get object by ID "+items[i].related[r]);
                                var related = data.getObjectByID(items[i].related[r], "app.Filter Untranslated Words");
                                if(related){
                                                                 // console.log("Looking at object "+related.name+": "+JSON.stringify(related));
                                    if (related.language === language2_id) {
                                        isTranslated = true;
                                    }   
                                }
                            }
                            if (isTranslated === false) {
                                filtered.push(items[i]);
                            }
                        }
                    } else {
                        filtered = items;
                    }
                    return filtered;
                };
            });
                  

            app.controller('TranslateController', function ($translate, $scope) {
                $scope.changeLanguage = function (id) {
                    console.log("Trying again to change language to "+id);
                    language_key = data.getObjectByID(id, "app.TranslateController").key;
                    $translate.use(language_key);
                };
            });

            app.controller('StatisticsController', ['$scope', '$interval', 'dataService', 'statisticsService', function ($scope, $interval, data, stats) {

                }
            ]);

        })();
