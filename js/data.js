/**
 This file is part of knowBetter.
 
 knowBetter is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 knowBetter is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with knowBetter.  If not, see <http://www.gnu.org/licenses/>.
 **/

(function () {

    var app = angular.module('dataService', ['LocalStorageModule']);
    app.factory('dataService', ['localStorageService', function (localStorageService) {
            var data = {};
            data.hasChanged = false; // track, whether data have changed to alert user that saving data file is necessary

            // initialize data arrays to file values
            data.languages = [];
            data.users = [];
            data.comments = [];
            data.words = [];
            data.categories = [];
            data.tags = [];

            data.user = data.users[0];

            data.themes = ['default', 'pink', 'grey', 'emerald', 'indigo'];

            data.newUser = function (name) {
                var user = {};
                user.name = name;
                user.id = data.getNewID();
                user.languageKnown_id = 1;
                user.languageLearn_id = 0;
                user.words_learned_count = 4;
                user.progress = [
                ];
                user.category = {};
                console.log("Created new user");
                return user;
            };

            /**
             * 
             * @param {type} wordsList      All the words being trained / tested in this session
             * @param {type} wordsSolved    The words which have been solved during the test succesfully
             * @returns {undefined}
             */
            data.setUserSessionToday = function (wordsList, wordsSolved) {

                // firstof all, create a date key, so for each day, sessions and their included solved and unsolved words can be tracked
                var dateObj = new Date();
                var month = dateObj.getUTCMonth() + 1;
                var day = dateObj.getUTCDate();
                var hour = dateObj.getHours();
                var minute = dateObj.getMinutes();
                var year = dateObj.getUTCFullYear();

                var dateKey = year + month + day + hour + minute;

                if (!data.user.sessionKeys) {
                    data.user.sessionKeys = [];
                }
                if (!data.user.sessionWords) {
                    data.user.sessionWords = [];
                }
                if (!data.user.sessionWordsSolved) {
                    data.user.sessionWordsSolved = [];
                }

                // if no session has been recorded for this user by this date yet, add a new entry
                if (data.user.sessionKeys.indexOf(dateKey) < 0) {
                    data.user.sessionKeys.push(dateKey);
                    data.user.sessionWords.push(wordsList);
                    data.user.sessionWordsSolved.push(wordsSolved);
                } else {
                    // if a session for this day was already recorded, add that session to the one for this day

                }
            };

            /**
             * get similar words - similarity is defined as the weighted sum of
             * a) the word length similarity (40)
             * b) the similarity of ngrams (30)
             * c) the similarity of character placement start to end and end to start (30)
             * 
             * @param {STRING} word_name
             * @param {STRING} language_id
             * @returns {undefined}
             */
            data.getSimilarWords = function (word_name, words) {

                console.log("getSimilarWords was called for word " + word_name);

                var index_similarity = [words.length];
                for (var i = 0; i < words.length; i++) {
                    var similarWord = words[i].name;
                    // console.log("Looking at combination " + similarWord + " - " + word_name);
                    if (similarWord !== word_name && !words[i].sentence) {

                        var similarity = {};
                        similarity.index = 0;
                        similarity.word = words[i];

                        var minWord = word_name.length < similarWord.length ? word_name : similarWord;
                        var maxWord = word_name.length > similarWord.length ? word_name : similarWord;
                        var minLength = word_name.length < similarWord.length ? word_name.length : similarWord.length;
                        var maxLength = word_name.length > similarWord.length ? word_name.length : similarWord.length;

                        // 1. word length will count as 20% of the similarity value
                        // assign similarity index for length in relation to the relative length difference between the two
                        similarity.index = (minLength / maxLength) * 40;


                        // 2. bigrams are checked for similarities
                        var ngrams_maxWord = data.getNGramFrequencies(maxWord);
                        var ngrams_minWord = data.getNGramFrequencies(minWord);

                        for (var m = 0; m < ngrams_maxWord.letters.length; m++) {
                            if (ngrams_minWord.letters.indexOf(ngrams_maxWord.letters[m]) > -1) {
                                similarity.index = similarity.index + ((30 / ngrams_maxWord.letters.length));
                            }
                        }


                        var similarity_start = 0;
                        var similarity_end = 0;

                        // 3. similarity by character order from beginning and end (range 0 to 20 for total similarity for each)
                        for (var m = 0; m < minWord.length; m++) {
                            if (minWord.charAt(m) === maxWord.charAt(m)) {
                                similarity_start = similarity_start + (15 / minWord.length);
                            }
                        }

                        for (var m = maxWord.length; m > 0; m--) {
                            if (minWord.charAt(m) === maxWord.charAt(m)) {
                                similarity_end = similarity_end + (15 / maxWord.length);
                            }
                        }


                        similarity.index = similarity.index + similarity_start + similarity_end;

                        // console.log("Similarity was at  " + similarity.index);

                        index_similarity.push(similarity);
                    }
                }



                // sort words by similarity.index and the _.sortBy function of underscore library
                index_similarity = _.sortBy(index_similarity, "index");
                // console.log("Returning " + index_similarity.length + " similar words for " + word_name);
                return index_similarity;
            }

            /**
             * 
             * @param {STRING} word_name
             * @returns {undefined}
             */
            data.getNGramFrequencies = function (word_name) {
                var ngrams = {};
                ngrams.letters = [];
                ngrams.frequencies = [];
                // the outer loop controls the size of the gram, so two for bigram, three for trigram, etc....
                for (var i = 2; i < word_name.length && i <= 3; i++) {
                    // the inner loop runs through all characters and creates n-grams based on the size of i
                    for (var m = 0; m + i < word_name.length; m++) {
                        // console.log("Looking at index " + i + "-" + m);

                        var nGram = word_name.substring(m, m + i);
                        var nIndex = ngrams.letters.indexOf(nGram);

                        // console.log("Ngram " + nGram);
                        if (nIndex > -1) {
                            ngrams.frequencies[nIndex] = ngrams.frequencies[nIndex] + 1;
                        } else {
                            ngrams.letters.push(nGram);
                            ngrams.frequencies.push(1);
                        }

                    }
                }
                return ngrams;
            };

            data.getUserTheme = function () {
                if (data.user) {
                    if (data.user.theme) {
                        if (data.themes.indexOf(data.user.theme) >= 0) {
                            return data.user.theme;
                        }
                    }
                }
                return "default";
            }

            data.setUserTheme = function (theme) {
                if (data.themes.indexOf(theme) >= 0) {
                    data.user.theme = theme;
                    data.updateSession();
                }
            }

            data.setUserLanguageKnown = function (language) {
                data.user.languageKnown_id = language.id;
            };

            data.setUserLanguageLearn = function (language) {
                data.user.languageLearn_id = language.id;
            };



            /**
             * sets data.hasChanged to true, to indicate to other functions and the user, that unsafed data exist
             */
            data.dataHaveChanged = function () {
                data.hasChanged = true;
                data.updateSession();
            };






            // searches within given array for comments with given parent word and return all matches
            data.getCommentsForWord = function (word) {
                var comments = [];
                for (var i = 0; i < data.comments.length; i++) {
                    var comment = data.comments[i];
                    if (comment.related_IDs.indexOf(word.id) >= 0) {
                        comments.push(comment);
                    }
                }
                // console.log("Returning "+comments.length+" comments for "+word.name);
                return comments;
            };


            /**
             * searches for sentences with relation to given word and returns them
             * 
             * @param {type} word
             * @returns {Array}
             */
            data.getSentencesForWord = function (word) {
                var sentences = [];
                // console.log("get sentences for word");
                for (var i = 0; i < data.words.length; i++) {
                    var item = data.words[i];
                    // check that it is a sentence
                    if (item.sentence) {
                        // check if language is the same
                        if (word.language_ID === item.language_ID) {
                            // check, if item relations contain word.id
                            for (var k = 0; k < item.related_IDs.length; k++) {
                                if (item.related_IDs[k] === word.id) {
                                    // console.log("Found sentence "+item.name+" for "+word.name);
                                    sentences.push(item);
                                }
                            }
                        }
                    } else {
                        // console.log("Word "+item.name+" is NOT a sentence.");
                    }
                }
                // console.log("Returning "+sentences.length+" sentences for "+word.name);
                return sentences;
            };


            data.getSentencesForLanguage = function (language_id) {
                var words = data.getWordsForLanguage(language_id);
                var result = [];
                for (var i = 0; i < words.length; i++) {
                    if (words[i].sentence) {
                        result.push(words[i]);
                    }
                }
                return result;
            }

            /**
             * Can be used to find other words of same language which have the same content
             * Usefull if you want to warn users before they add duplicates unknowingly
             * 
             * @param {type} name
             * @param {type} language_id
             * @param {type} word_id
             * @returns {Array}
             */
            data.getWordByNameAndLanguage = function (name, language_id, word_id) {
                var result = [];

                var words = data.getWordsForLanguage(language_id, null, true);
                if (words) {
                    for (var i = 0; i < words.length; i++) {
                        if (words[i].name === name) {
                            if (word_id) {
                                if (word_id !== words[i].id) {
                                    result.push(words[i]);
                                }
                            } else {
                                result.push(words[i]);
                            }
                        }
                    }
                }
                console.log("Found " + result.length + " words with same name as " + name + ": " + JSON.stringify(result));
                return result;
            }


            /**
             * Checks whether word is != null, and if so, tries to returns its comments.
             * 
             * @param {Word} word
             * @returns {Comments[]}
             */
            data.getComments = function (word) {
                if (word) {
                    return data.getCommentsForWord(word);
                }
            };



            /**
             * Checks whether word already loaded sentences and if not, tries to returns its sentences.
             * 
             * @param {Word} word
             * @returns {Sentences[]}
             */
            data.getSentences = function (word) {
                if (!word.sentences) {
                    word.sentences = data.getSentencesForWord(word);
                    return word.sentences;
                } else {
                    return word.sentences;
                }
            };




            // try to load the user by its name, if the name is in the database, retrieve the information, otherwise create a new user
            /**
             * load a user by its name from database, if the name is found for a user entry, load the data for that user entry to data.user
             * 
             * @param {String} name     The name of the user to be retrieved
             */
            data.loadUser = function (name) {
                console.log("The name " + data.nameEntered + " was entered.");
                for (var i = 0; i < data.users.length; i++) {
                    if (data.users[i].name === data.nameEntered) {
                        data.user = data.users[i];
                        console.log('User ' + data.user.name + ' found.');
                    } else {
                        console.log('User ' + data.users[i].name + " does not equal " + data.nameEntered);
                        data.user = new user(data.nameEntered);
                        data.user.languageKnown_id = data.languages[0];
                        data.user.languageLearn_id = data.languages[1];
                    }
                }
            };

            /**
             * Returns the username or undefined, if no previous session was stored - can be used to evaluate using a boolean test
             * @returns {localStorageService@call;get.name}
             */
            data.hasPreviousSession = function () {
                var user = localStorageService.get('user');
                if (user.name) {
                    return true;
                }
                return false;
            };

            /**
             * load previous session and all data from lcoal storage.
             * If that does not work, try loading the data from files and create a new user
             * and store those data in local storage
             */
            data.getPreviousSession = function () {
                var loadedFromFile = false;
                // localStorageService.clearAll();
                // console.log(JSON.stringify(data.getObjectectBByID(5)));
                data.user = localStorageService.get('user');
                if (!data.user) {
                    data.user = data.newUser("user");
                    console.log("Needed to create new user " + data.user.name);
                }
                data.languages = localStorageService.get('languages');
                if (!data.languages || data.languages.length === 0) {
                    data.languages = knowBetter_languages;
                    console.log("Needed to load " + data.languages.length + " languages from file");
                    loadedFromFile = true;
                }
                data.users = localStorageService.get('users');
                if (!data.users || data.users.length === 0) {
                    data.users = knowBetter_users;
                    console.log("Needed to load " + data.users.length + " users from file");
                    loadedFromFile = true;
                }
                data.comments = localStorageService.get('comments');
                if (!data.comments || data.comments.length === 0) {
                    data.comments = knowBetter_comments;
                    console.log("Needed to load " + data.comments.length + " comments from file");
                    loadedFromFile = true;
                }
                data.words = localStorageService.get('words');
                if (!data.words || data.words.length === 0) {
                    data.words = knowBetter_words;
                    console.log("Needed to load " + data.words.length + " words from file");
                    loadedFromFile = true;
                }
                data.categories = localStorageService.get('categories');
                if (!data.categories || data.categories.length === 0) {
                    data.categories = knowBetter_categories;
                    console.log("Needed to load " + data.categories.length + " categories from file");
                    loadedFromFile = true;
                }
                data.tags = localStorageService.get('tags');
                if (!data.tags || data.tags.length === 0) {
                    data.tags = knowBetter_tags;
                    console.log("Needed to load " + data.tags.length + " tags from file");
                    loadedFromFile = true;
                }

                // if it was necessary, to load data from files, put all data into local storage
                if (loadedFromFile) {
                    data.updateSession();
                } else {
                    console.log("Using existing local storage with " + data.words.length + " words, " + data.languages.length + " languages, " + data.categories.length + " categories.");
                }
            };

            /**
             *  Updates the local storage 
             *
             */
            data.updateSession = function () {
                localStorageService.set('user', data.user);

                data.overwriteObject(data.user);

                localStorageService.set('languages', data.languages);
                localStorageService.set('users', data.users);

                localStorageService.set('comments', data.comments);
                localStorageService.set('words', data.words);
                localStorageService.set('categories', data.categories);
                
                localStorageService.set('tags', data.tags);
                
                console.log("Updated local storage session data.");
                console.log(JSON.stringify(data.user));
            };

            /**
             * Clears the local storage
             */
            data.clearLocalStorage = function () {
                return localStorageService.clearAll();
            };


            /**
             * Get language object by ID
             * 
             * @param {type} id
             * @returns {data.languages}
             */
            data.getLanguageByID = function (id) {
                for (var i = 0; i < data.languages.length; i++) {
                    if (data.languages[i].id === id) {
                        return data.languages[i];
                    }
                }
            };

            /**
             * Get user by ID
             * 
             * @param {type} id
             * @returns {data.users}
             */
            data.getUserByID = function (id) {
                for (var i = 0; i < data.users.length; i++) {
                    if (data.users[i].id === id) {
                        return data.users[i];
                    }
                }
            };

            /**
             * Get category by ID
             * 
             * @param {type} id             id of category
             * @returns {data.categories}   the category object with that id
             */
            data.getCategoryByID = function (id) {
                for (var i = 0; i < data.categories.length; i++) {
                    if (data.categories[i].id === id) {
                        return data.categories[i];
                    }
                }
            };

            /**
             * Get object by ID
             * 
             * @param {type} id             id of object
             * @returns {object}   the object with that id
             */
            data.getObjectByID = function (id, callerName) {

                if (id && callerName) {
                    // console.log(callerName + " called getObjectByID " + id);
                    // console.log("Looking through " + data.users.length + " users, " + data.categories.length + " categories, " + data.words.length + " words, " + data.languages.length + " languages and " + data.comments.length + " comments.");
                    for (var i = 0; i < data.users.length; i++) {
                        if (data.users[i].id === id) {
                            return data.users[i];
                        }
                    }
                    for (var i = 0; i < data.categories.length; i++) {
                        if (data.categories[i].id === id) {
                            return data.categories[i];
                        }
                    }

                    for (var i = 0; i < data.comments.length; i++) {
                        if (data.comments[i].id === id) {
                            return data.comments[i];
                        }
                    }
                    for (var i = 0; i < data.languages.length; i++) {
                        if (data.languages[i].id === id) {
                            return data.languages[i];
                        }
                    }
                    for (var i = 0; i < data.words.length; i++) {
                        if (data.words[i].id === id) {
                            return data.words[i];
                        }
                    }
                }

                // console.log(callerName + " -> no ID " + id + " - " + JSON.stringify(id));
            };






            data.storeObject = function (object) {
                if (object && object.name && object.id) {

                    if (data.isWord(object)) {
                        // only words have property 'categories_IDs'
                        data.storeWord(object);

                    } else if (data.isCategory(object)) {
                        // only categories have property 'group'
                        data.storeCategory(object);

                    } else if (data.isLanguage(object)) {
                        // only language have property 'key'
                        data.storeLanguage(object);

                    } else if (data.isUser(object)) {
                        // only users have property 'languageKnown_id'
                        data.storeUser(object);
                    } else {
                        // store as comment
                        data.storeComment(object);
                    }
                }
            }

            data.storeCategory = function (category) {
                // if category with same id or same content exists, overwrite that one
                var cat_exists = false;
                for (var i = 0; i < data.categories.length; i++) {
                    var old_cat = data.categories[i];
                    if (old_cat.id === category.id || (old_cat.name === category.name && old_cat.group === category.group)) {
                        cat_exists = i;
                    }
                }

                if (cat_exists) {
                    data.categories[cat_exists] = category;
                } else {
                    data.categories.push(category);
                }
            }


            /**
             * 
             * @param {type} word       The word object to be stored in the database
             * @returns {word}          Returns word, helpful in case, id was not set before but is needed to add relation to another object
             */
            data.storeWord = function (word) {
                // console.log("data.storeWord(" + JSON.stringify(word) + ") was called.");
                // check, if actually an existing object was given and if it has a name and a language
                if (word && word.name && word.language_ID) {
                    // console.log("Storing word " + word.name + "[" + word.language_ID + "]");

                    // replace all ", as they would break the content.js format and potentially render the database unreadable
                    if (word.name.indexOf('"') >= 0) {
                        word.name = word.name.replace(/"/g, ''); // regex for replacing all instances of "
                    }

                    if (data.overwriteObject(word) === false) {
                        // if word does not yet have an ID, create one for it
                        if (!word.id) {
                            word.id = data.getNewID();
                        }
                        data.words.push(word);
                        console.log("Stored new word " + word.name);
                    }
                    console.log("Looking at relations of " + word.name);

                    var removeNonexistingRelations = [];
                    // make sure, that any other object, that this word is related to, knows about it
                    for (var i = 0; i < word.related_IDs.length; i++) {
                        var relation = data.getObjectByID(word.related_IDs[i], "data.storeWord");
                        console.log("Looking at related word with id " + word.related_IDs[i]);
                        if (relation) {
                            if (relation.related_IDs.indexOf(word.id) === -1) {
                                relation.related_IDs.push(word.id);
                            }
                        } else {
                            removeNonexistingRelations.push(i);
                        }
                    }

                    for (var i = 0; i < removeNonexistingRelations.length; i++) {
                        word.related_IDs.splice(removeNonexistingRelations[i], 1);
                        console.log("Removed relation from word, as the target does not exist.");
                    }

                    // console.log("Stored file " + JSON.stringify(data.getObjectByID(word.id, '')) + "with ID " + word.id + " in database.");

                    // remove orphaned relations, if the word is not related to other objects anymore but they to it, update their relations array
                    // data.removeOrphanedRelations(word);
                    data.dataHaveChanged();
                } else {
                    console.log("Could not store " + word);
                }

                return word;
            }

            /**
             * Remove an one-sided relations, which are not mutually shared by the supposedly related object
             * 
             * @param {type} object
             * @returns {undefined}
             */
            data.removeOrphanedRelations = function (object) {
                for (var i = 0; i < data.words.length; i++) {
                    // console.log("Working with word "+data.words[i].name+" to look for orphaned relations.");
                    var relation_From_Other = data.words[i].related_IDs.indexOf(object.id);
                    if (relation_From_Other !== -1) {
                        var relation_To_Other = object.related_IDs.indexOf(data.words[i].id);
                        if (relation_To_Other === -1) {
                            data.removeRelationFromObject(data.words[i].id, object.id);
                            console.log("Removed relation of " + data.words[i].name + " towards " + object.name + ", since it was one-sided.");
                        }
                    }
                }
            }

            /**
             * Add relation relation_ID to object object_ID
             * 
             * @param {type} object_ID
             * @param {type} relation_ID
             * @returns {undefined}
             */
            data.addRelationToObject = function (object_ID, relation_ID) {
                if (object_ID !== relation_ID) {
                    console.log("Retrieving " + object_ID + " to add relation " + relation_ID);
                    var object = data.getObjectByID(object_ID, "data.addRelationToObject()");
                    var relation = data.getObjectByID(relation_ID, 'data.addRelationToObject()');

                    // if relation_ID is not undefined and is not yet related to object, add the relation to object
                    if (relation && relation_ID && object.related_IDs.indexOf(relation_ID) < 0) {
                        object.related_IDs.push(relation_ID);
                        // store object
                        data.overwriteObject(object);
                    }
                    // make sure to add the reverse relation, if not yet existing
                    if (relation && relation.related_IDs.indexOf(object_ID) < 0) {
                        relation.related_IDs.push(object_ID);
                        // store object
                        data.overwriteObject(relation);
                    }
                }

            }

            /**
             * Remove Relation relation_ID from object object_ID
             * 
             * @param {type} object_ID
             * @param {type} relation_ID
             * @returns {undefined}
             */
            data.removeRelationFromObject = function (object_ID, relation_ID) {
                console.log("Retrieving " + object_ID + " to remove relation " + relation_ID);
                object = data.getObjectByID(object_ID, "data.removeRelationFromObject");
                var i = object.related_IDs.indexOf(relation_ID);
                if (i != -1) {
                    object.related_IDs.splice(i, 1);
                    data.overwriteObject(object);
                    console.log("Removed relation to " + relation_ID + " from object " + object.name);
                }
            }

            /**
             * Check, whether language already exists in databse (if so, overwrite it). Otherwise add it as new langzage to the database
             * 
             * @param {type} language
             * @returns {undefined}
             */
            data.storeLanguage = function (language) {
                if (!data.overwriteObject(language)) {
                    data.languages.push(language);
                }
                data.dataHaveChanged();
            }

            /**
             * Store comment
             * 
             * @param {type} comment
             * @returns {undefined}
             */
            data.storeComment = function (comment) {
                // if word does not yet have an ID, create one for it
                if (!comment.id) {
                    comment.id = data.getNewID();
                }

                // replace all ", as they would break the content.js format and potentially render the database unreadable
                if (comment.name.indexOf('"') >= 0) {
                    comment.name = comment.name.replace(/"/g, ''); // regex for replacing all instances of "
                }

                if (!data.overwriteObject(comment)) {
                    data.comments.push(comment);
                }
                data.dataHaveChanged();
            }


            /**
             * 
             * @param {type} object
             * @returns {Boolean}
             */
            data.overwriteObject = function (object) {
                if (object.id) {
                    // first get existing object by id
                    console.log("Overwriting " + object.id);
                    var object_old = data.getObjectByID(object.id, "data.overwriteObject");
                    if (object_old) { // if the word was found, get its position in the array
                        var iWord = data.words.indexOf(object_old);
                        var iComment = data.comments.indexOf(object_old);
                        var iLanguage = data.languages.indexOf(object_old);
                        var iUsers = data.users.indexOf(object_old);
                        if (iWord !== -1) {
                            // put new word in place of old word
                            data.words[iWord] = object;
                            console.log("Overwrote word " + object.name + ".");
                            data.dataHaveChanged();
                            return true;
                        }
                        if (iComment !== -1) {
                            // put new word in place of old word
                            data.comments[iComment] = object;
                            console.log("Overwrote comment " + object.name + ".");
                            data.dataHaveChanged();
                            return true;
                        }
                        if (iLanguage !== -1) {
                            // put new language in place of old word
                            data.languages[iLanguage] = object;
                            console.log("Overwrote language " + object.name + ".");
                            data.dataHaveChanged();
                            return true;
                        }
                        if (iUsers !== -1) {
                            // put new user in place of old user
                            data.users[iUsers] = object;
                            console.log("Overwrote user " + object.name + ".");
                            data.dataHaveChanged();
                            return true;
                        }
                    }
                }

                return false;
            }



            data.deleteObjectByID = function (id) {
                // search in data arrays for id
                var index = data.getIndexOfIDinArray(data.words, id);

                var deleted = false;

                if (index >= 0) {
                    console.log("Removed word " + data.words[index].name + " with id " + id);
                    data.words.splice(index, 1);
                    data.removeRelationsToID(id);
                    deleted = true;
                }
                index = data.getIndexOfIDinArray(data.languages, id);
                if (index >= 0) {
                    data.languages.splice(index, 1);
                    console.log("Removed language with id " + id);
                    deleted = true;
                }
                index = data.getIndexOfIDinArray(data.categories, id);
                if (index >= 0) {
                    data.categories.splice(index, 1);
                    console.log("Removed category with id " + id);
                    deleted = true;
                }
                index = data.getIndexOfIDinArray(data.users, id);
                if (index >= 0) {
                    data.users.splice(index, 1);
                    console.log("Removed user with id " + id);
                    deleted = true;
                }
                index = data.getIndexOfIDinArray(data.comments, id);
                if (index >= 0) {
                    data.comments.splice(index, 1);
                    console.log("Removed user with id " + id);
                    data.removeRelationsToID(id);
                    deleted = true;
                }

                return deleted;
            }


            /**
             * Used to create translation pairs
             * 
             * @param {type} category_id
             * @param {type} languageKnown_id
             * @param {type} languageLearn_id
             * @returns {Array}
             */
            data.getWordPairsForCategoryAndLanguages = function (category_id, languageKnown_id, languageLearn_id) {
                this.pairs = [];
                // console.log("Looking for words with language " + languageKnown_id + " and category " + category_id + " which have a related word of language " + languageLearn_id);
                for (var i = 0; i < data.words.length; i++) {
                    var word = data.words[i];
                    if (word.categories_IDs.indexOf(category_id) >= 0) {

                        // ignore sentences
                        if (word.language_ID === languageLearn_id && !word.sentence) {
                            console.log("Looking to add word " + word.name + " - " + word.sentence);

                            // if found a word in languageKnown, search within its relations for words in the language to be learned
                            for (var r = 0; r < word.related_IDs.length; r++) {
                                var relatedWord = data.getObjectByID(word.related_IDs[r], "data.getWordPairsForCategoryAndLanguages");
                                // check that language is the one we are looking for and check whether the category is correct - this is, because there
                                // are words like English "party" which can in different categories have very different meanings (e.g. political party or birthday party)
                                // only include translations in other languages which fit the category currently looked for
                                if (relatedWord.language_ID === languageKnown_id && relatedWord.categories_IDs.indexOf(category_id) >= 0) {
                                    // create a pair object, containing the word of language known and another word of language to be learned
                                    // use a copy of the objects to avoid interaction between different pairs sharing the same object
                                    // (if one word has several alternative translations, it will and should be added multiple time)
                                    this.pair = [angular.copy(word), angular.copy(relatedWord)];

                                    if (this.pairs.indexOf(this.pair) === -1) {
                                        this.pair.index = this.pairs.length;
                                        this.pairs.push(this.pair);
                                    }
                                }
                            }
                        }
                    }
                }


                console.log("Returning " + this.pairs.length + " pairs.");

                return this.pairs;
            };


            /**
             * Return all words in array words, which are in category category_id and of language language_id
             * 
             * @param {Words[]} words           Array of words      
             * @param {Integer} category_id     ID of category
             * @param {Integer} language_id     ID of language
             * @returns {Array}                 Array of words of language language_id and in category category_id
             */
            data.getRelatedWordsForLanguageInCategory = function (words, category_id, language_id) {
                var result = [];

                // first, get all related words
                for (var i = 0; i < words.length; i++) {
                    var word = words[i];
                    if (word.categories_IDs.indexOf(category_id) >= 0) {
                        // console.log(word.name+" belongs to category "+category_id);
                        // if word is in category, see if one of its related words is of the other language
                        for (var k = 0; k < word.related_IDs.length; k++) {
                            var wordRelated = data.getObjectByID(word.related_IDs[k], "data.getRelatedWordsForLanguageInCategory");
                            console.log(wordRelated.name + " is related to " + word.name + " and has language " + wordRelated.language_ID + " = " + language_id + "?");
                            if (wordRelated.language_ID === language_id) {
                                result.push(word);
                                result.push(wordRelated);
                                // console.log("Put "+word.name+" and "+wordRelated.name+" into the results, as they match the given parameters.");
                            }
                        }
                    }
                }
                console.log("Found " + result.length + " words for language " + language_id);
                return result;
            }

            /**
             * Return all words, which are related to given word and (optionally) are of language language_id
             * 
             * @param {WORD} word
             * @param {INTEGER} language_id
             * @param {BOOLEAN} true, if only words with no translation into language_id should be returned
             * @returns {Array}
             */
            data.getRelatedWordsForLanguage = function (word, language_id) {
                var resultRelated = [];

                for (var i = 0; i < data.words.length; i++) {
                    if (word.related_IDs.indexOf(data.words[i].id) > -1) {
                        resultRelated.push(data.words[i]);
                    }
                }

                var result = [];

                // if language_id was given, only return those words which are of language_ID
                // else return all words
                if (language_id) {
                    // if a language_id was given, only return words of that language
                    for (var i = 0; i < resultRelated.length; i++) {
                        if (resultRelated[i].language_ID === language_id) {
                            result.push(resultRelated[i]);
                            // console.log("Added " + result[result.length -1].name);
                        }
                    }
                } else {
                    return resultRelated;
                }


                return result;
            }


            /**
             * 
             * Returns frequency of words / sentences for each category
             * 
             * @param {type} language_id
             * @param {type} only_sentences
             * @returns {undefined}
             */
            data.getWordsPerCategory = function (language_id, only_sentences) {
                var count_result = [];

                // prefill count_result with integer values
                for (var i = 0; i < data.categories.length; i++) {
                    count_result[i] = 0;
                }

                for (var w = 0; w < data.words.length; w++) {
                    if (data.words[w].sentence) {
                        if (data.words[w].categories_IDs) {
                            if (data.words[w].language_ID === language_id) {
                                for (var c = 0; c < data.words[w].categories_IDs.length; c++) {
                                    // console.log("Looking at word " + data.words[w].name + "with category id " + data.words[w].categories_IDs[c]);
                                    var category = data.getCategoryByID(data.words[w].categories_IDs[c]);
                                    if (category) {
                                        if (only_sentences) {
                                            if (data.words[w].sentence) {
                                                var category_index = data.categories.indexOf(category);
                                                // console.log("Index of category is " + category_index);
                                                count_result[category_index] = count_result[category_index] + 1;
                                                // console.log("Category " + category.name + " has count " + count_result[category_index] + " for language " + language_id);
                                            }
                                        } else {
                                            var category_index = data.categories.indexOf(category);
                                            count_result[category_index] = count_result[category_index] + 1;
                                            // console.log("Category " + category.name + " has count " + count_result[category_index] + " for language " + language_id);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return count_result;

            };

            data.getCategoryByID = function (id) {
                for (var i = 0; i < data.categories.length; i++) {
                    if (data.categories[i].id === id) {
                        // console.log("Found category with id "+id+" - "+data.categories[i].name);
                        return data.categories[i];
                    }
                }
                console.log("Did not find category with id " + id);
                return null;
            };


            /**
             * Returns all frequencies of words which belong to language "language_id" per category-group, if "only_learned" return only those, which user "user_id" has already learned
             * 
             * @param {Integer} language_id     Language id, for which the word frequencies should be returned
             * @param {Integer} user_id         User id, if only the already learned words should be counted per group
             * @param {Boolean} only_learned    Indicates, whether all or only the words already learned (if true) should be counted by group
             * @returns {Numbers[]}             An array of numbers indicating the frequency of words in each category group (if only_learned === true, only returns count of words already learned per group)
             */
            data.getWordsForCategoryGroups = function (language_id, user_id, only_learned, only_sentences) {
                var count_result = [[], [], [], []];
                for (var i = 0; i < data.words.length; i++) {
                    if (only_learned) {
                        if (data.getProgressForWord(user_id, data.words[i].id) > 3) {
                            if (data.words[i].language_ID === language_id) {
                                for (var c = 0; c < data.words[i].categories_IDs.length; c++) {
                                    var category = data.getObjectByID(data.words[i].categories_IDs[c], "data.getWordsForCategoryGroups");
                                    var groupIndex = data.getCategoryGroup(category);
                                    if (groupIndex > -1) {
                                        count_result[groupIndex].push(data.words[i]);
                                    }
                                }
                            }
                        }
                    } else {
                        if (data.words[i].language_ID === language_id) {
                            for (var c = 0; c < data.words[i].categories_IDs.length; c++) {
                                var category = data.getObjectByID(data.words[i].categories_IDs[c], "data.getWordsForCategoryGroups");
                                var groupIndex = data.getCategoryGroup(category);
                                if (groupIndex > -1) {
                                    if (only_sentences) {
                                        if (data.words[i].sentence) {
                                            // console.log("Counting sentence " + data.words[i].sentence);
                                            count_result[groupIndex].push(data.words[i]);
                                        }
                                    } else {
                                        // console.log("Counting word "+data.words[i].name);
                                        count_result[groupIndex].push(data.words[i]);
                                    }
                                }
                            }
                        }
                    }
                }
                // console.log("For language "+language_id+" found "+count_result[0].length+" in BASICS, "+count_result[1].length+" in HUMANS, "+count_result[2].length+" in SOCIETY, and "+count_result[3].length+" in NATURE_AND_EVINROMENT" );
                var result = [count_result[0].length, count_result[1].length, count_result[2].length, count_result[3].length];
                // console.log("Returning frequencies for language " + language_id + ": " + JSON.stringify(result));

                return result;
            }

            /**
             * Returns integer representing category group, to which the given category belongs
             * 
             * @param {Category} category       Category with field .group
             * @returns {Integer}               integer representing the group of category
             */
            data.getCategoryGroup = function (category) {
                if (category.group === "BASICS") {
                    return 0;
                }
                if (category.group === "HUMANS") {
                    return 1;
                }
                if (category.group === "SOCIETY") {
                    return 2;
                }
                if (category.group === "NATURE_AND_ENVIRONMENT") {
                    return 3;
                }
                return -1;
            }

            /**
             * Returns Word[] with all words of given language, if the second parameter is not null, exclude words translated into that second language
             * 
             * @param {int} language_ID             int value for language id
             * @param {int} language_translate_ID   (optional) ID of translation language - if already translated into that language, do not return word
             * @returns {Array}                     Word[] with all words, where language variable matches language_id
             */
            data.getWordsForLanguage = function (language_ID, language_translate_ID) {
                var result = [];

                angular.forEach(data.words, function (word) {
                    if (word.language_ID === language_ID) {
                        if (language_translate_ID) {
                            var has_translation = false;
                            for (var i = 0; i < word.related_IDs.length; i++) {
                                var relation = data.getObjectByID(word.related_IDs[i], "data.getWordsForLanguage");
                                if (relation && relation.language_ID === language_translate_ID) {
                                    has_translation = true;
                                }
                            }
                            if (!has_translation) {
                                result.push(word);
                            }
                        } else {
                            result.push(word);
                        }
                    }
                });

                console.log("Returning " + result.length + " words for language " + language_ID + " which are not translated into " + language_translate_ID);
                return result;
            };

            /**
             * 
             * @param {Array} words
             * @param {ID} category
             * @returns {Array}
             */
            data.getWordsForCategory = function (words, category_ID) {
                var result = [];
                if (category_ID) {
                    for (var i = 0; i < words.length; i++) {
                        if (words[i].categories_IDs.indexOf(category_ID) > -1) {
                            result.push(words[i]);
                        }
                    }
                }
                return result;
            };



            /**
             * Return given word from database (right now most of the time not necessary, as I am directly accessing the data.words array and manipulate it - included for future database abstractions)
             * 
             * @param {Word} word       The word to be removed from database
             * @returns {Boolean}       True, if word was deleted, False, if that was not possible (most likely, because word was not found in data.words array)
             */
            data.deleteWord = function (word) {
                if (word.id) {
                    var indexOfWord = data.getIndexOfIDinArray(data.words, word.id);
                    if (indexOfWord >= 0) {
                        data.words.splice(data.words.indexOf(word), 1);
                        data.removeRelationsToID(word.id);
                        console.log("DELETED object " + word.name);
                        data.dataHaveChanged();
                        return true;
                    }
                }
                console.log("Was NOT able to remove " + word.name);
                return false;
            }


            /**
             * 
             * @param {Comment} comment     Comment to be deleted
             * @returns {Boolean}           True, if comment was deleted, False, if that was not possible (most likely, because comment was not found in data.comments array)
             */
            data.deleteComment = function (comment) {
                if (data.comments.indexOf(comment) >= 0) {
                    data.comments.splice(data.comments.indexOf(comment), 1);
                    data.removeRelationsToID(comment.id);
                    console.log("Removed object " + comment.name);
                    data.dataHaveChanged();
                    return true;
                } else {
                    return false;
                }
            }

            /**
             * 
             * @param {INTEGER} language_ID
             * @param {type} assigned_tags
             * @param {type} search_term
             * @returns {Array}
             */
            data.getAllTags = function (language_ID, assigned_tags, search_term) {

                var tags = [];
                if (language_ID) {
                    for (var i = 0; i < data.words.length; i++) {
                        if (data.words[i].tags && data.words[i].language_ID === language_ID) {
                            for (var k = 0; k < data.words[i].tags.length; k++) {
                                var tag = data.words[i].tags[k];
                                if (tags.indexOf(tag) < 0) {
                                    // if tags already assigned to word are given, check whether the tag is already present
                                    if (assigned_tags) {
                                        // console.log("Check whether " + JSON.stringify(assigned_tags) + " does not contain " + JSON.stringify(tag));
                                        if (assigned_tags.indexOf(tag) < 0) {
                                            tags.push(tag);
                                        }
                                    } else {
                                        tags.push(tag);
                                    }
                                }
                            }
                        }
                    }
                }

                // if search-term was given, remove tags not containing it
                if (search_term) {
                    for (var i = 0; i < tags.length; i++) {
                        console.log("Check whether " + tags[i] + " contains " + search_term + ": " + tags[i].indexOf(search_term));
                        if (tags[i].indexOf(search_term) < 0) {
                            console.log("Removed " + tags[i]);
                            tags = tags.slice(i, 1);
                        }
                    }
                }

                // console.log("Returning tags: " + JSON.stringify(tags));
                return tags;
            };


            /**
             * Returns sentences to which the sentence with given id is marked as a response
             * 
             * @param {INTEGER} id  The sentence, which might be a response to another sentence
             * @returns {Array}
             */
            data.getParentsOfReponse = function (id) {
                var result = [];
                if (id) {
                    for (var i = 0; i < data.words.length; i++) {
                        if (data.words[i].responses_IDs && data.words[i].responses_IDs.indexOf(id) >= 0) {
                            result.push(data.words[i]);
                        }
                    }
                }
                return result;
            }

            /**
             * Remove given id from all other word and comment relation arrays
             * 
             * @param {int} id          int id of Word or Comment, whose relations in other comments and words should be removed
             */
            data.removeRelationsToID = function (id) {
                for (var i = 0; i < data.words.length; i++) {
                    if (data.words[i].related_IDs.indexOf(id) >= 0) {
                        data.words[i].related_IDs.splice(data.words[i].related_IDs.indexOf(id), 1);
                        data.dataHaveChanged();
                    }
                }
                for (var i = 0; i < data.comments.length; i++) {
                    if (data.comments[i].related_IDs.indexOf(id) >= 0) {
                        data.comments[i].related_IDs.splice(data.comments[i].related_IDs.indexOf(id), 1);
                        data.dataHaveChanged();
                    }
                }
            }

            data.getFormsSecondary = function (word) {
                var result = [];

                // formsSec is an intermediate helper variable to store some inbetween data
                var formsSec = {};
                formsSec.categories = [];
                formsSec.formNames = angular.copy(word.formsSecondaryNames);
                formsSec.forms = angular.copy(word.formsSecondary);

                // if this word actually has secondary forms
                if (formsSec.forms && formsSec.forms.length > 0) {
                    // go through all forms
                    for (var i = 0; i < formsSec.formNames.length; i++) {
                        if (formsSec.formNames[i] && formsSec.formNames[i].length > 0) {
                            // form names are only category names, if there is nothing in the content for that index
                            if (!formsSec.forms[i] || formsSec.forms[i].length === 0) {
                                var category = {};
                                category.name = formsSec.formNames[i];
                                category.start = i;
                                category.id = formsSec.categories.length;


                                if (formsSec.categories.length > 0) {
                                    formsSec.categories[formsSec.categories.length - 1].end = i - 1;
                                }

                                // console.log("Found sec form category " + category.name + " beginning at index " + category.start);

                                formsSec.categories.push(category);
                            }
                        }
                    }


                    // for all categories, put their form names and forms into arrays and store the result
                    for (var i = 0; i < formsSec.categories.length; i++) {
                        var category = formsSec.categories[i];
                        // console.log("Looking at sec form " + JSON.stringify(category));
                        if (!category.end) {
                            category.end = formsSec.forms.length;
                        }
                        category.forms = formsSec.forms.slice(category.start + 1, category.end + 1);
                        category.formNames = formsSec.formNames.slice(category.start + 1, category.end + 1);

                        result.push(category);
                        // console.log("Storing category " + category.name + ": " + JSON.stringify(category));

                    }

                }
                console.log("Returning secondary forms " + JSON.stringify(result));

                return result;
            }




            /**
             * 
             * @param {type} user_id - ID of user, for which a word progress should be promoted (usually, because a correct answer was given in a test)
             * @param {type} word_id - ID of word, that should be promoted
             * @returns {undefined}
             */
            data.promoteWordProgress = function (user_id, word_id) {
                // console.log("Given " + JSON.stringify(user_id) + " and " + JSON.stringify(word_id));
                var user = data.getObjectByID(user_id, "data.promoteWordProgress");
                var wordProgress = data.getProgressForWord(user_id, word_id);
                // if word is already in progress_4, it cannot be promoted further
                if (wordProgress < 4) {
                    wordProgress = wordProgress + 1;
                    if (wordProgress === 1) {
                        user.progress_1.push(word_id);
                    }
                    if (wordProgress === 2) {
                        user.progress_1.splice(user.progress_1.indexOf(word_id), 1);
                        user.progress_2.push(word_id);
                    }
                    if (wordProgress === 3) {
                        user.progress_2.splice(user.progress_2.indexOf(word_id), 1);
                        user.progress_3.push(word_id);
                    }
                    if (wordProgress === 4) {
                        user.progress_3.splice(user.progress_3.indexOf(word_id), 1);
                        user.progress_4.push(word_id);
                    }
                    console.log("Promoted " + word_id + " for user " + user_id);
                    data.hasChanged = true;
                    data.updateSession();
                }
            }

            /**
             * 
             * @param {type} user_id - ID of user, for which a word progress should be demoted (usually, because a wrong answer was given in a test)
             * @param {type} word_id - ID of word, that should be demoted
             * @returns {undefined}
             */
            data.demoteWordProgress = function (user_id, word_id) {
                console.log("Demoting word " + word_id);
                var user = data.getObjectByID(user_id, "data.demoteWordProgress");
                var wordProgress = data.getProgressForWord(user_id, word_id);

                // if word has progress 0, it cannot be demoted further
                if (wordProgress > 0) {
                    wordProgress = wordProgress - 1;
                    if (wordProgress === 3) {
                        user.progress_4.splice(user.progress_4.indexOf(word_id), 1);
                        user.progress_3.push(word_id);
                    }
                    if (wordProgress === 2) {
                        user.progress_3.splice(user.progress_3.indexOf(word_id), 1);
                        user.progress_2.push(word_id);
                    }
                    if (wordProgress === 1) {
                        user.progress_2.splice(user.progress_2.indexOf(word_id), 1);
                        user.progress_1.push(word_id);
                    }
                    if (wordProgress === 0) {
                        user.progress_1.splice(user.progress_1.indexOf(word_id), 1);
                    }
                    console.log("Demoted " + word_id + " for user " + user_id);
                    data.dataHaveChanged();
                }
            }

            /**
             * A helper function for data.promotoeWordProgress & data.demoteWordProgress
             * @param {type} user_id - the user ID for which the progress of a word should be returned
             * @param {type} word_id - the id of the word, the progress is queried for
             * @returns {Number} - the count the word has been solved consecutively, if none of the progress arrays contains it, then it has not been solved successfully, and therefore rank ß is returned
             */
            data.getProgressForWord = function (user_id, word_id) {
                // console.log("Getting progress for " + word_id + " for user " + user_id);
                var user = data.getObjectByID(user_id, "data.getProgressForWord");

                var progress = 0;

                if (user) {

                    // console.log(JSON.stringify(user));

                    if (user.progress_1.indexOf(word_id) >= 0) {
                        progress = 1;
                    }
                    if (user.progress_2.indexOf(word_id) >= 0) {
                        progress = 2;
                    }
                    if (user.progress_3.indexOf(word_id) >= 0) {
                        progress = 3;
                    }
                    if (user.progress_4.indexOf(word_id) >= 0) {
                        progress = 4;
                    }
                }

                return progress;
            }

            /**
             * Method for statistical purposes
             * 
             * @param {type} words
             * @param {type} user_id
             * @returns {Array}
             */
            data.getUntrainedWords = function (words, user_id) {
                var result = [];
                for (var i = 0; i < words.length; i++) {
                    if (data.getProgressForWord(user_id, words[i].id) < 4) {
                        result.push(words[i]);
                        // console.log("Word " + words[i].name + " has not been trained enough yet.");
                    }
                }
                console.log("Returning " + result.length + " untrained words.")
                return result;
            }




            data.findDuplicateWordEntries = function (words) {
                var duplicates = [];

                for (var i = 0; i < words.length; i++) {
                    var word1 = words[i];
                    // compare word 1 to all the rest of the words coming afterwards (to avoid double comparisons of later with earlier words, which would already have been compared)
                    for (var k = i + 1; k < words.length; k++) {
                        var word2 = words[k];
                        if (word1.name === word2.name && word1.language_ID === word2.language_ID) {
                            var duplicate = [];
                            duplicate[0] = word1;
                            duplicate[1] = word2;
                            duplicates.push(duplicate);
                        }
                    }
                }

                return duplicates;
            }


            /**
             * Method finds new numerical ID, which is next to the highest ID
             * 
             * @returns {Number}    Lowest possible ID not yet used
             */
            data.getNewID = function () {
                return Math.uuid(12);
            };


            /**
             * 
             * @param {ARRAY} IDs   An array with ids of existing objects    
             * @returns {data.languages|data.users|data.words|data.comments|data.categories|object} Returns Array of objects of given IDs
             */
            data.getObjectArrayFromIDs = function (IDs, data) {
                var result = [];

                // console.log("Given IDs "+JSON.stringify(IDs));

                for (var i = 0; i < IDs.length; i++) {
                    var object = data.getObjectByID(IDs[i], "data.getObjectArrayFromIDs()");
                    // console.log("For ID "+IDs[i]+" got "+JSON.stringify(object));
                    if (object) {
                        result.push(object);
                    }
                }

                // console.log("Returning "+result.length+" objects: "+JSON.stringify(result));

                return result;
            }



            /**
             * Sorts both arrays using the standard javascript method just to make sure the order does not influence the outcome
             * Then performs an underscore _.isEqual test and returns the result
             * 
             * @param {ARRAY} arrayA[] containing only integer numbers
             * @param {type} arrayB
             * @returns {unresolved}
             */
            data.arraysNumericalEqual = function (arrayA, arrayB) {
                arrayA.sort();
                arrayB.sort();
                return _.isEqual(arrayA, arrayB);
            }


            /**
             * Expects a string, returns all chars exactly once (does differentiate between capital and lower letters) - helpful for language special characters to make sure, they only occur once
             * 
             * @param {type} str    String
             * @returns String with each character occurring only once
             */
            data.removeDoubleChars = function (str) {
                var charsUndoubled = "";
                for (var i = 0; i < str.length; i++) {
                    if (!charsUndoubled.contains(str[i])) {
                        charsUndoubled = charsUndoubled + str[i];
                    }
                }
                return charsUndoubled;
            };

            /**
             * 
             * @param {type} array  The array of objects, which are identified by an id
             * @param {type} id     The id for which should be checked if an object within array shares it
             * @returns {Boolean}   True if id is assigned to an object within array, False otherwise
             */
            data.arrayContainsObjectWithID = function (array, id) {
                for (var i = 0; i < array.length; i++) {
                    if (array[i].id && array[i].id === id) {
                        return true;
                    }
                }
                return false;
            }


            /**
             * Use underscore function to shuffle items in array
             * 
             * @param {type} array
             * @returns {array}
             */
            data.randomizeOrder = function (array) {
                return _.shuffle(array);
            };


            /**
             * 
             * @param {type} array  Array of objects, which are required to have a property "id"
             * @param {type} id     A property for which to look for within the objects in array
             * @returns {Number}    The index of the first object found with id in the given array
             */
            data.getIndexOfIDinArray = function (array, id) {
                for (var i = 0; i < array.length; i++) {
                    if (array[i].id && array[i].id === id) {
                        console.log("Found id " + id + " in array of " + array.length + " objects.");
                        return i;
                    }
                }
                return -1;
            }



            /**
             * Get all different groups of categories alphabetically sorted including translation
             * 
             * @returns {Array} Groups of categoires as strings
             */
            data.getCategoryGroups = function () {
                var groups = [];

                for (var i = 0; i < data.categories.length; i++) {
                    var containsGroupAlready = false;
                    for (var k = 0; k < groups.length; k++) {
                        if (groups[k] === data.categories[i].group) {
                            containsGroupAlready = true;
                        }
                    }
                    if (containsGroupAlready === false) {
                        // console.log("Group "+data.categories[i].group+" is being added to groups.");
                        groups.push(data.categories[i].group);
                    }
                }

                groups.sort;

                return groups;
            };



            /**
             * 
             * 
             * @param   {String} group  group name
             * @returns {String}        group name translated into user language 1
             * 
             * @UI_TRANSLATION  Necessary to adapt for UI translation if adding new languages. 
             */
            data.getCategoryGroupTranslated = function (group) {
                // console.log("Get translation for "+JSON.stringify(group));
                var groupTranslation = group;
                if (data.user.languageKnown_id === "1") {
                    if (group === "BASICS") {
                        groupTranslation = knowBetter_UI_sv.CATEGORY_BASICS;
                    } else if (group === "HUMANS") {
                        groupTranslation = knowBetter_UI_sv.CATEGORY_HUMANS;
                    } else if (group === "SOCIETY") {
                        groupTranslation = knowBetter_UI_sv.CATEGORY_SOCIETY;
                    } else if (group === "NATURE_AND_ENVIRONMENT") {
                        groupTranslation = knowBetter_UI_sv.CATEGORY_NATUR_AND_ENVIRONMENT;
                    }
                }
                if (data.user.languageKnown_id === "2") {
                    // console.log("Translating category groups into German");
                    if (group === "BASICS") {
                        groupTranslation = knowBetter_UI_de.CATEGORY_BASICS;
                    } else if (group === "HUMANS") {
                        groupTranslation = knowBetter_UI_de.CATEGORY_HUMANS;
                    } else if (group === "SOCIETY") {
                        groupTranslation = knowBetter_UI_de.CATEGORY_SOCIETY;
                    } else if (group === "NATURE_AND_ENVIRONMENT") {
                        groupTranslation = knowBetter_UI_de.CATEGORY_NATUR_AND_ENVIRONMENT;
                    }
                }
                if (data.user.languageKnown_id === "3") {
                    if (group === "BASICS") {
                        groupTranslation = knowBetter_UI_en.CATEGORY_BASICS;
                    } else if (group === "HUMANS") {
                        groupTranslation = knowBetter_UI_en.CATEGORY_HUMANS;
                    } else if (group === "SOCIETY") {
                        groupTranslation = knowBetter_UI_en.CATEGORY_SOCIETY;
                    } else if (group === "NATURE_AND_ENVIRONMENT") {
                        groupTranslation = knowBetter_UI_en.CATEGORY_NATUR_AND_ENVIRONMENT;
                    }
                }
                if (data.user.languageKnown_id === "5") {
                    if (group === "BASICS") {
                        groupTranslation = knowBetter_UI_es.CATEGORY_BASICS;
                    } else if (group === "HUMANS") {
                        groupTranslation = knowBetter_UI_es.CATEGORY_HUMANS;
                    } else if (group === "SOCIETY") {
                        groupTranslation = knowBetter_UI_es.CATEGORY_SOCIETY;
                    } else if (group === "NATURE_AND_ENVIRONMENT") {
                        groupTranslation = knowBetter_UI_es.CATEGORY_NATUR_AND_ENVIRONMENT;
                    }
                }

                // console.log("Returning category group translation for " + group + " into language " + data.user.languageKnown_id + " -> " + groupTranslation);
                return groupTranslation;
            };


            data.isWord = function (object) {
                return object.categories_IDs;
            }

            data.isLanguage = function (object) {
                return object.key;
            }

            data.isCategory = function (object) {
                return object.group;
            }

            data.isUser = function (object) {
                return object.languageKnown_id;
            }

            return data;
        }]);

})();
