/**
 This file is part of knowBetter.
 
 knowBetter is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 knowBetter is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with knowBetter.  If not, see <http://www.gnu.org/licenses/>.
 **/
(function () {

    var app = angular.module('testService', ['dataService']);
    /**
     * testService provides everything required for tests (constructs slides and slideSingles for words in given category and languages) and is able to check the answers for correctness
     */
    app.factory('testService', function (dataService) {
        var data = dataService;
        var test = {};
        test.words = [];
        test.sessionWords = [];
        // status can be 
        // 'start'          - app just started, showing screen for tutorial, continuing previous session and creating new session
        // 'select-test'    - select the tests you want to run during this session
        // 'train-grid'     - when the flip-grid is active
        // 'train-pairs'    - when the pair view is active
        // 'test-translate' - when the test for word translation is active
        // 'test-select'    - when the pair select test is active
        // 'test-listen'    - when the listen test is active
        // 'test-gaps'      - when the fill gaps test is active
        // 'finished'       - when all trainings and tests are finished, congratulate the user and show results

        test.status = "start";

        test.testsList = ["TRAIN_PAIRS", "TRAIN_GRID", "TEST_GRID", "TEST_TRANSLATE", "TEST_GAP", "TEST_LISTEN"];
        test.stati = ["start", "TRAIN_PAIRS", "TRAIN_GRID", "TEST_GRID", "TEST_TRANSLATE", "TEST_GAP", "TEST_LISTEN", "finished"];


        // taskStatus can be
        // 'input'          - whenever the input box is offered
        // 'solved'         - whenever input is not possible because a solution has been accepted
        // 'error'          - if solution was rejected
        test.taskStatus = "";

        // the answer being given by the user
        test.answer = "";

        // all sentences for the current test should be in this array (needed by the fill gaps test)
        test.translationPairsSentences = [];
        // all pairs with audio
        test.translationPairsAudio = [];


        test.wordKnown = {};
        test.wordTest = {};
        test.slides = [];
        test.slidesSingle = []; // used by test-grid.html, only one word per slide
        test.slidesSingle.word1 = {};
        test.slidesSingle.word2 = {};

        // used by grid pair test
        test.selectedWord1 = {};
        test.selectedWord2 = {};

        test.translationPairs = [];
        test.translationPairs.active = 0;

        test.currentPair = 0;

        test.countWords = 0;
        test.countSentences = 0;

        test.wordsKnown = [];
        test.wordsTest = [];
        // this records the results for all words in the current test, therefore it contains strings with "correct" or "false"
        test.wordsResults = [];
        // the index of the current test item
        test.index = {};



        test.start = function () {

            test.updateTestData();

            test.next();
        };


        test.updateTestData = function () {

            console.log("Trying to update test data with " + data.user.languageKnown_id + " - " + data.user.languageLearn_id + " : " + data.user.category);
            if (data.user.category) {
                test.translationPairs = data.getWordPairsForCategoryAndLanguages(data.user.category, data.user.languageKnown_id, data.user.languageLearn_id);

                // reset the example sentences array
                test.translationPairsSentences = [];


                test.prepareSession(true);

                test.words = data.randomizeOrder(test.getAllWordsFromPairs(test.translationPairs));
            }
        };


        /**
         * Prepare a training/test session
         * 
         * @todo    implement loading / continuing previous session
         */

        test.prepareSession = function (createNewSession) {

            if (createNewSession) {
                // filter all translationPairs for having their primary forms having the same translation into the other language and if so, combine them and remove one them
                test.translationPairs = test.filterFormsPrimaryDoubles(test.translationPairs);

                // optionally filter synonyms
                test.translationPairs = test.filterSynonyms(test.translationPairs);

                // after primary forms and synonyms have been filtered, take a random sample of eight
                test.translationPairs = _.sample(test.translationPairs, 8);

                // add several methods including audio option to all pairs
                for (var i = 0; i < test.translationPairs.length; i++) {
                    var pair = test.translationPairs[i];

                    pair.offerHint = 0;

                    pair.index = i;

                    console.log("Creating pair " + i + " for " + pair[0].name + " <--> " + pair[1].name);

                    pair[0].sentences = data.getSentences(pair[0]);
                    pair[0].comments = data.getComments(pair[0]);
                    pair[0].formsSecondaryProcessed = data.getFormsSecondary(pair[0]);
                    // pair[0].similarWords = data.getSimilarWords(pair[0].name, words); // too resource intensive to be used in its current state

                    pair[1].sentences = data.getSentences(pair[1]);
                    pair[1].comments = data.getComments(pair[1]);
                    pair[1].formsSecondaryProcessed = data.getFormsSecondary(pair[1]);
                    // pair[1].similarWords = data.getSimilarWords(pair[1].name, words); // too resource intensive to be used in its current state

                    pair.solution = pair[1].name;

                    // add audio
                    test.addAudioToWord(pair[0]);
                    test.addAudioToWord(pair[1]);

                    if (pair[1].hasAudio) {
                        test.translationPairsAudio.push(pair);
                    }

                    // take care of solution checking
                    pair.translation = pair[1].name;
                    // within the methods I can't access the properties defined before directly due to the way that Javascript handles referencing
                    // so pair needs to be passed back to be able to retrieve the correct values instead of only the last added
                    pair.checkAnswer = function (pair) {
                        test.checkAnswer(pair);
                        if (pair.solved) {
                            pair[1].playAudio(pair[1].audio);
                        }
                    };

                    pair.switchWords = function (pair) {
                        var a = pair[0];
                        pair[0] = pair[1];
                        pair[1] = a;
                        // console.log("Switched words, now [0] " + pair[0].name + ", [1] " + pair[1].name);
                    }
                }


                // load example sentences for the words
                for (var i = 0; i < test.translationPairs.length; i++) {
                    // only need to look at translationPairs[1] since that contains words and therefore related sentences
                    // need only to look at [i][1] since that contains all words and
                    // console.log(!test.translationPairs[i][1].sentence);
                    if (!test.translationPairs[i][0].sentence) {
                        var sentences = [];
                        for (var r = 0; r < test.translationPairs[i][0].related_IDs.length; r++) {
                            var relation = data.getObjectByID(test.translationPairs[i][0].related_IDs[r], "test.updateTestData");
                            // console.log("Trying to retrieve id "+test.translationPairs[i][0].related_IDs[r]+" of "+test.translationPairs[i][0].related_IDs.length+" ids...");
                            // console.log(JSON.stringify(relation));
                            if (relation) {
                                if (relation.sentence && (relation.language_ID === test.translationPairs[i][0].language_ID)) {
                                    var sentence = relation;
                                    test.addAudioToWord(sentence);
                                    sentence.solution = test.getRandomWordFromSentence(relation.name);
                                    sentence.answer = "";
                                    sentence.solved = false;
                                    sentence.part1 = relation.name.substring(0, relation.name.indexOf(sentence.solution));
                                    sentence.part2 = relation.name.substring(relation.name.indexOf(sentence.solution) + sentence.solution.length)
                                    sentence.offerHint = 0;
                                    // console.log("Added " + sentence.part1 + " - " + sentence.solution + " - " + sentence.part2 + " to sentences.");

                                    sentences.push(sentence);
                                }
                            }
                        }

                        console.log("Found " + sentences.length + " sentences for word " + test.translationPairs[i][0].name);

                        // if word had one sentence, push it to  now chose one of the sentences for this word to go into tests
                        if (sentences.length === 1) {
                            test.translationPairsSentences.push(sentences[0]);
                        } else if (sentences.length > 1) {
                            // if that word has more than one sentence, add only one of them
                            var randomChoice = sentences[test.getRandomInteger(0, sentences.length - 1)];
                            test.translationPairsSentences.push(randomChoice);
                        }
                    }
                }

                console.log("Loaded " + test.translationPairsSentences.length + " setences related to the words.");


                test.countWords = 0;
                test.countSentences = 0;
                test.currentPair = 0;

                test.countWords = test.translationPairs.length;
                test.countSentences = test.translationPairsSentences.length;
            }
            // if a previous session is to be continued
            if (createNewSession === false) {

            }
        };


        /**
         * 
         * @param {STRING} name
         * @param {INTEGER} hintIndex
         * @returns {STRING}    each hint step should reveil 1/5 of the word
         */
        test.getHint = function (name, hintIndex) {
            var hintText = name.substring(0, hintIndex * Math.ceil(name.length / 5));

            // console.log("Hint: " + hintText, hintIndex);
            return hintText;
        }

        test.previousPair = function () {
            if (test.currentPair > 0) {
                test.currentPair = test.currentPair - 1;
            }
        };


        /**
         * test.whatsNext looks at the current test.status and from there calculates the next step, taking into account that tests might have been deactivated
         * @returns {Array|String}  String with the follow-up status
         */
        test.whatsNext = function () {

            this.currentStatusIndex = 0;
            // find the currenst status, more precisely its index in the stati array
            for (var i = 0; i < this.stati.length; i++) {
                this.currentStatusIndex = i;
                if (this.stati[i] === test.status) {
                    break;
                }
            }

            var status = this.stati[this.currentStatusIndex];

            this.currentStatusIndex = this.currentStatusIndex + 1;

            console.log("Current status " + status);
            switch (this.currentStatusIndex) {
                case 1:
                    status = this.stati[1];
                    break;
                case 2:
                    status = this.stati[2];
                    break;
                case 3:
                    status = this.stati[3];
                    break;
                case 4:
                    status = this.stati[4];
                    break;
                case 5:
                    console.log("Old status " + status);
                    status = this.stati[5];
                    console.log("New status 5 " + status);
                    break;
                case 6:
                    console.log("Old status " + status);
                    status = this.stati[6];
                    console.log("New status 6 " + status);
                    break;
                case 7:
                    console.log("Old status " + status);
                    status = this.stati[7];
                    console.log("New status 7 " + status);
                    break;
                default:
                    status = this.stati[7];
                    console.log("Defaulting because of: " + this.currentStatusIndex);
            }

            return status;
        };




        /**
         * test.next is responsible once the trainings and tests are started to activate the next selected training or test
         */
        test.next = function () {

            // change the current pair by raising the current pair index
            test.currentPair = test.currentPair + 1;

            console.log(test.status + " - No " + test.currentPair + " of " + test.limit);
            // if the limit is reached, or it is the start or finish page, or if it is a test with only one page, go to the next test
            if (test.currentPair >= test.limit || test.status === "start" || test.status === "TRAIN_GRID" || test.status === "TEST_GRID" || test.status === "finished") {
                test.currentPair = 0;
                test.resetPairStatus();
                test.status = test.whatsNext();

                // adjust the limit for the new test
                test.limit = test.translationPairs.length;

                // the gaps test uses the number of sentences as limit, not the number of word pairs
                if (test.status === "TEST_GAP") {
                    test.limit = test.translationPairsSentences.length;
                }

                // the listen test uses its own subset as well, so also there the limit needs to be adjusted
                if (test.status === "TEST_LISTEN") {
                    test.limit = test.translationPairsAudio.length;
                }

                console.log("Set limit for test " + test.status + " to " + test.limit);
                
                // if a test has zero entries, skip it
                if (test.limit === 0) {
                    console.log("Skipped test.");
                    test.status = test.whatsNext();
                }

                if (test.testsList.indexOf(test.status) >= 0) {
                    test.title = test.status;
                } else {
                    test.title = "start";
                }
            }
        };


        test.setTest = function (status) {
            test.updateTestData();
            test.resetPairStatus();

            // randomize translation pairs order
            test.translationPairs = data.randomizeOrder(test.translationPairs);
            test.status = status;
            test.currentPair = 0;
        };

        test.resetPairStatus = function () {

            for (var i = 0; i < test.translationPairs.length; i++) {
                test.translationPairs[i].answer = "";
                test.translationPairs[i].solved = false;
            }
            for (var i = 0; i < test.translationPairsSentences.length; i++) {
                test.translationPairsSentences[i].answer = "";
                test.translationPairsSentences[i].solved = false;
            }
        };


        /**
         * Generic way to check whether answer matches solution
         * 
         * @param {type} object     needs fields answer, solution and solved as well as offerHint
         * @returns {undefined}
         */
        test.checkAnswer = function (object) {
            if (object.answer && object.answer.length > 0 && object.answer === object.solution) {
                object.solved = true;
            } else {
                object.offerHint = object.offerHint + 1;
            }
        };

        /**
         * Add the possibility to easily add chars to the answer
         * 
         * @param {type} pair
         * @param {type} char
         * @returns {undefined}
         */
        test.addCharToAnswer = function (object, char) {
            if (object.answer) {
                object.answer = object.answer + char;
            } else {
                object.answer = "" + char;
            }
        };



        /**
         * Necessary for styling of md-cards, as I cannot directly adjust the theming using md-warn, md-accent or md-primary for md-cards
         * 
         * @param {type} status     The status of the word
         * @returns {String}        The string designating the corresponding theme color indicator
         */
        test.getBackgroundStatus = function (status) {
            var background = data.getUserTheme();
            // console.log("getBackgroundStatus(" + status + ")");
            if (!status) {
                return background + "-primary-100";
            } else {
                if (status === "") {
                    return background;
                }
                if (status === "selected") {
                    return background + "-accent";
                }
                if (status === "solved") {
                    return background + "-primary";
                }
                if (status === "error") {
                    return background + "-warn";
                }
            }
            return background;
        }



        /**
         * 
         * @param {type} pairs      Array of several [word1, word2} array objects
         * @param {type} word1      The first word
         * @param {type} word2      The second word
         * @returns {undefined}     True, if any pair in pairs contains both words; False, if no pair in pairs contains both of the words
         */
        test.pairEqualsWords = function (pairs, word1, word2) {
            for (var i = 0; i < pairs.length; i++) {
                console.log("Are " + word1.name + " and " + word2.name + " both contained in pair " + pairs[i][0].name + " and " + pairs[i][1].name + "?");
                if (pairs[i][0].id === word1.id || pairs[i][0].id === word2.id) {
                    if (pairs[i][1].id === word1.id || pairs[i][1].id === word2.id) {
                        console.log("True!");
                        return true;
                    }
                }
            }
            return false;
        };



        /**
         * If given String contains space char " " then split it into separate words, clean them of normal sentence chars and return the array of words
         * 
         * @param {String} sentence       a String, usually containing spaces to separate words
         * @returns {String[]}        either the original String if it did not contain separate words, or an array of the words contained in the String
         */
        test.splitSentenceIntoWords = function (sentence) {
            if (sentence.indexOf(" ") > -1) {
                var words = sentence.split(" ");
                for (var i = 0; i < words.length; i++) {
                    while (words[i].indexOf(",") > -1) {
                        words[i] = words[i].replace(",", "");
                    }
                    while (words[i].indexOf(".") > -1) {
                        words[i] = words[i].replace(".", "");
                        console.log(words[i]);
                    }
                    while (words[i].indexOf("!") > -1) {
                        words[i] = words[i].replace("!", "");
                        console.log(words[i]);
                    }
                    while (words[i].indexOf("?") > -1) {
                        words[i] = words[i].replace("?", "");
                        console.log(words[i]);
                    }
                    while (words[i].indexOf(";") > -1) {
                        words[i] = words[i].replace(";", "");
                        console.log(words[i]);
                    }
                }
                return words;
            }
            return sentence;
        };


        /**
         * Returns a random word picked from the sentence
         * 
         * @param {String} sentence
         * @returns {String}    Word contained in sentence - if sentence contains several words, pick a pseudo-random one.
         */
        test.getRandomWordFromSentence = function (sentence) {
            var words = test.splitSentenceIntoWords(sentence);
            if (words.length > 1) {
                return words[test.getRandomInteger(0, words.length - 1)];
            }
            return sentence;
        };

        /**
         * Takes all pairs and adds both their words to an array words being returned
         * 
         * @param {type} pairs      an array consisting of two words [word1, word2]
         * @returns {Array}         an array containing all words of all pairs individually
         */
        test.getAllWordsFromPairs = function (pairs) {
            var words = [];
            for (var i = 0; i < pairs.length; i++) {
                words.push(pairs[i][0]);
                words.push(pairs[i][1]);
            }
            return words;
        };

        /**
         * Returns a pseudo-random integer between lower and upper limit integers
         * 
         * @param {Integer} lowerLimit
         * @param {Integer} upperLimit
         * @returns {Integer}
         */
        test.getRandomInteger = function (lowerLimit, upperLimit) {
            return Math.floor(Math.random() * (upperLimit - lowerLimit + 1)) + lowerLimit;
        };


        /**
         * Helper function to add audio to word objects
         * @param {type} word
         * @returns {undefined}
         */
        test.addAudioToWord = function (word) {
            word.audio = new Howl({
                src: ['./media/audio/' + word.id + '.ogg'],
                onload: function () {
                    word.hasAudio = true;
                },
                onerror: function () {
                    word.hasAudio = false;
                }
            });
            word.playAudio = function (item) {
                item.play();
            };
        };

        /**
         * 
         * 
         * @param {TranslationPairs} translationPairs storing two words at index 0 and 1 which translate each other
         * @returns {Array}
         */
        test.filterFormsPrimaryDoubles = function (translationPairs) {
            var result = [];
            console.log("Beginning to filter through translation pairs looking for primary forms doublettes...");
            // find items like German mutual primary forms "der", "die", "das" all having the English translation "the"
            for (var i = 0; i < translationPairs.length; i++) {
                var word1 = translationPairs[i][0];
                var word2 = translationPairs[i][1];


                // first, for all translation pairs go through their words and check, whether all primary forms of one word are translated by / related to the other word
                // if so, add them as primary forms
                if (word1.formsPrimary_IDs) {
                    var word1PrimaryFormsAreAllTranslatedByWord2 = true;

                    for (var f = 0; f < word1.formsPrimary_IDs.length; f++) {
                        var primaryForm = data.getObjectByID(word1.formsPrimary_IDs[f], "test.filterFormsPrimaryDoubles");
                        // if any primary forms of word1 are not related to / translated by word 2
                        if (primaryForm && primaryForm.related_IDs.indexOf(word2.id) < 0) {
                            word1PrimaryFormsAreAllTranslatedByWord2 = false;
                            // console.log("[1] Primary form " + primaryForm.name + " is not translatable by " + word2.name);
                        } else {
                            // console.log("[1] Primary form " + word1.formsPrimary_IDs[f] +": "+primaryForm + " is translateable by " + word2.name)
                        }
                    }

                    // if primary forms of word 1 are all translated by word 2, add all those forms to word 1 primary forms as word objects
                    if (word1PrimaryFormsAreAllTranslatedByWord2) {
                        // console.log("All primary forms of " + word1.name + " are translateable into " + word2.name);
                        word1.formsPrimary = [];
                        for (var f = 0; f < word1.formsPrimary_IDs.length; f++) {
                            var primaryForm = data.getObjectByID(word1.formsPrimary_IDs[f], "test.filterFormsPrimaryDoubles");
                            word1.formsPrimary.push(primaryForm);
                            // console.log("Added " + primaryForm.name + " to primary forms of " + word1.name);
                        }
                    }
                }

                if (word2.formsPrimary_IDs) {

                    var word2PrimaryFormsAreAllTranslatedByWord1 = true;

                    for (var f = 0; f < word2.formsPrimary_IDs.length; f++) {
                        var primaryForm = data.getObjectByID(word2.formsPrimary_IDs[f], "test.filterFormsPrimaryDoubles");
                        // if any primary forms of word2 are not related to / translated by word 1
                        if (primaryForm && primaryForm.related_IDs.indexOf(word1.id) < 0) {
                            word2PrimaryFormsAreAllTranslatedByWord1 = false;
                            // console.log("[2] Primary form " + primaryForm.name + " is not translatable by " + word1.name);
                        } else {
                            // console.log("[2] Primary form " + word2.formsPrimary_IDs[f] + ": " + primaryForm + " is translateable by " + word1.name);
                        }
                    }

                    // if primary forms of word 2 are all translated by word 1, add all those forms to word 2 primary forms as word objects
                    if (word2PrimaryFormsAreAllTranslatedByWord1) {
                        // console.log("All primary forms of " + word2.name + " are translateable into " + word1.name);
                        word2.formsPrimary = [];
                        for (var f = 0; f < word2.formsPrimary_IDs.length; f++) {
                            var primaryForm = data.getObjectByID(word2.formsPrimary_IDs[f], "test.filterFormsPrimaryDoubles");
                            word2.formsPrimary.push(primaryForm);
                            // console.log("Added " + primaryForm.name + " to primary forms of " + word2.name);
                        }
                    }
                }

            }

            var primaryForm_IDs = [];

            // walk again through all translation pairs and if they contain the newly added formsPrimary field, remove double entries
            for (var i = 0; i < translationPairs.length; i++) {
                // for all words, where there are formsPrimary objects, search for those formsPrimary ids among the translation pairs and if found, remove them

                // if one or both pair parts have primary forms (added in the previous loop), exclude their primary pairs from the results
                if (translationPairs[i][0].formsPrimary || translationPairs[i][1].formsPrimary) {
                    // if any of both words is excluded, do not add the pair again
                    if (primaryForm_IDs.indexOf(translationPairs[i][0].id) < 0 && primaryForm_IDs.indexOf(translationPairs[i][1].id) < 0) {
                        // console.log(JSON.stringify(primaryForm_IDs) + " does not contain " + translationPairs[i][0].id + "(" + primaryForm_IDs.indexOf(translationPairs[i][0].id) + ") and " + translationPairs[i][1].id + "(" + primaryForm_IDs.indexOf(translationPairs[i][1].id) + ")");
                        result.push(translationPairs[i]);
                        primaryForm_IDs.push(translationPairs[i][0].id);
                        primaryForm_IDs.push(translationPairs[i][1].id);
                        // console.log("Added " + translationPairs[i][0].id + ": " + translationPairs[i][0].name + " <-> " + translationPairs[i][1].id + ": " + translationPairs[i][1].name + " to the result and excluded their id_s from being added again.");
                        // console.log("Excluded primary forms are " + JSON.stringify(primaryForm_IDs));

                    }
                } else {
                    result.push(translationPairs[i]);
                    // console.log("Added " + translationPairs[i][0].id + ": " + translationPairs[i][0].name + " <-> " + translationPairs[i][1].id + ": " + translationPairs[i][1].name + " to the result");
                }
            }

            return result;
        }


        test.filterSynonyms = function (translationPairs) {
            var result = [];

            var excluded_ids = [];

            for (var i = 0; i < translationPairs.length; i++) {
                var word1 = translationPairs[i][0];
                var word2 = translationPairs[i][1];

                var include = true;

                // if word1 and word2 are not excluded yet
                if (excluded_ids.indexOf(word1.id) < 0 && excluded_ids.indexOf(word2.id) < 0) {

                    // find other words which translate into word1 or word2
                    for (var s = 0; s < translationPairs.length; s++) {
                        // avoid comparing the identical translationPairs
                        if (s !== i) {
                            var wordA = translationPairs[s][0];
                            var wordB = translationPairs[s][1];

                            // if wordA is related to word2 and is not identical with word1
                            if (word1.id !== wordA.id) {
                                if (wordA.related_IDs.indexOf(word2.id) >= 0) {
                                    if (!word1.synonyms) {
                                        word1.synonyms = [];
                                    }
                                    // console.log("Does " + word1.name + " synonym list already contain " + wordA.name + "?  " + data.arrayContainsObjectWithID(word1.synonyms, wordA.id));
                                    if (!data.arrayContainsObjectWithID(word1.synonyms, wordA.id)) {
                                        if (!word1.formsPrimary_IDs || word1.formsPrimary_IDs.indexOf(wordA.id) < 0) {
                                            word1.synonyms.push(wordA);
                                            excluded_ids.push(word1.id);
                                            excluded_ids.push(wordA.id);
                                            // console.log(wordA.name + " is synonym " + word1.synonyms.length + " of " + word1.name);
                                            for (var n = 0; n < word1.synonyms.length; n++) {
                                                console.log("     " + n + " -> " + word1.synonyms[n].name);
                                            }
                                        }
                                    }
                                }
                            }

                            // if wordB is related to word1 and is not identical with word2
                            if (wordB.id !== word2.id) {
                                if (wordB.related_IDs.indexOf(word1.id) >= 0) {
                                    if (!word2.synonyms) {
                                        word2.synonyms = [];
                                    }
                                    // console.log("Does " + word2.name + " synonym list already contain " + wordB.name + "?  " + data.arrayContainsObjectWithID(word2.synonyms, wordB.id));
                                    if (!data.arrayContainsObjectWithID(word2.synonyms, wordB.id)) {
                                        if (!word2.formsPrimary_IDs || word2.formsPrimary_IDs.indexOf(wordB.id) < 0) {
                                            word2.synonyms.push(wordB);
                                            excluded_ids.push(word2.id);
                                            excluded_ids.push(wordB.id);
                                            // console.log(wordB.name + " is synonym " + word2.synonyms.length + " of " + word2.name);

                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    include = false;
                }

                if (include) {
                    if (word1.synonyms) {
                        // console.log("Found " + word1.synonyms.length + " synonyms for word " + word1.name);
                        for (var n = 0; n < word1.synonyms.length; n++) {
                            // console.log("   " + n + word1.synonyms[n].name);
                        }
                    }

                    if (word2.synonyms) {
                        // console.log("Found " + word2.synonyms.length + " synonyms for word " + word2.name);
                        for (var n = 0; n < word2.synonyms.length; n++) {
                            console.log("   " + n + " " + word2.synonyms[n].name);
                        }
                    }
                    var pair = [];
                    pair[0] = word1;
                    pair[1] = word2;
                    result.push(pair)
                }
            }

            return result;
        };

        /**
         * test.selectAndCheck      is used by test-grid select/deselect items and to check, whether the selected items on that page match each other (are mutual translations) and then marks them as solved or erroneous
         * @param {type} word       the word clicked on
         * @returns {unresolved}
         */
        test.selectAndCheck = function (word) {
            if (word.status !== "solved") {
                // if word had been selected before, mark it as unselected and return it, to end this function
                if (test.selectedWord1 === word) {
                    console.log("Removed selection of " + word.name);
                    word.status = "";
                    test.selectedWord1 = {};
                    return word;
                } else {
                    if (test.selectedWord1.name) {
                        test.selectedWord2 = word;
                        word.status = "selected";
                        console.log("Selected 2nd word " + word.name);

                        if (test.pairEqualsWords(test.translationPairs, test.selectedWord1, test.selectedWord2)) {
                            console.log("Found solution!");
                            test.selectedWord1.status = "solved";
                            test.selectedWord2.status = "solved";
                            if (test.selectedWord1.language_ID === data.user.languageLearn_id) {
                                test.selectedWord1.playAudio(test.selectedWord1.audio);
                            } else {
                                test.selectedWord2.playAudio(test.selectedWord2.audio);
                            }

                        } else {
                            console.log("Wrong pair " + test.selectedWord1.name + "," + test.selectedWord2.name + " selected...");
                            word.status = "error";
                            test.selectedWord1.status = "error";
                        }

                        test.selectedWord1 = {};
                        test.selectedWord2 = {};

                    } else {
                        test.selectedWord1 = word;
                        word.status = "selected";
                        console.log("Selected word " + word.name);
                    }
                }
            }
        };

        return test;
    });

})();