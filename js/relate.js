/**
 This file is part of knowBetter.
 
 knowBetter is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 knowBetter is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with knowBetter.  If not, see <http://www.gnu.org/licenses/>.
 **/


(function () {

    var app = angular.module('relateService', ['relateService']);

    app.factory('relateService', function (dataService) {
        var data = dataService;

        var relate = {};

        relate.languageLeft = {};
        relate.languageRight = {};

        relate.activeLeft = {};
        relate.activeRight = {};

        relate.wordsLanguageRight = [];
        relate.wordsLanguageLeft = [];

        relate.wordsLeft = [];
        relate.wordsRight = [];

        relate.wordsLeftUntranslated = [];
        relate.wordsRightUntranslated = [];

        relate.categoryLeft = {};
        relate.categoryRight = {};

        relate.currentCategory_id;

        relate.filterUnrelated = false;

        relate.limitLeft = 20;
        relate.limitRight = 20;
        relate.limitRelateCategories = 20;


        relate.raiseLimitRight = function () {
            relate.limitRight = relate.limitRight + 20;
        };

        /**
         * @returns {Boolean}   Returns a boolean to indicate, whether the new limit is higher than the length of the item list to display
         */
        relate.limitRightReached = function () {
            if (relate.filterUnrelated === false) {
                return relate.limitRight >= relate.wordsRightUntranslated.length;
            } else {
                return relate.limitRight >= relate.wordsRight.length;
            }
        };

        relate.raiseLimitLeft = function () {
            relate.limitLeft = relate.limitLeft + 20;
        };       
        
        /**
         * @returns {Boolean}   Returns a boolean to indicate, whether the new limit is higher than the length of the item list to display
         */
        relate.limitLeftReached = function () {
            if (relate.filterUnrelated === false) {
                return relate.limitLeft >= relate.wordsLeftUntranslated.length;
            } else {
                return relate.limitLeft >= relate.wordsLeft.length;
            }
        };
        
        relate.resetLimitLeft = function() {
            relate.limitLeft = 20;
        };       
        
        relate.resetLimitRight = function() {
            relate.limitRight = 20;
        };    
        
        relate.resetLimitRelateCategories = function() {
            relate.limitRelateCategories = 20;
        };
        
         
        relate.raiseLimitRelateCategories =  function(){
            relate.limitRelateCategories = relate.limitRelateCategories + 20;
        };       
        
        /**
         * @returns {Boolean}   Returns a boolean to indicate, whether the new limit is higher than the length of the item list to display
         */
        relate.limitRelateCategoriesReached = function () {
            if (relate.filterUnrelated === false) {
                return relate.limitRelateCategories >= relate.wordsLeftUntranslated.length;
            } else {
                return relate.limitRelateCategories >= relate.wordsLeft.length;
            }
        };

        /**
         * 
         * @returns {undefined}
         */
        relate.updateShowUntranslated = function () {
            if (relate.filterUnrelated) {
                relate.wordsLeft = relate.wordsLeftUntranslated;
                relate.wordsRight = relate.wordsRightUntranslated;
            } else {
                relate.updateLanguageLeft();
                relate.updateLanguageRight();
            }
        };

        /**
         * returns an array containing all words not translated in the other language
         * @returns {undefined}
         */
        relate.getUntranslatedWords = function (items, language2_id) {
            var filtered = [];

            for (var i = 0; i < items.length; i++) {
                var isTranslated = false;
                for (var r = 0; r < items[i].related.length; r++) {
                    console.log("Tryng to get object by ID " + items[i].related[r]);
                    var related = data.getObjectByID(items[i].related[r], "relate.getUntranslatedWords");
                    if (related) {

                        if (related.language === language2_id) {
                            isTranslated = true;
                        }
                    }
                }
                if (isTranslated === false) {
                    filtered.push(items[i]);
                }
            }

            return filtered;
        }

        /**
         * 
         * @returns {undefined}
         */
        relate.updateLanguageLeft = function () {
            console.log("Updated language left to " + relate.languageLeft);
            relate.wordsLeft = data.getWordsForLanguage(relate.languageLeft);
            relate.wordsLeftUntranslated = relate.getUntranslatedWords(relate.wordsLeft, relate.languageRight);
            relate.wordsRightUntranslated = relate.getUntranslatedWords(relate.wordsRight, relate.languageLeft);
        };

        /**
         * 
         * @returns {undefined}
         */
        relate.updateLanguageRight = function () {
            console.log("Updated language right to " + relate.languageRight);
            relate.wordsRight = data.getWordsForLanguage(relate.languageRight);
            relate.wordsLeftUntranslated = relate.getUntranslatedWords(relate.wordsLeft, relate.languageRight);
            relate.wordsRightUntranslated = relate.getUntranslatedWords(relate.wordsRight, relate.languageLeft);
        };
        
        /**
         * 
         * @param {type} name
         * @param {type} category
         * @param {type} language
         * @returns {undefined}
         */
        relate.saveWord = function (name, category, language) {
            var word = {};
            word.name = name;
            word.categories = [];
            word.related = [];
            if(category){
                word.categories.push(category.id);
            }
            word.language = language.id;


            // if word.name contains ?, !, or . - mark it as sentence
            if (word.name.match(/[?!.]/)) {
                word.sentence = true;
            }

            if (language === relate.languageLeft) {
                relate.wordsLeftUntranslated.push(word);
            } else {
                relate.wordsRightUntranslated.push(word);
            }

            data.storeWord(word);
        };

        /**
         * 
         * @param {type} char
         * @returns {undefined}
         */
        relate.addCharToQuickWordRight = function (char) {
            if (relate.quickWordRightName) {
                relate.quickWordRightName = relate.quickWordRightName + char;
            } else {
                relate.quickWordRightName = char;
            }
        };

        /**
         * 
         * @param {type} char
         * @returns {undefined}
         */
        relate.addCharToQuickWordLeft = function (char) {
            if (relate.quickWordLeftName) {
                relate.quickWordLeftName = relate.quickWordLeftName + char;
            } else {
                relate.quickWordLeftName = char;
            }
        };

        /**
         * If on both sides a word is selected, add each others relations to each other
         * @returns {undefined}
         */
        relate.addRelations = function () {
            if (relate.activeLeft.id && relate.activeRight.id) {
                if (relate.activeLeft.id !== relate.activeRight.id) {
                    console.log("Trying to relate " + relate.activeLeft.name + " and " + relate.activeRight.name);
                    console.log(relate.activeLeft.name + " had " + relate.activeLeft.related.length + " relations before.");

                    // exchange categories
                    for(var i = 0; i < relate.activeLeft.categories.length; i++){
                        if(relate.activeRight.categories.indexOf(relate.activeLeft.categories[i]) < 0){
                            relate.activeRight.categories.push(relate.activeLeft.categories[i]);
                        }
                    }
                    
                    for(var i = 0; i < relate.activeRight.categories.length; i++){
                        if(relate.activeLeft.categories.indexOf(relate.activeRight.categories[i]) < 0){
                            relate.activeLeft.categories.push(relate.activeRight.categories[i]);
                        }
                    }

                    // exchange relationships
                    // right to left

                    for (var i = 0; i < relate.activeRight.related.length; i++) {
                        // console.log("Relating "+relate.activeLeft.name+" and "+ data.getObjectByID(relate.activeRight.related[i]).name);
                        if (relate.activeLeft.related.indexOf(relate.activeRight.related[i]) === -1) {
                            // console.log(relate.activeLeft.name+" does not yet contain relation to "+data.getObjectByID(relate.activeRight.related[i]).name);

                            // check that object is not a comment (comments do not have language properties)
                            if (data.getObjectByID(relate.activeRight.related[i], "relate.addRelations[2]").language) {
                                // check that I don't accidentally relate the object to itself, if the two were already related
                                if (relate.activeRight.related[i] !== relate.activeLeft.id) {
                                    relate.activeLeft.related.push(relate.activeRight.related[i]);
                                }
                            }

                        }
                    }
                    ;

                    // exchange relationships
                    // left to right
                    for (var i = 0; i < relate.activeLeft.related.length; i++) {
                        if (relate.activeRight.related.indexOf(relate.activeLeft.related[i]) === -1) {
                            if (!data.getObjectByID(relate.activeLeft.related[i], "relate.addRelations[3]").sentence) {
                                if (relate.activeLeft.related[i] !== relate.activeRight.id) {
                                    relate.activeRight.related.push(relate.activeLeft.related[i]);
                                }
                            }
                        }
                    }
                    ;

                    // add the relations between the two objects themselves
                    var i = relate.activeLeft.related.indexOf(relate.activeRight.id);
                    if (i === -1) {
                        relate.activeLeft.related.push(relate.activeRight.id);
                    }

                    var i = relate.activeRight.related.indexOf(relate.activeLeft.id);
                    if (i === -1) {
                        relate.activeRight.related.push(relate.activeLeft.id);
                    }

                    data.storeWord(relate.activeLeft);
                    data.storeWord(relate.activeRight);

                    console.log(relate.activeLeft.name + " has now " + relate.activeLeft.related.length + " relations.");
                    console.log(relate.activeRight.name + " has now " + relate.activeRight.related.length + " relations.");
                }
                // reset both active words
                relate.activeLeft = {};
                relate.activeRight = {};
            }
        };

        /**
         * 
         * @param {type} word
         * @returns {undefined}
         */
        relate.connectLeftElement = function (word) {
            // if word is selected twice, remove it from selections
            if (relate.activeLeft.id === word.id) {
                relate.activeLeft = {};
            } else {
                relate.activeLeft = word;
                relate.addRelations();
            }
        };

        /**
         * @param {type} word
         * @returns {undefined}
         */
        relate.connectRightElement = function (word) {
            if (relate.activeRight.id === word.id) {
                relate.activeRight = {};
            } else {
                relate.activeRight = word;
                relate.addRelations();
            }
        };


        /**
         * 
         * @param {type} word
         * @returns {undefined}
         */
        relate.addSelectedCategory = function (word) {
            if (relate.currentCategory_id) {
                if (word.categories.indexOf(relate.currentCategory_id) === -1) {
                    word.categories.push(relate.currentCategory_id);
                    data.storeWord(word);
                }
                // add category to all its related words
                for (var i = 0; i < word.related.length; i++) {
                    var relatedWord = data.getObjectByID(word.related[i], "relate.addSelectedCategory");
                    if (relatedWord) {
                        if (relatedWord.categories.indexOf(relate.currentCategory_id) === -1) {
                            relatedWord.categories.push(relate.currentCategory_id);
                            data.storeWord(relatedWord);
                        }
                    }
                }
            }
        };

        /**
         * @param {type} category_id
         * @returns {undefined}
         */
        relate.selectCategory = function (category_id) {
            relate.currentCategory_id = category_id;
        };

        relate.removeCategory = function (word, category_id) {
            if (word.categories) {
                var i = word.categories.indexOf(category_id);
                if (i !== -1) {
                    word.categories.splice(i, 1);
                }
                data.storeWord(word);
            }
        };

        return relate;
    });

})();
