/**
 This file is part of knowBetter.
 
 knowBetter is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 knowBetter is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with knowBetter.  If not, see <http://www.gnu.org/licenses/>.
 **/

(function () {

    var app = angular.module('exportDataService', ["dataService"]);
    app.factory('exportDataService', ["dataService", function (data) {
            var exportData = {};

            var data = data;

            /**
             * Exports words, languages, users, comments and categories in json format to a file made available for download
             */
            exportData.exportContent = function (prefix) {
                var exportText = "";
                exportText = prefix + "_languages = []; \r\n" + prefix + "_words = [];\r\n" + prefix + "_users = []; \r\n" + prefix + "_comments = []; \r\n" + prefix + "_categories = []; \r\n";
                exportText = exportText + "this." + prefix + "_words = [{\r\n";
                exportText = exportText + exportData.objectsToStrings(data.words);
                exportText = exportText + "\r\n }]; \r\n\r\n";
                exportText = exportText + "this." + prefix + "_languages = [{\r\n";
                exportText = exportText + exportData.objectsToStrings(data.languages);
                exportText = exportText + "\r\n }]; \r\n\r\n";
                exportText = exportText + "this." + prefix + "_tags = [{\r\n";
                exportText = exportText + exportData.objectsToStrings(data.tags);
                exportText = exportText + "\r\n }];\r\n\r\n";
                exportText = exportText + "this." + prefix + "_users = [{\r\n";
                exportText = exportText + exportData.objectsToStrings(data.users);
                exportText = exportText + "\r\n }];\r\n\r\n";
                exportText = exportText + "this." + prefix + "_comments = [{\r\n";
                exportText = exportText + exportData.objectsToStrings(data.comments);
                exportText = exportText + "\r\n }];\r\n\r\n";
                exportText = exportText + "this." + prefix + "_categories = [{\r\n"
                exportText = exportText + exportData.objectsToStrings(data.categories);
                exportText = exportText + "\r\n }];"

                var blob = new Blob(["" + exportText], {type: "text/plain;charset=utf-8"});
                if (prefix === "import") {
                    saveAs(blob, "import.js");
                } else {
                    saveAs(blob, "content.js");
                }
            };


            /**
             * Function to transform objects into JSON-like String representations
             * Takes care of special cases like arrays by their value key names
             * 
             * @param {type} array      array of objects to be transformed into String values
             * @returns {String}        JSON string of all objects in array given
             */
            exportData.objectsToStrings = function (array) {
                var exportText = "";

                for (var i = 0; i < array.length; i++) {
                    // Printing property names and values using Array.forEach
                    var item = array[i];
                    Object.getOwnPropertyNames(item).forEach(function (val, idx, array) {
                        // do not store the following key values, as they are programmatically added, but should not end up in the file database
                        if (val !== "$$hashKey" & val !== "sentences" & val !== "comments" & val !== "synonyms" & val !== "alternativeTranslations" & val !== "length" & val != "added_translation" & val != "translation" & val != "solution" & val != "part1" & val != "part2") {
                            // the following key values require to be escaped by string chars, as they contain strings
                            if (val === "id" || val === "name" || val === "tag" || val === "group" || val === "key" || val === "color" || val === "formPrimaryName" || val === "theme" || val === "language_ID" || val === "languageKnown_id" || val === "languageLearn_id") {
                                // console.log("Exporting field " + val + ": " + item[val]);

                                exportText = exportText + "\t \t" + (val + ' : "' + item[val]) + '",\r\n';

                            } else {
                                // if val is called "progress", it is the array where user progress is stored
                                if (val === "languageKnown" || val === "languageLearn") {
                                    exportText = exportText + "\t \t" + (val + ' : ' + item[val].id) + ",\r\n";
                                } else {
                                    if (Array.isArray(item[val])) {
                                        if (val === "chars") {
                                            exportText = exportText + "\t \t" + val + ' : [';
                                            var chars = item[val];
                                            for (var k = 0; k < chars.length; k++) {
                                                exportText = exportText + "'" + chars[k] + "', ";
                                            }
                                            exportText = exportText.substring(0, (exportText.length - 2)); // remove the last , 
                                            exportText = exportText + '],\r\n';
                                        } else {
                                            // some properties do store strings in an array which need to be escaped
                                            if ( val=== "formsPrimary_IDs" || val === "formsSecondary" || val === "formsSecondaryNames" || val === "related_IDs" ||
                                                    val === "categories_IDs" || val === "responses_IDs" || val === "progress_1" ||
                                                    val === "progress_2" || val === "progress_3" || val === "progress_4" || val === "articles") {
                                                exportText = exportText + "\t \t" + val + ' : [';
                                                for (var k = 0; k < item[val].length; k++) {
                                                    if (item[val][k] === null) {
                                                        item[val][k] = "";
                                                    }
                                                    exportText = exportText + '"' + item[val][k] + '"';
                                                    if (k < item[val].length - 1) {
                                                        exportText = exportText + ", ";
                                                    }
                                                }
                                                exportText = exportText + "],\r\n";
                                                // any other arrays only store raw numbers
                                            } else {
                                                exportText = exportText + "\t \t" + (val + ' : [' + item[val]) + "],\r\n";
                                            }
                                        }
                                    } else {
                                        exportText = exportText + "\t \t" + (val + ' : ' + item[val]) + ",\r\n";
                                    }
                                }
                            }
                        }
                    });
                    exportText = exportText.substring(0, (exportText.length - 3)); // remove the last three chars, to get rid of the last loop end ",\r\n"
                    if (i < array.length - 1) {
                        exportText = exportText + "\r\n \t }, { \r\n";
                    }
                }
                return exportText;
            };


            exportData.exportListByLanguage = function () {
                var exportList = "";

                exportList = "Id | Language | Content";

                for (var i = 0; i < data.words.length; i++) {
                    exportList = exportList + "\r\n";

                    exportList = exportList + data.words[i].id + " | ";
                    exportList = exportList + data.words[i].language_ID + " | ";
                    exportList = exportList + data.words[i].name + " | ";
                }

                var blob = new Blob(["" + exportList], {type: "text/plain;charset=utf-8"});
                saveAs(blob, "wordList.csv");

                return exportList;
            };

            exportData.exportListByCategory = function () {
                var exportList = "";

                exportList = "Category_ID | Category_Name | Id | Language_Name | Content";

                for (var i = 0; i < data.categories.length; i++) {
                    var words_in_category = data.getWordsForCategory(data.words, data.categories[i].id);
                    for (var k = 0; k < words_in_category.length; k++) {
                        exportList = exportList + "\r\n";
                        exportList = exportList + data.categories[i].id + " | ";
                        exportList = exportList + data.categories[i].name + " | ";
                        exportList = exportList + words_in_category[k].id + " | ";
                        exportList = exportList + data.getObjectByID(words_in_category[k].language_ID, "data.exportListByCategory").name + " | ";
                        exportList = exportList + words_in_category[k].name;
                    }
                }

                var blob = new Blob(["" + exportList], {type: "text/plain;charset=utf-8"});
                saveAs(blob, "wordList.csv");

                return exportList;
            };


            exportData.exportAudacityLabelsCutlist = function (language_ID, labelName) {
                var exportList = "";


                // store all words of language language_ID in wordsLanguage array
                var wordsLanguage = [];

                for (var i = 0; i < data.words.length; i++) {
                    if (data.words[i].language_ID === language_ID) {
                        wordsLanguage.push(data.words[i]);
                    }
                }

                var d = new Date();
                d.setTime(0);
                d.setHours(0);
                for (var i = 0; i < wordsLanguage.length; i++) {
                    var word = wordsLanguage[i];

                    // if not first line, add new line chars
                    if (exportList.length > 0) {
                        exportList = exportList + "\r\n";
                    }

                    // set a gap between words of 500 ms
                    d.setMilliseconds(d.getMilliseconds() + 500);

                    // first set the beginning (as the end of the last )
                    var seconds = (d.getHours() * 60 * 60) + (d.getMinutes() * 60) + d.getSeconds();
                    exportList = exportList + seconds + "." + d.getMilliseconds();


                    // then calculate the ending based on the approximate duration
                    var duration = word.name.length * 0.3;
                    if (duration < 2) {
                        duration = 2;
                    }



                    d.setSeconds(d.getSeconds() + duration);
                    seconds = (d.getHours() * 60 * 60) + (d.getMinutes() * 60) + d.getSeconds();


                    exportList = exportList + "\t" + seconds + "." + d.getMilliseconds() + "\t";

                    if (labelName) {
                        exportList = exportList + word.name;
                    } else {
                        exportList = exportList + word.id;
                    }
                }

                var blob = new Blob(["" + exportList], {type: "text/plain;charset=utf-8"});
                if (labelName) {
                    saveAs(blob, "cutlistWords.txt");
                } else {
                    saveAs(blob, "cutlistIDs.txt");
                }

                return exportList;
            }

            return exportData;
        }]);

})();
