/**
 This file is part of knowBetter.
 
 knowBetter is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 knowBetter is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with knowBetter.  If not, see <http://www.gnu.org/licenses/>.
 **/


(function () {

    var app = angular.module('editService', ['dataService', 'ngMaterial']);

    app.factory('editService', function (dataService) {
        var data = dataService;

        var edit = {};

        edit.relateWordsLeft = [];
        edit.relateWordsRight = [];


        edit.limitTranslationsList = 25;
        
        edit.untranslatedWords = [];
        // if app.edit is false, show test-mode and if it is true, show edit screen -> see index.html
        edit.edit = false;


        // edit.checkWordExists is used to notify user, if word being entered already exists
        edit.checkWordExists = function (word, language_id, word_id) {
            if (word) {
                var sameWords = data.getWordByNameAndLanguage(word, language_id, word_id);
            }

            if (sameWords) {
                edit.wordsSame = true;
                return sameWords;
            } else {
                edit.wordsSame = false;
                return [];
            }
            
        }


        edit.toggleEdit = function () {
            edit.edit = !edit.edit;
            return edit.edit;
        };

        edit.newLanguage = {};
        edit.newWord = {};
        edit.newWord.related_IDs = [];
        edit.newWord.categories_IDs = [];
        edit.newWord.responses_IDs = [];

        edit.isResponseTo = [];

        edit.newWord.tags = [];

        edit.comment = {};
        edit.comment.name = "";
        edit.comment.related_IDs = [];
        edit.comment.language_ID = {};

        edit.comments = [];

        edit.sentence_name = "";
        edit.sentences = [];


        edit.newWords = {};
        edit.newWords.language_ID = {};
        edit.newWords.categories_IDs = [];
        edit.newWords.words = "";

        edit.newQuickWord = {};


        edit.loadWord = function (word) {
            if (word) {
                edit.newWord = word;
                console.log("Loading word " + JSON.stringify(word));
                edit.comments = data.getCommentsForWord(word);
                console.log("Loaded comments " + JSON.stringify(edit.comments));
                edit.searchText = "";
                edit.sentences = data.getSentencesForWord(word);
                edit.sentences_created = [];
                edit.sentence_name = "";
                edit.commentName = "";
                edit.formSecondary = "";
                edit.formSecondaryName = "";
                edit.formPrimaryName = "";
                edit.formPrimaryWord = "";
                edit.tag.searchText = "";
                edit.searchRelationText = "";
                edit.checkWordExists(null, 0, 0);
                edit.isResponseTo = data.getParentsOfReponse(edit.newWord.id);
                if (!word.tags) {
                    word.tags = [];
                }
            }
        };

        edit.updateWordsList = function (language_translate, language_known) {
            if (language_translate && language_known && language_translate !== language_known) {
                edit.untranslatedWords = data.getWordsForLanguage(language_translate, language_known);
                console.log("Updated list of untranslated words, now containing " + edit.untranslatedWords.length + " words");
            }
        }

        edit.mergeObjects = function (objects) {
            var object1 = objects[0];
            var object2 = objects[1];

            console.log("Trying to merge " + object1.id + ": " + object1.name + " and " + object2.id + ": " + object2.name);
            if (object1.related_IDs && object2.related_IDs) {
                for (var i = 0; i < object2.related_IDs.length; i++) {
                    if (object1.related_IDs.indexOf(object2.related_IDs[i]) < 0) {
                        object1.related_IDs.push(object2.related_IDs[i]);
                        console.log("Merging relations ...");
                    }
                }
            }
            // 
            if (object1.categories_IDs && object2.categories_IDs) {
                for (var i = 0; i < object2.categories_IDs.length; i++) {
                    if (object1.categories_IDs.indexOf(object2.categories_IDs[i]) < 0) {
                        object1.categories_IDs.push(object2.categories_IDs[i]);
                        console.log("Merging categories...");
                    }
                }
            }
            data.storeWord(object1);
            data.deleteObjectByID(object2.id);
        }


        edit.saveWords = function () {

            var words = edit.newWords.words.split(/\n/);

            for (var i = 0; i < words.length; i++) {
                var word = {};
                if (words[i].indexOf(",") >= 0) {
                    var parts = words[i].split(",");
                    word.name = parts[0].trim();
                    word.tag = parts[1].trim();
                } else {
                    word.name = words[i];
                }
                word.language_ID = edit.newWords.language.id;
                word.categories_IDs = edit.newWords.categories_IDs;
                word.related_IDs = [];
                data.storeWord(word);
            }
        };

        edit.saveComment = function () {
            if (edit.comment.name && edit.comment.language_ID) {
                var comment = {};
                comment.name = edit.comment.name;
                comment.language_ID = edit.comment.language_ID;
                comment.related_IDs = [];
                comment.isComment = true;

                if (edit.newWord.id) {
                    comment.related_IDs.push(edit.newWord.id);
                } else {
                    comment.related_IDs.push(data.storeWord(edit.newWord).id);
                }
                data.storeComment(comment);
                console.log("Saved comment " + JSON.stringify(comment));
            }
        };

        edit.createWord = function () {
            edit.newWord = {};
            edit.newWord.categories_IDs = [];
            edit.newWord.related_IDs = [];

            edit.newWord.tags = [];

            edit.comment = {};
            edit.comment.name = "";
            edit.comment.related_IDs = [];
            edit.comment.language_ID = {};
            edit.comments = [];

            edit.sentence = "";
            edit.sentences = [];
            edit.sentences_created = [];

            edit.searchText = "";

            edit.sentence_name = "";
            edit.formSecondary = "";
            edit.formSecondaryName = "";
            edit.formPrimaryName = "";
            edit.formPrimaryWord = "";
            edit.tag.searchText = "";
            edit.searchRelationText = "";
        };

        edit.createSentence = function (sentence_name) {
            var sentence = {};
            sentence.name = sentence_name;
            sentence.language_ID = edit.newWord.language_ID;
            sentence.sentence = true;
            sentence.id = data.getNewID();
            sentence.related_IDs = [];
            sentence.categories_IDs = edit.newWord.categories_IDs;

            if (edit.newWord.id) {
                sentence.related_IDs.push(edit.newWord.id);
                data.storeWord(sentence);
            } else {
                edit.newWord.id = data.getNewID();
                sentence.related_IDs.push(edit.newWord.id);
                data.storeWord(sentence);
            }
            if (edit.newWord.related_IDs.indexOf(sentence.id) < 0) {
                edit.newWord.related_IDs.push(sentence.id);
                data.storeWord(edit.newWord);
            }
            edit.sentences = data.getSentencesForWord(edit.newWord);
        };

        edit.createResponse = function () {
            var response = {};
            response.name = edit.response;
            response.language_ID = edit.newWord.language_ID;
            response.sentence = true;
            response.categories_IDs = edit.newWord.categories_IDs;
            response.related_IDs = [];

            response = data.storeWord(response);

            // add id of response to word and store it
            edit.addResponse(response.id);

            data.storeWord(edit.newWord);
        };


        edit.saveLanguage = function (language) {
            var lang = jQuery.extend({}, language); // make a shallow copy to avoid the changes made here spilling over into the editor
            if (!lang.id) {
                lang.id = data.getNewID();
            }
            var charString = lang.chars;

            // sanitize the chars string
            charString = charString.replace(/\s/g, ""); // remove all whitespace characters (including tabs, etc...)
            charString = data.removeDoubleChars(charString);
            charString = charString.replace(",", "");
            charString = charString.replace(".", "");
            charString = charString.replace("-", "");
            lang.chars = charString.split("");

            data.storeLanguage(lang);
            console.log("Stored language " + JSON.stringify(lang));
            edit.newWord.language_ID = lang.id;
            edit.newLanguage = {}; // reset new language popup dialogue
        };



        // remove comment from comments array and then from the data.comments array
        edit.deleteComment = function (comment) {
            edit.comments.splice(edit.comments.indexOf(comment), 1);
            data.deleteComment(comment);
        };


        edit.deleteSentenceCreated = function (sentence) {
            edit.sentences_created.splice(edit.sentences_created.indexOf(sentence), 1);
        };

        edit.deleteWord = function (word) {
            if (word.id) {
                data.deleteWord(word);
                edit.createWord();
            }
        };

        edit.addCategoryToWords = function (category_ID) {
            var i = edit.newWords.categories_IDs.indexOf(category_ID);
            if (i === -1) {
                edit.newWords.categories_IDs.push(category_ID);
            }
        };


        /**
         * 
         * @param {ID} relation_ID
         * @returns nothing
         */
        edit.addRelation = function (relation_ID) {
            if (relation_ID) {
                var i = edit.newWord.related_IDs.indexOf(relation_ID);
                if (i === -1) {
                    edit.newWord.related_IDs.push(relation_ID);
                }

                // get the newly related word
                var relationWord = data.getObjectByID(relation_ID, 'edit.addRelation()');

                if (relationWord) {
                    console.log("Trying to add the categories of the newly related word " + relationWord.name + " to word...");
                    // add the categories of the newly related words
                    for (var i = 0; i < relationWord.categories_IDs.length; i++) {
                        if (edit.newWord.categories_IDs.indexOf(relationWord.categories_IDs[i]) < 0) {
                            edit.newWord.categories_IDs.push(relationWord.categories_IDs[i]);
                        }
                    }
                }

                edit.selectedRelation = "";

                // save the word to make sure, that it's new relations are recorded in the database
                data.storeWord(edit.newWord);
            }
        };

        /**
         * Add alternative primary form to word (if new word form, create it)
         * 
         * The function either accepts one pre-existing word form or in case of two parameters a new word form and a word form name
         * 
         * @param {String | Word}   formWord    Either a pre-existing word or a string representation of a new word form
         * @param {String}          formName    Only given for new word forms, then this is the name of the word form (e.g. female or male)
         * @returns {undefined}
         */
        edit.addFormPrimary = function (formWord, formName) {
            if (formWord) {
                console.log("Trying to create new primary word form....");
                console.log("Given parameters: " + JSON.stringify(formWord) + " - and - " + JSON.stringify(formName));
                if (!edit.newWord.formsPrimary_IDs) {
                    edit.newWord.formsPrimary_IDs = [];
                }

                if (arguments.length === 2) {
                    console.log("Was given 2 parameters, now trying to create a word from them");
                    var word = {};
                    word.name = formWord;
                    word.id = data.getNewID();
                    word.formPrimaryName = formName;
                    word.categories_IDs = edit.newWord.categories_IDs;
                    word.language_ID = edit.newWord.language_ID;
                    word.formsPrimary_IDs = [];

                    if (!edit.newWord.id) {
                        edit.newWord = data.storeWord(edit.newWord);
                    }

                    word.formsPrimary_IDs.push(edit.newWord.id);
                    word.related_IDs = [];

                    if (edit.newWord.sentence) {
                        word.sentence = edit.newWord.sentence;
                    }

                    // add relations of edit.newWord to new primary form
                    for (var i = 0; i < edit.newWord.related_IDs.length; i++) {
                        var relation = data.getObjectByID(edit.newWord.related_IDs[i], 'edit.addFormPrimary()');
                        if (relation && relation.sentence === word.sentence) {
                            if (word.related_IDs.indexOf(relation.id) < 0) {
                                word.related_IDs.push(relation.id);
                            }
                            if (relation.related_IDs.indexOf(word.id) < 0) {
                                relation.related_IDs.push(word.id);
                            }
                        }
                    }

                    // make sure, all primary forms are shared
                    for (var i = 0; i < edit.newWord.formsPrimary_IDs.length; i++) {
                        var primaryForm = data.getObjectByID(edit.newWord.formsPrimary_IDs[i], 'addFormPrimary');
                        if (word.formsPrimary_IDs.indexOf(primaryForm.id) < 0) {
                            word.formsPrimary_IDs.push(primaryForm.id);
                        }
                        if (primaryForm.formsPrimary_IDs.indexOf(word.id) < 0) {
                            primaryForm.formsPrimary_IDs.push(word.id);
                            data.storeWord(primaryForm);
                        }
                    }

                    // assign the now set id of the word form to the currently being edited word
                    edit.newWord.formsPrimary_IDs.push(word.id);

                    data.storeWord(word);
                    data.storeWord(edit.newWord);

                    console.log("Trying to store new primary form: " + JSON.stringify(word));

                    edit.formPrimaryName = "";
                    edit.formPrimaryWord = "";
                }

            }

        };

        edit.removeFormPrimary = function (id) {
            if (edit.newWord.formsPrimary_IDs.indexOf(id) > -1) {
                edit.newWord.formsPrimary_IDs.splice(edit.newWord.formsPrimary_IDs.indexOf(id), 1);
            }
        };


        edit.removeRelation = function (relation_ID) {
            var i = edit.newWord.related_IDs.indexOf(relation_ID);
            if (i !== -1) {
                edit.newWord.related_IDs.splice(i, 1);
            }
            data.removeRelationFromObject(relation_ID, edit.newWord.id);
        };

        /*+
         * 
         * @param {type} word
         * @param {type} translation
         * @returns {undefined}
         */
        edit.addTranslation = function (word, translation) {
            var translationWord = {};
            translationWord.name = translation;
            translationWord.language_ID = edit.translate_language_known;
            console.log("Translation language "+edit.translate_language_known);
            translationWord.categories_IDs = word.categories_IDs;
            if (word.sentence) {
                translationWord.sentence = word.sentence;
            }
            translationWord.related_IDs = [];
            translationWord.id = data.getNewID();

            data.storeWord(translationWord);
            data.addRelationToObject(translationWord.id, word.id);

            // add relations, if same type
            for (var i = 0; i < word.related_IDs.length; i++) {
                var relation = data.getObjectByID(word.related_IDs[i], "edit.addTranslation()");
                if (relation.sentence === translationWord.sentence) {
                    data.addRelationToObject(translationWord.id, relation.id);
                }
            }
            word.added_translation = true;
        }



        edit.addSentence = function (sentence_IDs) {
            // if word does not have sentences array yet, create one
            if (!edit.newWord.sentences_IDs) {
                edit.newWord.sentences_IDs = [];
            }
            // if word does not yet contain response with same id, add it to the array
            var i = edit.newWord.sentences_IDs.indexOf(sentence_IDs);
            if (i === -1) {
                edit.newWord.sentences_IDs.push(sentence_IDs);
            }
            edit.response = "";
        };

        /**
         * 
         * @param {INTEGER} response_ID
         * @returns {undefined}
         */
        edit.addResponse = function (response_ID) {
            if (response_ID) {
                // if word does not have reponses array yet, create one
                if (!edit.newWord.responses_IDs) {
                    edit.newWord.responses_IDs = [];
                }
                // if word does not yet contain response with same id, add it to the array
                var i = edit.newWord.responses_IDs.indexOf(response_ID);
                if (i === -1) {
                    edit.newWord.responses_IDs.push(response_ID);
                }
                edit.response = "";
            }

        };

        /**
         * 
         * @param {type} response_id
         * @returns {undefined}
         */
        edit.removeResponse = function (response_id) {
            console.log("Trying to remove respone " + response_id);
            var i = edit.newWord.responses_IDs.indexOf(response_id);
            if (i !== -1) {
                console.log("Removed response " + response_id);
                edit.newWord.responses_IDs.splice(i, 1);
            }
        };


        edit.removeCategory = function (category_ID) {
            var i = edit.newWord.categories_IDs.indexOf(category_ID);
            if (i !== -1) {
                edit.newWord.categories_ID.splice(i, 1);
            }
        };

        edit.removeSentence = function (sentence_id) {
            var i = edit.newWord.sentences_IDs.indexOf(sentence_id);
            if (i !== -1) {
                edit.newWord.sentences_IDs.splice(i, 1);
            }
        }

        edit.getImagePath = function (word_id) {
            var path = "./media/images/" + word_id + ".png";
            // console.log("Returning path "+path);
            return path;
        };


        edit.getAudioPathOgg = function (word_id) {
            var path = "./media/audio/" + word_id + ".ogg";
            // console.log("Returning path "+path);
            return path;
        };

        edit.getAudioPathMP3 = function (word_id) {
            var path = "./media/audio/" + word_id + ".mp3";
            // console.log("Returning path "+path);
            return path;
        };

        edit.addFormSecondary = function (newFormSecondaryName, newFormSecondary, index) {

            if (!edit.newWord.formsSecondary) {
                edit.newWord.formsSecondary = [];
            }
            if (!edit.newWord.formsSecondaryNames) {
                edit.newWord.formsSecondaryNames = [];
            }

            // if index was given, overwrite index of tables
            if (index && index <= edit.newWord.formsSecondary.length) {
                edit.newWord.formsSecondary[index] = newFormSecondary;
                edit.newWord.formsSecondaryNames[index] = newFormSecondaryName;
                data.storeWord(edit.newWord);
            } else {
                // if no index was given, add new entry to both arrays
                // if it is a category name, it has to be unique!
                if (!newFormSecondary || newFormSecondary.length === 0) {
                    if (edit.newWord.formsSecondaryNames.indexOf(newFormSecondaryName) > -1) {
                        console.log("Cannot add form category, as that name is already existant as form name.");
                    } else {
                        edit.newWord.formsSecondary.push(newFormSecondary);
                        edit.newWord.formsSecondaryNames.push(newFormSecondaryName);
                        data.storeWord(edit.newWord);
                    }
                    // if it was not a category name (a name without a form value), add it as a form with its form name
                } else {
                    edit.newWord.formsSecondary.push(newFormSecondary);
                    edit.newWord.formsSecondaryNames.push(newFormSecondaryName);
                    data.storeWord(edit.newWord);
                }
            }
        };

        edit.editFormSecondary = function (index) {
            if (index && edit.newWord.formsSecondary.length >= index) {
                edit.formSecondary = edit.newWord.formsSecondary[index];
                edit.newFormSecondaryName = edit.newWord.formsSecondaryNames[index];
            }
        };

        edit.removeFormSecondary = function (index) {
            edit.newWord.formsSecondary.splice(index, 1);
            edit.newWord.formsSecondaryNames.splice(index, 1);
        };

        edit.moveUpSecondaryForm = function (index) {
            if (index > 0) {
                // first, remove both items                
                var itemIndex = edit.newWord.formsSecondary.splice(index, 1)[0];
                var itemNameIndex = edit.newWord.formsSecondaryNames.splice(index, 1)[0];
                var itemOther = edit.newWord.formsSecondary.splice(index - 1, 1)[0];
                var itemNameOther = edit.newWord.formsSecondaryNames.splice(index - 1, 1)[0];

                // readd both items in switched index order
                edit.newWord.formsSecondary.splice(index - 1, 0, itemIndex);
                edit.newWord.formsSecondaryNames.splice(index - 1, 0, itemNameIndex);
                edit.newWord.formsSecondary.splice(index, 0, itemOther);
                edit.newWord.formsSecondaryNames.splice(index, 0, itemNameOther);

                data.storeWord(edit.newWord);
            }
        };

        edit.moveDownSecondaryForm = function (index) {
            if (index < edit.newWord.formsSecondary.length - 1) {
                // first, remove both items  - the [0] is needed to not add arrays but rather the string object itself
                var itemIndex = edit.newWord.formsSecondary.splice(index, 1)[0];
                var itemNameIndex = edit.newWord.formsSecondaryNames.splice(index, 1)[0];
                var itemOther = edit.newWord.formsSecondary.splice(index, 1)[0];
                var itemNameOther = edit.newWord.formsSecondaryNames.splice(index, 1)[0];

                // readd both items in switched index order
                edit.newWord.formsSecondary.splice(index, 0, itemOther);
                edit.newWord.formsSecondaryNames.splice(index, 0, itemNameOther);
                edit.newWord.formsSecondary.splice(index + 1, 0, itemIndex);
                edit.newWord.formsSecondaryNames.splice(index + 1, 0, itemNameIndex);

                data.storeWord(edit.newWord);
            }
        };

        /**
         * Helper function for angular-material chips to transform an item into a property of that item
         * @param {type} item
         * @returns {unresolved}
         */
        edit.transformChip = function (item) {
            var result = {};
            result.name = item;
            result.isGlobal = false;
            result.languages_IDs = [];
            result.languages_IDs.push(edit.newWord.language_ID);
            return result;
        };

        /**
         * Search for tags matching the given query
         */
        function searchTags(query) {
            var result = [];
            var tags = data.getAllTags(edit.newWord.language_ID);
            for (var i = 0; i < tags.length; i++) {
                if (tags[i].toLowerCase().indexOf(query.toLowerCase()) >= 0) {
                    result.push(tags[i]);
                }
            }
            console.log("Tags matching query " + query + ": " + JSON.stringify(result));
            return result;
        }
        ;


        edit.shortenText = function (text) {
            if (text.length > 63) {
                return text.substring(0, 60) + "...";
            }
            return text;
        };


        return edit;
    });

})();
