### knowBetter - [knowBetter](https://gitlab.com/geoadel/knowBetter/) Learn to translate words between languages

knowBetter allows to learn words of several languages online or offline in any browser with javascript abilities. You can add words or even whole new languages. Right now, knowBetter focuses on Left-to-Right languages with mostly latin-based characters.

# Requirements
- Any browser with Javascript enabled should work
- No internet connection required

# Installation
You can download knowBetter [directly from gitlab](https://gitlab.com/geoadel/knowBetter/repository/archive.zip?ref=master). You only need to extract all files and open the index.html file in the root folder in your javascript-capable browser.

Tested successfully in Firefox 45.0 - not working in Microsoft Edge, at least not locally.

# Saving changes
All data except for audio files are stored in the browsers own local storage, but you can export all data and transfer them to another computer and share them with others.

# UI translations
The translation system uses the [angular-translate](https://github.com/angular-translate/angular-translate) library for angular 1.x. The UI_translations.js file in the root directory contains all translations for the UI languages. There you can adapt and change the user interface text and correct translation mistakes.

# Reporting bugs, issues and feature requests
Please file bugs, issues and feature requests on the gitlab page of [knowBetter](https://gitlab.com/geoadel/knowBetter/issues).

In case of bugs, please state your browser and browser version you were using, if possible. Also it might help to include a screenshot and how to reproduce the bug.