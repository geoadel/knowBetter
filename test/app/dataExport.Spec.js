/**
 This file is part of knowBetter.
 
 knowBetter is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 knowBetter is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with knowBetter.  If not, see <http://www.gnu.org/licenses/>.
 **/


describe("exportDataService", function() {

  module.sharedInjector();

  beforeAll(module("knowBetter"));


    it("has calculated the answer correctly", inject(function(exportDataService) {
    // Because of sharedInjector, we have access to the instance of the DeepThought service 
    // that was provided to the beforeAll() hook. Therefore we can test the generated answer
     expect(exportDataService.objectsToStrings).toBeDefined();
  }));
 
});